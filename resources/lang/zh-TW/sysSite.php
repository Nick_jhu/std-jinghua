<?php 

 return [
    "addCmp" => "新增",
    "addDep" => "新增",
    "addGroup" => "新增",
    "addStn" => "新增",
    "titleName" => "集團資訊",
    "cmp" => "公司別",
    "updateCmp" => "公司別更新",
    "cancel" => "取消",
    "add" => "新增",
    "update" => "更新",
    "stn" => "站別",
    "updateStn" => "更新",
    "dep" => "部門別",
    "updateDep" => "更新",
    "group" => "集團",
    "updateGroup" => "更新"
];