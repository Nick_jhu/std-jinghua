<?php
return [
    "id"         => "id",
    "sellType"   => "購買類型",
    "cateName"   => "商品類別",
    "country"    => "適用地區",
    "title"      => "商品名稱",
    "subTitle"   => "副標題",
    "fPrice"     => "價格區間(起)",
    "tPrice"     => "價格區間(迄)",
    "ttlSellAmt" => "總銷量",
    "isAdded"    => "上架",
    "updatedBy"  => "修改人",
    "updatedAt"  => "修改時間",
    "createdBy"  => "建立人",
    "createdAt"  => "建立時間",
    "gKey"       => "集團",
    "cKey"       => "公司",
    "sKey"       => "站別",
    "dKey"       => "部門",
    'softStock'  => "軟體庫存"
];