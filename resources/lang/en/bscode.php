<?php 

return [
    "cd" => "Cd Code",
    "cdDescp" => "Cd Descp",
    "cdType" => "cdType",
    "gKey" => "Group",
    "cKey" => "Company",
    "sKey" => "Station",
    "dKey" => "Department",
    "createdBy" => "createdBy",
    "updatedBy" => "updatedBy",
    "createdAt" => "createdAt",
    "updatedAt" => "updatedAt",
    "value1" => "value1",
    "value2" => "value2",
    "value3" => "value3",
    "id" => "id",
    "titleName" => "BSCODE",
    ];