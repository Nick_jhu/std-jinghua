<?php 

return [
    "id" => "id",
    "cdType" => "Cd Type",
    "cdDescp" => "Cd Descp",
    "gKey" => "Group",
    "cKey" => "Company",
    "sKey" => "Station",
    "dKey" => "Department",
    "createdBy" => "Created By",
    "updatedBy" => "Updated By",
    "createdAt" => "Created At",
    "updatedAt" => "Updated At",
    "titleName" => "Basic Code",
    "baseInfo" => "Base Information",
    "detail" => "Detail",
    ];