<?php 

return [
    "titleName" => "Group Information",
    "group" => "Group",
    "add" => "Add",
    "cancel" => "Cancel",
    "update" => "Update",
    "addGroup" => "Add Group",
    "updateGroup" => "Update Group",
    "cmp" => "Company",
    "addCmp" => "Add Company",
    "updateCmp" => "Update Company",
    "stn" => "Station",
    "addStn" => "Add Station",
    "updateStn" => "Update Station",
    "dep" => "Department",
    "addDep" => "Add Department",
    "updateDep" => "Update Department",
    ];