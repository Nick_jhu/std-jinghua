<?php 

return [
    "save"        => "SAVE",
    "cancel"      => "CANCEL",
    "add"         => "Add",
    "delete"      => "Delete",
    "deleteMsg"   => "Delete Success",
    "close"       => "Close",
    "search"      => "Search",
    "clear"       => "Clear",
    "back"        => "Back",
    "excelImport" => "Excel Import",
    "gridOption"  => "Grid Option",
    "addNewRow"   => "Add New Row",
    "deleteSelectRow" => "Delete Selected Row",
    "msg1"        => "Please select at least one data",
    "copy"        => "Copy",
    "edit"        => "Edit",
    "msg2"        => "Are you sure to delete this data",
    "saveChange"  => "Save",
    "browse"      => "Browse",
    "exportExcel" => "Export Excel",
    "fail"        => "Fail"
    ];