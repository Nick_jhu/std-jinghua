<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Backup Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the backup system.
    | You are free to change them to anything you want to customize your views to better match your application.
    |
    */

    'backup'                             => '備份',
    'create_a_new_backup'                => '創建一個新備份',
    'existing_backups'                   => '已存在備份',
    'date'                               => '日期',
    'file_size'                          => '檔案大小',
    'actions'                            => '動作',
    'download'                           => '下載',
    'delete'                             => '刪除',
    'delete_confirm'                     => '確定要刪除這筆備份檔案?',
    'delete_confirmation_title'          => '完成',
    'delete_confirmation_message'        => '備份已刪除.',
    'delete_error_title'                 => 'Error',
    'delete_error_message'               => '備份未刪除.',
    'delete_cancel_title'                => "It's ok",
    'delete_cancel_message'              => '備份未刪除.',
    'create_confirmation_title'          => '備份完成',
    'create_confirmation_message'        => 'Reloading the page in 3 seconds.',
    'create_error_title'                 => '備份錯誤',
    'create_error_message'               => '備份無法建立.',
    'create_warning_title'               => '未知錯誤',
    'create_warning_message'             => 'Your backup may NOT have been created. Please check log files for details.',
    'location'                           => '位置',
    'no_disks_configured'                => 'No backup disks configured in config/laravel-backup.php',
    'backup_doesnt_exist'                => "備份不存在.",
    'only_local_downloads_supported'     => 'Only downloads from the Local filesystem are supported.',
];
