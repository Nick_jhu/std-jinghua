<?php 

return [
    "cd" => "cd",
    "cdDescp" => "cdDescp",
    "cdType" => "cdType",
    "gKey" => "gKey",
    "cKey" => "cKey",
    "sKey" => "sKey",
    "dKey" => "dKey",
    "createdBy" => "createdBy",
    "updatedBy" => "updatedBy",
    "createdAt" => "createdAt",
    "updatedAt" => "updatedAt",
    "value1" => "value1",
    "value2" => "value2",
    "value3" => "value3",
    "id" => "id",
    ];