<?php 

return [
    "id" => "id",
    "cntryCd" => "cntryCd",
    "cntryNm" => "cntryNm",
    "gKey" => "gKey",
    "cKey" => "cKey",
    "sKey" => "sKey",
    "dKey" => "dKey",
    "createdAt" => "createdAt",
    "updatedAt" => "updatedAt",
    "updatedBy" => "updatedBy",
    "createdBy" => "createdBy",
    ];