<?php 

return [
    "id" => "id",
    "cntryCd" => "cntryCd",
    "cntryNm" => "cntryNm",
    "stateCd" => "stateCd",
    "stateNm" => "stateNm",
    "cityCd" => "cityCd",
    "cityNm" => "cityNm",
    "distCd" => "distCd",
    "distNm" => "distNm",
    "zipF" => "zipF",
    "zipE" => "zipE",
    "remark" => "remark",
    "gKey" => "gKey",
    "cKey" => "cKey",
    "sKey" => "sKey",
    "dKey" => "dKey",
    "createdAt" => "createdAt",
    "updatedAt" => "updatedAt",
    "updatedBy" => "updatedBy",
    "createdBy" => "createdBy",
    ];