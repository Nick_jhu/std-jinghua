@extends('FrontEnd.newlayout',[
	"seo_title" => "會員登入/註冊",
	"seo_desc" => preg_replace("/\n+/","",strip_tags("會員登入/註冊")),
	"seo_img" => null
])

@section('after_style')
<link href="{{url('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="{{url('assets/fonts/FontAwesome/font-awesome.css')}}">
<link rel="stylesheet" href="{{url('assets/css/sign.css')}}" type="text/css" media="screen">
<link rel="stylesheet" href="{{url('assets/css/bootsnav.css?')}}" type="text/css" media="screen">
<link rel="stylesheet" href="{{url('assets/css/newstyle.css')}}" type="text/css" media="screen">


<link href="{{url('assets/css/owl.carousel.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{url('assets/css/owl.theme.default.min.css')}}" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="{{url('assets/css/rwd.css')}}" type="text/css" media="screen">


<style>

	.fbblue-btn {
		margin-top: 5px;
		margin-bottom: 5px;
		font-family: "Helvetica Neue", "Helvetica", "微軟正黑體修正", "Microsoft JhengHei", sans-serif;
		position: relative;
		background: #4267B2;
		opacity: 10;
		-moz-transition: opacity 0.5s;
		-o-transition: opacity 0.5s;
		-webkit-transition: opacity 0.5s;
		transition: opacity 0.5s;
	}
	.fbblue-btn:hover {
		transition: all 0.6s ease;
		background-color: #2D55A8;
		text-decoration: none;
	}
	.fb-login-text {
		margin-left: 16px;
		position: relative;
		top: -1px;
	}
	.fb-icon {
		vertical-align: sub;
	}

	.form-btn, .red-btn, .orange-btn, .fbblue-btn, .green-btn, .grey-btn, .login-thirdparty-btn {
		border-radius: 5px;
		font-size: 15px;
		color: #fff !important;
		text-align: center;
		cursor: pointer;
		line-height: 44px;
		height: 44px;
		display: inline-block;
		width: 100%;
		border: none;
	}	

	
	.oauth-google {
	height: 40px;
	border-radius: 7px;
	border: solid 1px #d0d0d0;
	cursor: pointer;
	position: relative;
	z-index: 2;
	background-color: #ffffff;
	text-align: center;
	display: -moz-flex;
	display: -ms-flex;
	display: -webkit-flex;
	display: flex;
	-moz-flex-direction: column;
	-ms-flex-direction: column;
	-webkit-flex-direction: column;
	flex-direction: column;
	-webkit-flex-wrap: nowrap;
	-moz-flex-wrap: nowrap;
	-ms-flex-wrap: nowrap;
	flex-wrap: nowrap;
	-moz-justify-content: center;
	-ms-justify-content: center;
	-webkit-justify-content: center;
	justify-content: center;
	-moz-flex-basis: auto;
	-webkit-flex-basis: auto;
	-ms-flex-basis: auto;
	flex-basis: auto;
	font-family: "Helvetica Neue", "Helvetica", "微軟正黑體修正", "Microsoft JhengHei", sans-serif;
	}
	.oauth-google:hover {
	background-color: #f8f8f8;
	}
	.oauth-google .oauth-google-inner {
	position: relative;
	display: flex;
	justify-content: center;
	align-items: center;
	}
	.oauth-google .oauth-google-inner img {
	width: 18px;
	height: 18px;
	}
	.oauth-google .oauth-google-text {
	max-width: 280px;
	font-size: 15px;
	color: #6a6a6a;
	display: inline-block;
	position: relative;
	padding-left: 6px;
	}
</style>

<script type="text/javascript" src="{{url('assets/libs/jquery-3.3.1.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/libs/tether.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/libs/bootstrap-3.3.7.min.js')}}"></script>
	@if($errors->any())
    <script>
        var errorMsg = "{{$errors->all()[0]}}";
        alert(errorMsg);
	</script>
	@endif

	<script type='text/javascript'>
		// var captchaContainer = null;
		// var loadCaptcha = function() {
		// 	captchaContainer = grecaptcha.render('captcha_container', {
		// 	//'sitekey' : '6LfaRlIUAAAAAH06_yVHG1W2PC488GrHqoFPz7wf',
		// 	'sitekey' : '6Ldkv5oUAAAAADzDzOOQashDs0vY1FPEMlLU4mTQ',
		// 	'callback' : function(response) {
		// 		//alert(response);
		// 	}
		// 	});
		// };
	</script>

@endsection

@section('header')
    @include('FrontEnd.layouts.newHeader')
@endsection

@section('content')
<div class="main-container" id="main-container" style="margin-bottom: 45px;">
    <div id="content-box">
        <div class="logo">
            <img src="{{url('assets/images/logo-black.png')}}" width="200" />
        </div>
        <div class="form tabs-box box">
            <div class="btn-box">
                <ul>
                    <li class="active">登入</li>
                    <li class="">註冊</li>
                </ul>
            </div>
            <div class="content-box">
                <!-- 登入 -->
                <div class="signin-box box active">
                    <form class="form" role="form" method="POST" action="{{ route('member.login.submit') }}">
                        {{ csrf_field() }}
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-user"></i></div>
                            <input type="text" class="form-control" name="phone"  placeholder="手機號碼 / E-MAIL" required>
                        </div>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-key"></i></div>
                            <input type="password" class="form-control" name="password"  placeholder="請輸入密碼(注意大小寫)" required>
                        </div>  

                        <div class="verificationcode-box">
                                <div>
                                    <div style="text-align: left; float:left;">
                                        <input class="form-check-input" type="checkbox" id="inlineFormCheck" name="remember">
                                        <label class="form-check-label" for="inlineFormCheck">
                                        記住我
                                        </label>
                                    </div>
                                    <div style="text-align: right;line-height:20px;">
                                        <div class="form-group">
                                            <a href="{{route('member.forget')}}"><small>忘記密碼</small></a>
                                        </div>
                                    </div>
                                </div>
                                

                                <div class="form-group">
                                    <!-- <div id="captcha_container"></div> -->
                                    <div class="g-recaptcha" data-sitekey="6Ldkv5oUAAAAADzDzOOQashDs0vY1FPEMlLU4mTQ"></div>
                                </div>
                        </div>
                        <button type="submit" class="btn btn-danger btn-block" style="background-color:#c9302c">登入</button>
                        <a class="fbblue-btn" onclick="loginWithfacebook()" style="">
                            <img class="fb-icon" src="{{url('assets/images/icon_login_fb.svg')}}">
                            <span class="fb-login-text">使用 Facebook 帳號登入</span>
                        </a>

                        <div class="OpenIdLoginModule" onclick="loginWithGoogle()">
                            <div id="googleOauthButton" class="oauth-google">
                                <div class="oauth-google-inner">
                                    <img src="{{url('assets/images/GGL_logo_googleg_18.png')}}">
                                    <div class="oauth-google-text">使用 Google 帳號登入</div>
                                </div>
                            </div>
                        </div>	
                    </form>
                </div>
                <!-- 註冊 -->
                <div class="signup-box box">
                    <form class="form" role="form" method="POST" action="{{ url('do/register') }}">
                        {{ csrf_field() }}
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fas fa-user"></i></div>
                            {{-- <input type="text" class="form-control"  name="phone" value="{{old('phone')}}" placeholder="使用手機號碼註冊" min="10" max="10" required> --}}
                            <input type="email" class="form-control"  name="email" value="{{old('email')}}" placeholder="example@example.com" required>
                            @if ($errors->has('email'))
                            <script>
                                alert("{{ $errors->first('email') }}");
                            </script>
                            @endif
                        </div>
                        {{-- <p style="color:red; font-size: 12px;padding:0;margin:0">(如非台灣手機請加國碼，如香港為852，則輸入852123456789。)</p> --}}
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fas fa-key"></i></div>
                            <input type="password" class="form-control"  name="password" placeholder="請輸入密碼(請輸入6到16位)" required>
                        </div> 
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fas fa-key"></i></div>
                            <input type="password" class="form-control"  name="confirm_password" placeholder="確認密碼(請輸入6到16位)" required>
                        </div> 
                        {{-- <a href="{{url('reSendVcode')}}">重新發送驗證碼</a> --}}
                        <button type="submit" class="btn btn-primary btn-block" style="background-color:#c9302c">註冊</button>
                        <!-- <button type="button" class="btn btn-light btn-block">使用電子信箱註冊</button> -->
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
    @include('FrontEnd.layouts.newfooter')
@endsection

@section('after_scripts')
	<!-- <script src="https://www.google.com/recaptcha/api.js?onload=loadCaptcha&render=explicit" async defer></script> -->
	<script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script type="text/javascript" src="{{url('assets/js/sign.js')}}"></script>
@endsection
