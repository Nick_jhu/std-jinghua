<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="zh-TW">
<head>
	<meta charset="UTF-8">
	<title>抽獎序號 / 中獎資訊 查詢</title>
	<meta name='description' content=""/>
	<meta name='keywords' content=""/>
	<meta name="MobileOptimized" content="width">
	<meta name="HandheldFriendly" content="true">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="{{url('assets/css/style.css?')}}">
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
	<script type="text/javascript" src="{{url('assets/js/goto.js')}}"></script>
</head>
<body>
	<header class="header-box">
		<div class="content-box inside-box clearfix">
			<a class="home" href="index.html" title="回首頁"></a>
			<h3 class="title">抽獎序號 / 中獎資訊 查詢</h3>
		</div>
	</header>
	<main class="main-box">
		<section class="info-box form-box">
			<ul class="day-select">
				<li>
					<input id="option" type="radio" name="field" value="2019-05-01 到 2019-07-31期 福袋" checked >
					<label for="option">2019-05-01 到 2019-07-31期 福袋</label>
				</li>
				<!-- <li>
					<input id="option" type="radio" name="field" value="">
					<label for="option">2019-05-01 到 2019-07-31期 福袋</label>
				</li> -->
			</ul>
			<div class="content-box">
				<form>
					<ul class="fillin">
						<li>
							<label for="name">購買人姓名</label>
							<input id="name" type="text" name="" value="阿哲 Kent" placeholder="請輸入購買人姓名">
						</li>
						<li>
							<label for="phone">購買人手機號碼</label>
							<input id="phone" type="text" name="" value="" placeholder="請輸入購買人手機號碼">
						</li>
						<li>
							<label for="code">驗證碼</label>
							<input id="code" type="text" name="" value="" placeholder="請輸入右方的4位數字">
							<div class="code-box"><img src="images/rand.png"></div>
						</li>
					</ul>
					<input class="btn" type="submit" value="送出查詢">
				</form>
			</div>
		</section>
		<footer class="footer_box none_pad">
			<ul class="link clearfix">
				<li><a href="search.html"><p class="icon_01">抽獎序號/<br>中獎查詢</p></a></li>
				<li><a href="#" target="_blank"><p class="icon_02">facebook<br>粉絲團</p></a></li>
				<li><a href="#"><p class="icon_03">隱私權條款</p></a></li>
			</ul>
			<p class="Copyright">本活動由xx第三方⾒證<br>
				© 2019 Copyright      客服電話 0800-000-000
			</p>
		</footer>
	</main>
</body>
</html>



