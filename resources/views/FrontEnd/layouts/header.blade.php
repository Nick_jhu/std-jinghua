<div class="web header">
    <div class="header-content">
        <a href="{{url('/')}}">
            <div class="logo">
                @if($viewName == 'index' || $viewName == 'about')
                <img src="{{url('assets/images/logo-white.png?t=242342234')}}" width="200" />
                @else
                <img src="{{url('assets/images/logo-black.png?t=24234342')}}" width="200" />
                @endif
            </div>
        </a>
        <div class="nav-box">
            <ul>
                <li class="dropdown btn @if($viewName == 'about' || $viewName == 'news') active @endif">
                    <a class="dropdown-toggle" data-toggle="dropdown">最新消息 <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                    <li><a href="{{url('about')}}">關於我們</a></li>
                    <li><a href="{{url('news')}}">最新消息</a></li>
                    </ul>
                </li>
                <a href="{{url('process')}}"><li class="btn @if($viewName == 'process') active @endif">租借流程</li></a>
                <li class="dropdown btn">
                    <a class="dropdown-toggle" data-toggle="dropdown">旅遊國家 <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                    <li><a href="{{url('product?country=7')}}">亞 洲</a></li>
                    <li><a href="{{url('product?country=8')}}">歐 洲</a></li>
                    <li><a href="{{url('product?country=9')}}">美 洲</a></li>
                    <li><a href="{{url('product?country=10')}}">中東、非洲</a></li>
                    <li><a href="{{url('product?country=11')}}">澳洲、紐西蘭</a></li>
                    <li><a href="{{url('product?country=12')}}">全球</a></li>
                    </ul>
                </li>
                <a href="{{url('product?cateId=55')}}"><li class="btn">智慧家居</li></a>
                <a href="{{url('product')}}"><li class="btn @if($viewName == 'product') active @endif">美Z商城</li></a>
                <a href="{{url('qa')}}"><li class="btn @if($viewName == 'qa') active @endif">Q & A</li></a>
                <li class="dropdown btn">
                    <a class="dropdown-toggle" data-toggle="dropdown">線上客服 <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                    <li><a href="https://www.facebook.com/Beautyz.net/?hc_ref=ARTidGfdmfADGNZsSGkXBnkTvgo7GNlwd4AIdE1HKwkN0RXX4Ys4lAqB3FAJxxiq8pk&amp;fref=nf" target="_blank">美z.人生粉絲團</a></li>
                    <li><a href="https://line.me/R/ti/p/FMPomkRiPU" target="_blank">美z.人生Line@</a></li>
                    </ul>
                </li>
                @if(Auth::guard('member')->check())
                <a href="{{url('center')}}"><li class="btn @if($viewName == 'memberCenter') active @endif">會員專區</li></a>
                <a href="{{url('member/logout')}}" ><li class="btn">登出</li></a>
                @else
                <a href="{{url('member/login')}}"><li class="btn @if($viewName == 'login') active @endif">登入</li></a>
                @endif
                <li class="cart">
                    <a href="{{url('cart')}}">
                        <img src="{{url('assets/images/shopping-cart.png')}}" width="45" />
                        <div class="num-box header-num-box">0</div>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="mobile header">
    <div class="top-box">
        <div class="menu-box"><i class="fas fa-bars"></i></div>
        <div class="logo-box">
            <a href="{{url('/')}}"><img src="{{url('assets/images/logo-black.png?t=24234234')}}" width="200" /></a>
        </div>
        <div class="cart-box">
            <a href="{{url('cart')}}">
                <img src="{{url('assets/images/shopping-cart.png')}}" width="30" />
                <div class="num-box header-num-box">0</div>
            </a>
        </div>
    </div>
    <div class="bottom-box">
        <ul>
            <a href="#">
                <li class="mbtn dropdown"><i class="fas fa-square"></i> 最新消息
                <ul class="subbtnlist">
                    <a href="{{url('about')}}"><li class="subbtn">關於我們</li></a>
                    <a href="{{url('news')}}"><li class="subbtn">最新消息</li></a>
                </ul>
                </li>
            </a>

            <a href="{{url('process')}}"><li class="mbtn"><i class="fas fa-square"></i> 租借流程</li></a>
            <a><li class="mbtn dropdown"><i class="fas fa-square"></i> 旅遊國家
                <ul class="subbtnlist">
                    <a href="{{url('product?country=7')}}">
                        <li class="subbtn"><i class="fas fa-angle-right"></i> 亞 洲</li>
                    </a>
                    <a href="{{url('product?country=8')}}">
                        <li class="subbtn"><i class="fas fa-angle-right"></i> 歐 洲</li>
                    </a>
                    <a href="{{url('product?country=9')}}">
                        <li class="subbtn"><i class="fas fa-angle-right"></i> 美 洲</li>
                    </a>
                    <a href="{{url('product?country=10')}}">
                        <li class="subbtn"><i class="fas fa-angle-right"></i> 中東、非洲</li>
                    </a>
                    <a href="{{url('product?country=11')}}">
                        <li class="subbtn"><i class="fas fa-angle-right"></i> 澳洲、紐西蘭</li>
                    </a>
                    <a href="{{url('product?country=12')}}">
                        <li class="subbtn"><i class="fas fa-angle-right"></i> 全球</li>
                    </a>
                </ul>
            </li></a>    
            
                
            <a href="{{url('product?cateId=55')}}"><li class="mbtn"><i class="fas fa-square"></i> 智慧家居</li></a>
            <a href="{{url('product')}}"><li class="mbtn"><i class="fas fa-square"></i> 美Z商城</li></a>
            <a href="{{url('qa')}}"><li class="mbtn"><i class="fas fa-square"></i> Q & A</li></a>
            <a><li class="mbtn dropdown"><i class="fas fa-square"></i> 線上客服
                <ul class="subbtnlist">
                    <a href="https://www.facebook.com/Beautyz.net/?hc_ref=ARTidGfdmfADGNZsSGkXBnkTvgo7GNlwd4AIdE1HKwkN0RXX4Ys4lAqB3FAJxxiq8pk&amp;fref=nf" target="_blank">
                        <li class="subbtn"><i class="fas fa-angle-right"></i> 美z.人生粉絲團</li>
                    </a>
                    <a href="https://line.me/R/ti/p/FMPomkRiPU" target="_blank">
                        <li class="subbtn"><i class="fas fa-angle-right"></i> 美z.人生Line@</li>
                    </a>
                </ul>
            </li></a>
            @if(Auth::guard('member')->check())
            <a href="{{url('center')}}"><li class="mbtn"><i class="fas fa-square"></i> 會員專區</li></a>
            <a href="{{url('member/logout')}}" ><li class="mbtn"><i class="fas fa-square"></i> 登出</li></a>
            @else
            <a href="{{url('member/login')}}"><li class="mbtn"><i class="fas fa-square"></i> 登入</li></a>
            @endif

        </ul>
    </div>
</div>

<script>
    $(function(){
        var cartNum = $.cookie('cartNum');
        var cartId  = $.cookie('cartId');
        var isLogin = "{{Auth::guard('member')->check()}}";
        if((typeof cartNum == "undefined" || cartNum == "null" || cartId == 0 || typeof cartId == "undefined") && isLogin != "1") {
            cartNum = 0;
        }

        $(".header-num-box").text(cartNum);
    });
</script>