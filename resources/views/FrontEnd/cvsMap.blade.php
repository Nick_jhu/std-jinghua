<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
    <div style="text-align:center;">
        <form id="ECPayForm" method="POST" action="{{config('ecpay.LogistURL')}}" target="_self" style="display:flex;align-items:center;justify-content:center;margin-top:200px;">
            <input type="hidden" name="MerchantID" value="{{$MerchantID}}">
            <input type="hidden" name="MerchantTradeNo" value="">
            <input type="hidden" name="LogisticsSubType" value="{{$LogisticsSubType}}">
            <input type="hidden" name="IsCollection" value="{{$IsCollection}}">
            <input type="hidden" name="ServerReplyURL" value="{{$ServerReplyURL}}">
            <input type="hidden" name="ExtraData" value="">
            <input type="hidden" name="Device" value="0">
            <input type="hidden" name="LogisticsType" value="CVS">
            <button style="color: #000;padding: 35px 20px;text-align: center;display: inline-block;font-size: 26px;margin: 4px 2px;cursor: pointer;" type="submit" id="__paymentButton">請點擊選取店家</button>
        </form>
    </div>
    <script>
        document.getElementById("ECPayForm").submit();
    </script>
</body>
</html>