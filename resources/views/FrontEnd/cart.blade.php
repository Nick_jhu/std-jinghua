@extends('FrontEnd.layout',[
	"seo_title" => "購物車",
	"seo_desc" => preg_replace("/\n+/","",strip_tags("購物車")),
	"seo_img" => null
])

@section('after_style')
    <link rel="stylesheet" href="{{url('assets/css/reset.css')}}" type="text/css" media="screen">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">
    <link rel="stylesheet" href="{{url('assets/libs/bootstrap-3.3.7.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/style.css')}}" type="text/css" media="screen">
    <link rel="stylesheet" href="{{url('assets/css/cart.css')}}" type="text/css" media="screen">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.css" type="text/css" media="screen">

    <script type="text/javascript" src="{{url('assets/libs/jquery-3.3.1.min.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/libs/tether.min.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/libs/bootstrap-3.3.7.min.js')}}"></script>
    <style>
        #tappay-iframe {
            font-family: Lato,'Helvetica Neue',Arial,Helvetica,sans-serif;
            margin: 0;
            outline: 0;
            -webkit-appearance: none;
            tap-highlight-color: rgba(255,255,255,0);
            line-height: 1.21428571em;
            padding: .578571em 1em;
            font-size: 1em;
            background: #fff;
            border: 1px solid rgba(34,36,38,.15);
            color: rgba(0,0,0,.87);
            border-radius: .28571429rem;
            box-shadow: 0 0 0 0 transparent inset;
            -webkit-transition: color .1s ease,border-color .1s ease;
            transition: color .1s ease,border-color .1s ease;
            width: 97%;
        }
    </style>

    @if($errors->has('message'))
    <script>
        var errorMsg = "{{$errors->get('message')[0]}}";
        alert(errorMsg);
	</script>
    @endif
    
    <script>
        var dlvArea = "";
        @if($memberData->is_updated == 'Y')
            dlvArea = '{{$memberData->dlv_area}}';
        @endif
    </script>
@endsection

@section('header')
    @include('FrontEnd.layouts.header')
@endsection



@section('content')
<div class="breadcrumbs-box">
    <div class="content-box">
        <div class="name">Basket</div>
        <div class="breadcrumbs">
            Start &gt; <a href="#">購物車</a>
        </div>
    </div>
</div>
<div id="content-box">
    
    <div id="rentArea" @if(count($cartData->rData) == 0) style="display:none" @endif>
        <div class="title"><i class="fas fa-square red"></i> 租賃列表</div>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <!-- <th class="col-md-1 text-center" scope="col">國家</th> -->
                    <th class="col-md-3 text-center" scope="col">商品</th>
                    <th class="col-md-1 text-center" scope="col">日期(起)</th>
                    <th class="col-md-1 text-center" scope="col">日期(迄)</th>
                    <th class="col-md-1 text-center" scope="col">顯示天數</th>
                    <!-- <th class="col-md-1 text-center" scope="col">結算天數</th> -->
                    <th class="col-md-1 text-center" scope="col">單價</th>
                    <th class="col-md-1 text-center" scope="col">數量</th>
                    <th class="col-md-1 text-center" scope="col">金額</th>
                    <th class="col-md-1 text-center" scope="col">取消</th>
                </tr>
            </thead>
            <tbody>
                <input type="hidden" id="cartStr" value="{{$cartStr}}">
                @foreach($cartData->rData as $key=>$row)
                <tr class="table-red">
                    <!-- <th class="text-center" scope="row">日本</th> -->
                    <td><a href="{{url('productDetail/'.$row->prodId)}}">@isset($row->prodNm){{$row->prodNm}}@endisset</a></td>
                    <td class="text-center">{{$row->startDate}}</td>
                    <td class="text-center">{{$row->endDate}}</td>
                    <td colspan="2" class="colspan-3 text-center">
                        <table class="table">
                            <tr class="top-tr">
                                <td class="col-md-4">{{(((strtotime($row->endDate) - strtotime($row->startDate))/3600)/24) + 1}}</td>
                                <!-- <td class="col-md-4">100</td> -->
                                <td class="col-md-4">{{$row->dPrice}}</td>
                            </tr>
                            <tr class="bottom-tr">
                                <td colspan="3">
                                    <div class="form-check">
                                        <input prodDetailID="{{$row->prodDetailId}}" uuid="{{$row->uuid}}" type="checkbox" class="form-check-input" name="insuranceCheckbox" day="{{(((strtotime($row->endDate) - strtotime($row->startDate))/3600)/24) + 1}}" num="{{$row->num}}" price="{{$row->amt}}" @if($row->insurance == "Y") checked @endif>
                                        <span class="red">安心險 NT 50/日</span>(<a href="#" data-toggle="modal" data-target="#insuranceModal">說明</a>)
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td class="text-center">{{$row->num}}</td>
                    <td class="text-center" prodDetailId="{{$row->prodDetailId}}" uuid="{{$row->uuid}}">{{number_format($row->amt)}}</td>
                    <td class="text-center"><button class="btn btn-link cartDel" item-index="{{$key}}" uuid="{{$row->uuid}}" sell-type="R"><i class="fas fa-trash-alt"></i></button></td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <!-- 合計 -->
        <table class="table total-box">
            <tr>
                <td class="col-md-8">
                    <div class="form-row align-items-center">
                        <div class="col-md-5 padding-l-0">
                            <input type="text" class="form-control mb-2" id="rentDiscountCode" placeholder="請輸入優惠碼">
                        </div>
                        <div class="col-auto">
                            <!-- <button type="button" class="btn btn-primary mb-2" id="useRentDiscount">使用優惠碼</button> -->
                        </div>
                    </div>
                </td>
                <td class="col-md-4 text-right">
                    <h4>Cart Totals</h4>
                    <h4>優 惠 折 扣： - $ <span id="rentDiscount">0</span> </h4>
                    <h4>小計 $<span id="rSumAmt">{{number_format($rSumAmt)}}</span></h4>
                    <input type="hidden" name="rSumAmt" value="{{$rSumAmt}}">
                    <!-- <h4>總金額 $10,000.00</h4> -->
                </td>
            </tr>
        </table>
    </div>
    
    <div id="sellArea" @if(count($cartData->sData) == 0) style="display:none" @endif>
        <div class="title"><i class="fas fa-square orange"></i> 購買列表</div>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th class="col-md-6 text-center" scope="col">商品</th>
                    <th class="col-md-1 text-center" scope="col">單價</th>
                    <th class="col-md-2 text-center" scope="col">數量</th>
                    <th class="col-md-2 text-center" scope="col">金額</th>
                    <th class="col-md-1 text-center" scope="col">取消</th>
                </tr>
            </thead>
            <tbody>
                @foreach($cartData->sData as $key=>$row)    
                <tr class="table-orange">
                    <th class="text-left" scope="row">
                        <a href="{{url('productDetail/'.$row->prodId)}}">{{$row->prodNm}}</a>
                    </th>
                    <td class="text-center">{{number_format($row->dPrice)}}</td>
                    <td class="text-center">{{$row->num}}</td>
                    <td class="text-center">{{number_format($row->amt)}}</td>
                    <td class="text-center">
                        <button class="btn btn-link cartDel" item-index="{{$key}}" uuid="{{$row->uuid}}" sell-type="S"><i class="fas fa-trash-alt"></i></button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <!-- 合計 -->
        <table class="table total-box">
            <tr>
                <td class="col-md-8">
                    <div class="form-row align-items-center" id="sellDiscountInput"  @if($canUseSellDiscount == false) style="display:none" @endif>
                        <div class="col-md-5 padding-l-0">
                            <input type="text" class="form-control mb-2" id="sellDiscountCode" placeholder="請輸入優惠碼">
                        </div>
                        <div class="col-auto">
                            <!-- <button type="submit" class="btn btn-primary mb-2" id="sellUseDiscount">使用優惠碼</button> -->
                        </div>
                    </div>
                </td>
                <td class="col-md-4 text-right">
                    <h4>Cart Totals</h4>
                    <h4>優 惠 折 扣： - $ <span id="sellDiscount">0</span> </h4>
                    <h4>小計 $<span id="sSumAmt">{{number_format($sSumAmt)}}</span></h4>
                    <input type="hidden" name="sSumAmt" value="{{$sSumAmt}}">
                    <!-- <h4>總金額 $10,000.00</h4> -->
                </td>
            </tr>
        </table>
    </div>
    
    <!-- 總計 -->
    <div class="all-total-box" id="totalBox" @if(count($cartData->rData) == 0 && count($cartData->sData) == 0) style="display:none" @endif>
        <h4>運       費： $ <span class="black" id="shipFee">0</span> </h4>
        <h4>特 殊 折 扣： - $ <span class="red" id="discountPrice">0</span> </h4>
        <h4>結帳金額： $ <span class="red" id="ttlAmt"> {{number_format($rSumAmt + $sSumAmt)}}</span></h4>
    </div>

    <p id="showDescp" style="font-size: 20px; font-weight: 600; text-align: center; @if(count($cartData->rData) > 0 || count($cartData->sData) > 0) display:none @endif">您還沒有加入商品到購物車中，請至<a href="{{url('product')}}">美Z商城</a>選購</p>

    @if(count($cartData->sData) != 0 || count($cartData->rData) != 0)
    <form action="{{url('pay')}}" method="POST" id="payForm">
        {{ csrf_field() }}
        <input type="hidden" name="ttlAmt" value="{{$rSumAmt + $sSumAmt}}">
        <input type="hidden" name="shipFee" />
        <input type="hidden" name="sellDiscountCode" />
        <input type="hidden" name="rentDiscountCode" />
        <input type="hidden" name="sellDntNum" />
        <input type="hidden" name="rentDntNum" />
        <input type="hidden" name="prime" />
        <input type="hidden" name="agreePayRemeber" />
        @if(isset($memberData->card_token))
        <input type="hidden" name="useQuickPay" value="YES"/>
        @endif
        @if(isset($memberData->id))
        <!-- 資訊 -->
        <div class="pay-info-box">
            <table class="table">
                <tr>
                    <td class="col-md-2">付款方式</td>
                    <td>
                        <div class="form-group col-md-3">
                            <select class="form-control" id="payWay" name="payWay">
                                <option value="Credit" selected>信用卡</option>
                                <option value="GooglePay">Google Pay</option>
                                <option value="ApplePay">Apple Pay</option>
                                <option value="LinePay">Line Pay</option>
                                <option value="SamsungPay">Samsung Pay</option>
                                <option value="WEBATM">Web ATM</option>
                                <option value="CVS">超商付款</option>
                            </select>
                        </div>
                    </td>
                </tr>
                <tr style="@if($defaultShip == 0) display:none @endif" id="pickWayTr">
                    <td class="col-md-2">取貨方式</td>
                    <td>
                        <div style="clear:both; padding-top:5px; color:red">
                            注意：1.若只選機場取貨，只能機場還貨。2. 若只選購軟體類商品，不需選取配送方式
                        </div>
                        <div id="pickWayForAirport" @if($showPickAir == 0) style="display:none" @endif>
                            <div class="">
                                <input class="form-check-input" type="radio" name="pick_way" id="get-wifi-1" value="A" @if(count($cartData->sData) == 0 && $memberData->pick_way == 'A') checked @endif>
                                <label class="form-check-label" for="get-wifi-1">
                                機場取貨                                        
                                </label>                                      
                            </div>
                            <div class="form-group col-md-3">
                                <select class="form-control" id="departureTerminal" name="departureTerminal">
                                    <option value="" selected>選擇航廈</option>
                                    <option value="T1">桃園機場第一航廈</option>
                                    <option value="T2">桃園機場第二航廈</option>
                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                <select class="form-control" id="departureHour" name="departureHour">
                                    <option value="" selected>航班大約時間(時)</option>
                                    <option value="00" >00</option>
                                    <option value="01">01</option>
                                    <option value="02">02</option>
                                    <option value="03">03</option>
                                    <option value="04">04</option>
                                    <option value="05">05</option>
                                    <option value="06">06</option>
                                    <option value="07">07</option>
                                    <option value="08">08</option>
                                    <option value="09">09</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                    <option value="21">21</option>
                                    <option value="22">22</option>
                                    <option value="23">23</option>
                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                <select class="form-control" id="departureMin" name="departureMin">
                                    <option value="" selected>航班大約時間(分)</option>
                                    <option value="00">00</option>
                                    <option value="15">15</option>
                                    <option value="30">30</option>
                                    <option value="45">45</option>
                                </select>
                            </div>
                        </div>
                        

                        {{--<div class="form-group col-md-3">
                            <select class="form-control" id="departureAirline" name="departureAirline">
                                <option selected>請選擇航空公司</option>
                                @foreach($airlineData as $row)
                                    <option value="{{$row->cd_descp}}">{{$row->cd_descp}}</option>
                                @endforeach
                            </select>
                        </div>
                        
                        <div class="form-group col-md-3" style="padding-left:20px;">
                            <input type="text" class="form-control" id="departureFlightNo" name="departureFlightNo" placeholder="請輸入班機號碼">
                        </div>--}}
                        
                        <div style="clear:both; padding-top:5px;">
                            <input class="form-check-input" type="radio" name="pick_way" id="get-wifi-2" value="B" @if((count($cartData->sData) > 0 || $memberData->pick_way == 'B' || empty($memberData->pick_way)) && $defaultShip == 1) checked @endif>
                            <label class="form-check-label" for="get-wifi-2">
                            宅配到府 (出國前一日送達) 歸還方式 <span style="color:coral">宅配歸還(請回國隔天三點前拿內附宅配單全省7-11寄回)</span>
                            </label>
                        </div>
                        <div style="clear:both; padding-top:5px;">
                            <input class="form-check-input" type="radio" name="pick_way" id="get-wifi-3" value="UNIMARTC2C">
                            <label class="form-check-label" for="get-wifi-3">
                            7-11
                            </label>
                        </div>
                        <div style="clear:both; padding-top:5px;">
                            <input class="form-check-input" type="radio" name="pick_way" id="get-wifi-4" value="FAMIC2C">
                            <label class="form-check-label" for="get-wifi-4">
                            全家
                            </label>
                        </div>
                        <div style="clear:both; padding-top:5px;">
                            <input class="form-check-input" type="radio" name="pick_way" id="get-wifi-5" value="HILIFEC2C">
                            <label class="form-check-label" for="get-wifi-5">
                            萊爾富
                            </label>
                        </div>
                    </td>
                </tr>
                <tr class="trCvsMap" style="display:none">
                    <td class="col-md-2">門市名稱：</td>
                    <td>
                        <div class="form-group col-sm-4">
                            <input type="hidden" class="form-control" id="CVSStoreID" name="CVSStoreID" readonly>
                            <input type="text" class="form-control" id="CVSStoreName" name="CVSStoreName" readonly>
                        </div>
                        <button type="button" class="btn btn-sm btn-primary" id="openMap">選擇門市</button>
                    </td>
                </tr>
                <tr class="trCvsMap" style="display:none">
                    <td class="col-md-2">門市地址：</td>
                    <td>
                        <div class="form-group col-sm-6">
                            <input type="text" class="form-control" id="CVSAddress" name="CVSAddress" readonly />
                        </div>
                    </td>
                </tr>
                <tr name="returnTr" @if(count($cartData->rData) == 0) style="display:none" @endif>
                    <td class="col-md-2">關於取貨</td>
                    <td><a href="{{url('process')}}" target="_blank">查看租借流程</a></td>
                </tr>
                
                <tr name="returnTr" @if(count($cartData->rData) == 0) style="display:none" @endif>
                    <td class="col-md-2">還貨方式</td>
                    <td>
                        <div class="">
                            <input class="form-check-input" type="radio" name="return_way" id="return-wifi-1" value="A" @if(count($cartData->rData) > 0 && $memberData->return_way == 'A') checked @endif>
                            <label class="form-check-label" for="return-wifi-1">
                            機場還貨                                        
                            </label>                                      
                        </div>
                        
                        <div class="form-group col-md-3">
                            <select class="form-control" id="returnTerminal" name="returnTerminal">
                                <option value="" selected>選擇航廈</option>
                                <option value="T1">桃園機場第一航廈</option>
                                <option value="T2">桃園機場第二航廈</option>
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <select class="form-control" id="returnHour" name="returnHour">
                                <option value="" selected>航班大約時間(時)</option>
                                <option value="00" >00</option>
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                                <option value="13">13</option>
                                <option value="14">14</option>
                                <option value="15">15</option>
                                <option value="16">16</option>
                                <option value="17">17</option>
                                <option value="18">18</option>
                                <option value="19">19</option>
                                <option value="20">20</option>
                                <option value="21">21</option>
                                <option value="22">22</option>
                                <option value="23">23</option>
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <select class="form-control" id="returnMin" name="returnMin">
                                <option value="" selected>航班大約時間(分)</option>
                                <option value="00">00</option>
                                <option value="15">15</option>
                                <option value="30">30</option>
                                <option value="45">45</option>
                            </select>
                        </div>
                        @if(count($cartData->rData) > 0)
                        <div style="clear:both; padding-top:5px;" id="ShipReturnWay">
                            <input class="form-check-input" type="radio" name="return_way" id="return-wifi-2" value="B" @if($memberData->return_way == 'B' || empty($memberData->return_way)) checked @endif>
                            <label class="form-check-label" for="return-wifi-2">
                            使用宅配歸還 <span style="color:coral">宅配歸還(請回國隔天三點前拿內附宅配單全省7-11寄回</span>
                            </label>
                        </div>
                        @endif
                    </td>
                </tr>
                <tr name="returnTr" @if(count($cartData->rData) == 0) style="display:none" @endif>
                    <td class="col-md-2">關於還貨</td>
                    <td><a href="{{url('process')}}" target="_blank">查看租借流程</a></td>
                </tr>
            </table>
        </div>
        <!-- 訂購人資訊 -->
        <div class="orderer-box">
            <div class="title" style="margin: 20px 0 10px 0;"><i class="fas fa-square green"></i> 訂購人資訊</div>
            <div style="border: 1px solid #9e9e9e; border-radius: 5px; padding: 5px;">
                <table class="table">
                    <tr>
                        <td class="col-md-1">姓名</td>
                        <td>
                            <div class="form-group col-md-12" style="padding-left:20px;">
                                <input type="text" class="form-control" id="userName" name="name" placeholder="請輸入姓名" value="{{$memberData->name}}">
                            </div>
                        </td>
                        <td class="col-md-4"></td>
                    </tr>
                    <tr>
                        <td class="col-md-1">手機</td>
                        <td>
                            <div class="form-group col-md-12" style="padding-left:20px;">
                                <input type="text" class="form-control" id="userPhone" name="phone" placeholder="0912345678" value="{{$memberData->cellphone}}">
                            </div>
                        </td>
                        <!-- <td class="col-md-4" style="color: #a2a2a2;">手機市話可擇一填寫</td> -->
                    </tr>
                    <tr name="dlvWay" class="trAddr" @if($defaultShip == 0) style="display:none" @endif>
                        <td class="col-md-1">收件地址</td>
                        <td>
                            <div class="form-group col-md-4" style="padding-left:20px;">
                                <input type="text" class="form-control" id="dlvZip" name="dlvZip" placeholder="郵地區號" value="{{$memberData->dlv_zip}}" readonly>
                            </div>
                            <div class="form-group col-md-4" style="padding-left:20px;">
                                <select class="form-control" name="dlvCity" id="dlvCity">
                                    <option value="" selected>請選擇縣市</option>
                                    @foreach($cityData as $row)
                                    <option value="{{$row->city_nm}}" @if($memberData->dlv_city == $row->city_nm) selected @endif>{{$row->city_nm}}</option>
                                    @endforeach
                                </select>          
                                <!-- <input type="text" class="form-control" id="dlvCity" name="dlvCity" placeholder="城市"> -->
                            </div>
                            <div class="form-group col-md-4" style="padding-left:20px;">
                                <select class="form-control" name="dlvArea" id="dlvArea">
                                    <option value="" selected>請選擇鄉鎮市區</option>
                                </select>   
                                <!-- <input type="text" class="form-control" id="dlvArea" name="dlvArea" placeholder="區域"> -->
                            </div>
                        </td>
                        <!-- <td class="col-md-4" style="color: #a2a2a2;">手機市話可擇一填寫</td> -->
                    </tr>
                    <tr name="dlvWay" class="trAddr"  @if($defaultShip == 0) style="display:none" @endif>
                        <td class="col-md-1"></td>
                        <td>
                            <div class="form-group col-md-12" style="padding-left:20px;">
                                <input type="text" class="form-control" id="dlvAddr" name="dlvAddr" placeholder="地址" value="{{$memberData->dlv_addr}}">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="col-md-1">Email</td>
                        <td>
                            <div class="form-group col-md-12" style="padding-left:20px;">
                                <input type="text" class="form-control" id="email" name="email" placeholder="test@gamil.com" value="{{$memberData->email}}">
                            </div>
                        </td>
                        <td class="col-md-4"></td>
                    </tr>
                    <tr>
                        <td class="col-md-1">備註</td>
                        <td>
                            <div class="form-group col-md-12" style="padding-left:20px;">
                                <textarea class="form-control" name="custRemark" id="custRemark" rows="5" placeholder="中間如要去其他國家或轉機，請詳細填寫日期與國家，我們將給您最好的旅遊上網服務品質"></textarea>
                            </div>
                        </td>
                        <td class="col-md-4"></td>
                    </tr>
                    <tr>
                        <td class="col-md-1"></td>
                        <td>
                            {{-- <div class="form-group col-md-12" style="padding-left:20px;display: flex;justify-content: baseline;align-items: center;">
                                <input type="checkbox" style="height: 20px; width: 20px;" id="syncUserData">
                                同步訂購人資訊到會員資料
                            </div> --}}
                        </td>
                        <td class="col-md-4"></td>
                    </tr>
                </table>
            </div>
        </div>

        <!-- 信用卡資訊 -->
        <div class="orderer-box" id="creditCardArea" @if(isset($memberData->card_token)) style="display:none" @endif>
            <div class="title" style="margin: 20px 0 10px 0;"><i class="fas fa-square green"></i> 信用卡資訊</div>
            <div style="border: 1px solid #9e9e9e; border-radius: 5px; padding: 5px;">
                <div class="ui grid centered doubling stackable" style="margin-bottom: 10px;">
                    <div class="six wide column">
                        <div class="ui segment">
                            <form class="ui form">
                                <div class="field">
                                    <label>信用卡卡號</label>
                                    <div id="tappay-iframe"></div>
                                </div> 
                            </form>
                        </div>
                    </div>
                </div>
                <div style="display: flex;justify-content: baseline;align-items: center;">
                    <div style="width: 60px;">
                        <input type="checkbox" name="creditRemember" style="height: 20px; width: 20px; margin: 10px 20px;" value="YES">
                    </div>
                    <div style="flex: 1;">快速結帳</div>
                </div>  
            </div>
        </div>

        @if(isset($memberData->card_token))
        <!-- 快速結帳 -->
        <div class="orderer-box" id="quickPayment">
            <div class="title" style="margin: 20px 0 10px 0;"><i class="fas fa-square green"></i> 快速結帳</div>
            <div style="border: 1px solid #9e9e9e; border-radius: 5px; padding: 5px;">
                <table class="table">
                    <tr>
                        <td class="col-md-1">信用卡號</td>
                        <td>
                            <div class="form-group col-md-6" style="padding-left:20px;">
                                <input type="text" class="form-control" value="{{'**** **** **** '.$memberData->last_four}}" disabled>
                            </div>
                            <div class="form-group col-md-6">
                                <button type="button" class="btn btn-sm btn-primary" id="btnChangeCard">換一張信用卡</button>
                            </div>
                        </td>
                    </tr>
                </table>
                
            </div>
        </div>
        @endif

        <!-- 發票資訊 -->
        <div class="invoice-box">
            <div class="title" style="margin: 20px 0 10px 0;"><i class="fas fa-square green"></i> 發票資訊</div>
            <div style="border: 1px solid #9e9e9e; border-radius: 5px; padding: 5px;">
                <table class="table">
                    <tr class="a1">
                        <td>
                            <input type="radio" class="c1" name="invoiceType" value="a1"> 
                            捐贈發票
                            <span style="color:coral">(捐贈發票說明)</span>
                        </td>
                    </tr>
                    <tr class="a2">
                        <td>
                            <input type="radio" class="c2" name="invoiceType" value="a2" checked> 
                            二聯電子發票<br>
                            <span style="margin-left: 18px;font-size: 10px;color: #a9a8a8;">配合國稅局勸止二聯換開三聯之政策，本公司保留換開發票的權利</span>
                        </td>
                    </tr>
                    <tr class="a3">
                        <td>
                            <input type="radio" class="c3" name="invoiceType" value="a3"> 
                            三聯電子發票
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>統一編號</span>
                            <input type="text" class="form-control" style="width: 50%;display: inline-block; margin: 0 20px;" id="customerIdentifier" placeholder="" name="customerIdentifier" disabled>
                            <span style="color: #a2a2a2;">限數字</span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>發票抬頭</span>
                            <input type="text" class="form-control" style="width: 50%;display: inline-block; margin: 0 20px;" id="customerTitle" placeholder="" name="customerTitle" disabled>
                            <span style="color: #a2a2a2;">限22個全形文字</span>
                        </td>
                    </tr>
                    <tr>
                        <td class="col-md-12" style="color: #a2a2a2;">。『統一編號及抬頭，將於發票開立時一併上傳至財政部電子發票整合平台，而電子發票證明聯警會列出統一編號』</td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="policy">
            <div style="display: flex;justify-content: baseline;align-items: center;">
                <input id="policy" type="checkbox" style="height: 20px; width: 20px; margin: 10px 20px;" >
                我同意 <a href="#" data-toggle="modal" data-target="#selfModal">美z.人生網路刷卡購物自訂爽快條款</a>，<a href="#" data-toggle="modal" data-target="#policyModal">隱私權政策</a>
            </div>
    
    
            <div style="display: flex;justify-content: baseline;align-items: center;">
                <div style="width: 60px;">
                    <input id="agreeReturn" type="checkbox" style="height: 20px; width: 20px; margin: 10px 20px;" >
                </div>
                <div style="flex: 1;">我同意辦理退貨時，美Z.人生將以「銷貨折讓」辦理銷退，於退貨完成時間開立電子銷貨折讓單(可至會員專區查詢)</div>
            </div>
            <br> 
        </div>
        
        

        <input type="text" id="refreshed" style="display:none">
        <script type="text/javascript">
            $(function(){
                if($("#refreshed").val() == "yes") {
                    location.reload();
                }
            });
        </script>
        
        <!-- 結帳 -->            
        <button type="submit" class="btn btn-lg btn-primary btn-block" id="btnPay" @if(!isset($memberData->card_token)) disabled @endif>結帳</button>
        {{-- <button type="button" class="btn btn-lg btn-primary btn-block" id="googlePay">GOOGLE PAY</button> --}}
        
        @else
        <button type="button" class="btn btn-lg btn-primary btn-block" id="loginBtn" data-toggle="modal" data-target="#loginModal">登入</button>
        @endif
    </form>
    @endif
</div>

@endsection

@section('footer')
    @include('FrontEnd.layouts.footer')
@endsection

@section('after_scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.all.min.js"></script>
    <script src="https://pay.google.com/gp/p/js/pay.js"></script>
    <script src="https://js.tappaysdk.com/tpdirect/v4"></script>
    <script> TPDirect.setupSDK(13155, 'app_4R0lByKiPVDYQOqemZ8ERcmsIn5TJB16B8G5R7BUQPmGESOjjdLlwl00zGT8', 'sandbox') </script>
    {{-- <script> TPDirect.setupSDK(13120, 'app_bVXJUlw94P3AqNs6AOQCzg6Ss8nwwgdfi9zfLF1QIZlY2Wi1A3mTM3CbaP9a', 'sandbox') </script> --}}
    <script type="text/javascript" src="{{url('assets/js/cart.js')}}?v={{Config::get('app.version')}}"></script>
    <script>
        var popup = null;
        $(function(){
            $("#openMap").on("click", function(){
                var pickWay = $("input[name='pick_way']:checked").val();
                popup = window.open('{{url("map/choose/N")}}' + '/' + pickWay, '_blank', 'width=500,height=500');
                
            });
        });

        function setData(CVSStoreID,CVSStoreName,CVSAddress) {
            $("#CVSStoreID").val(CVSStoreID);
            $("#CVSStoreName").val(CVSStoreName);
            $("#CVSAddress").val(CVSAddress);
        }        
        
        $("#googlePay").on("click", function(){
            
            TPDirect.googlePay.getPrime(function(err, prime){
				if(prime != null) {
					$("input[name='prime']").val(prime);
					//$this.submit();
				}
				else {
					alert("無法使用Google Pay");
					return;
				}
            });
            
        });
    </script>
@endsection
