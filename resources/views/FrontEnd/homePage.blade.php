<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="zh-TW">
<head>
	<meta charset="UTF-8">
	<title>抽獎序號/中獎查詢</title>
	<meta name='description' content=""/>
	<meta name='keywords' content=""/>
	<meta name="MobileOptimized" content="width">
	<meta name="HandheldFriendly" content="true">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	    <!-- CSRF Token -->
		<meta name="csrf-token" content="{{ csrf_token() }}">
	<!-- You can use open graph tags to customize link previews.
    Learn more: https://developers.facebook.com/docs/sharing/webmasters -->
    <meta property="og:url"           content="https://www.your-domain.com/your-page.html" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="Your Website Title" />
    <meta property="og:description"   content="Your description" />
    <meta property="og:image"         content="https://www.your-domain.com/path/image.jpg" />

	<link rel="stylesheet" href="{{url('assets/css/style.css?')}}">
    <script type="text/javascript" src="{{url('assets/js/jquery-1.11.1.min.js')}}"></script>
	<script type="text/javascript" src="{{url('assets/js/goto.js')}}"></script>
	<script type="text/javascript" src="{{url('assets/js/jquery-3.3.1.min.js')}}"></script>
	<link rel="stylesheet" type="text/css" href="{{url('assets/css/jquery.flipcountdown.css?')}}" />
	<script type="text/javascript" src="{{url('assets/js/jquery.flipcountdown.js')}}"></script>
	<script>
		$(function(){
		
			var NY = Math.round((new Date('6/01/2019 00:00:01')).getTime()/1000);
			$('#retroclockbox1').flipcountdown({
				tick:function(){
					var nol = function(h){
						return h>9?h:'0'+h;
					}
					var	range  	= NY-Math.round((new Date()).getTime()/1000),
						secday = 86400, sechour = 3600,
						days 	= parseInt(range/secday),
						hours	= parseInt((range%secday)/sechour),
						min		= parseInt(((range%secday)%sechour)/60),
						sec		= ((range%secday)%sechour)%60;
					return nol(days)+' '+nol(hours)+' '+nol(min)+' '+nol(sec);
				}
			});
		});
	</script>


</head>
<body>

	<!-- Load Facebook SDK for JavaScript -->
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	    var js, fjs = d.getElementsByTagName(s)[0];
	    if (d.getElementById(id)) return;
	    js = d.createElement(s); js.id = id;
	    js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
	    fjs.parentNode.insertBefore(js, fjs);
	  }(document, 'script', 'facebook-jssdk'));
	  function add_a() {
		  var num  = parseInt($("#num_a").val());
		  num+=1;
		  $("#num_a").val(num);
		}
		function add_b() {
			var num  = parseInt($("#num_b").val());
		  num+=1;
		  $("#num_b").val(num);
		}
		function remove_a() {
			var num  = parseInt($("#num_a").val());
			if(num==1){
				return;
			}else{
			num-=1;
			$("#num_a").val(num);
			}
		}
		function remove_b() {
			var num  = parseInt($("#num_b").val());
			if(num==1){
				return;
			}else{
			num-=1;
			$("#num_b").val(num);
			}
		}
		function addcart() {
			var num = 0;
			var type = $("input[name='pay-list']:checked").val();
			if(type=="a"){
				num  = parseInt($("#num_a").val());
			}
			else{
				num  = parseInt($("#num_b").val());
			}
			$.post(BASE_URL+'/cart', {"num": num,"prodId": type
			}, function(data){
                    if(data.status == "success") {
                        $(".header-num-box").text(cartNum);
                        Swal.fire({
                            type: 'success',
                            text: '商品已加入購物車',
                        })
                        //alert('商品已加入購物車');
                    }
                    else {
                        alert(data.message);
                    }
                    
                }, 'JSON');
		}
	</script>
	<script async defer crossorigin="anonymous" src="https://connect.facebook.net/zh_TW/sdk.js#xfbml=1&version=v3.2&appId=227613341408803&autoLogAppEvents=1"></script>

	<header class="header-box" id="header-box">
		<div class="content-box clearfix">
			<h3 class="title">抽獎序號/中獎查詢</h3>
			<p class="btn"><a href="{{url('search')}}">查詢</a></p>
		</div>
	</header>
	<main class="main-box">
		<section class="info-box" id="info-box">
			<img src="{{url('assets/images/img_800x1000.jpg')}}"  alt="" width="100%">
			<div class="content-box">
				<h1 class="manin-title">499元福袋送⾏充電源 or 無線充電盤(擇⼀)</h1>
				<h2 class="sub-title c-org">再抽<br>勞⼒⼠50週年年紅字紀念款鬼王<br>LV LOCK IT 經典包</h2>
				<h3 class="limited c-blue">每袋只要499，限量販售中</h3>
				<div class="freight-box">
					<p>⼀袋運費99元！ <br>
						兩袋以上免運費！<br>
						<span>9219 已出售</span>
					</p>
				</div>
				<div class="share-box clearfix">
					<div class="fb-likebox">
						<div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-width="" data-layout="box_count" data-action="like" data-size="large" data-show-faces="false" data-share="false"></div>
					</div>
					<ul class="social_media">
						<li><a href="#" class="fb"></a></li>
						<li><a href="#" class="messenger"></a></li>
					</ul>
				</div>
				<ul class="gift-list">
					<li><p class="c-org">(壹獎) 限定款ROLEX目前抽獎率1/1200</p></li>
					<li><p>(貳獎) LV包目前抽獎率1/1200 </p></li>
					<li><p>(參獎) PHILIPS 50吋電視⽬目抽獎率1/1200</p></li>
					<li><p>(肆獎) IPAD⽬前抽獎率1/1200</p></li>
					<li><p>(伍獎) SWITCH⽬前抽獎率1/1200</p></li>
					<li><p>(伍獎) PS4目前抽獎率1/1200</p></li>
					<li><p>(柒獎) GHD吹風機⽬前抽獎率1/1200</p></li>
					<li><p>(捌獎) 雀巢咖啡機⽬目前抽獎率3/1200</p></li>
				</ul>
				<p class="p-info">超高中獎率！<br>錯過不再！名錶、名包屬於你！</p>
				<div class="time-box"><!-- 時間倒數欄位 -->
					<table cellpadding="0" cellspacing="0" border="0" style="margin: 0 auto;">
						<tr>
							<td colspan="4" align="center">
								<div id="retroclockbox1" style="display: inline-block;"></div>
							</td>
						</tr>
						<tr>
							<td style="width:80px;text-align:center;">天</td>
							<td style="width:80px;text-align:center;">時</td>
							<td style="width:80px;text-align:center;">分</td>
							<td style="width:80px;text-align:center;">秒</td>
						</tr>
					</table>
					
				</div>
				<p class="p-info">
					<span class="c-org f-size-l">晶華50週年⽣⽇快樂！</span><br>
					<span class="c-blue">好康⼤禮贊助回饋！</span>
				</p>
				<p class="p-info f-size-s c-gray">委託XX第三⽅公正<br>
					全程直播<br>
					公正、溫暖、正義
				</p>
				<div class="bn-box">
					<img src="{{url('assets/images/img_800x458.jpg')}}"  alt="" width="100%">
				</div>
				<h3 class="product-name">福袋商品</h3>
				<div class="bn-box">
					<img src="{{url('assets/images/img_800x800_01.jpg')}}"  alt="" width="100%">
				</div>
				<ul class="product-info">
					<li><h5>商品說明</h5></li>
					<li><p class="left-p">商品廠商：</p><p>FANTASY</p></li>
					<li><p class="left-p" class="left-p">商品顏色：</p><p>紅⽩兩色(隨機出貨) </p></li>
					<li><p class="left-p">商品市值：</p><p>約599元/個 </p></li>
					<li><p class="left-p">使用⽅法：</p><p>將可無線充電手機 or ⾃行貼上無線充電貼之⼿機放上即可充電</p></li>
				</ul>
				<div class="bn-box">
					<img src="{{url('assets/images/img_800x800_02.jpg')}}"  alt="" width="100%">
				</div>
				<ul class="product-info">
					<li><h5>商品說明</h5></li>
					<li><p class="left-p">商品廠商：</p><p>FANTASY</p></li>
					<li><p class="left-p">商品顏色：</p><p>紅⽩兩色(隨機出貨)</li>
					<li><p class="left-p">商品市值：</p><p>約499元/個 </p></li>
					<li><p class="left-p">使用⽅法：</p><p>將行動充充滿電後，外出接上線後即可充電</p></li>
				</ul>
				<div class="bn-box">
					<img src="{{url('assets/images/img_800x800_03.jpg')}}"  alt="" width="100%">
				</div>
				<div class="youtube-box">
					<iframe src="https://www.youtube.com/embed/iBncuiDvPG4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>
				<h3 class="product-name">福袋獎項</h3>
				<div class="bn-box">
					<img src="{{url('assets/images/img_800x800_04.jpg')}}"  alt="" width="100%">
				</div>
				<div class="bn-box">
					<img src="{{url('assets/images/img_800x800_05.jpg')}}"  alt="" width="100%">
				</div>
				<div class="bn-box">
					<img src="{{url('assets/images/img_800x800_06.jpg')}}"  alt="" width="100%">
				</div>
				<div class="bn-box">
					<img src="{{url('assets/images/img_800x800_07.jpg')}}"  alt="" width="100%">
				</div>
				<div class="bn-box">
					<img src="{{url('assets/images/img_800x800_08.jpg')}}"  alt="" width="100%">
				</div>
				<div class="bn-box">
					<img src="{{url('assets/images/img_800x800_09.jpg')}}"  alt="" width="100%">
				</div>
				<div class="bn-box">
					<img src="{{url('assets/images/img_800x800_10.jpg')}}"  alt="" width="100%">
				</div>
				<div class="bn-box">
					<img src="{{url('assets/images/img_800x800_11.jpg')}}"  alt="" width="100%">
				</div>
				<div class="bn-box">
					<img src="{{url('assets/images/img_800x800_12.jpg')}}"  alt="" width="100%">
				</div>
				<div class="rule-info">
					<h5>獎  項</h5>
					<ul>
						<li><p>壹講⼀名：限定款50週年年ROLEX紅字⿁王(機率最低1/8000)</p></li>
						<li><p>貳獎⼀名：限定款LV LOCK IT包，市值約50萬(機率最低1/8000)</p></li>
						<li><p>參獎一名：PHILIPS 50吋電視(機率最低1/8000)</p></li>
						<li><p>肆獎⼀名：APPLE IPAD(機率最低1/8000)</p></li>
						<li><p>伍獎⼀名：任天堂SWITCH(機率最低1/8000)</p></li>
						<li><p>伍獎⼀名：SONY PS4(機率最低1/8000)</p></li>
						<li><p>陸獎⼀名：WINIX空氣清淨機(機率最低1/8000) </p></li>
						<li><p>柒獎⼀名：GHD吹風機(機率最低1/8000)</p></li>
						<li><p>捌獎三名：雀巢咖啡機(機率最低3/8000)</p></li>
					</ul>
				</div>
				<div class="rule-info">
					<h5>活動規則</h5>
					<ul>
						<li><p>活動期間：2019/05/15~07/15，晶華週年年慶之【499元福袋】單⼀售價：新台幣499元整。</p></li>
						<li><p>【499元福袋】限量銷售八千份，售完為⽌。</p></li>
						<li><p>【499元福袋】分別有兩款，內容說明如下。<br>
						   	一.包含xx無線充電盤及開運貔貅手環⼀組。<br>
						    二.包含xx行動電源及開運貔貅⼿環⼀組。</p>
						</li>
						<li><p>【活動期間於本活動網站完成【499元福】線上購買，即可 獲得⼀組序號(網站上查詢)，即可獲得【網站上壹～陸獎】抽獎資格，獎項請見以下獎項說明。 </p></li>
						<li><p>所有抽獎資格為獨立擁有，不限個人名額，故同一消費者，若完成購買複數【特規牛排福袋組合】，即代表獲得複數個抽獎資格，更多數量則以此類推。 </p></li>
						<li><p>所有獎項不限銷售數量，皆會抽出。</p></li>
						<li><p>本活動所有獎項為限量抽獎之獎項，數量與詳細內容請⾒獎項說明。 </p></li>
						<li><p>本活動無任何線下實體銷售通路路，特此說明。</p></li>
						<li><p>特別提醒：所有商品，恕不退貨，若商品有瑕疵，請以網站上各種聯聯絡⽅式聯繫客服換貨。</p></li>
						<li><p>致桃園市桃園區經國路515號晶華養身館自行領取。</p></li>
						<li><p>中獎者若未滿20歲，須獲得法定代理人同意及代為領取。依中華⺠國所得法規定，舉凡中獎⾦金金額或獎項年度累計價值超過1,001元（含）以上，將列列入本年度之個人 綜合所得稅申報。另中獎獎品價值超過新台幣20,000元（含）以上者，應⾃行負擔機會中獎所得稅，依法須預先扣繳10%稅金（稅額以獎品市價計算），請⾃行報稅及繳稅，未能如期依法繳納應繳稅額者，視同⾃願放棄中獎權利。 </p></li>
						<li><p>如遇不可抗力之因素，主辦單位保留隨時修改活動辦法及更換等值獎品之權利。</p></li>
						<li><p>本活動獎項寄送僅限台灣本島。</p></li>
						<li><p>若若因提供資料填寫不完整、不正確，或其他非不可抗 因素，導致主辦單位無法取得聯聯繫者，則視同放棄兌換資 格，且不另⾏行行通知。</p></li>
						<li><p>獎項或獎品⼀旦寄出後，若若有遺失、遭冒領或偷竊等 喪失佔有之情形，主辦單位恕不負責補發。</p></li>
						<li><p>若若中獎者不符合、不同意或違反本活動規定者，主辦單位保有取消中獎資格及參參與本活動的權利利，並對任何破 壞本活動⾏行行為保留留法律律追訴權。</p></li>
						<li><p>獎項內容及服務品質應參參照⽣生產商或服務商提供之標準，如因使⽤用該獎品服務產⽣生任何爭議，由該獎項或服 務之提供廠商依法承擔責任。 </p></li>
					</ul>
				</div>

				<div class="rule-info">
					<h5>隱私權政策</h5>
					<ul>
						<li><p><b>蒐集之⽬的：依諾維新股份有限公司(下稱本公司)，於舉辦【499福袋】福袋販及抽獎活動(下稱本活動)的特定⽬的範圍內將蒐集您個人資料。</b></p></li>
						<li><p><b>蒐集個人之資料之類別：包括您的姓名、⾝分證字號、出生年月日、電話、地址、電子郵件等個人資料及您的信⽤卡或金融機構帳戶資訊等個人資料。</b></p></li>
						<li><p><b>個⼈資料利用之期間、地區、對象及⽅式：</b><br>
							期間：本公司依業務需求及依法令規定應為保存之期間或經您提出書⾯要求本公司刪除⽌。<br>
							地區：臺灣。<br>
							對象：本公司及本公司合作之公司。<br>
							⽅式：您的個⼈資料將用於本活動商品或獎品之 通知及寄送或其他本活動目的範圍內之利利⽤⽅式。
							</p>
						</li>
						<li><p><b>針對您的個⼈資料，您可依法向本公司行使下述之權利：</b><br>
							查詢或請求閱覽您的個⼈資料。<br>
							請求製給複製本。<br>
							請求補充或更正。<br>
							請求停⽌蒐集、處理或利用。<br>
							請求刪除。<br>
							如您欲行使上述權利時，您可撥打本公司之消費者服務電話或以電⼦郵件⽅式聯聯絡本公司。
							</p>
						</li>
						<li><p><b>特別告知，您得⾃由選擇是否提供您的個⼈資料，惟若您拒絕提供或提供不完全時，將影響本活動商品、獎品之通知、寄送或您可能無法參加本活動。</b></p></li>
					</ul>
				</div>
				<div class="fb-box">
					<div class="fb-page" data-href="https://www.facebook.com/facebook/" data-tabs="timeline" data-width="100%" data-height="500" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/facebook/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/facebook/">Facebook</a></blockquote></div>
				</div>
			</div>
		</section>
		<footer class="footer_box">
			<ul class="link clearfix">
				<li><a href="search.html"><p class="icon_01">抽獎序號/<br>中獎查詢</p></a></li>
				<li><a href="#" target="_blank"><p class="icon_02">facebook<br>粉絲團</p></a></li>
				<li><a href="#"><p class="icon_03">隱私權條款</p></a></li>
			</ul>
			<p class="Copyright">本活動由xx第三方⾒證<br>
				© 2019 Copyright      客服電話 0800-000-000
			</p>
			<a class="go_top"></a>
			<div class="btn_buy" id="show_order"><p><a><span class="icon_cart">我要購買499福袋</span></a></p></div>
			<div class="btn_buy go_order" id="go_order">
			<form class="form" role="form" method="post" action="{{ url('cartsub') }}">
			{{ csrf_field() }}
				<div class="fix">
					<div class="buy_list">
						<a class="btn_close"></a>
						<ul>
							<li class="clearfix">
								<div class="title">
								<input id="buy-a" type="radio" name="type" value="a" checked="">
									<label for="buy-a">499元福袋送⾏充電源</label>
								</div>
								<div class="right_nb">
									<div class="input_quantity">
										<button type="button" onclick="remove_a()">–</button>
			                            <input type="text" name='num_a' id="num_a" value="1">
			                            <button type="button" onclick="add_a()">+</button>
			                        </div>
								</div>
							</li>
							<li class="clearfix">
								<div class="title">
									<input id="buy-b" type="radio" name="type" value="b" checked="">
									<label for="buy-b">499元福袋送無線充電盤</label>
								</div>
								<div class="right_nb">
									<div class="input_quantity">
										<button type="button" onclick="remove_b()">–</button>
			                            <input type="text" name='num_b' id="num_b" value="1">
			                            <button type="button" onclick="add_b()">+</button>
			                        </div>
								</div>
							</li>
						</ul>
					</div>
				<button type="submit">前往結帳</button>
				<!-- <p><a href="cart.html"><span>前往結帳</span></a></p> -->
				</div>
				</form>
			</div>
		</footer>
	</main>
</body>
</html>



