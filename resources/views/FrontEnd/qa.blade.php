@extends('FrontEnd.layout',[
	"seo_title" => "Q＆A",
	"seo_desc" => preg_replace("/\n+/","",strip_tags("Q＆A")),
	"seo_img" => null
])

@section('after_style')
    <link rel="stylesheet" href="{{url('assets/css/reset.css')}}" type="text/css" media="screen">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">
    <link rel="stylesheet" href="{{url('assets/libs/bootstrap-3.3.7.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/style.css')}}" type="text/css" media="screen">
    <link rel="stylesheet" href="{{url('assets/css/qa.css')}}" type="text/css" media="screen">
    
    <script type="text/javascript" src="{{url('assets/libs/jquery-3.3.1.min.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/libs/tether.min.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/libs/bootstrap-3.3.7.min.js')}}"></script>
@endsection

@section('header')
    @include('FrontEnd.layouts.header')
@endsection

@section('content')
<div class="breadcrumbs-box">
	<div class="content-box">
		<div class="name">Q &amp; A</div>
		<div class="breadcrumbs">
			<a href="{{url('/')}}">美z.人生</a>  &gt; Q &amp; A
		</div>
	</div>
</div>
<div id="content-box">
    <div class="left-box">
        <div class="title"><i class="fas fa-square"></i>問題類別</div>
        <ul>
            @foreach($cateData as $key=>$row)
            <a href="#item{{$row->id}}">
                <li @if($key == 0) class="active" @endif>{{$row->name}}</li>
            </a>
            @endforeach
        </ul>
    </div>
    <div class="right-box">
        @foreach($cateData as $key=>$row)
        <div class="title" id="item{{$row->id}}"><i class="fas fa-square"></i> {{$row->name}}</div>
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            @foreach($qaData as $k=>$val)
            @if($row->id == $val->cate_id)
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingThree">
                    <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-{{$key}}-{{$k}}" aria-expanded="false" aria-controls="collapse-{{$key}}-{{$k}}" @if(isset($val->title_color))style="color:{{$val->title_color}}"@endif>
                        {{$val->q_title}}
                    </a>
                    </h4>
                </div>
                <div id="collapse-{{$key}}-{{$k}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-{{$key}}-{{$k}}">
                    <div class="panel-body">
                    {!! $val->answer !!}
                    </div>
                </div>
            </div>
            @endif
            @endforeach
        </div> 
        @endforeach
    </div>
</div>
@endsection

@section('footer')
    @include('FrontEnd.layouts.footer')
@endsection

@section('after_scripts')
  <script type="text/javascript" src="{{url('assets/js/qa.js')}}"></script>
@endsection

