<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="zh-TW">
<head>
	<meta charset="UTF-8">
	<title>請下滑填寫 抽獎 / 付款 資訊</title>
	<meta name='description' content=""/>
	<meta name='keywords' content=""/>
	<meta name="MobileOptimized" content="width">
	<meta name="HandheldFriendly" content="true">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="{{url('assets/css/style.css?')}}">
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
	<script type="text/javascript" src="{{url('assets/js/goto.js')}}"></script>
	
</head>
<body>
	<header class="header-box">
		<div class="content-box inside-box clearfix">
			<a class="home arr" href="index.html" title="回首頁"></a>
			<h3 class="title">抽獎序號 / 中獎資訊 查詢</h3>
		</div>
	</header>
	<main class="main-box">
		<section class="info-box form-box">
			<div class="shopping-cart-list">
				<h1 class="title">499元福袋送⾏充電源 or 無線充電盤(擇⼀)</h1>
				<table class="buy-list" cellpadding="0" cellspacing="0" sborder="0">
					<tr>
						<th width="50%"><p>品項</p></th>
						<th><p>數量</p></th>
						<th align="right"><p>小計</p></th>
					</tr>
					<tr>
						<td><p>{{$prod_nm}}</p></td>
						<td align="center"><p>{{$num}}</p></td>
						<td align="right"><p class="c-red">{{number_format($sumamt)}}</p></td>
					</tr>
					<tr>
						<td><p>運費 <span class="c-red">+ ${{number_format($shipfee)}}</span></p></td>
						<td colspan="2" align="right"><p>福袋總計 <span class="c-red f-size-m">{{number_format($ttlamt)}}</span></p></td>
					</tr>
				</table>
			</div>
			<div class="content-box">
			<form class="shoppingcar_form_box" action="{{url('pay')}}" method="POST" id="payForm">
				{{ csrf_field() }}
				<input type="hidden" name="ttlAmt" value="559">
				<input type="hidden" name="shipFee" />
				<input type="hidden" name="sellDntNum" />
				<input type="hidden" name="prime" />
				<input type="hidden" name="agreePayRemeber" />
				<input type="hidden" name="shipFree"  value="80"/>
				
					<h3 class="from-title">付款方式</h3>
					<ul class="fillin pay">
						<li>
							<input id="pay-card" type="radio" name="pay-list" value=">信用卡-快速付款">
							<label for="pay-card">信用卡-快速付款</label>
						</li>
						<li>
							<input id="pay-atm" type="radio" name="pay-list" value="ATM付款" checked>
							<label for="pay-atm">ATM付款</label>
						</li>
						<li>
							<input id="pay-store" type="radio" name="pay-list" value="超商條碼付款">
							<label for="pay-store">超商條碼付款</label>
						</li>
					</ul>
					<br>
					<h3 class="from-title">抽獎資訊</h3>
					<ul class="fillin">
						<li>
							<label for="name">姓名</label>
							<input required id="name" type="text" name="" value="阿哲 Kent" placeholder="請輸入姓名">
						</li>
						<li>
							<label for="phone">手機號碼</label>
							<input required id="phone" type="text" name="" value="" placeholder="請輸入購買人手機號碼 例: 0912345678">
						</li>
						<li>
							<label for="phone">電子信箱</label>
							<input required id="phone" type="text" name="" value="" placeholder="請輸入購買人電子信箱">
						</li>
						<li>
							<label for="code">福袋收件地址</label>
							<input required id="code" type="text" name="" value="" placeholder="請輸入福袋完整收件地址(含縣市)">
						</li>
						<li class="the_terms">
							<input required id="terms" type="radio" name="field" value="option">
							<label for="terms">同意隱私權條款</label>
							<a href="#" class="link">隱私權條款</a>
						</li>
					</ul>
				
			</div>
		</section>
		<footer class="footer_box">
			<ul class="link clearfix">
				<li><a href="search.html"><p class="icon_01">抽獎序號/<br>中獎查詢</p></a></li>
				<li><a href="#" target="_blank"><p class="icon_02">facebook<br>粉絲團</p></a></li>
				<li><a href="#"><p class="icon_03">隱私權條款</p></a></li>
			</ul>
			<p class="Copyright">本活動由xx第三方⾒證<br>
				© 2019 Copyright      客服電話 0800-000-000
			</p>
			<!-- <div class="btn_buy"><p><a href="#"><span>付款</span></a></p></div> -->
			<button type="submit">付款</button>
		</footer>
		</form>
	</main>
</body>
</html>



