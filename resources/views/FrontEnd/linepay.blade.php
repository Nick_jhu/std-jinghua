@extends('FrontEnd.layout',[
	"seo_title" => "購物車",
	"seo_desc" => preg_replace("/\n+/","",strip_tags("購物車")),
	"seo_img" => null
])

@section('after_style')
    <link rel="stylesheet" href="{{url('assets/css/reset.css')}}" type="text/css" media="screen">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">
    <link rel="stylesheet" href="{{url('assets/libs/bootstrap-3.3.7.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/style.css')}}" type="text/css" media="screen">
    <link rel="stylesheet" href="{{url('assets/css/cart.css')}}" type="text/css" media="screen">

    <script type="text/javascript" src="{{url('assets/libs/jquery-3.3.1.min.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/libs/tether.min.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/libs/bootstrap-3.3.7.min.js')}}"></script>

    @if($errors->has('message'))
    <script>
        var errorMsg = "{{$errors->get('message')[0]}}";
        alert(errorMsg);
	</script>
    @endif
    
    <style>
        #tappay-iframe {
            font-family: Lato,'Helvetica Neue',Arial,Helvetica,sans-serif;
            margin: 0;
            outline: 0;
            -webkit-appearance: none;
            tap-highlight-color: rgba(255,255,255,0);
            line-height: 1.21428571em;
            padding: .578571em 1em;
            font-size: 1em;
            background: #fff;
            border: 1px solid rgba(34,36,38,.15);
            color: rgba(0,0,0,.87);
            border-radius: .28571429rem;
            box-shadow: 0 0 0 0 transparent inset;
            -webkit-transition: color .1s ease,border-color .1s ease;
            transition: color .1s ease,border-color .1s ease;
            width: 100%;
        }
    </style>
@endsection

@section('header')
    @include('FrontEnd.layouts.header')
@endsection

<script src="https://js.tappaysdk.com/tpdirect/v3"></script>
<script> TPDirect.setupSDK('13120', 'app_bVXJUlw94P3AqNs6AOQCzg6Ss8nwwgdfi9zfLF1QIZlY2Wi1A3mTM3CbaP9a', 'sandbox') </script>

@section('content')
<div class="breadcrumbs-box">
    <div class="content-box">
        <div class="name">Basket</div>
        <div class="breadcrumbs">
            Start &gt; <a href="#">結帳</a>
        </div>
    </div>
</div>
<div id="content-box">
    <div class="ui grid centered doubling stackable">
        <div class="six wide column">
            <div class="ui segment">
                <form class="ui form">
                    <div class="field">
                        <label>信用卡</label>
                        <div id="tappay-iframe"></div>
                    </div> 
                </form>
            </div>
        </div>
    </div>
    <button type="button" class="btn btn-lg btn-primary btn-block" id="submit" style="margin-top:5px;" disabled>確認付款</button>
</div>

@endsection

@section('footer')
    @include('FrontEnd.layouts.footer')
@endsection

@section('after_scripts')
    <script>
        var statusTable = {
            '0': '欄位已填好，並且沒有問題',
            '1': '欄位還沒有填寫',
            '2': '欄位有錯誤，此時在 CardView 裡面會用顯示 errorColor',
            '3': '使用者正在輸入中',
        }
        var defaultCardViewStyle = {
            color: 'rgb(0,0,0)',
            fontSize: '15px',
            lineHeight: '24px',
            fontWeight: '300',
            errorColor: 'red',
            placeholderColor: ''
        }
        TPDirect.card.setup('#tappay-iframe', defaultCardViewStyle)
        TPDirect.card.onUpdate(function (update) {
            var submitButton = document.querySelector('#submit')
            var cardViewContainer = document.querySelector('#tappay-iframe')
            if (update.canGetPrime) {
                $("#submit").prop("disabled", false);
            } else {
                $("#submit").prop("disabled", true);
            }
        });

        document.querySelector('#submit').addEventListener('click', function(event) {
            TPDirect.card.getPrime(function(result) {
                console.log(result);
            });
        });
    </script>
@endsection
