@extends('FrontEnd.layout',[
	"seo_title" => "商品列表",
	"seo_desc" => preg_replace("/\n+/","",strip_tags("商品列表")),
	"seo_img" => null
])

@section('after_style')
    <link rel="stylesheet" href="{{url('assets/css/reset.css')}}" type="text/css" media="screen">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{url('assets/css/style.css')}}" type="text/css" media="screen">
    <link rel="stylesheet" href="{{url('assets/css/prodslist.css?t=24223543')}}" type="text/css" media="screen">

    <script type="text/javascript" src="{{url('assets/libs/jquery-3.3.1.min.js')}}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.3/js/tether.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
@endsection

@section('header')
    @include('FrontEnd.layouts.header')
@endsection

@section('content')

<div class="breadcrumbs-box">
	<div class="content-box">
		<div class="name">美Z商城</div>
		<div class="breadcrumbs">
			<a href="{{url('/')}}">美z.人生</a> &gt; 美Z商城
		</div>
	</div>
</div>
<div id="content-box">
	<div class="search-box">
		<form method="GET" id="searchForm" action="{{url('product')}}">
			<div class="form-row">
				<div class="form-group col-md-2 col-sm-12 col-xs-12">
					<span class="web">&nbsp;</span>
					<select class="form-control" id="sellType" name="sellType">
						<option value="" @if($sellType == "") selected @endif>租賃分類</option>
						<option value="R" @if($sellType == "R") selected @endif>租</option>
						<option value="S" @if($sellType == "S") selected @endif>買</option>
					</select>
				</div>
				<div class="form-group col-md-2 col-sm-12 col-xs-12">
					<span class="web">&nbsp;</span>
					<select class="form-control" id="country" name="country">
						<option value="" @if($country == "") selected @endif>地區</option>
                        @foreach($areaData as $row)
                            <option value="{{$row->id}}" @if($country == $row->id ) selected @endif>{{$row->name}}</option>
                        @endforeach
					</select>
				</div>
				<div class="form-group col-md-2">
					取用(到貨)日期                            
					<div id="datepicker-start" class="input-group date" data-date-format="yyyy-mm-dd">
						<input class="form-control" type="text" readonly name="startDate" value="{{$startDate}}" required/>
						<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
					</div>
				</div>
				<div class="form-group col-md-2">
					歸還(寄還)日期                            
					<div id="datepicker-end" class="input-group date" data-date-format="yyyy-mm-dd">
						<input class="form-control" type="text" readonly name="endDate" value="{{$endDate}}" required/>
						<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
					</div>
				</div>
				<!-- <div class="form-group col-md-2">
					&nbsp;
					<select class="form-control" id="num" name="num">
						<option value="" selected>數量</option>
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
						<option value="5">5</option>
					</select>
				</div> -->
				<div class="col-md-2 col-sm-12 col-xs-12">&nbsp;
					<button class="btn btn-link btn-lg search-btn">
					<i class="fas fa-play-circle"></i> 查詢
					</button>
				</div>
			</div>
		</form>
	</div>
	<div class="prodslist-box">
		<div class="left-box">
			<div class="title"><i class="fas fa-square"></i> 商品類別</div>
			<ul class="categories">
                @foreach($cateData as $row)
				<a href="{{url('product?cateId='.$row->id)}}">
					<li @if($cateId == $row->id) class="active" @endif >{{$row->name}}</li>
                </a>
                @endforeach
			</ul>
			<!-- <div class="title"><i class="fas fa-square"></i> 相關商品</div>
			<ul class="recently">
				<a href="#">
					<li>
						<div class="icon-box">
							<img src="{{url('assets/images/prod-1.png')}}" width="71" height="71">
						</div>
						<div class="desc-box">
							<div class="name">Wifi 機</div>
							<div class="cost">$ 3000.00</div>
						</div>
					</li>
				</a>
			</ul> -->
		</div>
		<div class="right-box">
			@foreach($prodData as $row)
			
			<div class="prod-box">
				<div class="img-box">
					<a href="{{url('productDetail/'.$row->id.'?startDate='.$startDate.'&endDate='.$endDate)}}">
					<img src="{{(isset($row->img1)?Storage::url($row->img1):url('assets/images/no_image.png'))}}">
					<div class="@if($row->sell_type == 'R') red-s @else origne-s @endif show">
						<div class="txt">@if($row->sell_type == 'R')租 @else 購 @endif</div>
					</div>
					</a>
				</div>
				<div class="info-box">
					<div class="name"><a href="{{url('productDetail/'.$row->id.'?startDate='.$startDate.'&endDate='.$endDate)}}" style="color: #000;">{{$row->title}}</a></div>
					{{--  <div class="o-price">${{(int)$row->f_price}}</div>  --}}
					@if((int)$row->t_price == 0)
					<div class="price" style="font-weight:400; font-size: 16px; color:#ff8a00;">${{(int)$row->f_price}}</div>
					@else
					<div class="price" style="font-weight:400; font-size: 16px; color:#ff8a00;">${{(int)$row->f_price}} ~ ${{(int)$row->t_price}} </div>
					@endif
					<div>
						{{$row->sub_title}}
					</div>
				</div>
				<!-- <div class="btn-box">
					<a class="details-btn" href="#">DETAILS</a>
					<a class="add-btn" href="#">ADD TO</a>
				</div> -->
			</div>

			@endforeach
			
			@if(count($prodData) == 0)
				<p>查無商品</p>
			@endif
		</div>
	</div>
</div>
@endsection

@section('footer')
    @include('FrontEnd.layouts.footer')
@endsection

@section('after_scripts')
    <script type="text/javascript" src="{{url('assets/js/prodslist.js')}}"></script>
@endsection
