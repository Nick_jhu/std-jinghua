
@extends('FrontEnd.layout',[
	"seo_title" => $prodData->title,
	"seo_desc" => preg_replace("/\n+/","",strip_tags($prodData->slogan)),
	"seo_img" => (isset($prodData->img1)?url(Storage::url($prodData->img1)):url('assets/images/no_image.png'))
])

@section('after_style')
    <link rel="stylesheet" href="{{url('assets/css/reset.css')}}" type="text/css" media="screen">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">
    <link rel="stylesheet" href="{{url('assets/libs/bootstrap-3.3.7.min.css')}}">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" media="all" href="{{url('assets/css/daterangepicker.css')}}" />
    <link rel="stylesheet" href="{{url('assets/css/style.css')}}" type="text/css" media="screen">
    <link rel="stylesheet" href="{{url('assets/css/prod.css?t=249807234')}}" type="text/css" media="screen">

    <script type="text/javascript" src="{{url('assets/libs/jquery-3.3.1.min.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/libs/tether.min.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/libs/bootstrap-3.3.7.min.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
	<style>
		a.disabled {
			color: #bbb !important;
			border-color: #bbb !important;
			pointer-events: none;
		}
	</style>
@endsection

@section('header')
    @include('FrontEnd.layouts.header')
@endsection

@section('content')
<div class="breadcrumbs-box">
	<div class="content-box">
		<div class="name">美Z商城</div>
		<div class="breadcrumbs">
				<a href="{{url('/')}}">美z.人生</a> &gt; <a href="{{url('product')}}">美Z商城</a> &gt; @if(isset($prodData)) {{$prodData->title}} @endif
		</div>
	</div>
</div>
<div id="content-box">
	<div class="left-box">
		<div class="title"><i class="fas fa-square"></i> Categories</div>
		<ul class="categories">
            @foreach($cateData as $row)
            <a href="{{url('product?cateId='.$row->id)}}">
                <li @if($prodData->cate_id == $row->id) class="active" @endif >{{$row->name}}</li>
            </a>
            @endforeach
		</ul>
		<div class="title"><i class="fas fa-square"></i> 相關商品</div>
		<ul class="recently">
			@foreach($relateData as $row)
			<a href="{{url('/productDetail/'.$row->id)}}">
				<li>
					<div class="icon-box">
						<img src="{{(isset($row->img1)?Storage::url($row->img1):url('assets/images/no_image.png'))}}" width="71" height="71">
					</div>
					<div class="desc-box">
						<div class="name" style="font-size: 12px;">{{$row->title}}</div>
						@if($row->t_price > 0)
						<div class="cost">$ {{number_format($row->f_price)}} ~ $ {{number_format($row->t_price)}}</div>
						@else
						<div class="cost">$ {{number_format($row->f_price)}}</div>
						@endif
					</div>
				</li>
			</a>
			@endforeach
		</ul>
	</div>
	<div class="right-box">
		<div class="prod-box">
			<div class="img-box">
				<img src="{{(isset($prodData->img1)?Storage::url($prodData->img1):url('assets/images/no_image.png'))}}" id="prodImg">
				<div class="@if($prodData->sell_type == 'R') red-s @else origne-s @endif show">
					<div class="txt">@if($prodData->sell_type == 'R')租 @else 購 @endif</div>
				</div>
			</div>
		</div>
		<div class="info-box">
			<div class="name">{{$prodData->title}}</div>
			<div class="o-price">$ {{number_format($prodData->t_price)}}</div>
			@if($prodData->t_price > 0)
			<div class="price">$ {{number_format($prodData->f_price)}} ~ $ {{number_format($prodData->t_price)}}</div>
			@else
			<div class="price">$ {{number_format($prodData->f_price)}}</div>
			@endif
			<div class="desc">
				{!!$prodData->slogan!!}
			</div>
			<div class="select-box">
				<table>
					@if($prodData->sell_type == "R")
					<tr>
						<td>旅遊期間：</td>
						<td>
							<div class="form-group col-md-6">                       
								<div id="datepicker-start" class="input-group date" data-date-format="yyyy-mm-dd">
									<input class="form-control" type="text" readonly name="startDate" value="{{$startDate}}" required/>
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								</div>
							</div>
							<div class="form-group col-md-6">                          
								<div id="datepicker-end" class="input-group date" data-date-format="yyyy-mm-dd">
									<input class="form-control" type="text" readonly name="endDate" value="{{$endDate}}" required/>
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								</div>
							</div>
						</td>
					</tr>
					@endif
					<tr>
						<td>
							@if($prodData->sell_type == "S")
							商品型號：
							@else
							選擇國家：
							@endif
						</td>
						<td>
                            @foreach($prodDetailData as $dRow)
								<a href="#prod" style="margin:5px;" class="prodDetailLink @if($dRow->stock <=0) disabled @endif" 
								o-price="{{$dRow->o_price}}" 
								d-price="{{$dRow->d_price}}" 
								prod-id="{{$prodData->id}}"
								prod-no="{{$dRow->prod_no}}"
								prod-detail-id="{{$dRow->id}}"
								sell-type="{{$prodData->sell_type}}"
								prod-nm="{{$prodData->title}}({{$dRow->title}})"
								prod-type="{{$prodData->prod_type}}"
								prod-img = "{{(isset($dRow->img1)?Storage::url($dRow->img1):'')}}"
								>@if($dRow->stock > 0)
								{{$dRow->title}}
								@else
								{{$dRow->title}}(售完)
								@endif</a>
                            @endforeach
						</td>
					</tr>
				</table>
			</div>
			<div class="num-box">
				<table>
					
					<tr>
						<td>數量：</td>
						<td>
							<div id="number-picker" class="form-group">
								<div class="input-group">
									<span class="input-group-btn">
									<button type="button" class="btn btn-default cut-btn">-</button>
									</span>
									<input id="num" class="form-control" type="text" value="1" min="1" max="100" style="text-align: center;">
									<span class="input-group-btn">
									<button type="button" class="btn btn-default add-btn">+</button>
									</span>
								</div>
							</div>
						</td>
					</tr>
					
				</table>
			</div>
			<div class="addto-box">
				<button class="btn btn-block addto-btn" id="addCart">加入購物車</button>
				{{--  @if($prodData->sell_type == "S")
				<button class="btn btn-block addto-btn" id="addCart">加入購物車</button>
				@else
				<p style="padding: 10px;color:red;border: 1px solid #000;">各位使用者大家好最近開始陸續發生歐洲某些國家，還有幾名亞洲區 日本用戶，使用我們WIFI出國時發生網路連線問題，為了維護各位權益，請先暫停下單，我們將於兩個星期，伺服器調整設定完畢後恢復服務，造成不便，敬請見諒，謝謝。<br><span style="margin-left:380px">美z.人生 旅遊</span></p>
				@endif  --}}
			</div>
		</div>
		<div class="tabs-box">
			<ul class="nav nav-tabs">
				<li class="active"><a data-toggle="tab" href="#tab-content-1">商品詳情</a></li>
				<li><a data-toggle="tab" href="#tab-content-2">支援地區</a></li>
			</ul>
			<div class="tab-content">
				<div id="tab-content-1" class="tab-pane fade in active">
					商品詳情
                    <hr>
                    {!! $prodData->descp !!}
				</div>
				<div id="tab-content-2" class="tab-pane fade">
					支援地區
					<hr>
					{!! $prodData->other_content !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('footer')
    @include('FrontEnd.layouts.footer')
@endsection

@section('after_scripts')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.all.min.js"></script>
	<script type="text/javascript" src="{{url('assets/js/prod.js?v=')}}{{Config::get('app.version')}}"></script>
	
	@if(count($prodDetailData) == 1) 
	<script>
		$(function(){
			$(".prodDetailLink").not(".disabled").click();
		});
	</script>
	@endif
@endsection