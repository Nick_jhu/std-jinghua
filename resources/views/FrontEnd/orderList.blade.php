@extends('FrontEnd.layout',[
	"seo_title" => "會員專區",
	"seo_desc" => preg_replace("/\n+/","",strip_tags("會員專區")),
	"seo_img" => null
])
@section('after_style')
   
    <link rel="stylesheet" href="{{url('assets/fonts/FontAwesome/font-awesome.css')}}">
    <link href="{{url('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    
    <link rel="stylesheet" href="{{url('assets/css/bootsnav.css?')}}" type="text/css" media="screen">
    <link rel="stylesheet" href="{{url('assets/css/newstyle.css')}}" type="text/css" media="screen">
    <link rel="stylesheet" href="{{url('assets/css/rwd.css')}}" type="text/css" media="screen">
    <link rel="stylesheet" href="{{url('assets/css/orderList.css')}}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">

    <script type="text/javascript" src="{{url('assets/libs/jquery-3.3.1.min.js')}}"></script>
    <script src="{{url('assets/js/bootstrap.min.js')}}"></script>
    <script src="{{url('assets/js/am-pagination.js')}}"></script>

    @if(Session::get('message'))
    <script>
        alert("{{Session::get('message')}}");
    </script>
    @endif

    @if($errors->any())
    <script>
        var errorMsg = "{{$errors->all()[0]}}";
        alert(errorMsg);
    </script>
    @endif
    <script type="text/javascript">
        $(function(){
            $('.btn a.show_bottom_details').click(function(){
                $(this).parents().parent().parent().parent().next('.show_details').slideToggle();
            });
            return false;
        });
    </script>
@endsection
@section('header')
    @include('FrontEnd.layouts.newHeader')
@endsection
@section('content')
<main class="main_center">
    <section class="top_item">
        <ul class="contents_box clearfix">
            <li class="btn_in"><p><a href="{{url('center')}}">我的訂單</a></p></li>
            <li class=""><p><a href="{{url('memberInfo')}}">會員資料</a></p></li>
        </ul>
    </section>

    <!-- 我的訂單 - 列表 -->
    <section class="order_list">
        <div class="contents_box clearfix">
            <div class="searchbox">
                <form id="searchForm" role="form" method="GET" action="{{ url('center') }}">
                    <input class="fbox" type="text" name="ordNo" placeholder="搜尋訂單">
                    <a class="icon searchLink" href="#"><i class="fas fa-search"></i></a>
                </form>
            </div>

            <ul class="item_filter">
                <li><p @if($status == 'A') class="btn_in" @endif><a role="tab" href="{{url('center')}}?s=A">待付款({{$orderDataCnt}})</a></p></li>
                <li><p @if($status == 'B') class="btn_in" @endif><a role="tab" href="{{url('center')}}?s=B">處理中({{$processOrderData}})</a></p></li>
                <li><p @if($status == 'H' || $status == null) class="btn_in" @endif><a role="tab" href="{{url('center')}}?s=H">歷史訂單({{$orderHistoryData}})</a></p></li>
            </ul>
        </div>

        <div child="tab-content" id="tab1">
            <ul class="list contents_box">
                @foreach($orderData as $key=>$row)
                <li class="clearfix">
                    @if($row->status == 'A')
                    <a class="delete" name="cancelBtn" url="{{url('cancelOrder/'.$row->id)}}" title="取消訂單"></a>
                    @endif
                    <div class="onder_main_info clearfix">
                        <div class="product_photo">
                            <img src="{{url('assets/images/img_default.jpg')}}">
                        </div>
                        <div class="product_info clearfix">
                            <div class="order_number">
                                <p class="order_date">{{$row->created_at}}</p>
                                <p class="number">
                                    <span class="the_type 
                                        @if($row->status == 'A')
                                            nonepay_red
                                        @elseif($row->status == 'B')
                                            done_green
                                        @elseif($row->status == 'C' || $row->status == 'I')
                                            done_green
                                        @elseif($row->status == 'D')
                                            done_green
                                        @endif
                                    ">
                                        @if($row->status == 'A')
                                            未付款
                                        @elseif($row->status == 'B')
                                            已付款
                                        @elseif($row->status == 'C' || $row->status == 'I')
                                            運送中
                                        @elseif($row->status == 'D')
                                            已送達
                                        @elseif($row->status == 'E')
                                            已歸還
                                        @elseif($row->status == 'F')
                                            通知取消
                                        @elseif($row->status == 'G')
                                            已取消
                                        @elseif($row->status == 'H')
                                            已刷退
                                        @endif
                                    </span>
                                    訂單編號  {{$row->ord_no}}
                                </p>
                            </div>
                            <div class="order-total">
                                @if(isset($row->CVSPaymentNo))
                                <p class="order_date">查詢單號 <span><a href="https://eservice.7-11.com.tw/e-tracking/search.aspx" target="_blank">{{$row->CVSPaymentNo.$row->CVSValidationNo}}</a></span></p>
                                @endif
                                <p class="order_money">訂單金額<span class="red">$ {{number_format($row->d_price)}}</span></p>
                            </div>
                        </div>
                        <div class="product_subtotal">
                        
                            <div class="exbox clearfix">
                                @if($row->status == 'A')
                                <p class="btn"><a class="go_pay" name="payBtn" href="#" data-toggle="modal" data-target="#myModal" ordNo="{{$row->ord_no}}" ordId="{{$row->id}}">我要付款</a></p>
                                @endif
                                <p class="btn"><a class="show_bottom_details" href="javascript:;">查看明細</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="show_details">
                        <div class="single_order clearfix">
                            <div class="sub_title"><p>訂單狀況</p></div>
                            <div class="order_details">
                                <div class="abox clearfix">
                                    <ul class="order_step clearfix">
                                        <li  @if($row->status == 'A') class="btn_in" @endif><p>訂單建立<!--<span>2019-01-12 18:18</span>--></p></li>
                                        @if(!in_array($row->status, ['F', 'G']))
                                        <li @if($row->status == 'B') class="btn_in" @endif><p>完成付款<!--<span>2019-01-12 18:18</span>--></p></li>
                                        <li @if(in_array($row->status, ['C', 'I'])) class="btn_in" @endif><p>運送中<!--<span>2019-01-12 18:18</span>--></p></li>
                                        <li @if(in_array($row->status, ['D', 'E'])) class="btn_in" @endif><p>已送達<!--<span>2019-01-12 18:18</span>--></p></li>
                                        @else
                                        <li class="btn_in"><p>已取消<!--<span>2019-01-12 18:18</span>--></p></li>
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="single_order clearfix">
                            <div class="sub_title"><p>訂單明細</p></div>
                            <div class="order_details">
                                @if(count($orderRentData[$key]) >  0)
                                    @foreach($orderRentData[$key] as $val)
                                    <div class="abox clearfix">
                                        <div class="product_photo">
                                            <img src="{{url('assets/images/img_default_2.jpg')}}">
                                        </div>
                                        <div class="product_info">
                                            <h3 class="product_title"><span class="red">租</span><a href="#" target="_blank"> {{$val->prod_nm}}</a></h3>
                                            <div class="product_item"><p class="product_price">單價  ${{number_format($val->unit_price)}}</p><span>|</span><p class="use_day">租賃日間   {{substr($val->f_day, 0, 10)}} ～ {{substr($val->e_day, 0, 10)}}（{{$val->use_day}}天）</p></div>
                                            @if($val->insurance_price > 0)
                                            <div class="bottom_item clearfix">
                                                <p class="add_purchase">已加購 安心險</p>
                                            </div>
                                            @endif
                                        </div>
                                        <div class="right_amount">
                                            <p>x {{$val->num}} </p> 
                                            <p>$ {{$val->amt}}</p>
                                        </div>
                                    </div>
                                    @endforeach
                                @endif

                                @if(count($orderDetailData[$key]) >  0)
                                    @foreach($orderDetailData[$key] as $val)
                                    <div class="abox label_buy clearfix">
                                        <div class="product_photo">
                                            <img src="{{url('assets/images/img_default_2.jpg')}}">
                                        </div>
                                        <div class="product_info">
                                            <h3 class="product_title"><span>買</span><a href="{{url('productDetail/'.$val->prod_id)}}" target="_blank">{{$val->prod_nm}}</a></h3>
                                            <div class="product_item"><p class="product_price">單價  ${{number_format($val->unit_price)}}</p></div>
                                        </div>
                                        <div class="right_amount">
                                            <p>x {{number_format($val->num)}} </p> 
                                            <p>$ {{number_format($val->amt)}}</p>
                                            @if($val->prod_type == 'S')
                                            <p class="serial_number"><a class="reSendSn" data-target="#snModal" order-detail-id="{{$val->id}}" ordNo="{{$row->ord_no}}">查看序號</a></p>
                                            @endif
                                        </div>
                                    </div>
                                    @endforeach
                                @endif
                                <div class="abox">
                                    <p class="freight"><span>@if($row->ship_fee == 0) 滿額 $ 888 免運 @endif</span> 運費 ${{number_format($row->ship_fee)}}</p>
                                    @if(isset($row->cust_remark))
                                    <p class="note">備註：{!! $row->cust_remark !!}</p><!-- 有備註才出現 -->
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                @endforeach
            </ul>
            <div class="light-theme pageBox">
            {{ $orderData->links() }}
            </div>
        </div>

        {{-- 
        <div child="tab-content" id="tab2" style="display:none">
            <ul class="list contents_box" tab="#tab2">
                @foreach($processOrderData as $key=>$row)
                <li class="clearfix">
                    <div class="onder_main_info clearfix">
                        <div class="product_photo">
                            <img src="{{url('assets/images/img_default.jpg')}}">
                        </div>
                        <div class="product_info clearfix">
                            <div class="order_number">
                                <p class="order_date">{{$row->created_at}}</p>
                                <p class="number">
                                    <span class="the_type 
                                        @if($row->status == 'A')
                                            nonepay_red
                                        @elseif($row->status == 'B')
                                            done_green
                                        @elseif($row->status == 'C' || $row->status == 'I')
                                            done_green
                                        @elseif($row->status == 'D')
                                            done_green
                                        @endif
                                    ">
                                        @if($row->status == 'A')
                                            未付款
                                        @elseif($row->status == 'B')
                                            已付款
                                        @elseif($row->status == 'C' || $row->status == 'I')
                                            運送中
                                        @elseif($row->status == 'D')
                                            已送達
                                        @elseif($row->status == 'E')
                                            已歸還
                                        @elseif($row->status == 'F')
                                            通知取消
                                        @elseif($row->status == 'G')
                                            已取消
                                        @elseif($row->status == 'H')
                                            已刷退
                                        @endif
                                    </span>
                                    訂單編號  {{$row->ord_no}}
                                </p>
                            </div>
                            <div class="order-total">
                                @if(isset($row->CVSPaymentNo))
                                <p class="order_date">查詢單號 <span><a href="https://eservice.7-11.com.tw/e-tracking/search.aspx" target="_blank">{{$row->CVSPaymentNo.$row->CVSValidationNo}}</a></span></p>
                                @endif
                                <p class="order_money">訂單金額<span class="red">$ {{number_format($row->d_price)}}</span></p>
                            </div>
                        </div>
                        <div class="product_subtotal">
                            <div class="exbox clearfix">
                                <p class="btn"><a class="show_bottom_details" href="javascript:;">查看明細</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="show_details">
                        <div class="single_order clearfix">
                            <div class="sub_title"><p>訂單狀況</p></div>
                            <div class="order_details">
                                <div class="abox clearfix">
                                    <ul class="order_step clearfix">
                                        <li  @if($row->status == 'A') class="btn_in" @endif><p>訂單建立<!--<span>2019-01-12 18:18</span>--></p></li>
                                        @if(!in_array($row->status, ['F', 'G']))
                                        <li @if($row->status == 'B') class="btn_in" @endif><p>完成付款<!--<span>2019-01-12 18:18</span>--></p></li>
                                        <li @if(in_array($row->status, ['C', 'I'])) class="btn_in" @endif><p>運送中<!--<span>2019-01-12 18:18</span>--></p></li>
                                        <li @if(in_array($row->status, ['D', 'E'])) class="btn_in" @endif><p>已送達<!--<span>2019-01-12 18:18</span>--></p></li>
                                        @else
                                        <li class="btn_in"><p>已取消<!--<span>2019-01-12 18:18</span>--></p></li>
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="single_order clearfix">
                            <div class="sub_title"><p>訂單明細</p></div>
                            <div class="order_details">
                                @if(count($orderProcessRentDetail[$key]) >  0)
                                    @foreach($orderProcessRentDetail[$key] as $val)
                                    <div class="abox clearfix">
                                        <div class="product_photo">
                                            <img src="{{url('assets/images/img_default_2.jpg')}}">
                                        </div>
                                        <div class="product_info">
                                            <h3 class="product_title"><span class="red">租</span><a href="#" target="_blank"> {{$val->prod_nm}}</a></h3>
                                            <div class="product_item"><p class="product_price">單價  ${{number_format($val->unit_price)}}</p><span>|</span><p class="use_day">租賃日間   {{substr($val->f_day, 0, 10)}} ～ {{substr($val->e_day, 0, 10)}}（{{$val->use_day}}天）</p></div>
                                            @if($val->insurance_price > 0)
                                            <div class="bottom_item clearfix">
                                                <p class="add_purchase">已加購 安心險</p>
                                            </div>
                                            @endif
                                        </div>
                                        <div class="right_amount">
                                            <p>x {{$val->num}} </p> 
                                            <p>$ {{$val->amt}}</p>
                                        </div>
                                    </div>
                                    @endforeach
                                @endif

                                @if(count($orderProcessDetail[$key]) >  0)
                                    @foreach($orderProcessDetail[$key] as $val)
                                    <div class="abox label_buy clearfix">
                                        <div class="product_photo">
                                            <img src="{{url('assets/images/img_default_2.jpg')}}">
                                        </div>
                                        <div class="product_info">
                                            <h3 class="product_title"><span>買</span><a href="{{url('productDetail/'.$val->prod_id)}}" target="_blank">{{$val->prod_nm}}</a></h3>
                                            <div class="product_item"><p class="product_price">單價  ${{number_format($val->unit_price)}}</p></div>
                                        </div>
                                        <div class="right_amount">
                                            <p>x {{number_format($val->num)}} </p> 
                                            <p>$ {{number_format($val->amt)}}</p>
                                            @if($val->prod_type == 'S')
                                            <p class="serial_number"><a class="reSendSn" data-target="#snModal" order-detail-id="{{$val->id}}" ordNo="{{$row->ord_no}}">查看序號</a></p>
                                            @endif
                                        </div>
                                    </div>
                                    @endforeach
                                @endif
                                <div class="abox">
                                    <p class="freight"><span>滿額 $ 888 免運</span> 運費 ${{number_format($row->ship_fee)}}</p>
                                    @if(isset($row->cust_remark))
                                    <p class="note">備註：{!! $row->cust_remark !!}</p><!-- 有備註才出現 -->
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                @endforeach
            </ul>
            <div class="light-theme pageBox">
                {{ $processOrderData->links() }}
            </div>
        </div>

        <div child="tab-content" id="tab3" style="display:none">
            <ul class="list contents_box" tab="#tab2">
                @foreach($orderHistoryData as $key=>$row)
                <li class="clearfix">
                    <div class="onder_main_info clearfix">
                        <div class="product_photo">
                            <img src="{{url('assets/images/img_default.jpg')}}">
                        </div>
                        <div class="product_info clearfix">
                            <div class="order_number">
                                <p class="order_date">{{$row->created_at}}</p>
                                <p class="number">
                                    <span class="the_type 
                                        @if($row->status == 'A')
                                            nonepay_red
                                        @elseif($row->status == 'B')
                                            done_green
                                        @elseif($row->status == 'C' || $row->status == 'I')
                                            done_green
                                        @elseif($row->status == 'D')
                                            done_green
                                        @endif
                                    ">
                                        @if($row->status == 'A')
                                            未付款
                                        @elseif($row->status == 'B')
                                            已付款
                                        @elseif($row->status == 'C' || $row->status == 'I')
                                            運送中
                                        @elseif($row->status == 'D')
                                            已送達
                                        @elseif($row->status == 'E')
                                            已歸還
                                        @elseif($row->status == 'F')
                                            通知取消
                                        @elseif($row->status == 'G')
                                            已取消
                                        @elseif($row->status == 'H')
                                            已刷退
                                        @endif
                                    </span>
                                    訂單編號  {{$row->ord_no}}
                                </p>
                            </div>
                            <div class="order-total">
                                @if(isset($row->CVSPaymentNo))
                                <p class="order_date">查詢單號 <span><a href="https://eservice.7-11.com.tw/e-tracking/search.aspx" target="_blank">{{$row->CVSPaymentNo.$row->CVSValidationNo}}</a></span></p>
                                @endif
                                <p class="order_money">訂單金額<span class="red">$ {{number_format($row->d_price)}}</span></p>
                            </div>
                        </div>
                        <div class="product_subtotal">
                            <div class="exbox clearfix">
                                <p class="btn"><a class="show_bottom_details" href="javascript:;">查看明細</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="show_details">
                        <div class="single_order clearfix">
                            <div class="sub_title"><p>訂單狀況</p></div>
                            <div class="order_details">
                                <div class="abox clearfix">
                                    <ul class="order_step clearfix">
                                        <li  @if($row->status == 'A') class="btn_in" @endif><p>訂單建立<!--<span>2019-01-12 18:18</span>--></p></li>
                                        @if(!in_array($row->status, ['F', 'G']))
                                        <li @if($row->status == 'B') class="btn_in" @endif><p>完成付款<!--<span>2019-01-12 18:18</span>--></p></li>
                                        <li @if(in_array($row->status, ['C', 'I'])) class="btn_in" @endif><p>運送中<!--<span>2019-01-12 18:18</span>--></p></li>
                                        <li @if(in_array($row->status, ['D', 'E'])) class="btn_in" @endif><p>已送達<!--<span>2019-01-12 18:18</span>--></p></li>
                                        @else
                                        <li class="btn_in"><p>已取消<!--<span>2019-01-12 18:18</span>--></p></li>
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="single_order clearfix">
                            <div class="sub_title"><p>訂單明細</p></div>
                            <div class="order_details">
                                @if(count($orderHistoryRentData[$key]) >  0)
                                    @foreach($orderHistoryRentData[$key] as $val)
                                    <div class="abox clearfix">
                                        <div class="product_photo">
                                            <img src="{{url('assets/images/img_default_2.jpg')}}">
                                        </div>
                                        <div class="product_info">
                                            <h3 class="product_title"><span class="red">租</span><a href="#" target="_blank"> {{$val->prod_nm}}</a></h3>
                                            <div class="product_item"><p class="product_price">單價  ${{number_format($val->unit_price)}}</p><span>|</span><p class="use_day">租賃日間   {{substr($val->f_day, 0, 10)}} ～ {{substr($val->e_day, 0, 10)}}（{{$val->use_day}}天）</p></div>
                                            @if($val->insurance_price > 0)
                                            <div class="bottom_item clearfix">
                                                <p class="add_purchase">已加購 安心險</p>
                                            </div>
                                            @endif
                                        </div>
                                        <div class="right_amount">
                                            <p>x {{$val->num}} </p> 
                                            <p>$ {{$val->amt}}</p>
                                        </div>
                                    </div>
                                    @endforeach
                                @endif

                                @if(count($orderHistoryDetailData[$key]) >  0)
                                    @foreach($orderHistoryDetailData[$key] as $val)
                                    <div class="abox label_buy clearfix">
                                        <div class="product_photo">
                                            <img src="{{url('assets/images/img_default_2.jpg')}}">
                                        </div>
                                        <div class="product_info">
                                            <h3 class="product_title"><span>買</span><a href="{{url('productDetail/'.$val->prod_id)}}" target="_blank">{{$val->prod_nm}}</a></h3>
                                            <div class="product_item"><p class="product_price">單價  ${{number_format($val->unit_price)}}</p></div>
                                        </div>
                                        <div class="right_amount">
                                            <p>x {{number_format($val->num)}} </p> 
                                            <p>$ {{number_format($val->amt)}}</p>
                                            @if($val->sn_no)
                                            <p class="serial_number"><a href="javascript:;" rel="modal:open">查看序號</a></p>
                                            @endif
                                        </div>
                                    </div>
                                    @endforeach
                                @endif
                                <div class="abox">
                                    <p class="freight"><span>@if($row->ship_fee == 0)滿額 $ 888 免運 @endif</span> 運費 ${{number_format($row->ship_fee)}}</p>
                                    @if(isset($row->cust_remark))
                                    <p class="note">備註：{!! $row->cust_remark !!}</p><!-- 有備註才出現 -->
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                @endforeach
            </ul>
            <div class="light-theme pageBox">
                <ul>
                    <li class="disabled"><span class="current prev">Prev</span></li>
                    <li class="active"><span class="current">1</span></li>
                    <li><a href="#" class="page-link">2</a></li>
                    <li><a href="#" class="page-link">3</a></li>
                    <li class=""><span class="ellipse clickable">…</span></li>
                    <li><a href="#" class="page-link">69</a></li>
                    <li><a href="#" class="page-link">70</a></li>
                    <li><a href="#" class="page-link next">Next</a></li>
                </ul>
            </div>
        </div>--}}
        

    </section>

    <!-- Modal HTML embedded directly into document -->
    <div id="ex1" class="modal fade">
      <p>訂單編號 <span class="ordNo"></span></p>
      <div class="show_order_no"><p>序號：</p>
          <input type="text" name="snNo" id="snNo" value="">
      </div>
      <!-- <a href="#" rel="modal:close">Close</a> -->
    </div>
    <div id="ex2" class="modal fade">
      <p>安心險說明</p>
      <!-- <a href="#" rel="modal:close">Close</a> -->
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="myModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">付款確認-訂單號：<span id="ordNo"></span></h4>
            </div>
            <form class="userinfo_form_box" action="{{url('rePay')}}" method="POST" id="payForm" style="padding:0;">
                {{ csrf_field() }}
                <input type="hidden" name="ttlAmt" value="">
                <input type="hidden" name="prime" />
                <input type="hidden" name="agreePayRemeber" />
                @if(isset($userData->card_token))
                <input type="hidden" name="oldQuickPay" value="YES"/>
                <input type="hidden" name="useQuickPay" value="YES"/>
                @endif
                <input type="hidden" name="ordId" id="ordId">
                <div class="modal-body">
                    <div class="group clearfix">
                        <label class="left_label">付款方式</label>
                        <div class="right_fillbox">
                            <div class="clearfix">
                                <div class="styled-select w_01">
                                    <select class="md-input form-control" id="payWay" name="payWay">
                                        <option value="Credit">信用卡(一次付清)</option>
                                        <!-- <option value="Installment">信用卡(分期付款)</option> -->
                                        <!-- <option value="GooglePay">Google Pay</option> -->
                                        <option value="ApplePay">Apple Pay</option>
                                        <!-- <option value="LinePay">Line Pay</option>
                                        <option value="SamsungPay">Samsung Pay</option> -->
                                        <option value="WEBATM">Web ATM</option>
                                        <option value="CVS">超商付款</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="group clearfix creditCardArea" @if(isset($userData->card_token)) style="display:none" @endif>
                        <label class="left_label">信用卡卡號</label>
                        <div class="right_fillbox for_w100">
                            <div id="tappay-iframe"></div>
                            <!-- <p class="p_note p_k"><input type="checkbox" name="creditRemember" value="YES"> 記錄本次付款資訊，並設定為快速結帳</p> -->
                            <div class="redbox-checkbox clearfix">
                                <input id="iknow7" type="checkbox" name="creditRemember" value="YES" checked="">
                                <label class="checkbox_style" for="iknow7"></label>
                                <span> 記錄本次付款資訊，並設定為快速結帳</span>
                            </div>
                        </div>
                    </div>

                    @if(isset($userData->card_token))
                    <div class="group clearfix" id="quickPayment">
                        <label class="left_label">信用卡卡號</label>
                        <div class="right_fillbox for_w100">
                            <input type="text" class="fbox" value="{{'**** **** **** '.$userData->last_four}}" disabled>
                            <button type="button" class="btn btn-sm btn-primary" id="btnChangeCard">換一張信用卡</button>
                        </div>
                    </div>
                    @endif

                    <div class="group clearfix">
                        <label class="left_label">發票類型</label>
                        <div class="right_fillbox">
                            <div class="styled-select w_01">
                                <select class="md-input form-control" name="invoiceType" id="invoiceType">
                                    <option value="a2" selected="selected">二聯電子發票</option>
                                    <option value="a1">捐贈發票</option>
                                    <option value="a3">三聯電子發票</option>
                                </select>
                            </div>
                            <p class="p_note p_gray">配合國稅局勸止二聯換開三聯之政策，本公司保留換開發票的權利</p>
                        </div>
                    </div>
                    <div class="group clearfix a3" style="display: none">
                        <label class="left_label">統一編號</label>
                        <div class="right_fillbox">
                            <input class="w_01 fbox" type="text" name="customerIdentifier" placeholder="限數字">
                        </div>
                    </div>
                    <div class="group clearfix a3" style="display: none">
                        <label class="left_label">發票抬頭</label>
                        <div class="right_fillbox">
                            <input class="fbox" type="text" name="customerIdentifier" placeholder="限22個全形文字">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">關閉</button>
                    <button type="submit" class="btn btn-primary">送出</button>
                </div>
            </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</main>

<div class="modal fade" tabindex="-1" role="dialog" id="snModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">訂單號：<span id="snOrdNo"></span></h4>
        </div>
        <div class="modal-body">
            <div class="form-group">
                <label for="customerIdentifier">序號</label>
                <textarea type="text" class="form-control" id="sn" rows="10" readonly></textarea>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">關閉</button>
        </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection

@section('footer')
    @include('FrontEnd.layouts.newFooter')
@endsection

@section('after_scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.all.min.js"></script>
    <script src="https://pay.google.com/gp/p/js/pay.js"></script>
    <script src="https://js.tappaysdk.com/tpdirect/v4"></script>
    @if(env('APP_ENV') == 'production')
    <script> TPDirect.setupSDK(13155, 'app_4R0lByKiPVDYQOqemZ8ERcmsIn5TJB16B8G5R7BUQPmGESOjjdLlwl00zGT8', 'production') </script>
    @else
    <script> TPDirect.setupSDK(13155, 'app_4R0lByKiPVDYQOqemZ8ERcmsIn5TJB16B8G5R7BUQPmGESOjjdLlwl00zGT8', 'sandbox') </script>
    @endif
    
    <script type="text/javascript" src="{{url('assets/js/orderList.js')}}?v={{Config::get('app.version')}}"></script>

    <script>
        const tab = location.hash;
        $('html,body').animate({
            scrollTop: $('body').offset().top
        }, 100);

        // $('a[role="tab"]').on('click', function(){
        //     const id = $(this).attr('href');

        //     $(this).parents('li').siblings().find('p').removeClass('btn_in');
        //     $(this).parent('p').addClass('btn_in');
            

        //     $('div[child="tab-content"]').hide();
        //     $(id).show();

        //     $('html,body').animate({
        //         scrollTop: $('body').offset().top
        //     }, 100);
        // });

        //$('a[role="tab"][href="'+tab+'"]').click();
    </script>
@endsection




