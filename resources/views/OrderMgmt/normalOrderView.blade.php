@extends('backpack::layout') 
@section('header')
<style>
.lmask {
position: absolute;
height: 100%;
width: 100%;
background-color: #000;
bottom: 0;
left: 0;
right: 0;
top: 0;
z-index: 9999;
opacity: 0.4;
}
.lmask.fixed {
position: fixed;
}
.lmask:before {
content: '';
background-color: rgba(0, 0, 0, 0);
border: 5px solid rgba(0, 183, 229, 0.9);
opacity: .9;
border-right: 5px solid rgba(0, 0, 0, 0);
border-left: 5px solid rgba(0, 0, 0, 0);
border-radius: 50px;
box-shadow: 0 0 35px #2187e7;
width: 50px;
height: 50px;
-moz-animation: spinPulse 1s infinite ease-in-out;
-webkit-animation: spinPulse 1s infinite linear;
margin: -25px 0 0 -25px;
position: absolute;
top: 50%;
left: 50%;
}
.lmask:after {
content: '';
background-color: rgba(0, 0, 0, 0);
border: 5px solid rgba(0, 183, 229, 0.9);
opacity: .9;
border-left: 5px solid rgba(0, 0, 0, 0);
border-right: 5px solid rgba(0, 0, 0, 0);
border-radius: 50px;
box-shadow: 0 0 15px #2187e7;
width: 30px;
height: 30px;
-moz-animation: spinoffPulse 1s infinite linear;
-webkit-animation: spinoffPulse 1s infinite linear;
margin: -15px 0 0 -15px;
position: absolute;
top: 50%;
left: 50%;
}

@-moz-keyframes spinPulse {
0% {
-moz-transform: rotate(160deg);
opacity: 0;
box-shadow: 0 0 1px #2187e7;
}
50% {
-moz-transform: rotate(145deg);
opacity: 1;
}
100% {
-moz-transform: rotate(-320deg);
opacity: 0;
}
}
@-moz-keyframes spinoffPulse {
0% {
-moz-transform: rotate(0deg);
}
100% {
-moz-transform: rotate(360deg);
}
}
@-webkit-keyframes spinPulse {
0% {
-webkit-transform: rotate(160deg);
opacity: 0;
box-shadow: 0 0 1px #2187e7;
}
50% {
-webkit-transform: rotate(145deg);
opacity: 1;
}
100% {
-webkit-transform: rotate(-320deg);
opacity: 0;
}
}
@-webkit-keyframes spinoffPulse {
0% {
-webkit-transform: rotate(0deg);
}
100% {
-webkit-transform: rotate(360deg);
}
}

</style>
<section class="content-header">
	<h1>
		一般商品訂單總覽
		<small></small>
	</h1>
	<ol class="breadcrumb">
		<li class="active">一般商品訂單</li>
	</ol>
</section>
<div class='lmask' id="cssLoading" style="display:none"></div>

<form method="POST" accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">
    {{ csrf_field() }}
    <input type="file" id="excelFile" name="import_file" style="display:none;"/>
    <button type="submit" class="btn btn-primary" id="btnImport" style="display:none;">{{ trans('excel.btnImport') }}</button>
</form>
@endsection 
@section('before_scripts')


<script>
    var gridOpt = {};
    gridOpt.pageId        = "normalOrderView";
    gridOpt.enabledStatus = true;
    gridOpt.fieldsUrl     = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/normal_order_view') }}";
    gridOpt.dataUrl       = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/normal_order_view') }}";
    gridOpt.fieldsUrl     = gridOpt.fieldsUrl + "?key=" + gridOpt.dataUrl;
    gridOpt.createUrl     = "{{ url(config('backpack.base.route_prefix', 'admin') . '/OrderMgmt/create') }}";
    gridOpt.editUrl       = "{{ url(config('backpack.base.route_prefix', 'admin') . '/OrderMgmt') }}" + "/{id}/edit#3";
    gridOpt.height        = 800;
    gridOpt.selectionmode = "checkbox";
    gridOpt.searchOpt     = true;
    gridOpt.enablebrowserselection = true;
    gridOpt.rowdoubleclick = true;
    statusGridObj = [{"key":"SEND","val":"123"}];
    var btnGroup = [
        {
            btnId: "btnOpenGridOpt",
            btnIcon: "fa fa-table",
            btnText: "{{ trans('common.gridOption') }}",
            btnFunc: function () {
                $('#gridOptModal').modal('show');
            }
        },
        
        {
            btnId:"btnDelete",
            btnIcon:"fa fa-trash-o",
            btnText:"{{ trans('common.delete') }}",
            btnFunc:function(){
                searchMultiDel("jqxGrid", BASE_URL + '/order/multi/del');
            }
        },

        // {
        //     btnId:"btnSendQrCode",
        //     btnIcon:"fa fa-qrcode",
        //     btnText:"QR Code",
        //     btnFunc:function(){
        //         var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
        //         var ids = new Array();
        //         for (var m = 0; m < rows.length; m++) {
        //             var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
        //             if(typeof row != "undefined") {
        //                 if(row.pick_way != "機場取機") {
        //                     alert("只有機場取機才能發送QR Code");
        //                     return;
        //                 }
        //                 ids.push(row.id);
        //             }
        //         }
        //         if(ids.length == 0) {
        //             swal("請至少選擇一筆資料", "", "warning");
        //             return;
        //         }

        //         $.post(BASE_URL + '/sendQrCode', {'ids': ids}, function(data){
        //             if(data.msg == "success") {
        //                 swal("操作成功", "", "success");
        //                 $("#jqxGrid").jqxGrid('updatebounddata');
        //                 $('#jqxGrid').jqxGrid('clearselection');
        //             }
        //             else{
        //                 swal("操作失敗", "", "error");
        //             }
        //         });

                
        //     }
        // },

        {
            btnId:"btnConfirmCancel",
            btnIcon:"fa fa-check-circle",
            btnText:"確認取消",
            btnFunc:function(){
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var ids = new Array();
                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    if(typeof row != "undefined") {
                        ids.push(row.id);
                    }
                }
                if(ids.length == 0) {
                    swal("請至少選擇一筆資料", "", "warning");
                    return;
                }

                $.post(BASE_URL + '/confirmOrderCancel', {'ids': ids}, function(data){
                    console.log(data);
                    if(data.message == "success") {
                        swal("操作成功", "", "success");
                        $("#jqxGrid").jqxGrid('updatebounddata');
                        $('#jqxGrid').jqxGrid('clearselection');
                    }
                    else{
                        swal("操作失敗", "", "error");
                    }
                });

                
            }
        },
    ];
    $(function(){
        $("#myForm").submit(function () {
            var postData = new FormData($(this)[0]);
            var mainCol = 14;
			$.ajax({
				url: BASE_URL + "/OrderMgmt/importExcel/",
				type: 'POST',
				data: postData,
				async: false,
				beforeSend: function () {
					
				},
				error: function () {
                    swal("excel 匯入發生錯誤", "", "error");
                    $('#myForm')[0].reset();
					return false;
				},
				success: function (data) {
					if(data.msg == "error") {
                        swal("excel 匯入發生錯誤", "", "error");
                        $('#myForm')[0].reset();
                        return;
                    }
                    
                    swal("匯入成功", "", "success");
                    $('#myForm')[0].reset();
                    $("#jqxGrid").jqxGrid('updatebounddata');
                    $("#jqxGrid").jqxGrid('clearselection');
				},
				cache: false,
				contentType: false,
				processData: false
			});
			return false;
        });
        
        $("#excelFile").on("change", function(){
            $("#myForm").submit();
        });
    })
</script>
@endsection 
@include('backpack::template.search')