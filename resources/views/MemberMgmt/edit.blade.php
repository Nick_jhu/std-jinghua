@extends('backpack::layout') 
@section('header')
<section class="content-header">
	<h1>
	會員編輯<small></small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ url(config('backpack.base.route_prefix'),'OrderMgmt') }}">會員總覽</a></li>
		<li class="active">會員編輯</li>
	</ol>
</section>
@endsection 

@section('content')
	@include('backpack::template.toolbar')
	<div class="row">
		<div class="col-md-12">
			<div class="callout callout-danger" id="errorMsg" style="display:none">
				<h4>{{ trans('backpack::crud.please_fix') }}</h4>
				<ul>

				</ul>
			</div>
			<form method="POST" accept-charset="UTF-8" id="mainForm" enctype="multipart/form-data">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">基本資料</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="tab_1">
							<div class="box-body">
								<div class="row">
									<div class="form-group col-md-3">
										<label for="phone">電話</label>
										<input type="text" class="form-control" id="phone" name="phone">
									</div>
                                    <div class="form-group col-md-3">
										<label for="name">名稱</label>
										<input type="text" class="form-control" id="name" name="name">
									</div>
									<div class="form-group col-md-3">
										<label for="email">E-mail</label>
										<input type="text" class="form-control" id="email" name="email">
									</div>
								</div>

                                <div class="row">
									<div class="form-group col-md-3">
										<label for="dlv_zip">郵地區號</label>
										<input type="text" class="form-control" id="dlv_zip" name="dlv_zip">
									</div>
									<div class="form-group col-md-3">
										<label for="dlv_city">城市</label>
										<input type="text" class="form-control" id="dlv_city" name="dlv_city">
									</div>
                                    <div class="form-group col-md-3">
										<label for="dlv_area">區域</label>
										<input type="text" class="form-control" id="dlv_area" name="dlv_area">
									</div>
                                    <div class="form-group col-md-3">
										<label for="dlv_addr">住址</label>
										<input type="text" class="form-control" id="dlv_addr" name="dlv_addr">
									</div>
								</div>

                                <div class="row">
									<div class="form-group col-md-3">
										<label for="pick_way">取貨方式</label>
										<select class="form-control" id="pick_way" name="pick_way"></select>
									</div>
									<div class="form-group col-md-3">
										<label for="return_way">歸還方式</label>
										<select class="form-control" id="return_way" name="return_way"></select>
									</div>
								</div>

								<div class="row">
									<div class="form-group col-md-3">
										<label for="password">密碼</label>
										<input type="password" class="form-control" id="password" name="password">
									</div>
								</div>

								@if(isset($id))
								<input type="hidden" name="id" value="{{$id}}" class="form-control">
								<input type="hidden" name="_method" value="PUT" class="form-control"> 
								@endif

							</div>
						</div>
					</div>

				</div>
			</form>
		</div>	
	</div>
@endsection 
@include('backpack::template.lookup') 
@section('after_scripts')
	<script>
		var mainId = "";
		var editData = null;
		var editObj = null;
		var SAVE_URL = "{{ url(config('backpack.base.route_prefix', 'admin') . '/MemberMgmt') }}";

		var fieldData = null;
		var fieldObj = null;

		@if(isset($crud -> create_fields))
		fieldData = '{{!! json_encode($crud->create_fields) !!}}';
		fieldData = fieldData.substring(1);
		fieldData = fieldData.substring(0, fieldData.length - 1);
		fieldObj = JSON.parse(fieldData);
		@endif

		@if(isset($id))
		mainId   = "{{$id}}";
		editData = '{{!! $entry !!}}';
		editData = editData.substring(1);
		editData = editData.substring(0, editData.length - 1);
		var objJson = editData.replace(/(?:\r\n|\r|\n)/g, '<br />');
		editObj  = JSON.parse(objJson);
		@endif

		$(function () {
			//var formOpt = {};
			formOpt.formId = "mainForm";
			formOpt.editObj = editObj;
			formOpt.fieldObj = fieldObj;
			formOpt.editUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/MemberMgmt') }}";
			formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', 'admin') .'/get/mod_member') }}/" + mainId;
			formOpt.saveUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/MemberMgmt') }}";
			formOpt.afterInit = function() {
				if(editObj != null) {

				}
			}

			formOpt.initFieldCustomFunc = function () {

			};

			formOpt.afterDel = function() {

			}

			formOpt.addFunc = function() {

			}

			formOpt.editFunc = function() {

			}

			formOpt.copyFunc = function() {

			}

			formOpt.saveSuccessFunc = function(data) {

			}

			var btnGroup = [

			];

			initBtn(btnGroup);
		});
	</script>
@endsection