<?php return array (
  'alfa6661/laravel-autonumber' => 
  array (
    'providers' => 
    array (
      0 => 'Alfa6661\\AutoNumber\\AutoNumberServiceProvider',
    ),
  ),
  'artisaninweb/laravel-soap' => 
  array (
    'providers' => 
    array (
      0 => 'Artisaninweb\\SoapWrapper\\ServiceProvider',
    ),
    'aliases' => 
    array (
      'SoapWrapper' => 'Artisaninweb\\SoapWrapper\\Facade',
    ),
  ),
  'backpack/backupmanager' => 
  array (
    'providers' => 
    array (
      0 => 'Backpack\\BackupManager\\BackupManagerServiceProvider',
    ),
  ),
  'backpack/base' => 
  array (
    'providers' => 
    array (
      0 => 'Backpack\\Base\\BaseServiceProvider',
    ),
  ),
  'backpack/crud' => 
  array (
    'providers' => 
    array (
      0 => 'Backpack\\CRUD\\CrudServiceProvider',
    ),
  ),
  'backpack/generators' => 
  array (
    'providers' => 
    array (
      0 => 'Backpack\\Generators\\GeneratorsServiceProvider',
    ),
  ),
  'backpack/langfilemanager' => 
  array (
    'providers' => 
    array (
      0 => 'Backpack\\LangFileManager\\LangFileManagerServiceProvider',
    ),
  ),
  'backpack/logmanager' => 
  array (
    'providers' => 
    array (
      0 => 'Backpack\\LogManager\\LogManagerServiceProvider',
    ),
  ),
  'backpack/menucrud' => 
  array (
    'providers' => 
    array (
      0 => 'Backpack\\MenuCRUD\\MenuCRUDServiceProvider',
    ),
  ),
  'backpack/pagemanager' => 
  array (
    'providers' => 
    array (
      0 => 'Backpack\\PageManager\\PageManagerServiceProvider',
    ),
  ),
  'backpack/permissionmanager' => 
  array (
    'providers' => 
    array (
      0 => 'Backpack\\PermissionManager\\PermissionManagerServiceProvider',
    ),
  ),
  'backpack/settings' => 
  array (
    'providers' => 
    array (
      0 => 'Backpack\\Settings\\SettingsServiceProvider',
    ),
  ),
  'barryvdh/laravel-cors' => 
  array (
    'providers' => 
    array (
      0 => 'Barryvdh\\Cors\\ServiceProvider',
    ),
  ),
  'barryvdh/laravel-elfinder' => 
  array (
    'providers' => 
    array (
      0 => 'Barryvdh\\Elfinder\\ElfinderServiceProvider',
    ),
  ),
  'cviebrock/eloquent-sluggable' => 
  array (
    'providers' => 
    array (
      0 => 'Cviebrock\\EloquentSluggable\\ServiceProvider',
    ),
  ),
  'intervention/image' => 
  array (
    'providers' => 
    array (
      0 => 'Intervention\\Image\\ImageServiceProvider',
    ),
    'aliases' => 
    array (
      'Image' => 'Intervention\\Image\\Facades\\Image',
    ),
  ),
  'jenssegers/date' => 
  array (
    'providers' => 
    array (
      0 => 'Jenssegers\\Date\\DateServiceProvider',
    ),
    'aliases' => 
    array (
      'Date' => 'Jenssegers\\Date\\Date',
    ),
  ),
  'laracasts/generators' => 
  array (
    'providers' => 
    array (
      0 => 'Laracasts\\Generators\\GeneratorsServiceProvider',
    ),
  ),
  'laravel/passport' => 
  array (
    'providers' => 
    array (
      0 => 'Laravel\\Passport\\PassportServiceProvider',
    ),
  ),
  'laravel/socialite' => 
  array (
    'providers' => 
    array (
      0 => 'Laravel\\Socialite\\SocialiteServiceProvider',
    ),
    'aliases' => 
    array (
      'Socialite' => 'Laravel\\Socialite\\Facades\\Socialite',
    ),
  ),
  'laravel/tinker' => 
  array (
    'providers' => 
    array (
      0 => 'Laravel\\Tinker\\TinkerServiceProvider',
    ),
  ),
  'laravelcollective/html' => 
  array (
    'providers' => 
    array (
      0 => 'Collective\\Html\\HtmlServiceProvider',
    ),
    'aliases' => 
    array (
      'Form' => 'Collective\\Html\\FormFacade',
      'Html' => 'Collective\\Html\\HtmlFacade',
    ),
  ),
  'leochien/laravel-spgateway' => 
  array (
    'providers' => 
    array (
      0 => 'LeoChien\\Spgateway\\SpgatewayServiceProvider',
    ),
  ),
  'maatwebsite/excel' => 
  array (
    'providers' => 
    array (
      0 => 'Maatwebsite\\Excel\\ExcelServiceProvider',
    ),
    'aliases' => 
    array (
      'Excel' => 'Maatwebsite\\Excel\\Facades\\Excel',
    ),
  ),
  'mbarwick83/shorty' => 
  array (
    'providers' => 
    array (
      0 => 'Mbarwick83\\Shorty\\ShortyServiceProvider',
    ),
    'aliases' => 
    array (
      'Shorty' => 'Mbarwick83\\Shorty\\Facades\\Shorty',
    ),
  ),
  'minchao/mitake-laravel' => 
  array (
    'providers' => 
    array (
      0 => 'Mitake\\Laravel\\MitakeServiceProvider',
    ),
    'aliases' => 
    array (
      'Mitake' => 'Mitake\\Laravel\\Facade\\Mitake',
    ),
  ),
  'nesbot/carbon' => 
  array (
    'providers' => 
    array (
      0 => 'Carbon\\Laravel\\ServiceProvider',
    ),
  ),
  'prologue/alerts' => 
  array (
    'providers' => 
    array (
      0 => 'Prologue\\Alerts\\AlertsServiceProvider',
    ),
    'aliases' => 
    array (
      'Alert' => 'Prologue\\Alerts\\Facades\\Alert',
    ),
  ),
  'spatie/laravel-backup' => 
  array (
    'providers' => 
    array (
      0 => 'Spatie\\Backup\\BackupServiceProvider',
    ),
  ),
  'spatie/laravel-permission' => 
  array (
    'providers' => 
    array (
      0 => 'Spatie\\Permission\\PermissionServiceProvider',
    ),
  ),
);