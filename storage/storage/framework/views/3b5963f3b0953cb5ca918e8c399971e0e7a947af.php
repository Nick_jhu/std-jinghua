 <?php $__env->startSection('header'); ?>
<section class="content-header">
  <h1>
  <?php echo e(trans('sysSite.titleName')); ?><small></small>
  </h1>
  <ol class="breadcrumb">
  	<li class="active"><?php echo e(trans('sysSite.titleName')); ?></li>
  </ol>
</section>
<?php $__env->stopSection(); ?> 

<?php $__env->startSection('content'); ?>
<style>
	#addGroupForm {display: none}
	#addCmpForm {display: none}
	#addStnForm {display: none}
	#addDepForm {display: none}

	#updateGroupForm {display: none}
	#updateCmpForm {display: none}
	#updateStnForm {display: none}
	#updateDepForm {display: none}
</style>
<div class="row">
  <div class="col-md-3">
    <div class="box box-info">
      <div class="box-header with-border">
        <i class="fa fa-building-o"></i>

        <h3 class="box-title"><?php echo e(trans('sysSite.group')); ?></h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body" id="groupBody">
	  	
      </div>
      <!-- /.box-body -->
	  <div class="box-footer">
	  	<form id="addGroupForm" method="POST"  accept-charset="UTF-8" enctype="multipart/form-data">
			<input type="text" class="form-control" id="g_key" name="g_key" placeholder="Group Code" >
			<input type="text" class="form-control" id="cname" name="cname" placeholder="Group Chinese Name" >
			<input type="text" class="form-control" id="ename" name="ename" placeholder="Group English Name" >
			<input type="hidden" class="form-control" id="type" name="type" value="0" >
			<button type="submit" class="btn btn-sm btn-success"><?php echo e(trans('sysSite.add')); ?></button>
			<button type="button" class="btn btn-sm btn-danger" id="cancelGroupBtn"><?php echo e(trans('sysSite.cancel')); ?></button>
		</form>
		<form id="updateGroupForm" method="POST"  accept-charset="UTF-8" enctype="multipart/form-data">
			<input type="hidden" class="form-control" name="id">
			<input type="text" class="form-control" name="cname" placeholder="Group Chinese Name" >
			<input type="text" class="form-control" name="ename" placeholder="Group English Name" >
			<button type="submit" class="btn btn-sm btn-success"><?php echo e(trans('sysSite.update')); ?></button>
			<button type="button" class="btn btn-sm btn-danger" id="cancelUpdateGroupBtn"><?php echo e(trans('sysSite.cancel')); ?></button>
		</form>
	  	<button class="btn btn-sm btn-default" id="addGroupBtn"><?php echo e(trans('sysSite.addGroup')); ?></button>
	  	<button class="btn btn-sm btn-default" id="updateGroupBtn"><?php echo e(trans('sysSite.updateGroup')); ?></button>
	  </div>
    </div>
    <!-- /.box -->
  </div>
  <div class="col-md-3">
    <div class="box box-info">
      <div class="box-header with-border">
        <i class="fa fa-building-o"></i>

        <h3 class="box-title"><?php echo e(trans('sysSite.cmp')); ?></h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body" id="cmpBody">
        
      </div>
      <!-- /.box-body -->
	  <div class="box-footer">
	  	<form id="addCmpForm" method="POST"  accept-charset="UTF-8" enctype="multipart/form-data">
		  <input type="text" class="form-control" id="c_key" name="c_key" placeholder="Company Code" >
		  <input type="text" class="form-control" id="cname" name="cname" placeholder="Company Chinese Name" >
		  <input type="text" class="form-control" id="ename" name="ename" placeholder="Company English Name" >
		  <input type="hidden" class="form-control" id="type" name="g_key">
		  <input type="hidden" class="form-control" id="type" name="type" value="1" >
		  <button type="submit" class="btn btn-sm btn-success"><?php echo e(trans('sysSite.add')); ?></button>
		  <button type="button" class="btn btn-sm btn-danger" id="cancelCmpBtn"><?php echo e(trans('sysSite.cancel')); ?></button>
		</form>
		<form id="updateCmpForm" method="POST"  accept-charset="UTF-8" enctype="multipart/form-data">
			<input type="hidden" class="form-control" name="id">
			<input type="text" class="form-control" name="cname" placeholder="Company Chinese Name" >
			<input type="text" class="form-control" name="ename" placeholder="Company English Name" >
			<button type="submit" class="btn btn-sm btn-success"><?php echo e(trans('sysSite.update')); ?></button>
			<button type="button" class="btn btn-sm btn-danger" id="cancelUpdateCmpBtn"><?php echo e(trans('sysSite.cancel')); ?></button>
		</form>
		<button class="btn btn-sm btn-default" id="addCmpBtn"><?php echo e(trans('sysSite.addCmp')); ?></button>
		<button class="btn btn-sm btn-default" id="updateCmpBtn"><?php echo e(trans('sysSite.updateCmp')); ?></button>
	  </div>
    </div>
    <!-- /.box -->
  </div>
  <div class="col-md-3">
    <div class="box box-info">
      <div class="box-header with-border">
        <i class="fa fa-building-o"></i>

        <h3 class="box-title"><?php echo e(trans('sysSite.stn')); ?></h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body" id="stnBody">

      </div>
      <!-- /.box-body -->
	  <div class="box-footer">
	  	<form id="addStnForm" method="POST"  accept-charset="UTF-8" enctype="multipart/form-data">
		  <input type="text" class="form-control" id="s_key" name="s_key" placeholder="Station Code" >
		  <input type="text" class="form-control" id="cname" name="cname" placeholder="Station Chinese Name" >
		  <input type="text" class="form-control" id="ename" name="ename" placeholder="Station English Name" >
		  <input type="hidden" class="form-control" id="type" name="type" value="2" >
		  <input type="hidden" class="form-control" id="g_key" name="g_key">
		  <input type="hidden" class="form-control" id="c_key" name="c_key">
		  <button type="submit" class="btn btn-sm btn-success"><?php echo e(trans('sysSite.add')); ?></button>
		  <button type="button" class="btn btn-sm btn-danger" id="cancelStnBtn"><?php echo e(trans('sysSite.cancel')); ?></button>
		</form>
		<form id="updateStnForm" method="POST"  accept-charset="UTF-8" enctype="multipart/form-data">
			<input type="hidden" class="form-control" name="id">
			<input type="text" class="form-control" name="cname" placeholder="Station Chinese Name" >
			<input type="text" class="form-control" name="ename" placeholder="Station English Name" >
			<button type="submit" class="btn btn-sm btn-success"><?php echo e(trans('sysSite.update')); ?></button>
			<button type="button" class="btn btn-sm btn-danger" id="cancelUpdateStnBtn"><?php echo e(trans('sysSite.cancel')); ?></button>
		</form>
	  	<button class="btn btn-sm btn-default" id="addStnBtn"><?php echo e(trans('sysSite.addStn')); ?></button>
		  <button class="btn btn-sm btn-default" id="updateStnBtn"><?php echo e(trans('sysSite.updateStn')); ?></button>
	  </div>
    </div>
    <!-- /.box -->
  </div>
  <div class="col-md-3">
    <div class="box box-info">
      <div class="box-header with-border">
        <i class="fa fa-building-o"></i>

        <h3 class="box-title"><?php echo e(trans('sysSite.dep')); ?></h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body" id="depBody">

      </div>
      <!-- /.box-body -->
	  <div class="box-footer">
	  	<form id="addDepForm" method="POST"  accept-charset="UTF-8" enctype="multipart/form-data">
		  <input type="text" class="form-control" name="d_key" placeholder="Department Code" >
		  <input type="text" class="form-control" id="cname" name="cname" placeholder="Department Chinese Name" >
		  <input type="text" class="form-control" id="ename" name="ename" placeholder="Department English Name" >
		  <input type="hidden" class="form-control" id="type" name="type" value="3" >
		  <input type="hidden" class="form-control" id="g_key" name="g_key">
		  <input type="hidden" class="form-control" id="c_key" name="c_key">
		  <input type="hidden" class="form-control" id="s_key" name="s_key">
		  <button type="submit" class="btn btn-sm btn-success"><?php echo e(trans('sysSite.add')); ?></button>
		  <button type="button" class="btn btn-sm btn-danger" id="cancelDepBtn"><?php echo e(trans('sysSite.cancel')); ?></button>
		</form>
		<form id="updateDepForm" method="POST"  accept-charset="UTF-8" enctype="multipart/form-data">
			<input type="hidden" class="form-control" name="id">
			<input type="text" class="form-control" name="cname" placeholder="Department Chinese Name" >
			<input type="text" class="form-control" name="ename" placeholder="Department English Name" >
			<button type="submit" class="btn btn-sm btn-success"><?php echo e(trans('sysSite.update')); ?></button>
			<button type="button" class="btn btn-sm btn-danger" id="cancelUpdateDepBtn"><?php echo e(trans('sysSite.cancel')); ?></button>
		</form>
	  	<button class="btn btn-sm btn-default" id="addDepBtn"><?php echo e(trans('sysSite.addDep')); ?></button>
		<button class="btn btn-sm btn-default" id="updateDepBtn"><?php echo e(trans('sysSite.updateDep')); ?></button>
	  </div>
    </div>
    <!-- /.box -->
  </div>

</div>



<?php $__env->stopSection(); ?> 
<?php echo $__env->make('backpack::template.lookup', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 

<?php $__env->startSection('after_scripts'); ?> 

<script>
	$(function(){
		$('#addGroupBtn').on('click', function(){
			$('#addGroupForm').toggle();
			$('#addGroupBtn').toggle();
			$('#updateGroupBtn').toggle();
		});

		$('#cancelGroupBtn').on('click', function(){
			$('#addGroupForm').toggle();
			$('#addGroupBtn').toggle();
			$('#updateGroupBtn').toggle();
		});

		$('#updateGroupBtn').on('click', function(){
			var id = $('#updateGroupForm input[name="id"]').val();
			if(id) {
				$('#updateGroupForm').toggle();
				$('#updateGroupBtn').toggle();
				$('#addGroupBtn').toggle();
			}
			else {
				alert('Please select group first');
				return;
			}
		});

		$('#cancelUpdateGroupBtn').on('click', function(){
			$('#updateGroupForm').toggle();
			$('#updateGroupBtn').toggle();
			$('#addGroupBtn').toggle();
		});

		$('#addCmpBtn').on('click', function(){
			var g_key = $('#addCmpForm input[name="g_key"]').val();
			if(g_key) {
				$('#addCmpForm').toggle();
				$('#addCmpBtn').toggle();
				$('#updateCmpBtn').toggle();
			}
			else {
				alert('Please select group first');
				return;
			}
			
		});

		$('#cancelCmpBtn').on('click', function(){
			$('#addCmpForm').toggle();
			$('#addCmpBtn').toggle();
			$('#updateCmpBtn').toggle();
		});

		$('#updateCmpBtn').on('click', function(){
			var id = $('#updateCmpForm input[name="id"]').val();
			if(id) {
				$('#updateCmpForm').toggle();
				$('#updateCmpBtn').toggle();
				$('#addCmpBtn').toggle();
			}
			else {
				alert('Please select company first');
				return;
			}
		});

		$('#cancelUpdateCmpBtn').on('click', function(){
			$('#updateCmpForm').toggle();
			$('#updateCmpBtn').toggle();
			$('#addCmpBtn').toggle();
		});

		$('#addStnBtn').on('click', function(){
			var g_key = $('#addStnForm input[name="g_key"]').val();
			if(g_key) {
				$('#addStnForm').toggle();
				$('#addStnBtn').toggle();
				$('#updateStnBtn').toggle();
			}
			else {
				alert('Please select Station first');
				return;
			}
			
		});

		$('#cancelStnBtn').on('click', function(){
			$('#addStnForm').toggle();
			$('#addStnBtn').toggle();
			$('#updateStnBtn').toggle();
		});

		$('#updateStnBtn').on('click', function(){
			var id = $('#updateStnForm input[name="id"]').val();
			if(id) {
				$('#updateStnForm').toggle();
				$('#updateStnBtn').toggle();
				$('#addStnBtn').toggle();
			}
			else {
				alert('Please select company first');
				return;
			}
			
		});

		$('#cancelUpdateStnBtn').on('click', function(){
			$('#updateStnForm').toggle();
			$('#updateStnBtn').toggle();
			$('#addStnBtn').toggle();
		});

		$('#addDepBtn').on('click', function(){
			var g_key = $('#addDepForm input[name="g_key"]').val();
			if(g_key) {
				$('#addDepForm').toggle();
				$('#addDepBtn').toggle();
				$('#updateDepBtn').toggle();
			}
			else {
				alert('Please select Station first');
				return;
			}
			
		});

		$('#cancelDepBtn').on('click', function(){
			$('#addDepForm').toggle();
			$('#addDepBtn').toggle();
			$('#updateDepBtn').toggle();
		});

		$('#updateDepBtn').on('click', function(){
			var id = $('#updateDepForm input[name="id"]').val();
			if(id) {
				$('#updateDepForm').toggle();
				$('#updateDepBtn').toggle();
				$('#addDepBtn').toggle();
			}
			else {
				alert('Please select company first');
				return;
			}
			
		});

		$('#cancelUpdateDepBtn').on('click', function(){
			$('#updateDepForm').toggle();
			$('#updateDepBtn').toggle();
			$('#addDepBtn').toggle();
		});

		$('#addGroupForm').on('submit', function(){
			store('groupBody', new FormData($(this)[0]));
			return false;
		});

		$('#updateGroupForm').on('submit', function(){
			update('groupBody', new FormData($(this)[0]));
			return false;
		});

		$('#addCmpForm').on('submit', function(){
			store('cmpBody', new FormData($(this)[0]));
			return false;
		});

		$('#updateCmpForm').on('submit', function(){
			update('cmpBody', new FormData($(this)[0]));
			return false;
		});

		$('#addStnForm').on('submit', function(){
			store('stnBody', new FormData($(this)[0]));
			return false;
		});

		$('#updateStnForm').on('submit', function(){
			update('stnBody', new FormData($(this)[0]));
			return false;
		});

		$('#addDepForm').on('submit', function(){
			store('depBody', new FormData($(this)[0]));
			return false;
		});

		$('#updateDepForm').on('submit', function(){
			update('depBody', new FormData($(this)[0]));
			return false;
		});

        $.post( "<?php echo e(url(config('backpack.base.route_prefix', 'admin').'/SysSite/get')); ?>", {'type': -1}, function( data ) {
			appendItem('groupBody', data.data);
			
			$('.close').on('click', function(event){
				event.stopPropagation();
				var thisItem = $(this).parents('.item-box');
				var type  = $(this).parents('.item-box').attr('type');
				var g_key = $(this).parents('.item-box').attr('g_key');
				var c_key = $(this).parents('.item-box').attr('c_key');
				var s_key = $(this).parents('.item-box').attr('s_key');
				var cname = $(this).parents('.item-box').attr('cname');
				var ename = $(this).parents('.item-box').attr('ename');
				var u_key = $(this).parents('.item-box').attr('u_key');
				$.post( "<?php echo e(url(config('backpack.base.route_prefix', 'admin').'/SysSite/del')); ?>",{'g_key': g_key, 'c_key': c_key, 's_key': s_key, 'type': type}, function( data ) {
					if(data.msg == 'success') {
						thisItem.remove();
					}
					
				}, 'JSON');
			})

			$('.item-box').on('click', function(event){
				$(this).removeClass( "bg-teal" );
				$(this).siblings('.item-box').removeClass( "bg-teal-active" );
				$(this).siblings('.item-box').addClass( "bg-teal" );
				$(this).addClass( "bg-teal-active" );

				var type  = $(this).attr('type');
				var g_key = $(this).attr('g_key');
				var c_key = $(this).attr('c_key');
				var s_key = $(this).attr('s_key');
				var cname = $(this).attr('cname');
				var ename = $(this).attr('ename');
				var u_key = $(this).attr('u_key');

				$('#addGroupForm')[0].reset();
				$('#addCmpForm')[0].reset();
				$('#addStnForm')[0].reset();
				$('#addDepForm')[0].reset();

				$.post( "<?php echo e(url(config('backpack.base.route_prefix', 'admin').'/SysSite/get')); ?>",{'g_key': g_key, 'c_key': c_key, 's_key': s_key, 'type': type}, function( data ) {
					var itemId = '';

					if(type == 0) {
						itemId = 'cmpBody';
						$('#addCmpForm input[name="g_key"]').val(g_key);

						$('#updateGroupForm input[name="id"]').val(u_key);
						$('#updateGroupForm input[name="cname"]').val(cname);
						$('#updateGroupForm input[name="ename"]').val(ename);

						$('#updateCmpForm input').val('');
						$('#updateStnForm input').val('');
						$('#updateDepForm input').val('');
					}
					else if(type == 1) {
						itemId = 'stnBody';
						$('#addStnForm input[name="g_key"]').val(g_key);
						$('#addStnForm input[name="c_key"]').val(c_key);

						$('#updateCmpForm input[name="id"]').val(u_key);
						$('#updateCmpForm input[name="cname"]').val(cname);
						$('#updateCmpForm input[name="ename"]').val(ename);

						$('#updateStnForm input').val('');
						$('#updateDepForm input').val('');
					}
					else if(type == 2) {
						itemId = 'depBody';
						$('#addDepForm input[name="g_key"]').val(g_key);
						$('#addDepForm input[name="c_key"]').val(c_key);
						$('#addDepForm input[name="s_key"]').val(s_key);

						$('#updateStnForm input[name="id"]').val(u_key);
						$('#updateStnForm input[name="cname"]').val(cname);
						$('#updateStnForm input[name="ename"]').val(ename);

						$('#updateDepForm input').val('');
					}
					else if(type == 3) {
						$('#updateDepForm input[name="id"]').val(u_key);
						$('#updateDepForm input[name="cname"]').val(cname);
						$('#updateDepForm input[name="ename"]').val(ename);
					}
					appendItem(itemId, data.data);
				}, 'JSON');
			});
        });

		function store(itemId, formData) {
			$.ajax({
				url: "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/SysSite/store')); ?>",
				type: 'POST',
				data: formData,
				async: false,
				beforeSend: function () {
					loadingFunc.show();
				},
				error: function (jqXHR, exception) {
					loadingFunc.hide();
					return false;
				},
				success: function (data) {
					if(data.msg == "success") {
						appendItem(itemId, data.data);
						
						if(itemId == 'groupBody') {
							$('#cancelGroupBtn').trigger('click');
						}
						else if(itemId == 'cmpBody') {
							$('#cancelCmpBtn').trigger('click');
						}
						else if(itemId == 'stnBody') {
							$('#cancelStnBtn').trigger('click');
						}
						else if(itemId == 'depBody') {
							$('#cancelDepBtn').trigger('click');
						}
					}
					loadingFunc.hide();
					return false;
				},
				cache: false,
				contentType: false,
				processData: false
			});
		}

		function update(itemId, formData) {
			$.ajax({
				url: "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/SysSite/update')); ?>",
				type: 'POST',
				data: formData,
				async: false,
				beforeSend: function () {
					loadingFunc.show();
				},
				error: function (jqXHR, exception) {
					loadingFunc.hide();
					return false;
				},
				success: function (data) {
					if(data.msg == "success") {
						$('#'+itemId+' .bg-teal-active').find('p[name="cname"]').text(data.data[0].cname);
						$('#'+itemId+' .bg-teal-active').find('p[name="ename"]').text(data.data[0].ename);

						if(itemId == 'groupBody') {
							$('#cancelUpdateGroupBtn').trigger('click');
						}
						else if(itemId == 'cmpBody') {
							$('#cancelUpdateCmpBtn').trigger('click');
						}
						else if(itemId == 'stnBody') {
							$('#cancelUpdateStnBtn').trigger('click');
						}
						else if(itemId == 'depBody') {
							$('#cancelUpdateDepBtn').trigger('click');
						}
					}
					loadingFunc.hide();
					return false;
				},
				cache: false,
				contentType: false,
				processData: false
			});
		}

		function appendItem(itemId, data) {
			var str = '';
			if(itemId == "groupBody") {
				$('#cmpBody').html('');
				$('#stnBody').html('');
				$('#depBody').html('');
			}
			else if(itemId == "cmpBody") {
				$('#stnBody').html('');
				$('#depBody').html('');
			}
			else if(itemId == "stnBody") {
				$('#depBody').html('');
			}
			$.each(data, function(i, v){
				var code = '';
				if(itemId == "groupBody") {
					code = data[i]["g_key"];
				}
				else if(itemId == "cmpBody") {
					code = data[i]["c_key"];
				}
				else if(itemId == "stnBody") {
					code = data[i]["s_key"];
				}
				else if(itemId == "depBody") {
					code = data[i]["d_key"];
				}
				str += '<div class="alert bg-teal alert-dismissible item-box" type="'+data[i]["type"]+'" g_key="'+data[i]["g_key"]+'" c_key="'+data[i]["c_key"]+'" s_key="'+data[i]["s_key"]+'" d_key="'+data[i]["d_key"]+'" u_key="'+data[i]["id"]+'" cname="'+data[i]["cname"]+'" ename="'+data[i]["ename"]+'">\
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>\
							<h4><i class="icon fa fa-check"></i> '+code+'</h4>\
							<p name="cname">'+data[i]["cname"]+'</p>\
							<p name="ename">'+data[i]["ename"]+'</p>\
						</div>';
				
			});

			$('#'+itemId).html(str);
		}
	})
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('backpack::layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>