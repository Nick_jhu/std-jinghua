 
<?php $__env->startSection('header'); ?>
<section class="content-header">
	<h1>
		<?php echo e(trans('dashboard.titleName')); ?>

		<small></small>
	</h1>
	<ol class="breadcrumb">
		<li class="active"><?php echo e(trans('dashboard.titleName')); ?></li>
	</ol>
</section>
<?php $__env->stopSection(); ?> 
<?php $__env->startSection('content'); ?>


<div class="row">
	<div class="col-md-12">
		<div class="box">
			<table class="table table-bordered">
                <thead>
                    <th>id</th>
                    <th>User Id</th>
                    <th>Name</th>
                    <th>secret</th>
                    <th>Create Time</th>
                    <th>Update Time</th>
                </thead>
				<tbody>
					
				</tbody>
			</table>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?> 
<?php $__env->startSection('after_scripts'); ?> 

<script>
    $(function(){
        $.get('<?php echo e(url("oauth/clients")); ?>', {}, function(data){
            if(data) {
                var tr = "";
                for(i in data) {
                    tr += "<tr>\
                                <td>"+data[i].id+"</td>\
                                <td>"+data[i].user_id+"</td>\
                                <td>"+data[i].name+"</td>\
                                <td>"+data[i].secret+"</td>\
                                <td>"+data[i].created_at+"</td>\
                                <td>"+data[i].updated_at+"</td>\
                        ";
                }

                $("table tbody").html(tr);
            }
        });
    })
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('backpack::layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>