 <?php $__env->startSection('header'); ?>
<section class="content-header">
    <h1>
        <?php echo e(trans('modOrder.titleName')); ?>

        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="<?php echo e(url(config('backpack.base.route_prefix'),'dashboard')); ?>"><?php echo e(trans('backpack::crud.admin')); ?></a>
        </li>
        <li>
            <a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin').'/log')); ?>"><?php echo e(trans('backpack::logmanager.log_manager')); ?></a>
        </li>
        <li class="active"><?php echo e(trans('backpack::logmanager.existing_logs')); ?></li>
    </ol>
</section>
<div class="modal fade" id="ts-modal" tabindex="-1" role="dialog" aria-labelledby="lookupTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="lookupTitle"><?php echo e(trans('modOrder.cargochoice')); ?></h5>
            </div>
            <div class="modal-body" id="ts-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
    <input type="hidden" id="lookupEvent" name="lookupEvent" />
</div>
<?php $__env->stopSection(); ?> <?php $__env->startSection('before_scripts'); ?>


<script>
    var gridOpt = {};
    gridOpt.enabledStatus = false;
    gridOpt.fieldsUrl =
        "<?php echo e(url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/sys_mail_format')); ?>";
    gridOpt.dataUrl =
        "<?php echo e(url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/sys_mail_format')); ?>";
    gridOpt.createUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/sysMailFormat/create')); ?>";
    gridOpt.editUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/sysMailFormat')); ?>" + "/{id}/edit";
    gridOpt.height = 800;

    var btnGroup = [{
            btnId: "btnOpenGridOpt",
            btnIcon: "fa fa-table",
            btnText: "<?php echo e(trans('common.gridOption')); ?>",
            btnFunc: function () {
                $('#gridOptModal').modal('show');
            }
        },
        {
            btnId: "btnAdd",
            btnIcon: "fa fa-edit",
            btnText: "<?php echo e(trans('common.add')); ?>",
            btnFunc: function () {
                location.href = gridOpt.createUrl;
            }
        },
        {
            btnId: "btnDelete",
            btnIcon: "fa fa-trash-o",
            btnText: "<?php echo e(trans('common.delete')); ?>",
            btnFunc: function () {
                swal("<?php echo e(trans('common.deleteMsg')); ?>", "", "success");
            }
        },
    ];
</script>
<?php $__env->stopSection(); ?> <?php echo $__env->make('backpack::template.search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('backpack::layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>