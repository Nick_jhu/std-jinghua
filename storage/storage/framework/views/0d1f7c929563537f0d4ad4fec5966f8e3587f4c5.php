 
<?php $__env->startSection('header'); ?>
<section class="content-header">
	<h1>
	<?php echo e(trans('modDlv.dlvPlan')); ?><small></small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(url(config('backpack.base.route_prefix'),'TranPlanMgmt/SendCar')); ?>"><?php echo e(trans('modDlv.titleName')); ?></a></li>
		<li class="active"><?php echo e(trans('modDlv.dlvPlan')); ?></li>
	</ol>
</section>
<?php $__env->stopSection(); ?> 
<?php $__env->startSection('content'); ?>
	<?php echo $__env->make('backpack::template.toolbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	<div class="row">
		
		<div class="col-md-12">
			<div class="callout callout-danger" id="errorMsg" style="display:none">
				<h4><?php echo e(trans('backpack::crud.please_fix')); ?></h4>
				<ul>

				</ul>
			</div>
			<form method="POST" accept-charset="UTF-8" id="mainForm" enctype="multipart/form-data">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true"><?php echo e(trans('modDlv.sendCarInfo')); ?></a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="tab_1">

							<div class="box-body">
								<div class="row">
									<div class="form-group col-md-4">
										<label for="dlv_no"><?php echo e(trans('modDlv.dlvNo')); ?></label>
										<input type="text" class="form-control" id="dlv_no" name="dlv_no">
									</div>
									<div class="form-group col-md-4">
										<label for="status"><?php echo e(trans('modDlv.status')); ?></label>
										<select class="form-control" id="status" name="status">
                                            <option value="UNTREATED"><?php echo e(trans('modDlv.STATUS_UNTREATED')); ?></option>
                                            <option value="DLV"><?php echo e(trans('modDlv.STATUS_DLV')); ?></option>
                                            <option value="ERROR"><?php echo e(trans('modDlv.STATUS_ERROR')); ?></option>
                                            <option value="FINISHED"><?php echo e(trans('modDlv.STATUS_FINISHED')); ?></option>
                                        </select>
									</div>
								</div>

                                <div class="row">
                                    <div class="form-group col-md-4">
										<label for="cust_no"><?php echo e(trans('modDlv.custNo')); ?></label>
										<input type="text" class="form-control" id="cust_no" name="cust_no">
									</div>
                                    <div class="form-group col-md-4">
										<label for="car_no"><?php echo e(trans('modDlv.carNo')); ?></label>
										<input type="text" class="form-control" id="car_no" name="car_no">
									</div>
                                    <div class="form-group col-md-4">
										<label for="car_type_nm"><?php echo e(trans('modDlv.carType')); ?></label>
										<select class="form-control" id="car_type" name="car_type">
                                        </select>
									</div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-4">
										<label for="driver_nm"><?php echo e(trans('modDlv.driverNm')); ?></label>
										<input type="text" class="form-control" id="driver_nm" name="driver_nm">
									</div>
                                    <div class="form-group col-md-4">
										<label for="driver_phone"><?php echo e(trans('modDlv.driverPhone')); ?></label>
										<input type="text" class="form-control" id="driver_phone" name="driver_phone">
									</div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-4">
										<label for="load_rate"><?php echo e(trans('modDlv.loadRate')); ?></label>
										<input type="text" class="form-control" id="load_rate" name="load_rate">
									</div>
                                    <div class="form-group col-md-4">
										<label for="load_tweight"><?php echo e(trans('modDlv.loadTweight')); ?></label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="load_tweight" name="load_tweight">
                                            <span class="input-group-addon">KGS</span>
                                        </div>
									</div>
                                    <div class="form-group col-md-4">
										<label for="load_tcbm"><?php echo e(trans('modDlv.loadTcbm')); ?></label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="load_tcbm" name="load_tcbm">
                                            <input type="hidden" class="form-control" id="load_tcbmu" name="load_tcbmu">
                                            <span class="input-group-addon">M3</span>
                                        </div>
										
									</div>
                                </div>


								<?php if(isset($id)): ?>
								<input type="hidden" name="id" value="<?php echo e($id); ?>" class="form-control">
								<input type="hidden" name="_method" value="PUT" class="form-control"> 
								<?php endif; ?>

							</div>
						</div>

					</div>
					<!-- /.tab-content -->
				</div>
			</form>
			<div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_3" data-toggle="tab" aria-expanded="false"><?php echo e(trans('modDlv.loadingPlan')); ?></a></li>
                    <li class=""><a href="#tab_4" data-toggle="tab" aria-expanded="false"><?php echo e(trans('modDlv.loadingDetails')); ?></a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_3">
						<button type="button" class="btn btn-primary btn-sm" id="ordLookup" btnName="ordLookup" info1="<?php echo e(Crypt::encrypt('mod_order')); ?>" info2="<?php echo e(Crypt::encrypt('ord_no,ord_no,truck_cmp_no,truck_cmp_nm')); ?>" info3="<?php echo e(Crypt::encrypt('')); ?>" info4="" selectionmode="checkbox" triggerfunc="ordCallBackFunc"><?php echo e(trans('modDlv.chooseOrder')); ?></button>
						<button type="button" class="btn btn-primary btn-sm" id="handBtn"><?php echo e(trans('modDlv.manualFilling')); ?></button>
						<button type="button" class="btn btn-primary btn-sm" id="autoBtn"><?php echo e(trans('modDlv.autoFilling')); ?></button>
                        <div id="packGrid"></div>
                    </div>
					<div class="tab-pane" id="tab_4">
                        <div id="carLoadGrid"></div>
                    </div>
                </div>
                <!-- /.tab-content -->
            </div>
		</div>	
	</div>




 <?php $__env->stopSection(); ?> 
 <?php echo $__env->make('backpack::template.lookup', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
 <?php $__env->startSection('after_scripts'); ?>
	<script>
		var mainId = "";
		var dlv_no = "";
		var editData = null;
		var editObj = null;
		var SAVE_URL = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/TranPlanMgmt/SendCar')); ?>";

		var fieldData = null;
		var fieldObj = null;

		<?php if(isset($crud -> create_fields)): ?>
		fieldData = '{<?php echo json_encode($crud->create_fields); ?>}';
		fieldData = fieldData.substring(1);
		fieldData = fieldData.substring(0, fieldData.length - 1);
		fieldObj = JSON.parse(fieldData);
		<?php endif; ?>



		<?php if(isset($id)): ?>
		mainId = "<?php echo e($id); ?>";
		editData = '{<?php echo $entry; ?>}';
		editData = editData.substring(1);
		editData = editData.substring(0, editData.length - 1);
		editObj = JSON.parse(editData);
		dlv_no = editObj.dlv_no;
		<?php endif; ?>




		$(function () {

			$( "#lookupEvent" ).on( "ordCallBackFunc", function(event,indexs) {
				console.log(indexs);
				var ordArray = [];
				$.each(indexs,function(k,v){
					var data = $('#lookupGrid').jqxGrid('getrowdata', indexs[k]);
					var ordNo = data.ord_no;

					ordArray.push(ordNo);
				});

				$.ajax({
					url: "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/TranPlanMgmt/SendCar')); ?>" + '/getPack',
					type: 'POST',
					async: false,
					cache: false,
					data: {ordArray: ordArray},
					beforeSend: function () {

					},
					error: function (jqXHR, exception) {

					},
					success: function (data) {

						var source = {
										datatype: "json",
										localdata: data.Rows
									};
						var dataAdapter = new $.jqx.dataAdapter(source);
						$('#packGrid').jqxGrid({source: dataAdapter});

						$("#packGrid").jqxGrid('selectallrows');
					}
					
				});
			});

			//var formOpt = {};
			formOpt.formId = "mainForm";
			formOpt.editObj = editObj;
			formOpt.fieldObj = fieldObj;
			formOpt.editUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/TranPlanMgmt/SendCar')); ?>";
			formOpt.fieldsUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') .'/get/mod_dlv')); ?>/" + mainId;
			formOpt.saveUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/TranPlanMgmt/SendCar')); ?>";

			formOpt.initFieldCustomFunc = function () {

			};

			formOpt.afterDel = function() {
				$('#ordGrid').jqxGrid('clear');
			}

			var btnGroup = [];

			initBtn(btnGroup);

			$('#ordLookup').on('click', function(){
				$('#lookupModal').modal('show');
				initLookup('ordLookup', "<?php echo e(trans('modDlv.ordQuery')); ?>");
			});

			
		});


        setField.disabled("mainForm", ["load_rate", "load_tweight", "load_tcbm", "status"]);
	
        //#region Order Pack
		$.ajax({
			url: "<?php echo e(url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/mod_order_pack')); ?>",
			type: 'GET',
			async: false,
			beforeSend: function () {
			},
			error: function (jqXHR, exception) {

			},
			success: function (fieldData) {
				var opt = {};
				opt.gridId = "packGrid";
				opt.fieldData = fieldData;
				opt.formId = "sub1Form";
				opt.saveId = "packSave";
				opt.cancelId = "packCancel";
				opt.showBoxId = "sub1Box";
				opt.height = 300;
				opt.getUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin').'/orderPack/get')); ?>" + '/' + dlv_no;
				opt.addUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/orderPack')); ?>" + "/store";
				opt.updateUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/orderPack')); ?>" + "/update";
				opt.delUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/orderPack')); ?>" + "/delete/";
				opt.showaggregates = false;
				opt.commonBtn = false;
				opt.inlineEdit = false;
				opt.showtoolbar = false;
				opt.selectionmode = "checkbox";
				opt.defaultKey = {};
				opt.beforeSave = function (formData) {

				}

				opt.afterSave = function (data) {

				}

				genDetailGrid(opt);
			},
			cache: false,
			contentType: false,
			processData: false
		});
		//#endregion

        //#region 裝填結果
		$.ajax({
			url: "<?php echo e(url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/mod_car_load')); ?>",
			type: 'GET',
			async: false,
			beforeSend: function () {
			},
			error: function (jqXHR, exception) {

			},
			success: function (fieldData) {
				console.log(fieldData);
				var col = [
					[
						{name: "id", type: "number"},
						{name: "dlv_no", type: "string"},
						{name: "ord_no", type: "string"},
						{name: "pack_no", type: "string"},
					],
					[
						{text: "<?php echo e(trans('modCarLoad.id')); ?>", datafield: "id", width: 100, dbwidth: "10", nullable: false},
						{text: "<?php echo e(trans('modCarLoad.dlvNo')); ?>", datafield: "dlv_no", width: 150, dbwidth: "10", nullable: false},
						{text: "<?php echo e(trans('modCarLoad.ordNo')); ?>", datafield: "ord_no", width: 150, dbwidth: "10", nullable: false},
						{text: "<?php echo e(trans('modCarLoad.packNo')); ?>", datafield: "pack_no", width: 150, dbwidth: "10", nullable: false},
					]
				];
				var opt = {};
				opt.gridId = "carLoadGrid";
				opt.fieldData = col;
				opt.formId = "sub2Form";
				opt.saveId = "carLoadSave";
				opt.cancelId = "carLoadCancel";
				opt.showBoxId = "sub2Box";
				opt.height = 300;
				opt.getUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin').'/carLoad/get')); ?>" + '/' + dlv_no;
				opt.addUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/orderPack')); ?>" + "/store";
				opt.updateUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/orderPack')); ?>" + "/update";
				opt.delUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/orderPack')); ?>" + "/delete/";
				opt.showaggregates = false;
				opt.commonBtn = false;
				opt.inlineEdit = false;
				opt.showtoolbar = false;
				opt.defaultKey = {};
				opt.beforeSave = function (formData) {

				}

				opt.afterSave = function (data) {

				}

				genDetailGrid(opt);
			},
			cache: false,
			contentType: false,
			processData: false
		});
		//#endregion


		$("#handBtn").on("click", function(){
			var selIndex = $("#packGrid").jqxGrid('getselectedrowindexes');
			var rowData = [];
			for(var i=0; i<selIndex.length; i++) {
				var data = $("#packGrid").jqxGrid('getrowdata', selIndex[i]);
				data["dlv_no"] = dlv_no;
				rowData.push(data);
			}

			var carNo = $("#car_no").val();

			if(carNo == "") {
				notifyMsg("<?php echo e(trans('modDlv.msg5')); ?>", "<?php echo e(trans('modDlv.msg6')); ?>", 'warning');
				return;
			}

			$.ajax({
				url: "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/TranPlanMgmt/SendCar')); ?>" + '/handPickup',
				type: 'POST',
				async: false,
				cache: false,
				data: {data: JSON.stringify(rowData)},
				beforeSend: function () {

				},
				error: function (jqXHR, exception) {

				},
				success: function (data) {
					if(data.msg == "success") {
						notifyMsg("<?php echo e(trans('modDlv.msg7')); ?>", "<?php echo e(trans('modDlv.msg8')); ?>", 'success');
					}
					else {
						notifyMsg("<?php echo e(trans('modDlv.msg5')); ?>", "<?php echo e(trans('modDlv.msg9')); ?>", 'warning');
					}
					$('#carLoadGrid').jqxGrid('updatebounddata');
				}
				
			});
		});

		$("#autoBtn").on("click", function(){
			var rowData = $("#packGrid").jqxGrid('getrows');

			var carNo = $("#car_no").val();

			if(carNo == "") {
				//notifyMsg('警告訊息', '請先輸入車號', 'warning');
				//return;
			}

			$.ajax({
				url: "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/TranPlanMgmt/SendCar')); ?>" + '/autoPickup',
				type: 'POST',
				async: false,
				cache: false,
				data: {data: JSON.stringify(rowData), dlv_no: dlv_no},
				beforeSend: function () {

				},
				error: function (jqXHR, exception) {

				},
				success: function (data) {
					if(data.msg == "success") {
						notifyMsg("<?php echo e(trans('modDlv.msg7')); ?>", "<?php echo e(trans('modDlv.msg8')); ?>", 'success');

						var source = {
										datatype: "json",
										localdata: data.data.Rows
									};
						var dataAdapter = new $.jqx.dataAdapter(source);
						//$('#packGrid').jqxGrid('clear');
						$('#packGrid').jqxGrid({source: dataAdapter});
					}
					else {
						notifyMsg("<?php echo e(trans('modDlv.msg5')); ?>", "<?php echo e(trans('modDlv.msg9')); ?>", 'warning');
					}
					$('#carLoadGrid').jqxGrid('updatebounddata');
				}
				
			});
		});
	</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backpack::layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>