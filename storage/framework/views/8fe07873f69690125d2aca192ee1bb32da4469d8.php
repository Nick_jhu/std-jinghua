<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">
     
    <title>美Z.人生 旅遊網 - 出國Wifi機、旅行背包</title>
    <meta name="description" content="<?php echo e(isset($seo_desc) ? $seo_desc : "漫遊世界 美z.人生"); ?>" />

    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="美Z.人生 旅遊網 - 出國Wifi機、旅行背包">
    <meta itemprop="description" content="<?php echo e(isset($seo_desc) ? $seo_desc : "漫遊世界 美z.人生"); ?>">
    <meta itemprop="image" content="<?php echo e(url('assets/images/banner/2.jpg')); ?>">

    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@beautyz_net">
    <meta name="twitter:title" content="美Z.人生 旅遊網 - 出國Wifi機、旅行背包">
    <meta name="twitter:description" content="<?php echo e(isset($seo_desc) ? $seo_desc : "漫遊世界 美z.人生"); ?>">
    <meta name="twitter:creator" content="@beautyz_net">
    <!-- Twitter summary card with large image must be at least 280x150px -->
    <meta name="twitter:image:src" content="<?php echo e(url('assets/images/banner/2.jpg')); ?>">
    <!-- Open Graph data -->
    <meta property="og:title" content="美Z.人生 旅遊網 - 出國Wifi機、旅行背包" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="<?php echo e(Request::url()); ?>" />
    <meta property="og:image" content="<?php echo e(url('assets/images/banner/2.jpg')); ?>" />
    <meta property="og:description" content="<?php echo e(isset($seo_desc) ? $seo_desc : "漫遊世界 美z.人生"); ?>" />
    <meta property="og:site_name" content="美Z.人生 旅遊網" />
    <meta property="article:published_time" content="2018-04-15T05:59:00+01:00" />
    <meta property="article:modified_time" content="2018-04-15T05:59:00+01:00" />
    <meta property="article:section" content="<?php echo e(isset($seo_section) ? $seo_section : "出國WIFI機"); ?>" />
    <meta property="article:tag" content="<?php echo e(isset($seo_tag) ? $seo_tag : "出國WIFI機"); ?>" />
    <meta property="fb:admins" content="100000307128936" />
    
    <link rel="stylesheet" href="<?php echo e(url('assets/css/reset.css')); ?>" type="text/css" media="screen">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo e(url('assets/libs/bootstrap-3.3.7.min.css')); ?>">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?php echo e(url('assets/css/style.css?t=24098394')); ?>" type="text/css" media="screen">
    <link rel="stylesheet" href="<?php echo e(url('assets/css/home.css?t=22423424565')); ?>" type="text/css" media="screen">    
    <link rel="shortcut icon" href="<?php echo e(url('assets/images/favicon-32x32.png')); ?>">
    <script type="text/javascript" src="<?php echo e(url('assets/libs/jquery-3.3.1.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/libs/tether.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/libs/bootstrap-3.3.7.min.js')); ?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-117435537-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-117435537-1');
    </script>

</head>

<body>
    <div class="main-container" id="main-container">
        
            <?php echo $__env->make('FrontEnd.layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        

        <div id="content-box-1">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <?php $__currentLoopData = $bannerData; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="item <?php if($key == 0): ?> active <?php endif; ?>">
                        <?php if(isset($row->video)): ?>
                        <?php if($row->image != null): ?>
                        <img class="video-Img" src="<?php echo e(url('storage/'.$row->image)); ?>">
                        <?php endif; ?>
                        <div class="video-box">
                            <video id="movie" autoplay loop preload>
                                <source src="<?php echo e($row->video); ?>" type="video/mp4"/>
                                您的瀏覽器不支援HTML 5影片播放標籤 video 格式。
                                Your browser doesn't support the video tag.
                            </video>
                        </div>
                        <?php else: ?>
                        <img src="<?php echo e(url('storage/'.$row->image)); ?>">
                        <?php endif; ?>
                        <div class="carousel-caption carousel-caption-1">
                            <?php echo $row->descp; ?>

                        </div>
                        <?php if(isset($bannerProd[$row->id])): ?>
                        <?php $__currentLoopData = $bannerProd[$row->id]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k=>$val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="prod-<?php echo e($k+1); ?>">
                            <a href="<?php echo e(url('productDetail/'.$val->id.'?startDate=&endDate=')); ?>">
                                <img src="<?php echo e(Storage::url($val->img1)); ?>" width="200">
                                <div class="name">Korin Design ClickPack防盜後背包-大全配X版</div>
                            </a>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    
                </div>
                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                <i class="fas fa-angle-left"></i>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                <i class="fas fa-angle-right"></i>
                </a>
            </div>
        </div>

        <form method="GET" id="searchForm" action="<?php echo e(url('product')); ?>">
            <div id="content-box-2" class="search-box">
                <div class="right-box">
                    
                </div>
                <div class="center-box">
                        <div class="form-row">
                        <div class="form-group col-md-2 col-sm-2 col-xs-12"><span class="space">&nbsp;</span>
                            <select class="form-control" id="sellType" name="sellType">
                                <option value=""  selected >租賃分類</option>
                                <option value="R">租</option>
                                <option value="S">買</option>
                            </select>
                            </div>
                            <div class="form-group col-md-2 col-sm-2 col-xs-12"><span class="space">&nbsp;</span>
                                <select class="form-control" id="country" name="country">
                                <option value="" selected>地區</option>
                                <?php $__currentLoopData = $areaData; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($row->id); ?>"><?php echo e($row->name); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>

                             <div class="form-group col-md-3 col-sm-3 col-xs-12">取用(到貨)日期                            
                                <div id="datepicker-start" class="input-group date" data-date-format="yyyy-mm-dd">
                                    <input class="form-control" type="text" readonly name="startDate" />
                                    <span class="input-group-addon"><i class="far fa-calendar-alt"></i></span>
                                </div>
                            </div>
                             <div class="form-group col-md-3 col-sm-3 col-xs-12">歸還(寄還)日期                            
                                <div id="datepicker-end" class="input-group date" data-date-format="yyyy-mm-dd">
                                    <input class="form-control" type="text" readonly name="endDate" />
                                    <span class="input-group-addon"><i class="far fa-calendar-alt"></i></span>
                                </div>
                            </div>
                            <!-- <div class="form-group col-md-2 col-xs-2">&nbsp;
                                <select class="form-control" id="">
                                <option selected>數量</option>
                                <option>...</option>
                                </select>
                            </div> -->
                        </div>
                    </div>
                    <div class="left-box">
                        <div class="col-md-2">
                        <span class="web">&nbsp;</span>
                            <button class="btn btn-link btn-lg search-btn">
                                <i class="fas fa-play-circle"></i>&nbsp;&nbsp;查詢
                            </button>
                        </div>
                    </div>
                                        
                </div>
        </form>

        <div id="content-box-3">
            <p class="slogan-1">一機走全球，費率自由選，就是給您業界自由度最高旅遊上網品質。</p>
            <p class="slogan-2">漫遊世界   美z.人生</p>
        </div>

        <div id="content-box-4" class="web">
            <div id="country-slide-box-web" class="country-slide-box">
                <div class="photos-box">
                    <div class="photo-box photo-0-box">
                        <div class="right-box">
                            <div class="triangle-box"></div>
                            <div class="slogan-bg">
                                <a class="btn btn-lg btn-orange"  href="https://www.z-trip.com/productDetail/3?startDate=&amp;endDate=">機種詳細介紹</a>
                            </div>
                        </div>
                        <div class="left-box">
                            <div class="black-box">
                                <div class="country"></div>
                                <p>業界最單一，一機全球通用</p>
                                <p>費率依想去的國家自動選</p>
                            </div>
                        </div>
                    </div>

                    <div class="photo-box photo-1-box">
                        <div class="right-box">
                            <div class="triangle-box"></div>
                            <div class="slogan-bg">
                                <a class="btn btn-lg btn-orange" href="https://www.z-trip.com/productDetail/3?startDate=&amp;endDate=">機種詳細介紹</a>
                            </div>
                        </div>
                        <div class="left-box">
                            <div class="black-box">
                                <div class="country"></div>
                                <p>業界最單一，一機全球通用</p>
                                <p>費率依想去的國家自動選</p>
                            </div>
                        </div>
                    </div>

                    <div class="photo-box photo-2-box">
                        <div class="right-box">
                            <div class="triangle-box"></div>
                            <div class="slogan-bg">
                                <a class="btn btn-lg btn-orange" href="https://www.z-trip.com/productDetail/3?startDate=&amp;endDate=">機種詳細介紹</a>
                            </div>
                        </div>
                        <div class="left-box">
                            <div class="black-box">
                                <div class="country"></div>
                                <p>業界最單一，一機全球通用</p>
                                <p>費率依想去的國家自動選</p>
                            </div>
                        </div>
                    </div>

                    <div class="photo-box photo-3-box">
                        <div class="right-box">
                            <div class="triangle-box"></div>
                            <div class="slogan-bg">
                                <a class="btn btn-lg btn-orange" href="https://www.z-trip.com/productDetail/4?startDate=&amp;endDate=">機種詳細介紹</a>
                            </div>
                        </div>
                        <div class="left-box">
                            <div class="black-box">
                                <div class="country"></div>
                                <p>業界最單一，一機全球通用</p>
                                <p>費率依想去的國家自動選</p>
                            </div>
                        </div>
                    </div>

                    <div class="photo-box photo-4-box">
                        <div class="right-box">
                            <div class="triangle-box"></div>
                            <div class="slogan-bg">
                                <a class="btn btn-lg btn-orange" href="https://www.z-trip.com/productDetail/8?startDate=&amp;endDate=">機種詳細介紹</a>
                            </div>
                        </div>
                        <div class="left-box">
                            <div class="black-box">
                                <div class="country"></div>
                                <p>業界最單一，一機全球通用</p>
                                <p>費率依想去的國家自動選</p>
                            </div>
                        </div>
                    </div>

                    <div class="photo-box photo-5-box">
                        <div class="right-box">
                            <div class="triangle-box"></div>
                            <div class="slogan-bg">
                                <a class="btn btn-lg btn-orange" href="https://www.z-trip.com/productDetail/5?startDate=&amp;endDate=">機種詳細介紹</a>
                            </div>
                        </div>
                        <div class="left-box">
                            <div class="black-box">
                                <div class="country"></div>
                                <p>業界最單一，一機全球通用</p>
                                <p>費率依想去的國家自動選</p>
                            </div>
                        </div>
                    </div>

                    <div class="photo-box photo-6-box">
                        <div class="right-box">
                            <div class="triangle-box"></div>
                            <div class="slogan-bg">
                                <a class="btn btn-lg btn-orange" href="https://www.z-trip.com/productDetail/6?startDate=&amp;endDate=">機種詳細介紹</a>
                            </div>
                        </div>
                        <div class="left-box">
                            <div class="black-box">
                                <div class="country"></div>
                                <p>業界最單一，一機全球通用</p>
                                <p>費率依想去的國家自動選</p>
                            </div>
                        </div>
                    </div>

                    <div class="photo-box photo-7-box">
                        <div class="right-box">
                            <div class="triangle-box"></div>
                            <div class="slogan-bg">
                                <a class="btn btn-lg btn-orange" href="https://www.z-trip.com/productDetail/9?startDate=&amp;endDate=">機種詳細介紹</a>
                            </div>
                        </div>
                        <div class="left-box">
                            <div class="black-box">
                                <div class="country"></div>
                                <p>業界最單一，一機全球通用</p>
                                <p>費率依想去的國家自動選</p>
                            </div>
                        </div>
                    </div>                    
                </div>    
                <div class="btn-box">
                    <div class="btn-0 btnitem active"><div class="img-box"></div></div>
                    <div class="btn-1 btnitem"><div class="img-box"></div></div>
                    <div class="btn-2 btnitem"><div class="img-box"></div></div>
                    <div class="btn-3 btnitem"><div class="img-box"></div></div>
                    <div class="btn-4 btnitem"><div class="img-box"></div></div>
                    <div class="btn-5 btnitem"><div class="img-box"></div></div>
                    <div class="btn-6 btnitem"><div class="img-box"></div></div>
                    <div class="btn-7 btnitem"><div class="img-box"></div></div>                                        
                </div> 
            </div>                   
        </div>

        <div id="content-box-4" class="mobile">
            <div id="country-slide-box-mobile" class="country-slide-box">
                <div class="photos-box">
                    <div class="photo-box photo-0-box">
                        <div class="top-box">                            
                            
                        </div>
                        <div class="bottom-box">
                            <div class="black-box">
                                <div class="logo">
                                    <img src="assets/images/logo-white.png" width="150" />
                                </div>                                
                                <p>業界最單一，一機全球通用</p>
                                <p>費率依想去的國家自動選</p>
                                <a class="btn btn-lg btn-orange" href="https://www.z-trip.com/productDetail/3?startDate=&amp;endDate=">機種詳細介紹</a>
                            </div>
                        </div>
                        <div class="prod"></div>
                        <div class="prodname"></div>
                        <div class="country"></div>
                    </div>

                    <div class="photo-box photo-1-box">
                        <div class="top-box">                            
                            
                        </div>
                        <div class="bottom-box">
                            <div class="black-box">
                                <div class="logo">
                                    <img src="assets/images/logo-white.png" width="150" />
                                </div>                                
                                <p>業界最單一，一機全球通用</p>
                                <p>費率依想去的國家自動選</p>
                                <a class="btn btn-lg btn-orange" href="https://www.z-trip.com/productDetail/3?startDate=&amp;endDate=">機種詳細介紹</a>
                            </div>
                        </div>
                        <div class="prod"></div>
                        <div class="prodname"></div>
                        <div class="country"></div>
                    </div>

                    <div class="photo-box photo-2-box">
                        <div class="top-box">                            
                            
                        </div>
                        <div class="bottom-box">
                            <div class="black-box">
                                <div class="logo">
                                    <img src="assets/images/logo-white.png" width="150" />
                                </div>                                
                                <p>業界最單一，一機全球通用</p>
                                <p>費率依想去的國家自動選</p>
                                <a class="btn btn-lg btn-orange" href="https://www.z-trip.com/productDetail/3?startDate=&amp;endDate=">機種詳細介紹</a>
                            </div>
                        </div>
                        <div class="prod"></div>
                        <div class="prodname"></div>
                        <div class="country"></div>
                    </div>

                    <div class="photo-box photo-3-box">
                        <div class="top-box">                            
                            
                        </div>
                        <div class="bottom-box">
                            <div class="black-box">
                                <div class="logo">
                                    <img src="assets/images/logo-white.png" width="150" />
                                </div>                                
                                <p>業界最單一，一機全球通用</p>
                                <p>費率依想去的國家自動選</p>
                                <a class="btn btn-lg btn-orange" href="https://www.z-trip.com/productDetail/4?startDate=&amp;endDate=">機種詳細介紹</a>
                            </div>
                        </div>
                        <div class="prod"></div>
                        <div class="prodname"></div>
                        <div class="country"></div>
                    </div>

                    <div class="photo-box photo-4-box">
                        <div class="top-box">                            
                           
                        </div>
                        <div class="bottom-box">
                            <div class="black-box">
                                <div class="logo">
                                    <img src="assets/images/logo-white.png" width="150" />
                                </div>                                
                                <p>業界最單一，一機全球通用</p>
                                <p>費率依想去的國家自動選</p>
                                <a class="btn btn-lg btn-orange" href="https://www.z-trip.com/productDetail/8?startDate=&amp;endDate=">機種詳細介紹</a>
                            </div>
                        </div>
                        <div class="prod"></div>
                        <div class="prodname"></div>
                        <div class="country"></div>
                    </div>

                    <div class="photo-box photo-5-box">
                        <div class="top-box">                            
                            
                        </div>
                        <div class="bottom-box">
                            <div class="black-box">
                                <div class="logo">
                                    <img src="assets/images/logo-white.png" width="150" />
                                </div>                                
                                <p>業界最單一，一機全球通用</p>
                                <p>費率依想去的國家自動選</p>
                                <a class="btn btn-lg btn-orange" href="https://www.z-trip.com/productDetail/5?startDate=&amp;endDate=">機種詳細介紹</a>
                            </div>
                        </div>
                        <div class="prod"></div>
                        <div class="prodname"></div>
                        <div class="country"></div>
                    </div>

                    <div class="photo-box photo-6-box">
                        <div class="top-box">                            
                            
                        </div>
                        <div class="bottom-box">
                            <div class="black-box">
                                <div class="logo">
                                    <img src="assets/images/logo-white.png" width="150" />
                                </div>                                
                                <p>業界最單一，一機全球通用</p>
                                <p>費率依想去的國家自動選</p>
                                <a class="btn btn-lg btn-orange" href="https://www.z-trip.com/productDetail/6?startDate=&amp;endDate=">機種詳細介紹</a>
                            </div>
                        </div>
                        <div class="prod"></div>
                        <div class="prodname"></div>
                        <div class="country"></div>
                    </div>

                    <div class="photo-box photo-7-box">
                        <div class="top-box">                            
                            
                        </div>
                        <div class="bottom-box">
                            <div class="black-box">
                                <div class="logo">
                                    <img src="assets/images/logo-white.png" width="150" />
                                </div>                                
                                <p>業界最單一，一機全球通用</p>
                                <p>費率依想去的國家自動選</p>
                                <a class="btn btn-lg btn-orange" href="https://www.z-trip.com/productDetail/9?startDate=&amp;endDate=">機種詳細介紹</a>
                            </div>
                        </div>
                        <div class="prod"></div>
                        <div class="prodname"></div>
                        <div class="country"></div>
                    </div>
                </div>            

                <div class="btn-box">
                    <select class="form-control selectCountry">
                      <option value="0">日本</option>
                      <option value="1">韓國</option>
                      <option value="2">新加坡</option>
                      <option value="3">香港</option>
                      <option value="4">澳洲</option>
                      <option value="5">歐洲</option>
                      <option value="6">美洲</option>
                      <option value="7">美球各地跑</option>
                    </select>                       
                </div>
            </div> 
        </div>

        <div id="content-box-5">
            <div class="service-box service-box-0">
                <div class="bg-box">
                    <div class="desc-box">
                        <div class="box-1"><?php if(isset($baseData->title1)): ?><?php echo e($baseData->title1); ?><?php endif; ?></div>
                        <div class="box-2">
                            <h3><?php if(isset($baseData->title1)): ?><?php echo e($baseData->title1); ?><?php endif; ?></h3>
                            <hr>
                            <?php if(isset($baseData->descp1)): ?>
                                <?php echo $baseData->descp1; ?>

                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="service-box service-box-1">
                <div class="bg-box">
                    <div class="desc-box">
                        <div class="box-1"><?php if(isset($baseData->title2)): ?><?php echo e($baseData->title2); ?><?php endif; ?></div>
                        <div class="box-2">
                            <h3><?php if(isset($baseData->title2)): ?><?php echo e($baseData->title2); ?><?php endif; ?></h3>
                            <hr>
                            <?php if(isset($baseData->descp2)): ?>
                                <?php echo $baseData->descp2; ?>

                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="service-box service-box-2">
                <div class="bg-box">
                    <div class="desc-box">
                        <div class="box-1"><?php if(isset($baseData->title3)): ?><?php echo e($baseData->title3); ?><?php endif; ?></div>
                        <div class="box-2">
                            <h3><?php if(isset($baseData->title3)): ?><?php echo e($baseData->title3); ?><?php endif; ?></h3>
                            <hr>
                            <?php if(isset($baseData->descp3)): ?>
                                <?php echo $baseData->descp3; ?>

                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="service-box service-box-3">
                <div class="bg-box">
                    <div class="desc-box">
                        <div class="box-1"><?php if(isset($baseData->title4)): ?><?php echo e($baseData->title4); ?><?php endif; ?></div>
                        <div class="box-2">
                            <h3><?php if(isset($baseData->title4)): ?><?php echo e($baseData->title4); ?><?php endif; ?></h3>
                            <hr>
                            <?php if(isset($baseData->descp4)): ?>
                                <?php echo $baseData->descp4; ?>

                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="content-box-6">
            <div class="images-box">
                <div class="img img-0"></div>
                <div class="img img-1"></div>
                <div class="img img-2"></div>
                <div class="img img-3"></div>
                <div class="img img-4"></div>
                <div class="img img-5"></div>    
                <div class="img img-6"></div>
                <div class="img img-7"></div>
                <div class="img img-8"></div>
                <div class="img img-9"></div>           
            </div>   
            <div class="mobile-box"></div> 
            <div class="fans-box">
                <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fm.facebook.com%2FBeautyz.net&amp;tabs=timeline&amp;width=300&amp;height=500&amp;small_header=true&amp;adapt_container_width=false&amp;hide_cover=false&amp;show_facepile=true&amp;appId=1470026979698151" ;="" width="351" height="760" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true"></iframe>
                <div class="mask"></div>
            </div>               
            <div class="qrcod-box-line">
                <p>LINE</p>
                <img src="http://qr-official.line.me/L/FMPomkRipU.png" width="150">
            </div>
            <div class="qrcod-box-wechat">
                <p>We Chat</p>
                <img src="./assets/images/wehat.png" width="150">
            </div> 
            <div class="desc-box">
                <p>
                    最新資訊和最新活動即時掌握<br>
                    跟著 美z.人生 !一起去旅行!
                </p>
                <br>
                <a href="https://www.facebook.com/Beautyz.net/?hc_ref=ARTidGfdmfADGNZsSGkXBnkTvgo7GNlwd4AIdE1HKwkN0RXX4Ys4lAqB3FAJxxiq8pk&fref=nf" target="_blank" class="btn btn-primary btn-lg btn-block fans-btn">
                美z.人生 FB 粉絲團</a>
            </div>
        </div>


        <?php echo $__env->make('FrontEnd.layouts.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    </div>
    <script type="text/javascript" src="<?php echo e(url('assets/js/home.js?t=54523423')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/common.js?t=234234131')); ?>"></script>
</body>
</html>
