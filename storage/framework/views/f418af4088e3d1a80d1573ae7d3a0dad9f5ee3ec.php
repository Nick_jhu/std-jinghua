<?php $__env->startSection('after_style'); ?>
    <link rel="stylesheet" href="<?php echo e(url('assets/css/reset.css')); ?>" type="text/css" media="screen">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo e(url('assets/libs/bootstrap-3.3.7.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(url('assets/css/style.css')); ?>" type="text/css" media="screen">
    <link rel="stylesheet" href="<?php echo e(url('assets/css/qa.css')); ?>" type="text/css" media="screen">
    
    <script type="text/javascript" src="<?php echo e(url('assets/libs/jquery-3.3.1.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/libs/tether.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/libs/bootstrap-3.3.7.min.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('header'); ?>
    <?php echo $__env->make('FrontEnd.layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="breadcrumbs-box">
	<div class="content-box">
		<div class="name">Q &amp; A</div>
		<div class="breadcrumbs">
			<a href="<?php echo e(url('/')); ?>">美z.人生</a>  &gt; Q &amp; A
		</div>
	</div>
</div>
<div id="content-box">
    <div class="left-box">
        <div class="title"><i class="fas fa-square"></i>問題類別</div>
        <ul>
            <?php $__currentLoopData = $cateData; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <a href="#item<?php echo e($row->id); ?>">
                <li <?php if($key == 0): ?> class="active" <?php endif; ?>><?php echo e($row->name); ?></li>
            </a>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>
    </div>
    <div class="right-box">
        <?php $__currentLoopData = $cateData; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="title" id="item<?php echo e($row->id); ?>"><i class="fas fa-square"></i> <?php echo e($row->name); ?></div>
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <?php $__currentLoopData = $qaData; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k=>$val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php if($row->id == $val->cate_id): ?>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingThree">
                    <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-<?php echo e($key); ?>-<?php echo e($k); ?>" aria-expanded="false" aria-controls="collapse-<?php echo e($key); ?>-<?php echo e($k); ?>" <?php if(isset($val->title_color)): ?>style="color:<?php echo e($val->title_color); ?>"<?php endif; ?>>
                        <?php echo e($val->q_title); ?>

                    </a>
                    </h4>
                </div>
                <div id="collapse-<?php echo e($key); ?>-<?php echo e($k); ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-<?php echo e($key); ?>-<?php echo e($k); ?>">
                    <div class="panel-body">
                    <?php echo $val->answer; ?>

                    </div>
                </div>
            </div>
            <?php endif; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div> 
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer'); ?>
    <?php echo $__env->make('FrontEnd.layouts.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('after_scripts'); ?>
  <script type="text/javascript" src="<?php echo e(url('assets/js/qa.js')); ?>"></script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('FrontEnd.layout',[
	"seo_title" => "Q＆A",
	"seo_desc" => preg_replace("/\n+/","",strip_tags("Q＆A")),
	"seo_img" => null
], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>