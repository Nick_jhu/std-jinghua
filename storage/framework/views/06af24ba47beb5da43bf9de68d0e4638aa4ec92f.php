<html itemscope itemtype="http://schema.org/Article">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <title><?php echo e(isset($seo_title) ? $seo_title : ""); ?>-ZI 你愛的生活品味</title>
    <meta name="description" content="<?php echo e(isset($seo_desc) ? $seo_desc : "ZI 你愛的生活品味"); ?>" />
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="<?php echo e(isset($seo_title) ? $seo_title : ""); ?>-ZI 你愛的生活品味">
    <meta itemprop="description" content="<?php echo e(isset($seo_desc) ? $seo_desc : "ZI 你愛的生活品味"); ?>">
    <meta itemprop="image" content="<?php echo e(isset($seo_img) ? $seo_img : "ZI 你愛的生活品味"); ?>">

    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@beautyz_net">
    <meta name="twitter:title" content="<?php echo e(isset($seo_title) ? $seo_title : ""); ?>-ZI 你愛的生活品味">
    <meta name="twitter:description" content="<?php echo e(isset($seo_desc) ? $seo_desc : "ZI 你愛的生活品味"); ?>">
    <meta name="twitter:creator" content="@beautyz_net">
    <!-- Twitter summary card with large image must be at least 280x150px -->
    <meta name="twitter:image:src" content="<?php echo e(isset($seo_img) ? $seo_img : ""); ?>">

    <!-- Open Graph data -->
    <meta property="og:title" content="<?php echo e(isset($seo_title) ? $seo_title : ""); ?>-ZI 你愛的生活品味" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="<?php echo e(Request::url()); ?>" />
    <meta property="og:image" content="<?php echo e(isset($seo_img) ? $seo_img : ""); ?>" />
    <meta property="og:description" content="<?php echo e(isset($seo_desc) ? $seo_desc : "ZI 你愛的生活品味"); ?>" />
    <meta property="og:site_name" content="ZI 你愛的生活品味" />
    <meta property="article:published_time" content="2018-04-15T05:59:00+01:00" />
    <meta property="article:modified_time" content="2018-04-15T05:59:00+01:00" />
    <meta property="article:section" content="<?php echo e(isset($seo_section) ? $seo_section : "ZI 你愛的生活品味"); ?>" />
    <meta property="article:tag" content="<?php echo e(isset($seo_tag) ? $seo_tag : "ZI 你愛的生活品味"); ?>" />
    <meta property="fb:admins" content="100000307128936" />
    <link rel="shortcut icon" href="<?php echo e(url('assets/images/zi.png')); ?>">
    <?php echo $__env->yieldContent('after_style'); ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>

    <script>
        var BASE_URL = "<?php echo e(url('/')); ?>";
    </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-117435537-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-117435537-1');

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    </script>

</head>

<body>
    <?php echo $__env->yieldContent('before_scripts'); ?>
        
    <?php echo $__env->yieldContent('header'); ?>
    
    <?php echo $__env->yieldContent('content'); ?>

    <?php echo $__env->yieldContent('footer'); ?>
        
    <script type="text/javascript" src="<?php echo e(url('assets/js/common.js')); ?>"></script>
    <?php echo $__env->yieldContent('after_scripts'); ?>
    
</body>
</html>
