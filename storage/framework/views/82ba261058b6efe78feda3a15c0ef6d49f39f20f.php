<?php $__env->startSection('after_style'); ?>
<link rel="stylesheet" href="<?php echo e(url('assets/css/reset.css')); ?>" type="text/css" media="screen">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo e(url('assets/libs/bootstrap-3.3.7.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(url('assets/css/style.css')); ?>" type="text/css" media="screen">
    <link rel="stylesheet" href="<?php echo e(url('assets/css/process.css?t=989343')); ?>" type="text/css" media="screen">
    
    <script type="text/javascript" src="<?php echo e(url('assets/libs/jquery-3.3.1.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/libs/tether.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/libs/bootstrap-3.3.7.min.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('header'); ?>
    <?php echo $__env->make('FrontEnd.layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="breadcrumbs-box">
    <div class="content-box">
        <div class="name">租借流程</div>
        <div class="breadcrumbs">
        <a href="<?php echo e(url('/')); ?>">美z.人生</a> &gt; 租借流程
        </div>
    </div>            
</div>

<div id="content-box" class="web">
    <button class="btn btn-link detail-1" data-toggle="modal" data-target="#modal1">詳細內容</button>
    <button class="btn btn-link detail-2" data-toggle="modal" data-target="#modal2">詳細內容</button>
    <button class="btn btn-link detail-3" data-toggle="modal" data-target="#modal3">詳細內容</button>
    <button class="btn btn-link detail-3-get" data-toggle="modal" data-target="#modal3-get">取機機場櫃台</button>
    <button class="btn btn-link detail-4" data-toggle="modal" data-target="#modal4">詳細內容</button>
    <button class="btn btn-link detail-5" data-toggle="modal" data-target="#modal5">詳細內容</button>
    <button class="btn btn-link detail-5-set" data-toggle="modal" data-target="#modal3-get">還機機場櫃台</button>
</div>

<div id="content-box" class="mobile">
    <button class="btn btn-link detail-1" data-toggle="modal" data-target="#modal1">詳細內容</button>
    <button class="btn btn-link detail-2" data-toggle="modal" data-target="#modal2">詳細內容</button>
    <button class="btn btn-link detail-3" data-toggle="modal" data-target="#modal3">詳細內容</button>
    <button class="btn btn-link detail-3-get" data-toggle="modal" data-target="#modal3-get">取機機場櫃台</button>
    <button class="btn btn-link detail-4" data-toggle="modal" data-target="#modal4">詳細內容</button>
    <button class="btn btn-link detail-5" data-toggle="modal" data-target="#modal5">詳細內容</button>
    <button class="btn btn-link detail-5-set" data-toggle="modal" data-target="#modal3-get">還機機場櫃台</button>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('after_scripts'); ?>

    <!-- Modal 1 -->
    <div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">            
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h5 class="modal-title" id="exampleModalLongTitle">預約申請</h5>
          </div>
          <div class="modal-body">
            <table>
                <tr>
                    <td class="icon-box"><img src="./assets/images/process/1.png"></td>
                    <td class="desc">
                        <div class="title">網路預約服務</div>
                        <div class="content">
                            <p>
                            漫遊世界，美z.您的人生，我們提供24小時網路流暢的出國WIFI機預約環境。
                            目前我們2018年全新機種一機可達漫遊多國，但是依選擇國家，可以自由選擇費率。
                            </p>
                            <p>
                            提醒您預約機器最晚需提前三天預約，並可於桃園機場一、二航廈的服務櫃台取機，或是宅配到府、甚至選取店到店。
                            </p>
                            <p class="note">
                            註一、請特別留意要去的國家，雖然我們機型可以全球通用，但網路會依您所選擇的設定國家開通服務。
                            </p>
                            <p class="note">
                            註二、如您帶著我們的機器身在日本，但是一時興起認為玩不夠，想直接再去歐洲、美洲，甚至是地球上我們有支援的國家，請前一天直接連絡線上客服開通，直接線上付款，輕鬆搞定您的小任性。
                            </p>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="icon-box">
                        <img src="./assets/images/process/2.png">
                        <img src="./assets/images/process/3.png">
                    </td>
                    <td class="desc">
                        <div class="title">機場當日租借</div>
                        <div class="content">
                            <p>
                            美z.人生可以提供旅客急單處理，讓你直接在桃園機場櫃檯內租借機器。但是由於庫存量無法即時掌握，請要過去前，最好先行聯絡美z.人生粉絲團、Line@、Wechat官方帳號，詳細詢問。
                            </p>
                            <p>
                            建議先提早預約，以免熱門旅遊時段庫存不足。
                            </p>
                        </div>
                    </td>
                </tr>
            </table>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">確認</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 2 -->
    <div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">            
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h5 class="modal-title" id="">付款</h5>
          </div>
          <div class="modal-body">
            <table>
                <tr>
                    <td class="icon-box"><img src="./assets/images/process/4.png"></td>
                    <td class="desc">
                        <div class="title">信用卡</div>
                        <div class="content">
                            <p>
                            出國WIFI機可於機場櫃台、台中門市租借，也可使用信用卡付款，輕鬆方便。<br />可使用以下信用卡「VISA」、「MASTER」、「JCB」。 <span class="red">註:不支援大來卡、美國運通卡。</span>
                            </p>
                            <p>
                            刷卡金額為出國天數租金與機器安心險(選配 <a href="https://www.z-trip.com/uploads/image/%E5%AE%89%E5%BF%83%E9%9A%AA%E8%A1%A8%E6%A0%BC.jpg" target="_blank">點我看說明</a>)，由於機器都是今年最新款，請用生命與愛保護它。
                            </p>
                            <p>統一發票將在你刷卡同時與E-mail同時寄出電子發票，請使用載具登錄。</p>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="icon-box">
                        <img src="./assets/images/process/5.png">
                    </td>
                    <td class="desc">
                        <div class="title">現金</div>
                        <div class="content">
                            <p>
                            機場櫃台取機不使用現金交易方式，但您可以線上直接選擇7-11或全家代碼、條碼現金繳款。
                            </p>
                            <p>
                            如您出國當日急需機場取機服務，請直接私訊美z.人生粉絲團或是Wechat、Line@，急件處理會加收處理費用100元整，
                            </p>
                            <p>如出國當日急件又臨時取消，需加收400元行政處理費用，以保障我們的完美服務品質，敬請見諒。</p>
                        </div>
                    </td>
                </tr>
            </table>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">確認</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 3 -->
    <div class="modal fade" id="modal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">            
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h5 class="modal-title" id="">取機</h5>
          </div>
          <div class="modal-body">
            <table>
                <tr>
                    <td class="icon-box">
                        <img src="./assets/images/process/2.png">
                        <img src="./assets/images/process/3.png">
                    </td>
                    <td class="desc">
                        <div class="title">預約取機與還機</div>
                        <div class="content">
                            <p>機器將於您出國前2或1日配送到您指定的全省便利商店，請於各便利商店的營業時間內前往取機。</p>
                            <p>隨機器內會附上宅配歸還貨運單，請您回國隔日下午三點前至全省便利超商寄出，無須再付運費給便利超商店員。</p>
                            <p>機器歸還請注意WIFI機與所有配件的完整，包裝好後，請直接貼上我們隨貨付的宅配單交給全省便利超商櫃台人員即可，謝謝。</p>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="icon-box">
                        <img src="./assets/images/process/7.png">
                    </td>
                    <td class="desc">
                        <div class="title">宅配到府服務</div>
                        <div class="content">
                            <p>本公司宅配通常會在貨運上班時間的早上9點到晚上7點(含下午)送達您府上。如您府上屬偏遠地區，請自行查詢注意貨運到貨時間，實際以貨運公司為主。</p>
                            <p>由於網站初期我們配合的貨運公司有三家，新竹物流、黑貓宅急便、台灣宅配通，
                            如想提前知道貨運單號，也請您直接私訊美z.人生粉絲團、美z.人生官方Line@、WeChat即時詢問，謝謝。</p>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="icon-box">
                        <img src="./assets/images/process/8.png">
                    </td>
                    <td class="desc">
                        <div class="title">超商店到店</div>
                        <div class="content">
                            <p>本公司使用全省7-11超商與全家便利商店取貨，機器通常會在您下單完畢之後自動通知你到貨，並在出國前一日送達指定的超商門市。實際送達配貨時間以物流業者為主。</p>
                            <p>由於機器內含鋰電池所以無法空運，目前宅配服務僅限於台灣本島，不包含澎湖、金、馬地區。某些特殊門市不提供配送服務，下單店到店服務時請詳細確認所選門市是否有提供該項服務，美z.人生旅遊網將不另行告知。</p>
                            <p><a href="http://www.family.com.tw/Marketing/Integration/Default.aspx?ID=168" target="_blank">【全家特殊門市查詢系統】</a></p>
                            <p><a href="https://www.7-11.com.tw/form/180326.pdf" target="_blank">【7-11無保證二日到店查詢系統】</a></p>
                            <p>若遇天災不可抗拒之因素以及部分門市因週末不營業或其他特殊原因，不在保證二日到貨承諾範圍，詳情請參考保證二日到店排外門市名單。<br>由於系統作業關係，選擇店到店取機服務，若需修改取機時間或是取消，請最晚於出國4天前告知我們處理，商品寄出即視為服務成立。<br>若您於指定到貨時間無法取機或是剛好出國行程取消，我們由於機器流量已經開通，定會視同這筆訂單成立，敬請見諒。</p>
                            <p>取貨方請於您指定的全台灣便利商店出示您的姓名或可證明的身分證件，始可領取完成此次交易。<br>【台灣(含)身分證、健保卡、個人駕照，本國護照，其一即可。】</p>
                        </div>
                    </td>
                </tr>
            </table>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">確認</button>
          </div>
        </div>
      </div>
    </div>


     <!-- Modal 3 Get -->
     <div class="modal fade" id="modal3-get" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">            
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h5 class="modal-title" id="">取/還機機場櫃台</h5>
          </div>
          <div class="modal-body">
            <div class="row">
                <div class="col-xs-12">
                
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                <table class="table-responsive" style="table-layout:fixed; border:none">
                    <tr>
                        <td class="desc">
                            <div class="title">桃園機場 第一航廈</div>
                            <div class="content">
                                <p class="title">領取提示</p>
                                <p>接機大廳 >> 北會面點 >> 順著大廳走到底端 >> 右側玻璃窗前分享器預約提領2號櫃檯</p>
                                <p class="title">歸還提示</p>
                                <p>領完行李 >> 出電動門後左邊順著大廳走到底 >> 右側玻璃窗前分享器預約提領2號櫃檯</p>
                                <p> <img src="./assets/images/process/where-1.jpg" class="img-responsive"></p>
                                <p> <img src="./assets/images/process/where-1-1.jpg" class="img-responsive"></p>
                                <p>【營業時間】週一～週日　06:00~24:00</p>
                                <p>【服務項目】取機、還機</p>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="desc"  >
                            <div class="title">桃園機場 第二航廈</div>
                            <div class="content">
                                <p class="title">領取提示</p>
                                <p>接機大廳 >> 北會面點柱子到底右轉 >> 順著搭乘巴士指示牌走 >> 分享器預約提領4號櫃檯 </p>
                                <p class="title">歸還提示</p>
                                <p>領完行李 >> 出電動門後右邊順著大廳走到底 >> 右轉通道跟著搭乘巴士指示牌走 >> 分享器預約提領4號櫃檯</p>
                                <p> <img src="./assets/images/process/where-2.jpg" class="img-responsive"></p>
                                <p> <img src="./assets/images/process/where-2-1.jpg" class="img-responsive"></p>
                                <p>【營業時間】週一～週日　06:00~24:00</p>
                                <p>【服務項目】取機、還機</p>
                            </div>
                        </td>
                    </tr>
                </table>
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">確認</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 4 -->
    <div class="modal fade" id="modal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">            
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h5 class="modal-title" id="">使用中</h5>
          </div>
          <div class="modal-body">
            <table>
                <tr>
                    <td class="icon-box"><img src="./assets/images/process/9.png"></td>
                    <td class="desc">
                        <div class="title">出國使用遇上問題</div>
                        <div class="content">
                            <p>如您在國外連線上網遇上使用問題，請到達下榻飯店後，先行使用飯店網路直接連絡
                            <a style="color:blue;" target="_blank" href="https://www.facebook.com/Beautyz.net/?hc_ref=ARTidGfdmfADGNZsSGkXBnkTvgo7GNlwd4AIdE1HKwkN0RXX4Ys4lAqB3FAJxxiq8pk&fref=nf">美z.人生粉絲團</a><a>、</a><a style="color:blue;" target="_blank" href="https://line.me/R/ti/p/FMPomkRiPU">官方Line@</a>、中國請使用WeChat詢問 <img style="border: 5px solid white" src="https://www.z-trip.com/uploads/image/weChatQR.jpg" alt="WeChatQRCode">。</p>
                            <p>我們提供全年無休客戶即時線上處理服務，我們會幫忙盡快協助您排除釐清問題。</p>
                        </div>
                    </td>
                </tr>
            </table>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">確認</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 5 -->
    <div class="modal fade" id="modal5" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">            
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h5 class="modal-title" id="">還機</h5>
          </div>
          <div class="modal-body">
            <table>
                <tr>
                    <td class="icon-box">
                        <img src="./assets/images/process/2.png">
                        <img src="./assets/images/process/3.png">
                    </td>
                    <td class="desc">
                        <div class="title">歸還服務時注意事項</div>
                        <div class="content">
                            <p>請於機場櫃檯門市營業時間內還機，如回國時間為機場櫃台下班時間，請直接投遞還機箱即可。</p>
                            <p>註:如您當初訂購下單時選擇為宅配歸還，也請回國後隔天下午三點前將機器拿到全省便利超商刷條碼寄回即可。</p>
                            <p class="red">回國隔日為最後還機日，超過一日將會跟您收取逾期費用，敬請留意。</p>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="icon-box">
                        <img src="./assets/images/process/11.png">
                    </td>
                    <td class="desc">
                        <div class="title">機場還機箱服務</div>
                        <div class="content">
                            <p>回國後，如已超過機場櫃台營業時間，可將機器直接投遞還機箱即可。
                            <br>機場櫃台服務時間為早上06:00~晚上24:00整。</p>
                            <p>註:機器投遞前請再次確認配件完整，或是個人隨身物品是否有放置在內，
                            如遇個人物品遺失在機器內並順利找回，需要寄件回去，每筆處理行政費用將收500元整(含運費)。</p>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="icon-box">
                        <img src="./assets/images/process/10.png">
                    </td>
                    <td class="desc">
                        <div class="title">寄送還機</div>
                        <div class="content">
                            <p>敬請回國隔日下午三點前歸還，宅配單已在您收到機器當時隨付在內。</p>
                            <p>再次提醒，機器可拿到全省便利超商刷條碼寄回即可。</p>
                            <p>希望這次的旅程我們的機器為您帶來愉快的使用狀況，並且豐富您的生活。</p>
                            <p><img src="./assets/images/slogan.png" width="200"></p>
                        </div>
                    </td>
                </tr>
            </table>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">確認</button>
          </div>
        </div>
      </div>
    </div>
  <script type="text/javascript" src="<?php echo e(url('assets/js/process.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer'); ?>
    <?php echo $__env->make('FrontEnd.layouts.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('FrontEnd.layout',[
	"seo_title" => "租借流程",
	"seo_desc" => preg_replace("/\n+/","",strip_tags("租借流程")),
	"seo_img" => null
], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>