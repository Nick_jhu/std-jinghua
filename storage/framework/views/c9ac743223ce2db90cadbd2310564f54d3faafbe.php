 
<?php $__env->startSection('header'); ?>
<section class="content-header">
	<h1>
	訂單編輯<small></small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(url(config('backpack.base.route_prefix'),'OrderMgmt')); ?>">訂單總覽</a></li>
		<li class="active">訂單編輯</li>
	</ol>
</section>
<?php $__env->stopSection(); ?> 

<?php $__env->startSection('content'); ?>
	<?php echo $__env->make('backpack::template.toolbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	<div class="row">
		<div class="col-md-12">
			<div class="callout callout-danger" id="errorMsg" style="display:none">
				<h4><?php echo e(trans('backpack::crud.please_fix')); ?></h4>
				<ul>

				</ul>
			</div>
			<form method="POST" accept-charset="UTF-8" id="mainForm" enctype="multipart/form-data">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">基本資料</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="tab_1">
							<div class="box-body">
								<div class="row">
									<div class="form-group col-md-3">
										<label for="ord_no">訂單號</label>
										<input type="text" class="form-control" id="ord_no" name="ord_no">
                                    </div>
                                    <div class="form-group col-md-3">
										<label for="status">狀態</label>
										<select class="form-control" id="status" name="status">
                                            <option value="A">已下單未付款</option>
                                            <option value="B">已付款</option>
                                            <option value="C">出貨</option>
                                            <option value="D">送達</option>
                                            <option value="E">歸還</option>
                                            <option value="F">通知取消</option>
                                            <option value="G">確認取消</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
										<label for="pay_way">付款方式</label>
										<select class="form-control" id="pay_way" name="pay_way">
                                            <option value="Credit">信用卡</option>
                                            <option value="CSV">超商付款</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
										<label for="ship_way">物流方式</label>
										<select class="form-control" id="ship_way" name="ship_way">
                                        </select>
                                    </div>
								</div>

                                <div class="row">
									<div class="form-group col-md-3">
										<label for="ord_nm">訂購人</label>
										<input type="text" class="form-control" id="ord_nm" name="ord_nm">
									</div>
                                    <div class="form-group col-md-3">
                                        <label for="dlv_nm">收貨人</label>
                                        <input type="text" class="form-control" id="dlv_nm" name="dlv_nm">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="dlv_zip">郵地區號</label>
                                        <input type="text" class="form-control" id="dlv_zip" name="dlv_zip">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="dlv_city">城市</label>
                                        <input type="text" class="form-control" id="dlv_city" name="dlv_city">
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="dlv_area">區域</label>
                                        <input type="text" class="form-control" id="dlv_area" name="dlv_area">
                                    </div>
                                    <div class="form-group col-md-9">
                                        <label for="dlv_addr">地址</label>
                                        <input type="text" class="form-control" id="dlv_addr" name="dlv_addr">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-3">
										<label for="discount_no">促俏碼</label>
										<input type="number" class="form-control" id="discount_no" name="discount_no">
									</div>
									<div class="form-group col-md-3">
										<label for="o_price">原價</label>
										<input type="number" class="form-control" id="o_price" name="o_price">
									</div>
									<div class="form-group col-md-3">
										<label for="d_price">特價</label>
										<input type="number" class="form-control" id="d_price" name="d_price">
                                    </div>
                                    <div class="form-group col-md-3">
										<label for="ship_fee">運費</label>
										<input type="number" class="form-control" id="ship_fee" name="ship_fee">
									</div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-3">
										<label for="pay_no">支付號</label>
										<input type="text" class="form-control" id="pay_no" name="pay_no">
                                    </div>
                                    <div class="form-group col-md-3">
										<label for="delay_pay_no">展延支付號</label>
										<input type="text" class="form-control" id="delay_pay_no" name="delay_pay_no">
                                    </div>
                                    <div class="form-group col-md-3">
										<label for="ship_no">物流號</label>
										<input type="text" class="form-control" id="ship_no" name="ship_no">
									</div>
                                </div>

								<?php if(isset($id)): ?>
								<input type="hidden" name="id" value="<?php echo e($id); ?>" class="form-control">
								<input type="hidden" name="_method" value="PUT" class="form-control"> 
								<?php endif; ?>

							</div>
                        </div>
					</div>
				</div>
			</form>
		</div>	
    </div>
    
    <div class="row">

        <div class="col-md-12">
            <div class="nav-tabs-custom" <?php if(!isset($id)): ?> style="display:none" <?php endif; ?> id="subPanel">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#tab_2" data-toggle="tab" aria-expanded="false">訂單租用明細</a>
                    </li>
                    <li>
                        <a href="#tab_3" data-toggle="tab" aria-expanded="false">訂單明細</a>
                    </li>
                </ul>
                <div class="tab-content">

                    <div class="tab-pane active" id="tab_2">
                        <div class="box box-primary" id="subBox" style="display:none">
                            <div class="box-header with-border">
                            <h3 class="box-title">租用明細</h3>
                            </div>
                            <form method="POST"  accept-charset="UTF-8" id="subForm" enctype="multipart/form-data">
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="row">

                                </div>
                            </div>
                            <!-- /.box-body -->
    
                            <div class="box-footer">
                                <input type="hidden" class="form-control input-sm" name="id" grid="true" >
                                <button type="button" class="btn btn-sm btn-primary" id="Save"><?php echo e(trans('common.save')); ?></button>
                                <button type="button" class="btn btn-sm btn-danger" id="Cancel"><?php echo e(trans('common.cancel')); ?></button>
                            </div>
                            </button>
                        </div>
                        
                        <div id="jqxGrid"></div>
                    </div>

                    <div class="tab-pane" id="tab_3">
                        <div class="box box-primary" id="subBox" style="display:none">
                            <div class="box-header with-border">
                                <h3 class="box-title">購買明細</h3>
                            </div>
                            <form method="POST"  accept-charset="UTF-8" id="subForm1" enctype="multipart/form-data">
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="row">

                                </div>
                            </div>
                            <!-- /.box-body -->
    
                            <div class="box-footer">
                                <input type="hidden" class="form-control input-sm" name="id" grid="true" >
                                <button type="button" class="btn btn-sm btn-primary" id="Save1"><?php echo e(trans('common.save')); ?></button>
                                <button type="button" class="btn btn-sm btn-danger" id="Cancel1"><?php echo e(trans('common.cancel')); ?></button>
                            </div>
                            </button>
                        </div>
                        
                        <div id="jqxGrid1"></div>
                    </div>

    
                </div>

            </div>
        </div>
        
    </div>
<?php $__env->stopSection(); ?> 
<?php echo $__env->make('backpack::template.lookup', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<?php $__env->startSection('after_scripts'); ?>
<script src="<?php echo e(asset('vendor/backpack/ckeditor/ckeditor.js')); ?>"></script>
<script src="<?php echo e(asset('vendor/backpack/ckeditor/adapters/jquery.js')); ?>"></script>
	<script>
		var mainId = "";
		var editData = null;
		var editObj = null;
		var SAVE_URL = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/OrderMgmt')); ?>";

		var fieldData = null;
        var fieldObj = null;
        
        var imgPath = "<?php echo e(Storage::url('/')); ?>";

		<?php if(isset($crud -> create_fields)): ?>
		fieldData = '{<?php echo json_encode($crud->create_fields); ?>}';
		fieldData = fieldData.substring(1);
		fieldData = fieldData.substring(0, fieldData.length - 1);
		fieldObj = JSON.parse(fieldData);
		<?php endif; ?>

		<?php if(isset($id)): ?>
		mainId   = "<?php echo e($id); ?>";
		editData = '{<?php echo $entry; ?>}';
		editData = editData.substring(1);
		editData = editData.substring(0, editData.length - 1);
		var objJson = editData.replace(/(?:\r\n|\r|\n)/g, '<br />');
		editObj  = JSON.parse(objJson);
		<?php endif; ?>

		$(function () {
			//var formOpt = {};
			formOpt.formId = "mainForm";
			formOpt.editObj = editObj;
			formOpt.fieldObj = fieldObj;
			formOpt.editUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/OrderMgmt')); ?>";
			formOpt.fieldsUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') .'/get/mod_order')); ?>/" + mainId;
			formOpt.saveUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/OrderMgmt')); ?>";
			formOpt.afterInit = function() {
				if(editObj != null) {
                    
				}
			}

			formOpt.initFieldCustomFunc = function () {

			};

			formOpt.afterDel = function() {

			}

			formOpt.addFunc = function() {
                $('#jqxGrid').jqxGrid('clear');
                $('#jqxGrid1').jqxGrid('clear');
                
			}

			formOpt.editFunc = function() {

                
			}

			formOpt.copyFunc = function() {
                $('#jqxGrid').jqxGrid('clear');
                $('#jqxGrid1').jqxGrid('clear');
            }

			formOpt.saveSuccessFunc = function(data) {
                var result = data.data;
			}

			var btnGroup = [
				{
					btnId: "btnConfirmCancel",
					btnIcon: "fa fa-check-circle",
					btnText: "取消確認",
					btnFunc: function () {
						if(mainId != null) {
                            $.get(BASE_URL+'/confirmOrderCancel/'+mainId, {}, function(data){
                                if(data.message == "success") {
                                    $("#status").val("G");
                                    alert("確認完成");
                                }
                            });
                        }
					}
				}
			];

			initBtn(btnGroup);
            
            var col = [
                [
                    {name: "id", type: "number"},
                    {name: "prod_nm", type: "string"},
                    {name: "f_day", type: "string"},
                    {name: "e_day", type: "string"},
                    {name: "extension", type: "string"},
                    {name: "use_day", type: "number"},
                    {name: "num", type: "number"},
                    {name: "unit_price", type: "number"},
                    {name: "insurance_price", type: "number"},
                    {name: "amt", type: "number"},
                ],
                [
                    {text: "id", datafield: "id", width: 100, hidden: true},
                    {text: "名稱", datafield: "prod_nm", width: 130},
                    {text: "日期(起)", datafield: "f_day", width: 150},
                    {text: "日期(迄)", datafield: "e_day", width: 150},
                    {text: "是否展延", datafield: "extension", width: 150},
                    {text: "天數", datafield: "use_day", width: 150},
                    {text: "數量", datafield: "num", width: 150},
                    {text: "單價", datafield: "unit_price", width: 150},
                    {text: "保險金", datafield: "insurance_price", width: 150},
                    {text: "總價", datafield: "amt", width: 150}
                ]
            ];
            var opt = {};
            opt.gridId = "jqxGrid";
            opt.fieldData = col;
            opt.formId = "subForm";
            opt.saveId = "Save";
            opt.cancelId = "Cancel";
            opt.showBoxId = "subBox";
            opt.height = 300;
            opt.getUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin').'/orderRentDetail')); ?>" + '/' + mainId;
            opt.addUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/orderRentDetail')); ?>" + "/store";
            opt.updateUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/orderRentDetail')); ?>" + "/update/"+ mainId;
            opt.delUrl  = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/orderRentDetail')); ?>" + "/delete/";
            opt.defaultKey = {'prod_id': mainId};
            opt.showaggregates = false;
            opt.commonBtn = true;
            opt.inlineEdit = false;
            opt.showtoolbar = true;
            opt.rowsheight = 60;
            opt.beforeSave = function(formData) {

            }

            opt.afterSave = function(data) {
                console.log(data);
            }
            genDetailGrid(opt);


            var col = [
                [
                    {name: "id", type: "number"},
                    {name: "prod_nm", type: "string"},
                    {name: "num", type: "number"},
                    {name: "unit_price", type: "number"},
                    {name: "insurance_price", type: "number"},
                    {name: "amt", type: "number"},
                ],
                [
                    {text: "id", datafield: "id", width: 100, hidden: true},
                    {text: "名稱", datafield: "prod_nm", width: 130},
                    {text: "數量", datafield: "num", width: 150},
                    {text: "單價", datafield: "unit_price", width: 150},
                    {text: "保險金", datafield: "insurance_price", width: 150},
                    {text: "總價", datafield: "amt", width: 150}
                ]
            ];
            var opt = {};
            opt.gridId = "jqxGrid1";
            opt.fieldData = col;
            opt.formId = "subForm1";
            opt.saveId = "Save";
            opt.cancelId = "Cancel1";
            opt.showBoxId = "subBox1";
            opt.height = 300;
            opt.getUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin').'/orderDetail')); ?>" + '/' + mainId;
            opt.addUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/orderDetail')); ?>" + "/store";
            opt.updateUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/orderDetail')); ?>" + "/update/"+ mainId;
            opt.delUrl  = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/orderDetail')); ?>" + "/delete/";
            opt.defaultKey = {'prod_id': mainId};
            opt.showaggregates = false;
            opt.commonBtn = true;
            opt.inlineEdit = false;
            opt.showtoolbar = true;
            opt.rowsheight = 60;
            opt.beforeSave = function(formData) {

            }

            opt.afterSave = function(data) {
                console.log(data);
            }
            genDetailGrid(opt);

            
		});
	</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('backpack::layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>