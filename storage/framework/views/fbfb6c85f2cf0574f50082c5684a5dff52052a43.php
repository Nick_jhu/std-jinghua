<?php $__env->startSection('after_style'); ?>
    <link href="<?php echo e(url('assets/css/bootstrap.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?php echo e(url('assets/fonts/FontAwesome/font-awesome.css')); ?>">
    
    
    <link rel="stylesheet" href="<?php echo e(url('assets/css/bootsnav.css?')); ?>" type="text/css" media="screen">
    <link rel="stylesheet" href="<?php echo e(url('assets/css/newstyle.css')); ?>" type="text/css" media="screen">
    <link href="<?php echo e(url('assets/css/owl.carousel.min.css')); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo e(url('assets/css/owl.theme.default.min.css')); ?>" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?php echo e(url('assets/css/rwd.css')); ?>" type="text/css" media="screen">

    <script type="text/javascript" src="<?php echo e(url('assets/libs/jquery-3.3.1.min.js')); ?>"></script>
    <script src="<?php echo e(url('assets/js/bootstrap.min.js')); ?>"></script>
    <script src="<?php echo e(url('assets/js/am-pagination.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('header'); ?>
    <?php echo $__env->make('FrontEnd.layouts.newHeader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="index-banner owl-carousel owl-theme" id="index-banner" style="max-height: 420px">
    <?php $__currentLoopData = $bannerData; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <div class="item banner-overlay">
        <div class="index-banner_text text-<?php echo e($row->position); ?>">
            <?php echo $row->descp; ?>

            <?php if(isset($row->link)): ?>
            <a class="btn" href="<?php echo e($row->link); ?>"><?php echo e($row->btn_text); ?></a>
            <?php endif; ?>
        </div>
        <a href="<?php echo e($row->link); ?>">
            <img class="banner-img" src="<?php echo e(url('storage/'.$row->image)); ?>" />
        </a>
    </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</div>
<section class="section" style="padding: 20px 0 40px">
    <div class="container">
        <h2 class="section-title text-center">全球各國閃購商品<span> 精選商品，限量限時折扣</span></h2>
        <div id="full" class="carousel slide sale-slider" data-ride="carousel">
            <div class="carousel-inner">
                <div class="row item active responsive-slider-col-6">
                    <?php $__currentLoopData = $flashingProdData; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <div class="pro-s1-item">
                            <a href="<?php echo e(url('productDetail/'.$row->id)); ?>">
                                <h4 class="text-center"><?php echo e($row->title); ?></h4>
                                <div class="pro-img-box">
                                    <div class="cover"></div><img src="<?php echo e(Storage::url($row->img1)); ?>">
                                </div>
                                <div class="index-pro01-footer">
                                    <h6 class="pro-money"><span>TWD</span> <?php echo e(number_format($row->f_price)); ?></h6>
                                    <span class="last-time"><?php echo e(mb_substr($row->action_date, 0, 10, 'UTF-8')); ?> 結束</span>
                                </div>
                                <div class="pro-s1-tag"><?php echo e($row->d_percent); ?>% OFF</div>
                            </a>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container index-blog">
        <div class="row responsive-slider-col-3">
            <?php $__currentLoopData = $unboxingData; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="col-sm-4 col-xs-6">
                <div class="blog-item">
                    <a href="<?php echo e($row->link); ?>" target="_blank">
                        <img src="<?php echo e(Storage::url($row->image)); ?>" style="height: 218px; width: 413px;">
                        <div class="blog-list-info">
                            <h5><?php echo e($row->title); ?></h5>
                            <p class="readmore">查看文章 </p>
                        </div>
                    </a>
                </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
</section>
<section>
    <div class="container product-list index_product-list">
        <h2 class="section-title text-center">全館精選商品推薦<span> 本館熱銷各類產品,與熱門品牌,優惠推薦</span></h2>

        <div class="row">
            <?php $__currentLoopData = $focusProdData; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="col-md-3 col-sm-4 col-xs-6 product-item">
                <a href="<?php echo e(url('productDetail').'/'.$row->id); ?>">
                    <div class="pro-img-box">
                        <div class="cover"></div>
                        <img src="<?php echo e(Storage::url($row->img1)); ?>">
                    </div>
                    <h6 class="pro-list-name"><?php echo e($row->title); ?></h6>
                    <h6 class="pro-money"><span>TWD</span> <?php echo e(number_format($row->f_price)); ?></h6>
                </a>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <div class="col-md-12 text-center">
                <a class="btn" href="<?php echo e(url('product')); ?>">查看全部商品</a>
            </div>
        </div>
    </div>
</section>
<section>
    <?php if(isset($bottomAds)): ?>
    <div class="container index-banner_footer">
        <div class="index-banner_foote-text text-<?php echo e($bottomAds->position); ?>">
            <?php echo $bottomAds->descp; ?>

            <?php if(isset($bottomAds->link)): ?>
                <a class="btn" href="<?php echo e($bottomAds->link); ?>">馬上搶購</a>
            <?php endif; ?>
        </div>
        <a href="<?php echo e($bottomAds->link); ?>"><img src="<?php echo e(url('storage/'.$bottomAds->image)); ?>"></a>
    </div>
    <?php endif; ?>
    <!--

    //文字在中
    <div class="container index-banner_footer">
        <div class="index-banner_foote-text text-center">
            <h2>義大利 行動電源</h2>
            <h3>極俐落，都會男子的充電日常</h3>
            <a class="btn" href="#">馬上搶購</a>
        </div>
        <a href="#"><img src="images/demo/index_banner_footer.jpg"></a>
    </div>

    //文字在右
    <div class="container index-banner_footer">
        <div class="index-banner_foote-text text-right">
            <h2>義大利 行動電源</h2>
            <h3>極俐落，都會男子的充電日常</h3>
            <a class="btn" href="#">馬上搶購</a>
        </div>
        <a href="#"><img src="images/demo/index_banner_footer.jpg"></a>
    </div>
    -->
</section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer'); ?>
    <?php echo $__env->make('FrontEnd.layouts.newfooter', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('after_scripts'); ?>
    <script type="text/javascript" src="<?php echo e(url('assets/js/imagefill.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/owl.carousel.min.js')); ?>"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var owlOpt = $('#index-banner').owlCarousel({
                center: true,
                loop: true,
                autoplay: true,
                responsive: {
                    0: {
                        items: 1,
                        loop: true,
                        nav: false,
                        dots: true
                    },
                    992: {
                        items: 1,
                        nav: true,
                        autoWidth: true,
                        dots: false,
                        navText: ['<i class="fa fa-angle-left"></i>',
                            '<i class="fa fa-angle-right"></i>'
                        ],
                        navClass: ["banner-prev", "banner-next"]
                    }
                }
            });
            $("#index-banner").owlCarousel(owlOpt).find(".owl-nav").addClass("container");

            
        });
        $("body").addClass("index");
        $('.pro-img-box').imagefill();

        $('.responsive-slider-col-6').slick({
            dots: false,
            infinite: false,
            speed: 300,
            slidesToShow: 6,
            slidesToScroll: 6,
            autoplay: true,
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 4,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                    }
                },
                {
                    breakpoint: 640,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                }
            ]
        });
        $('.responsive-slider-col-3').slick({
            dots: true,
            infinite: false,
            speed: 300,
            slidesToShow: 3,
            slidesToScroll: 3,
            responsive: [{
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
        $('.responsive-slider-nav').slick({
            dots: false,
            infinite: true,
            speed: 300,
            slidesToShow: 4,
            slidesToScroll: 4,
            responsive: [{
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }]
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('FrontEnd.newlayout',[
	"seo_title" => "ZI 你愛的生活品味",
	"seo_desc" => preg_replace("/\n+/","",strip_tags("ZI 你愛的生活品味")),
	"seo_img" => url('assets/images/banner/2.jpg')
], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>