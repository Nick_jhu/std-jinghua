<div class="footer-box">
    <div class="top-box">
        <div class="content-box">
            
            <div class="l-box">
                <div class="logo">
                    <img src="<?php echo e(url('assets/images/logo-white.png')); ?>" width="200" />
                </div>
                <div class="info">
                    <?php echo nl2br($baseData->contract_us); ?>

                </div>
                <div class="social-box">
                    <ul>
                        <a href="https://www.facebook.com/Beautyz.net/" target="_blank"><li><i class="fab fa-facebook-f"></i></li></a>
                        <a href="https://twitter.com/beautyz_net" target="_blank"><li><i class="fab fa-twitter"></i></li></a>
                        <a href="https://line.me/R/ti/p/FMPomkRiPU" target="_blank" ><li><i class="fab fa-line"></i></li></a>
                        <!--<a href="#" target="_blank"><li><i class="fab fa-instagram"></i></li></a>-->
                    </ul>                            
                </div>
            </div>
            <div class="c-box">
                <div class="title">最新商品</div>
                <div class="news-box">
                    <ul>
                        <?php $__currentLoopData = $footerProdData; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <a href="<?php echo e(url('productDetail/'.$row->id)); ?>"><li><?php echo e($row->title); ?></li></a>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>                            
                </div>
            </div>
            <div class="r-box">
                <div class="t-box">
                    <div class="title">關於 BeautyZ-Trip</div>
                    <ul>
                        <a href="<?php echo e(url('about')); ?>"><li>認識哥哥</li></a>
                        <a href="https://www.messenger.com/login.php?next=https%3A%2F%2Fwww.messenger.com%2Ft%2FBeautyz.net" target="_blank"><li>聯絡我們</li></a>
                    </ul>
                </div>

                <div class="m-box">
                    <div class="title">客戶服務</div>
                    <ul>
                        <a href="<?php echo e(url('qa')); ?>"><li>常見問題</li></a>
                        <a href="<?php echo e(url('process')); ?>"><li>租借洽詢</li></a>
                    </ul>
                </div>

                <div class="b-box">
                    <div class="title">服務條款</div>
                    <ul>
                        <a href="#" data-toggle="modal" data-target="#selfModal"><li>自訂爽快條款</li></a>
                        <a href="#" data-toggle="modal" data-target="#policyModal"><li>隱私條款</li></a>
                    </ul>
                </div>
            </div>

        </div>
    </div>
    <div class="bottom-box"></div>
</div>
<!--安心險說明-->
<div class="modal fade" id="insuranceModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">安心險說明</h4>
            </div>
            <div class="modal-body">
                <img class="img-responsive" src="<?php echo e(url('uploads/image/安心險表格.jpg')); ?>" alt="">
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="policyModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="exampleModalLongTitle">隱私權政策</h4>
            </div>
            <div class="modal-body">
                <div class="title" style="color:#0081c8; font-size: 1.2em">服務條款</div>
                <div class="content">
                    <p>為了保障您的權益，請您務必詳細閱讀下列約定條款</p>
                    <p>
                        1、為確保完成所有的交易程序，您必須保證在本站登記的個人資料與事實相符，如有變動，應即時通知網站服務台修正或是自行至 會員專區 介面修改。
                    </p>
                    <p>
                        2、您所登錄之個人資料，除了應提供廠商相關送貨與結帳資訊，本站應負相關之保密義務，不會任意洩漏貨提供給第三人。
                    </p>
                    <p>
                    3、您應妥善保管在本站登記之帳號與密碼，所有使用該帳號登入系統之任何行為，本站皆視為帳號與密碼持有人之行為。
                    </p>
                    <p>
                        在下列情況下，本站有權查看或提供使用者的個人資料給有權機關、或主張其權利受侵害並提出適當證明之第三人： <br />
                        依法令規定、或依司法機關或其他有權機關的命令； <br />
                        <ul>
                            <li>
                            ※為執行本約定條款、或使用者違反約定條款； 
                            </li>
                            <li>
                            ※為維護本站系統之正常運作及安全； 
                            </li>
                            <li>
                            ※為保護本站、其他使用者、或其他第三人的合法權益；
                            </li>
                        </ul>
                    </p>
                    <P>
                    5、相關商品或服務之品質、保固及售後服務，由提供各該商品或服務的廠商負責，但本站承諾全力協助使用者解決關於因為線上消費所產生之疑問或爭議。
                    </P>
                    <p>
                    6、使用者一旦在本站進行線上消費，即表示願意購買該商品或服務並願遵守交易規則。使用者資料 ( 如地址、電話 ) 如有變更時，應立即上線修正其所留存之資料，且不得以資料不符為理由，否認其訂購行為或拒絕付款。 本站並保有接受您訂單與否的權利。
                    </p>
                    <p>
                    7、所有在本站所進行的線上消費，使用者應同意以本站所紀錄之電子交易資料為準，如有糾紛，並以該電子交易資料為認定標準。使用者如果發現交易資料不正確，應立即通知本站服務台。
                    </p>
                    <p>
                    8、本站在發生下列情形之一時，可以停止、中斷提供服務；停止或中斷服務時，原則上本站將先通知會員，惟緊急時刻不在此限： 
                    (1)對本站的電子通信設備，進行必要的保養及施工 <br />
                    (2)發生突發性的電子通信設備故障； 由於本站所申請的電子通信服務被停止，致使提供服務發生困難；由於天災等不可抗力之因素，致使本站無法提供服務。<br />
                    </p>
                    <p>
                    9、本站保留隨時修改本使用條款之權利，修改後的使用條款將公佈在本網站上，不另外個別通知使用者。使用者應同意遵守修改後之約定條款。
                    </p>
                    <p>
                    10、使用者應同意，本約定條款及所有在本站所進行的線上消費或交易，均以中華民國法令為準據法。因本約定條款所發生之糾紛，以台灣台中地方法院為第一審管轄。
                    </p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">確認</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="selfModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="exampleModalLongTitle">美z.人生網路刷卡購物自訂爽快條款</h4>
            </div>
            <div class="modal-body">
                <div class="title" style="color:#0081c8; font-size: 1.2em">創立美z.人生網路購物緣由:</div>
                <div class="content">
                    <p>哥哥做生意也已經快滿三年了，這三年間處理服務過的客人也算形形色色，但可能上輩子有燒好香，
所以幾乎沒有遇上很難搞的客人，為了讓大家熟悉我這邊的購物流程跟換貨過程，哥認為有必要親自寫一篇使用自己口吻語氣的服務條款。
這才是負責任的態度，我超不喜歡制式化，所有的服務背後就因為我們都是人，就該有人性化一點!</p>
                    <p>
                    1、因為我這邊除了出國租賃的事業，也會加上往後很多的電商產品，您也可以把這當作是一個看一堆狂物的購物網站，但是，我所有上架的物品，可能會比較稀少，至少這三年來都是這樣運作的。
產品很爛，我不賣，產品不屌，我不賣，產品功能性測試出來不到水平，我不賣，還有很多超龜毛的細節，只要真的無法入我的眼，我不賣就是不賣，為的就是至少可以站在第一線，為大家把關。
讓您所買回去的商品，可以達到在我心目中會有90分以上，用起來才會舒爽愉快。
                    </p>
                    <p>
                    2、創業三年，產品保固問題，一直是我心中最軟的那一塊，所佔的比例如果真要我說出來，您也會嚇到，所以我用以下文字來表達，這三年來，也已經有許多粉絲、使用者，顧客可以替我們的服務背書。
                    </p>
                    <p>
                    3、許多3C產品，哥哥這邊至少都會提供超過三個月內故障直接免費換新的服務，有些甚至超過半年，哥知道大家的時間都很寶貴，因為我也這樣認為，所以只要不是太頻繁，我也幾乎一定會答應。
                    </p>
                    <p>
                    4、呈第三條說明，因為我一開始就是一個玩家，所以我知道玩家最需要的服務是什麼，快速換貨，減少時間浪費，減少原廠官方制式打交道的往返，就是一個很狂的服務。
你們大家要的，就是有一個安心購物的平台，有一個可以直接抒發問題的管道，原廠這部分就讓我們來替你們打交道，這些，我們都知道，也會做，畢竟已經做了三年，還沒被大家玩倒，很是感謝。

                    </p>
                    <P>
                    5、相關商品或服務之品質、保固及售後服務，由提供各該商品或服務的廠商負責，但本站承諾全力協助使用者解決關於因為線上消費所產生之疑問或爭議。
                    </P>
                    <p>
                    5、因應物流費用一直上漲，我覺得我們有必要來好好討論一下我們的分配比例，當然，這三年來大家使用過我的服務，甚至超過，我都是沒再跟大家算的!
但為了持續維護好服務品質，並且可以拿來幫同事、員工們加薪，改天我們兩方自己看情況，該給您付出，大家就互相一下，不要推諉，謝謝。

                    </p>
                    <p>
                    6、哥哥這邊一共簽約黑貓宅急便、新竹貨運、宅配通三家貨運，為的就是服務好365天您的快速到貨需求，這三年來，只要是晚上八點以內的單，幾乎也都是隔天就可以讓你們收到了，
接下來當然也是，希望大家多多配合，不要超過九點還要哥哥幫您快速出貨，大家的孩子都還小，別亂操。

                    </p>
                    <p>
                    7、現在購物網站正式架好後，服務功能只會越來越多，越來越符合人性化，晚上八點前的單，都會在當天就配送出去，隔天中午前讓您收到，超過晚上八點後的單，則可以給您自己下單猶豫期。
只要系統還未鎖定出貨，這張訂單，您就可以自己到會員後台，親自了解它的生命，也順便斷了哥的生路，如果您這三年實在看我很賭爛，我很希望您這樣來玩我幾次，然後系統就會封鎖您了，
您的世界就會美z.人生，漫遊其他世界了，珍重再見。

                    </p>
                    <p>
                    8、看到這邊，很謝謝您，您還看得下去表示您很認同我，不要客氣，外面打個勾，您就可以等待貨物到達了。
                    </p>
                    <p>
                    10、其他想到的哥會再補上，有事沒事多私多健康，哥哥休息了，See you。
                    </p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">確認</button>
            </div>
        </div>
    </div>
</div>