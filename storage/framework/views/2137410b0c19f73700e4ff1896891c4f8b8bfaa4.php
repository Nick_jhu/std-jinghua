<?php $__env->startSection('after_style'); ?>
    <link rel="stylesheet" href="<?php echo e(url('assets/css/reset.css')); ?>" type="text/css" media="screen">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo e(url('assets/libs/bootstrap-3.3.7.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(url('assets/css/style.css')); ?>" type="text/css" media="screen">
    <link rel="stylesheet" href="<?php echo e(url('assets/css/cart.css')); ?>" type="text/css" media="screen">

    <script type="text/javascript" src="<?php echo e(url('assets/libs/jquery-3.3.1.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/libs/tether.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/libs/bootstrap-3.3.7.min.js')); ?>"></script>

    <?php if($errors->has('message')): ?>
    <script>
        var errorMsg = "<?php echo e($errors->get('message')[0]); ?>";
        alert(errorMsg);
	</script>
	<?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('header'); ?>
    <?php echo $__env->make('FrontEnd.layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<script>
    var dlvArea = "";
    <?php if($memberData->is_updated == 'Y'): ?>
     dlvArea = '<?php echo e($memberData->dlv_area); ?>';
    <?php endif; ?>
</script>

<?php $__env->startSection('content'); ?>
<div class="breadcrumbs-box">
    <div class="content-box">
        <div class="name">Basket</div>
        <div class="breadcrumbs">
            Start &gt; <a href="#">購物車</a>
        </div>
    </div>
</div>
<div id="content-box">
    <div class="title"><i class="fas fa-square red"></i> 租賃列表</div>
    <table class="table table-bordered">
        <thead>
            <tr>
                <!-- <th class="col-md-1 text-center" scope="col">國家</th> -->
                <th class="col-md-3 text-center" scope="col">商品</th>
                <th class="col-md-1 text-center" scope="col">日期(起)</th>
                <th class="col-md-1 text-center" scope="col">日期(迄)</th>
                <th class="col-md-1 text-center" scope="col">顯示天數</th>
                <!-- <th class="col-md-1 text-center" scope="col">結算天數</th> -->
                <th class="col-md-1 text-center" scope="col">單價</th>
                <th class="col-md-1 text-center" scope="col">數量</th>
                <th class="col-md-1 text-center" scope="col">金額</th>
                <th class="col-md-1 text-center" scope="col">取消</th>
            </tr>
        </thead>
        <tbody>
            <input type="hidden" id="cartStr" value="<?php echo e($cartStr); ?>">
            <?php $__currentLoopData = $cartData->rData; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr class="table-red">
                <!-- <th class="text-center" scope="row">日本</th> -->
                <td><a href="<?php echo e(url('productDetail/'.$row->prodId)); ?>"><?php if(isset($row->prodNm)): ?><?php echo e($row->prodNm); ?><?php endif; ?></a></td>
                <td class="text-center"><?php echo e($row->startDate); ?></td>
                <td class="text-center"><?php echo e($row->endDate); ?></td>
                <td colspan="2" class="colspan-3 text-center">
                    <table class="table">
                        <tr class="top-tr">
                            <td class="col-md-4"><?php echo e((((strtotime($row->endDate) - strtotime($row->startDate))/3600)/24) + 1); ?></td>
                            <!-- <td class="col-md-4">100</td> -->
                            <td class="col-md-4"><?php echo e($row->dPrice); ?></td>
                        </tr>
                        <tr class="bottom-tr">
                            <td colspan="3">
                                <div class="form-check">
                                    <input prodDetailID="<?php echo e($row->prodDetailId); ?>" uuid="<?php echo e($row->uuid); ?>" type="checkbox" class="form-check-input" name="insuranceCheckbox" day="<?php echo e((((strtotime($row->endDate) - strtotime($row->startDate))/3600)/24) + 1); ?>" num="<?php echo e($row->num); ?>" price="<?php echo e($row->amt); ?>" <?php if($row->insurance == "Y"): ?> checked <?php endif; ?>>
                                    <span class="red">安心險 NT 50/日</span>(<a href="#" data-toggle="modal" data-target="#insuranceModal">說明</a>)
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="text-center"><?php echo e($row->num); ?></td>
                <td class="text-center" prodDetailId="<?php echo e($row->prodDetailId); ?>" uuid="<?php echo e($row->uuid); ?>"><?php echo e(number_format($row->amt)); ?></td>
                <td class="text-center"><button class="btn btn-link cartDel" item-index="<?php echo e($key); ?>" uuid="<?php echo e($row->uuid); ?>" sell-type="R"><i class="fas fa-trash-alt"></i></button></td>
            </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
    </table>
    <!-- 合計 -->
    <table class="table total-box">
        <tr>
            <td class="col-md-8">
                <div class="form-row align-items-center">
                    <div class="col-md-5 padding-l-0">
                        <input type="text" class="form-control mb-2" id="rentDiscountCode" placeholder="請輸入優惠碼">
                    </div>
                    <div class="col-auto">
                        <!-- <button type="button" class="btn btn-primary mb-2" id="useRentDiscount">使用優惠碼</button> -->
                    </div>
                </div>
            </td>
            <td class="col-md-4 text-right">
                <h4>Cart Totals</h4>
                <h4>優 惠 折 扣： - $ <span id="rentDiscount">0</span> </h4>
                <h4>小計 $<span id="rSumAmt"><?php echo e(number_format($rSumAmt)); ?></span></h4>
                <input type="hidden" name="rSumAmt" value="<?php echo e($rSumAmt); ?>">
                <!-- <h4>總金額 $10,000.00</h4> -->
            </td>
        </tr>
    </table>
    <!-- *** -->
    <div class="title"><i class="fas fa-square orange"></i> 購買列表</div>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th class="col-md-6 text-center"  scope="col">商品</th>
                <th class="col-md-1 text-center" scope="col">單價</th>
                <th class="col-md-2 text-center" scope="col">數量</th>
                <th class="col-md-2 text-center" scope="col">金額</th>
                <th class="col-md-1 text-center" scope="col">取消</th>
            </tr>
        </thead>
        <tbody>
            <?php $__currentLoopData = $cartData->sData; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>    
            <tr class="table-orange">
                <th class="text-left" scope="row">
                    <a href="<?php echo e(url('productDetail/'.$row->prodId)); ?>"><?php echo e($row->prodNm); ?></a>
                </th>
                <td class="text-center"><?php echo e(number_format($row->dPrice)); ?></td>
                <td class="text-center"><?php echo e($row->num); ?></td>
                <td class="text-center"><?php echo e(number_format($row->amt)); ?></td>
                <td class="text-center">
                    <button class="btn btn-link cartDel" item-index="<?php echo e($key); ?>" uuid="<?php echo e($row->uuid); ?>" sell-type="S"><i class="fas fa-trash-alt"></i></button>
                </td>
            </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
    </table>
    <!-- 合計 -->
    <table class="table total-box">
        <tr>
            <td class="col-md-8">
                <div class="form-row align-items-center" id="sellDiscountInput"  <?php if($canUseSellDiscount == false): ?> style="display:none" <?php endif; ?>>
                    <div class="col-md-5 padding-l-0">
                        <input type="text" class="form-control mb-2" id="sellDiscountCode" placeholder="請輸入優惠碼">
                    </div>
                    <div class="col-auto">
                        <!-- <button type="submit" class="btn btn-primary mb-2" id="sellUseDiscount">使用優惠碼</button> -->
                    </div>
                </div>
            </td>
            <td class="col-md-4 text-right">
                <h4>Cart Totals</h4>
                <h4>優 惠 折 扣： - $ <span id="sellDiscount">0</span> </h4>
                <h4>小計 $<span id="sSumAmt"><?php echo e(number_format($sSumAmt)); ?></span></h4>
                <input type="hidden" name="sSumAmt" value="<?php echo e($sSumAmt); ?>">
                <!-- <h4>總金額 $10,000.00</h4> -->
            </td>
        </tr>
    </table>
    <!-- 總計 -->
    <div class="all-total-box">
        <h4>運       費： $ <span class="black" id="shipFee">0</span> </h4>
        <h4>特 殊 折 扣： - $ <span class="red" id="discountPrice">0</span> </h4>
        <h4>結帳金額： $ <span class="red" id="ttlAmt"> <?php echo e(number_format($rSumAmt + $sSumAmt)); ?></span></h4>
    </div>

    <?php if(count($cartData->sData) != 0 || count($cartData->rData) != 0): ?>
    <form action="<?php echo e(url('pay')); ?>" method="POST" id="payForm">
        <?php echo e(csrf_field()); ?>

        <input type="hidden" name="ttlAmt" value="<?php echo e($rSumAmt + $sSumAmt); ?>">
        <input type="hidden" name="shipFee" />
        <input type="hidden" name="sellDiscountCode" />
        <input type="hidden" name="rentDiscountCode" />
        <input type="hidden" name="sellDntNum" />
        <input type="hidden" name="rentDntNum" />
        <!-- 資訊 -->
        <div class="pay-info-box">
            <table class="table">
                <tr>
                    <td class="col-md-2">付款方式</td>
                    <td>
                        <div class="form-group col-md-3">
                            <select class="form-control" id="payWay" name="payWay">
                                <option value="Credit" selected>信用卡</option>
                                
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">取貨方式</td>
                    <td>
                        <div id="pickWayForAirport" <?php if(count($cartData->sData) > 0): ?> style="display:none" <?php endif; ?>>
                            <div class="">
                                <input class="form-check-input" type="radio" name="pick_way" id="get-wifi-1" value="A" <?php if(count($cartData->sData) == 0 && $memberData->pick_way == 'A'): ?> checked <?php endif; ?>>
                                <label class="form-check-label" for="get-wifi-1">
                                機場取貨                                        
                                </label>                                      
                            </div>
                            <div class="form-group col-md-3">
                                <select class="form-control" id="departureTerminal" name="departureTerminal">
                                    <option value="" selected>選擇航廈</option>
                                    <option value="T1">桃園機場第一航廈</option>
                                    <option value="T2">桃園機場第二航廈</option>
                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                <select class="form-control" id="departureHour" name="departureHour">
                                    <option value="" selected>航班大約時間(時)</option>
                                    <option value="00" >00</option>
                                    <option value="01">01</option>
                                    <option value="02">02</option>
                                    <option value="03">03</option>
                                    <option value="04">04</option>
                                    <option value="05">05</option>
                                    <option value="06">06</option>
                                    <option value="07">07</option>
                                    <option value="08">08</option>
                                    <option value="09">09</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                    <option value="21">21</option>
                                    <option value="22">22</option>
                                    <option value="23">23</option>
                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                <select class="form-control" id="departureMin" name="departureMin">
                                    <option value="" selected>航班大約時間(分)</option>
                                    <option value="00">00</option>
                                    <option value="15">15</option>
                                    <option value="30">30</option>
                                    <option value="45">45</option>
                                </select>
                            </div>
                        </div>
                        

                        
                        <div style="clear:both; padding-top:5px;">
                            <input class="form-check-input" type="radio" name="pick_way" id="get-wifi-2" value="B" <?php if(count($cartData->sData) > 0 || $memberData->pick_way == 'B' || empty($memberData->pick_way)): ?> checked <?php endif; ?>>
                            <label class="form-check-label" for="get-wifi-2">
                            宅配到府 (出國前一日送達) 歸還方式 <span style="color:coral">宅配歸還(請回國隔天三點前拿內附宅配單全省7-11寄回</span>
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">關於取貨</td>
                    <td><a href="<?php echo e(url('process')); ?>" target="_blank">查看租借流程</a></td>
                </tr>
                
                <tr name="returnTr" <?php if(count($cartData->rData) == 0): ?> style="display:none" <?php endif; ?>>
                    <td class="col-md-2">還貨方式</td>
                    <td>
                        <div class="">
                            <input class="form-check-input" type="radio" name="return_way" id="return-wifi-1" value="A" <?php if(count($cartData->rData) > 0 && $memberData->return_way == 'A'): ?> checked <?php endif; ?>>
                            <label class="form-check-label" for="return-wifi-1">
                            機場還貨                                        
                            </label>                                      
                        </div>
                        
                        <div class="form-group col-md-3">
                            <select class="form-control" id="returnTerminal" name="returnTerminal">
                                <option value="" selected>選擇航廈</option>
                                <option value="T1">桃園機場第一航廈</option>
                                <option value="T2">桃園機場第二航廈</option>
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <select class="form-control" id="returnHour" name="returnHour">
                                <option value="" selected>航班大約時間(時)</option>
                                <option value="00" >00</option>
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                                <option value="13">13</option>
                                <option value="14">14</option>
                                <option value="15">15</option>
                                <option value="16">16</option>
                                <option value="17">17</option>
                                <option value="18">18</option>
                                <option value="19">19</option>
                                <option value="20">20</option>
                                <option value="21">21</option>
                                <option value="22">22</option>
                                <option value="23">23</option>
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <select class="form-control" id="returnMin" name="returnMin">
                                <option value="" selected>航班大約時間(分)</option>
                                <option value="00">00</option>
                                <option value="15">15</option>
                                <option value="30">30</option>
                                <option value="45">45</option>
                            </select>
                        </div>
                        

                        <?php if(count($cartData->rData) > 0): ?>
                        <div style="clear:both; padding-top:5px;">
                            <input class="form-check-input" type="radio" name="return_way" id="return-wifi-2" value="B" <?php if($memberData->return_way == 'B' || empty($memberData->return_way)): ?> checked <?php endif; ?>>
                            <label class="form-check-label" for="return-wifi-2">
                            使用宅配歸還 <span style="color:coral">宅配歸還(請回國隔天三點前拿內附宅配單全省7-11寄回</span>
                            </label>
                        </div>
                        <?php endif; ?>
                    </td>
                    
                </tr>
                <tr name="returnTr" <?php if(count($cartData->rData) == 0): ?> style="display:none" <?php endif; ?>>
                    <td class="col-md-2">關於還貨</td>
                    <td><a href="<?php echo e(url('process')); ?>" target="_blank">查看租借流程</a></td>
                </tr>
            </table>
        </div>
        <!-- 訂購人資訊 -->
        <div class="orderer-box">
            <div class="title" style="margin: 20px 0 10px 0;"><i class="fas fa-square green"></i> 訂購人資訊</div>
            <div style="border: 1px solid #9e9e9e; border-radius: 5px; padding: 5px;">
                <table class="table">
                    <tr>
                        <td class="col-md-1">姓名</td>
                        <td>
                            <div class="form-group col-md-12" style="padding-left:20px;">
                                <input type="text" class="form-control" id="userName" name="name" placeholder="請輸入姓名" value="<?php if($memberData->is_updated == 'Y'): ?> <?php echo e($memberData->name); ?> <?php endif; ?>">
                            </div>
                        </td>
                        <td class="col-md-4"></td>
                    </tr>
                    <tr>
                        <td class="col-md-1">手機</td>
                        <td>
                            <div class="form-group col-md-12" style="padding-left:20px;">
                                <input type="text" class="form-control" id="userPhone" name="phone" placeholder="0912345678" value="<?php if($memberData->is_updated == 'Y'): ?> <?php echo e($memberData->phone); ?> <?php endif; ?>">
                            </div>
                        </td>
                        <!-- <td class="col-md-4" style="color: #a2a2a2;">手機市話可擇一填寫</td> -->
                    </tr>
                    <tr name="dlvWay">
                        <td class="col-md-1">收件地址</td>
                        <td>
                            <div class="form-group col-md-4" style="padding-left:20px;">
                                <input type="text" class="form-control" id="dlvZip" name="dlvZip" placeholder="郵地區號" value="<?php if($memberData->is_updated == 'Y'): ?> <?php echo e($memberData->dlv_zip); ?> <?php endif; ?>" readonly>
                            </div>
                            <div class="form-group col-md-4" style="padding-left:20px;">
                                <select class="form-control" name="dlvCity" id="dlvCity">
                                    <option value="" selected>請選擇縣市</option>
                                    <?php $__currentLoopData = $cityData; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($row->city_nm); ?>" <?php if($memberData->is_updated == 'Y' && $memberData->dlv_city == $row->city_nm): ?> selected <?php endif; ?>><?php echo e($row->city_nm); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>          
                                <!-- <input type="text" class="form-control" id="dlvCity" name="dlvCity" placeholder="城市"> -->
                            </div>
                            <div class="form-group col-md-4" style="padding-left:20px;">
                                <select class="form-control" name="dlvArea" id="dlvArea">
                                    <option value="" selected>請選擇鄉鎮市區</option>
                                </select>   
                                <!-- <input type="text" class="form-control" id="dlvArea" name="dlvArea" placeholder="區域"> -->
                            </div>
                        </td>
                        <!-- <td class="col-md-4" style="color: #a2a2a2;">手機市話可擇一填寫</td> -->
                    </tr>
                    <tr name="dlvWay">
                        <td class="col-md-1"></td>
                        <td>
                            <div class="form-group col-md-12" style="padding-left:20px;">
                                <input type="text" class="form-control" id="dlvAddr" name="dlvAddr" placeholder="地址" value="<?php if($memberData->is_updated == 'Y'): ?> <?php echo e($memberData->dlv_addr); ?> <?php endif; ?>">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="col-md-1">Email</td>
                        <td>
                            <div class="form-group col-md-12" style="padding-left:20px;">
                                <input type="text" class="form-control" id="email" name="email" placeholder="test@gamil.com" value="<?php if($memberData->is_updated == 'Y'): ?> <?php echo e($memberData->email); ?> <?php endif; ?>">
                            </div>
                        </td>
                        <td class="col-md-4"></td>
                    </tr>
                    <tr>
                        <td class="col-md-1"></td>
                        <td>
                            
                        </td>
                        <td class="col-md-4"></td>
                    </tr>
                </table>
            </div>
        </div>
        <!-- 發票資訊 -->
        <div class="invoice-box">
            <div class="title" style="margin: 20px 0 10px 0;"><i class="fas fa-square green"></i> 發票資訊</div>
            <div style="border: 1px solid #9e9e9e; border-radius: 5px; padding: 5px;">
                <table class="table">
                    <tr class="a1">
                        <td>
                            <input type="radio" class="c1" name="invoiceType" value="a1"> 
                            捐贈發票
                            <span style="color:coral">(捐贈發票說明)</span>
                        </td>
                    </tr>
                    <tr class="a2">
                        <td>
                            <input type="radio" class="c2" name="invoiceType" value="a2" checked> 
                            二聯電子發票<br>
                            <span style="margin-left: 18px;font-size: 10px;color: #a9a8a8;">配合國稅局勸止二聯換開三聯之政策，本公司保留換開發票的權利</span>
                        </td>
                    </tr>
                    <tr class="a3">
                        <td>
                            <input type="radio" class="c3" name="invoiceType" value="a3"> 
                            三聯電子發票
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>統一編號</span>
                            <input type="text" class="form-control" style="width: 50%;display: inline-block; margin: 0 20px;" id="customerIdentifier" placeholder="" name="customerIdentifier" disabled>
                            <span style="color: #a2a2a2;">限數字</span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>發票抬頭</span>
                            <input type="text" class="form-control" style="width: 50%;display: inline-block; margin: 0 20px;" id="customerTitle" placeholder="" name="customerTitle" disabled>
                            <span style="color: #a2a2a2;">限22個全形文字</span>
                        </td>
                    </tr>
                    <tr>
                        <td class="col-md-12" style="color: #a2a2a2;">。『統一編號及抬頭，將於發票開立時一併上傳至財政部電子發票整合平台，而電子發票證明聯警會列出統一編號』</td>
                    </tr>
                </table>
            </div>
        </div>

        <div style="display: flex;justify-content: baseline;align-items: center;">
            <input id="policy" type="checkbox" style="height: 20px; width: 20px; margin: 10px 20px;" >
            我同意 <a href="#" data-toggle="modal" data-target="#selfModal">美z.人生網路刷卡購物自訂爽快條款</a>，<a href="#" data-toggle="modal" data-target="#policyModal">隱私權政策</a>
        </div>


        <div style="display: flex;justify-content: baseline;align-items: center;">
            <div style="width: 60px;">
                <input id="agreeReturn" type="checkbox" style="height: 20px; width: 20px; margin: 10px 20px;" >
            </div>
            <div style="flex: 1;">我同意辦理退貨時，美Z.人生將以「銷貨折讓」辦理銷退，於退貨完成時間開立電子銷貨折讓單(可至會員專區查詢)</div>
        </div>  
        <br>

        <input type="text" id="refreshed" style="display:none">
        <script type="text/javascript">
            $(function(){
                if($("#refreshed").val() == "yes") {
                    location.reload();
                }
            });
        </script>
        <!-- 結帳 -->            
        <button class="btn btn-lg btn-primary btn-block">結帳</button>
    </form>
    <?php endif; ?>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer'); ?>
    <?php echo $__env->make('FrontEnd.layouts.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('after_scripts'); ?>
    <script type="text/javascript" src="<?php echo e(url('assets/js/cart.js')); ?>?t=989035"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('FrontEnd.layout',[
	"seo_title" => "購物車",
	"seo_desc" => preg_replace("/\n+/","",strip_tags("購物車")),
	"seo_img" => null
], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>