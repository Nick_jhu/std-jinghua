<?php if(Auth::check()): ?>
    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
          <div class="pull-left image">
            <img src="https://placehold.it/160x160/00a7d0/ffffff/&text=<?php echo e(mb_substr(Auth::user()->name, 0, 1)); ?>" class="img-circle" alt="User Image">
          </div>
          <div class="pull-left info">
            <p><?php echo e(Auth::user()->name); ?></p>
            
          </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
          <li class="header"><?php echo e(trans('backpack::base.administration')); ?></li>
          <!-- ================================================ -->
          <!-- ==== Recommended place for admin menu items ==== -->
          <!-- ================================================ -->
          <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('Dashboard')): ?>
          <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin').'/board')); ?>"><i class="fa fa-dashboard"></i> <span><?php echo e(trans('backpack::base.dashboard')); ?></span></a></li>
          <?php endif; ?>

          <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('BasicInfo')): ?>
          <li class="treeview">
            <a href="#"><i class="fa fa-folder"></i> <span><?php echo e(trans('menu.basicInfo')); ?></span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
              <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('modBase')): ?>
                <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/modBase/1/edit')); ?>"><i class="fa fa-tag"></i> <span>基本設定</span></a></li>
              <?php endif; ?>
              <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('modNews')): ?>
                <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/modNews')); ?>"><i class="fa fa-tag"></i> <span>最新消息</span></a></li>
              <?php endif; ?>
              <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('modQa')): ?>
                <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/modQa')); ?>"><i class="fa fa-tag"></i> <span>QA</span></a></li>
              <?php endif; ?>
              <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('modBanner')): ?>
                <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/modBanner')); ?>"><i class="fa fa-tag"></i> <span>首頁輪播</span></a></li>
              <?php endif; ?>
              <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('modMarquee')): ?>
                <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/modMarquee')); ?>"><i class="fa fa-tag"></i> <span>跑馬燈管理</span></a></li>
              <?php endif; ?>
              <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('modUnboxing')): ?>
                <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/modUnboxing')); ?>"><i class="fa fa-tag"></i> <span>開箱文管理</span></a></li>
              <?php endif; ?>
              <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('customerProfile')): ?>
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/customerProfile')); ?>"><i class="fa fa-group"></i> <span><?php echo e(trans('menu.customerProfile')); ?></span></a></li>
              <?php endif; ?>
              <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('goodsProfile')): ?>
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/goodsProfile')); ?>"><i class="fa fa-file"></i> <span><?php echo e(trans('menu.modGoods')); ?></span></a></li>
              <?php endif; ?>
              <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('companyProfile')): ?>
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/companyProfile')); ?>"><i class="fa fa-building-o"></i> <span><?php echo e(trans('menu.groupSetting')); ?></span></a></li>
              <?php endif; ?>
              <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('bscodeKind')): ?>
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/bscodeKind')); ?>"><i class="fa fa-file"></i> <span><?php echo e(trans('menu.bscode')); ?></span></a></li>
              <?php endif; ?>
              <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('sysCountry')): ?>
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/sysCountry')); ?>"><i class="fa fa-file"></i> <span><?php echo e(trans('menu.sysCountry')); ?></span></a></li>
              <?php endif; ?>
              <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('sysArea')): ?>
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/sysArea')); ?>"><i class="fa fa-file"></i> <span><?php echo e(trans('menu.sysArea')); ?></span></a></li>
              <?php endif; ?>
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/mailFormat')); ?>"><i class="fa fa-envelope"></i> <span><?php echo e(trans('menu.mailFormat')); ?></span></a></li>
            </ul>
          </li>
          <?php endif; ?>
          <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('MemberMgmt')): ?>
          <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin').'/MemberMgmt')); ?>"><i class="fa fa-user"></i> <span>會員總覽</span></a></li>
          <?php endif; ?>

          <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('ProdMgmt')): ?>
          <li class="treeview">
            <a href="#"><i class="fa fa-folder"></i> <span>商品管理</span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
              <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('ProdOverView')): ?>
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/ProdMgmt')); ?>"><i class="fa fa-list-alt"></i> <span>商品總覽</span></a></li>              
              <?php endif; ?>
              <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('ProdCateOverView')): ?>
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/CateMgmt')); ?>"><i class="fa fa-list-alt"></i> <span>類別總覽</span></a></li>              
              <?php endif; ?>
              <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('FileMgmt')): ?>
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/elfinder')); ?>"><i class="fa fa-files-o"></i> <span><?php echo e(trans('menu.fileMgmt')); ?></span></a></li>
              <?php endif; ?>
            </ul>
          </li>     
          <?php endif; ?>

          <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('OrderMgmt')): ?>
          <li class="treeview">
            <a href="#"><i class="fa fa-folder"></i> <span>訂單管理</span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
              <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('OrderOverView')): ?>
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/wifiOrderView')); ?>"><i class="fa fa-truck"></i> <span>WIFI訂單總覽</span></a></li>
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/normalOrderView')); ?>"><i class="fa fa-truck"></i> <span>一般訂單總覽</span></a></li>
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/rentDetailView')); ?>"><i class="fa fa-truck"></i> <span>wifi訂單明細</span></a></li>
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/orderDetailView')); ?>"><i class="fa fa-truck"></i> <span>一般訂單明細</span></a></li>
              <?php endif; ?>
              
            </ul>
          </li>  
          <?php endif; ?>
          <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('VendorOrderView')): ?>
          <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/vendorOrderDetail')); ?>"><i class="fa fa-truck"></i> <span>廠商訂單明細</span></a></li>
          <?php endif; ?>
          

          <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('DiscountMgmt')): ?>
          <li class="treeview">
            <a href="#"><i class="fa fa-folder"></i> <span>折扣管理</span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/modDiscount')); ?>"><i class="fa fa-list-alt"></i> <span>折扣設定</span></a></li>              
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/discount')); ?>"><i class="fa fa-list-alt"></i> <span>折扣總覽</span></a></li>
            </ul>
          </li>     
          <?php endif; ?>
          <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('setting')): ?>
          <li class="treeview">
            <a href="#"><i class="fa fa-folder"></i> <span><?php echo e(trans('menu.sysSetting')); ?></span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/apiMgmt')); ?>"><i class="fa fa-files-o"></i> <span>API Mgmt.</span></a></li>
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/language')); ?>"><i class="fa fa-flag-o"></i> <span><?php echo e(trans('menu.langSetting')); ?></span></a></li>
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/language/texts')); ?>"><i class="fa fa-language"></i> <span><?php echo e(trans('menu.langFile')); ?></span></a></li>
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin').'/backup')); ?>"><i class="fa fa-hdd-o"></i> <span><?php echo e(trans('menu.backup')); ?></span></a></li>
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin').'/log')); ?>"><i class="fa fa-terminal"></i> <span><?php echo e(trans('menu.logs')); ?></span></a></li>              
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin').'/setting')); ?>"><i class="fa fa-cog"></i> <span><?php echo e(trans('menu.settings')); ?></span></a></li>
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin').'/page')); ?>"><i class="fa fa-file-o"></i> <span><?php echo e(trans('menu.pages')); ?></span></a></li>
              <li><a href="<?php echo e(url('admin/menu-item')); ?>"><i class="fa fa-list"></i> <span>Menu</span></a></li>              
            </ul>
          </li>
          <?php endif; ?>
          <!-- Users, Roles Permissions -->
          <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('Permissions')): ?>
          <li class="treeview">
            <a href="#"><i class="fa fa-folder"></i> <span><?php echo e(trans('menu.permissions')); ?></span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/user')); ?>"><i class="fa fa-user"></i> <span><?php echo e(trans('menu.users')); ?></span></a></li>
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/role')); ?>"><i class="fa fa-group"></i> <span><?php echo e(trans('menu.roles')); ?></span></a></li>
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/permission')); ?>"><i class="fa fa-key"></i> <span><?php echo e(trans('menu.permission')); ?></span></a></li>              
            </ul>
          </li>
          <?php endif; ?>
          <!-- ======================================= -->
          <li class="header"><?php echo e(trans('backpack::base.user')); ?></li>
          <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin').'/logout')); ?>"><i class="fa fa-sign-out"></i> <span><?php echo e(trans('backpack::base.logout')); ?></span></a></li>
        </ul>
      </section>
      <!-- /.sidebar -->
    </aside>
<?php endif; ?>
