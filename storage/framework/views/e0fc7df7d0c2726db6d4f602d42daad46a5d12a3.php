 
<?php $__env->startSection('header'); ?>
<section class="content-header">
	<h1>
	訂單編輯<small></small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(url(config('backpack.base.route_prefix'),'OrderMgmt')); ?>">訂單總覽</a></li>
		<li class="active">訂單編輯</li>
	</ol>
</section>
<style>
    .lmask {
    position: absolute;
    height: 100%;
    width: 100%;
    background-color: #000;
    bottom: 0;
    left: 0;
    right: 0;
    top: 0;
    z-index: 9999;
    opacity: 0.4;
    }
    .lmask.fixed {
    position: fixed;
    }
    .lmask:before {
    content: '';
    background-color: rgba(0, 0, 0, 0);
    border: 5px solid rgba(0, 183, 229, 0.9);
    opacity: .9;
    border-right: 5px solid rgba(0, 0, 0, 0);
    border-left: 5px solid rgba(0, 0, 0, 0);
    border-radius: 50px;
    box-shadow: 0 0 35px #2187e7;
    width: 50px;
    height: 50px;
    -moz-animation: spinPulse 1s infinite ease-in-out;
    -webkit-animation: spinPulse 1s infinite linear;
    margin: -25px 0 0 -25px;
    position: absolute;
    top: 50%;
    left: 50%;
    }
    .lmask:after {
    content: '';
    background-color: rgba(0, 0, 0, 0);
    border: 5px solid rgba(0, 183, 229, 0.9);
    opacity: .9;
    border-left: 5px solid rgba(0, 0, 0, 0);
    border-right: 5px solid rgba(0, 0, 0, 0);
    border-radius: 50px;
    box-shadow: 0 0 15px #2187e7;
    width: 30px;
    height: 30px;
    -moz-animation: spinoffPulse 1s infinite linear;
    -webkit-animation: spinoffPulse 1s infinite linear;
    margin: -15px 0 0 -15px;
    position: absolute;
    top: 50%;
    left: 50%;
    }
    
    @-moz-keyframes spinPulse {
    0% {
    -moz-transform: rotate(160deg);
    opacity: 0;
    box-shadow: 0 0 1px #2187e7;
    }
    50% {
    -moz-transform: rotate(145deg);
    opacity: 1;
    }
    100% {
    -moz-transform: rotate(-320deg);
    opacity: 0;
    }
    }
    @-moz-keyframes spinoffPulse {
    0% {
    -moz-transform: rotate(0deg);
    }
    100% {
    -moz-transform: rotate(360deg);
    }
    }
    @-webkit-keyframes spinPulse {
    0% {
    -webkit-transform: rotate(160deg);
    opacity: 0;
    box-shadow: 0 0 1px #2187e7;
    }
    50% {
    -webkit-transform: rotate(145deg);
    opacity: 1;
    }
    100% {
    -webkit-transform: rotate(-320deg);
    opacity: 0;
    }
    }
    @-webkit-keyframes spinoffPulse {
    0% {
    -webkit-transform: rotate(0deg);
    }
    100% {
    -webkit-transform: rotate(360deg);
    }
    }
    
    </style>
<?php $__env->stopSection(); ?> 

<?php $__env->startSection('content'); ?>
    <?php echo $__env->make('backpack::template.toolbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class='lmask' id="cssLoading" style="display:none"></div>
	<div class="row">
		<div class="col-md-12">
			<div class="callout callout-danger" id="errorMsg" style="display:none">
				<h4><?php echo e(trans('backpack::crud.please_fix')); ?></h4>
				<ul>

				</ul>
			</div>
			<form method="POST" accept-charset="UTF-8" id="mainForm" enctype="multipart/form-data">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">基本資料</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="tab_1">
							<div class="box-body">
								<div class="row">
									<div class="form-group col-md-3">
										<label for="ord_no">訂單號</label>
										<input type="text" class="form-control" id="ord_no" name="ord_no">
                                    </div>
                                    <div class="form-group col-md-3">
										<label for="status">狀態</label>
										<select class="form-control" id="status" name="status">
                                            <option value="A">已下單未付款</option>
                                            <option value="B">已付款</option>
                                            <option value="C">出貨</option>
                                            <option value="I">商品出貨</option>
                                            <option value="D">送達</option>
                                            <option value="E">歸還</option>
                                            <option value="F">通知取消</option>
                                            <option value="G">確認取消</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
										<label for="pay_way">付款方式</label>
										<select class="form-control" id="pay_way" name="pay_way">
                                            <option value="Credit">信用卡</option>
                                            <option value="CSV">超商付款</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="dlv_nm">收貨人</label>
                                        <input type="text" class="form-control" id="dlv_nm" name="dlv_nm">
                                    </div>
								</div>

                                <div class="row">
                                <div class="form-group col-md-3">
										<label for="d_price">總價</label>
										<input type="number" class="form-control" id="d_price" name="d_price">
                                    </div>
                                    <div class="form-group col-md-3">
										<label for="ship_fee">運費</label>
										<input type="number" class="form-control" id="ship_fee" name="ship_fee">
									</div>
                                    <div class="form-group col-md-3">
                                        <label for="dlv_zip">郵地區號</label>
                                        <input type="text" class="form-control" id="dlv_zip" name="dlv_zip">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="email">email</label>
                                        <input type="text" class="form-control" id="email" name="email">
                                    </div>
                                </div>
                                
                                <div class="row">
                                <div class="form-group col-md-3">
                                        <label for="dlv_city">城市</label>
                                        <input type="text" class="form-control" id="dlv_city" name="dlv_city">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="dlv_area">區域</label>
                                        <input type="text" class="form-control" id="dlv_area" name="dlv_area">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="dlv_addr">地址</label>
                                        <input type="text" class="form-control" id="dlv_addr" name="dlv_addr">
                                    </div>
                                </div>

                                <!-- <div class="row">
									<div class="form-group col-md-3">
										<label for="d_price">總價</label>
										<input type="number" class="form-control" id="d_price" name="d_price">
                                    </div>
                                    <div class="form-group col-md-3">
										<label for="ship_fee">運費</label>
										<input type="number" class="form-control" id="ship_fee" name="ship_fee">
									</div>
                                </div> -->

                                <!-- <div class="row"> -->
                                    <!-- <div class="form-group col-md-3">
										<label for="shipper_cd">物流商</label>
                                        <select class="form-control" id="shipper_cd" name="shipper_cd">
                                        </select>
                                    </div> -->
                                    <!-- <div class="form-group col-md-3">
										<label for="ShipmentNo">寄送單號/客人查詢單號</label>
										<input type="text" class="form-control" id="CVSPaymentNo" name="CVSPaymentNo">
                                    </div>
                                    <div class="form-group col-md-3">
										<label for="CVSValidationNo">寄送單號驗證碼(7-11)</label>
										<input type="text" class="form-control" id="CVSValidationNo" name="CVSValidationNo">
									</div>
                                    <div class="form-group col-md-3">
										<label for="store_name">超商名稱</label>
										<input type="text" class="form-control" id="store_name" name="store_name">
									</div>
                                    <div class="form-group col-md-3">
										<label for="pay_no">支付號</label>
										<input type="text" class="form-control" id="pay_no" name="pay_no">
                                    </div> -->
                                    <!-- <div class="form-group col-md-3">
										<label for="number_of_periods">分期</label>
                                        <select class="form-control select2" multiple="multiple" data-placeholder="請選擇" style="width: 100%;" name="number_of_periods[]"></select>
									</div> -->
                                <!-- </div> -->

                                <!-- <div class="row"> -->
                                    <!-- <div class="form-group col-md-3">
										<label for="store_type">超商</label>
										<input type="text" class="form-control" id="store_type" name="store_type">
                                    </div>
                                    <div class="form-group col-md-3">
										<label for="store_id">超商號</label>
										<input type="text" class="form-control" id="store_id" name="store_id">
                                    </div> -->
                                    
                                    <!-- <div class="form-group col-md-3">
										<label for="store_addr">超商地址</label>
										<input type="text" class="form-control" id="store_addr" name="store_addr">
									</div> -->
                                <!-- </div> -->

                                <!-- <div class="row">
                                <div class="form-group col-md-6">
                                        <label>出貨備註</label>
                                        <textarea class="form-control" rows="3" id="sendprod_remark" name="sendprod_remark" placeholder=""></textarea>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>備註</label>
                                        <textarea class="form-control" rows="3" id="remark" name="remark" placeholder=""></textarea>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label>客戶備註</label>
                                        <textarea class="form-control" rows="3" id="cust_remark" name="cust_remark" placeholder=""></textarea>
                                    </div>
                                </div> -->

								<?php if(isset($id)): ?>
								<input type="hidden" name="id" value="<?php echo e($id); ?>" class="form-control">
								<input type="hidden" name="_method" value="PUT" class="form-control"> 
								<?php endif; ?>

							</div>
                        </div>
					</div>
				</div>
			</form>
		</div>	
    </div>
    

<?php $__env->stopSection(); ?> 
<?php echo $__env->make('backpack::template.lookup', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<?php $__env->startSection('after_scripts'); ?>
<script src="<?php echo e(asset('vendor/backpack/ckeditor/ckeditor.js')); ?>"></script>
<script src="<?php echo e(asset('vendor/backpack/ckeditor/adapters/jquery.js')); ?>"></script>
	<script>
		var mainId = "";
		var editData = null;
		var editObj = null;
		var SAVE_URL = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/OrderMgmt')); ?>";

		var fieldData = null;
        var fieldObj = null;
        
        var imgPath = "<?php echo e(Storage::url('/')); ?>";

		<?php if(isset($crud -> create_fields)): ?>
		fieldData = '{<?php echo json_encode($crud->create_fields); ?>}';
		fieldData = fieldData.substring(1);
		fieldData = fieldData.substring(0, fieldData.length - 1);
		fieldObj = JSON.parse(fieldData);
		<?php endif; ?>

		<?php if(isset($id)): ?>
		mainId   = "<?php echo e($id); ?>";
		
        <?php endif; ?>
        

		$(function () {
            Date.prototype.dateDiff = function(interval,objDate){
                var dtEnd = new Date(objDate);
                if(isNaN(dtEnd)) return undefined;
                switch (interval) {
                case "s":return parseInt((dtEnd - this) / 1000);
                case "n":return parseInt((dtEnd - this) / 60000);
                case "h":return parseInt((dtEnd - this) / 3600000);
                case "d":return parseInt((dtEnd - this) / 86400000);
                case "w":return parseInt((dtEnd - this) / (86400000 * 7));
                case "m":return (dtEnd.getMonth()+1)+((dtEnd.getFullYear()-this.getFullYear())*12) - (this.getMonth()+1);
                case "y":return dtEnd.getFullYear() - this.getFullYear();
                }
            }
            function calUseDay() {
                var sDT       = new Date( $('#subForm input[name="f_day"]').val());
                var eDT       = new Date( $('#subForm input[name="e_day"]').val());
                var day       = sDT.dateDiff("d",eDT) + 1;

                $('#subForm input[name="use_day"]').val(day);
            }

            $('#subForm input[name="f_day"]').on('change', function(){
                calUseDay();
            });

            $('#subForm input[name="e_day"]').on('change', function(){
                calUseDay();
            });

            $('#subForm input[name="f_day"]').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd'
            });
            $('#subForm input[name="e_day"]').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd'
            });
			//var formOpt = {};
			formOpt.formId = "mainForm";
			formOpt.editObj = editObj;
			formOpt.fieldObj = fieldObj;
			formOpt.editUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/OrderMgmt')); ?>";
			formOpt.fieldsUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') .'/get/mod_order')); ?>/" + mainId;
			formOpt.saveUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/OrderMgmt')); ?>";
			formOpt.afterInit = function() {
				if(editObj != null) {
                    
				}
			}

			formOpt.initFieldCustomFunc = function () {

			};

			formOpt.afterDel = function() {

			}

			formOpt.addFunc = function() {
                $('#jqxGrid').jqxGrid('clear');
                $('#jqxGrid1').jqxGrid('clear');
                
			}

			formOpt.editFunc = function() {

                
			}

			formOpt.copyFunc = function() {
                $('#jqxGrid').jqxGrid('clear');
                $('#jqxGrid1').jqxGrid('clear');
            }

			formOpt.saveSuccessFunc = function(data) {
                var result = data.data;
			}

			var btnGroup = [
				{
					btnId: "btnConfirmCancel",
					btnIcon: "fa fa-check-circle",
					btnText: "取消確認",
					btnFunc: function () {
						if(mainId != null) {
                            $.get(BASE_URL+'/confirmOrderCancel/'+mainId, {}, function(data){
                                if(data.message == "success") {
                                    $("#status").val("G");
                                    alert("確認完成");
                                }
                            });
                        }
					}
				},
			];

			initBtn(btnGroup);
            


            
		});
	</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('backpack::layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>