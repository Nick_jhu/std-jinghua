 
<?php $__env->startSection('header'); ?>
<section class="content-header">
	<h1>
		商品總覽 - <span class="subtitle">一般商品</span>
		<small></small>
	</h1>
    
	<ol class="breadcrumb">
		<li class="active">商品總覽 - <span class="subtitle">一般商品</span></li>
	</ol>
</section>
<form method="POST" accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">
    <?php echo e(csrf_field()); ?>

    <input type="file" id="excelFile" name="import_file" style="display:none;"/>
    <button type="submit" class="btn btn-primary" id="btnImport" style="display:none;"><?php echo e(trans('excel.btnImport')); ?></button>
</form>
<?php $__env->stopSection(); ?> 
<?php $__env->startSection('before_scripts'); ?>
<script>
    var baseCondition = "<?php echo e($basecondition); ?>";
    var gridOpt = {};
    var mode = "N";
    gridOpt.pageId        = "modProduct";
    gridOpt.enabledStatus = false;
    gridOpt.fieldsUrl     = "<?php echo e(url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/mod_product_view')); ?>";
    gridOpt.dataUrl       = "<?php echo e(url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/mod_product_view')); ?>" + "?dbRaw="+baseCondition+"&";
    gridOpt.fieldsUrl     = gridOpt.fieldsUrl + "?key=" + gridOpt.pageId;
    gridOpt.createUrl     = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/ProdMgmt/create')); ?>";
    gridOpt.editUrl       = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/ProdMgmt')); ?>" + "/{id}/edit";
    gridOpt.height        = 800;
    gridOpt.selectionmode = "checkbox";
    gridOpt.searchOpt     = true;
    gridOpt.enablebrowserselection = true;
    gridOpt.rowdoubleclick = true;
    statusGridObj = [{"key":"SEND","val":"123"}];
    var btnGroup = [
        {
            btnId: "btnOpenGridOpt",
            btnIcon: "fa fa-table",
            btnText: "<?php echo e(trans('common.gridOption')); ?>",
            btnFunc: function () {
                $('#gridOptModal').modal('show');
            }
        },
        {
            btnId: "btnAdd",
            btnIcon: "fa fa-edit",
            btnText: "<?php echo e(trans('common.add')); ?>",
            btnFunc: function () {
            window.open(gridOpt.createUrl + "?prodType="+mode);
            }
        },
        {
            btnId:"btnDelete",
            btnIcon:"fa fa-trash-o",
            btnText:"<?php echo e(trans('common.delete')); ?>",
            btnFunc:function(){
                searchMultiDel("jqxGrid", BASE_URL + '/product/multi/del');
            }
        },
        {
            btnId:"btnNormalProd",
            btnIcon:"fa fa-folder",
            btnText:"一般商品",
            btnFunc:function(){
                var dataSource = $("#jqxGrid").jqxGrid('source');
                dataSource._source.url = BASE_API_URL + "/admin/baseApi/getGridJson/mod_product_view?dbRaw=" + "<?php echo e($nUrl); ?>";
                $("#jqxGrid").jqxGrid('source', dataSource);
                $(".subtitle").text("一般商品");
                mode = "N";
            }
        },
        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('DiscountMgmt')): ?>
        {
            btnId:"btnWifiProd",
            btnIcon:"fa fa-folder",
            btnText:"WIFI",
            btnFunc:function(){
                var dataSource = $("#jqxGrid").jqxGrid('source');
                dataSource._source.url = BASE_API_URL + "/admin/baseApi/getGridJson/mod_product_view?dbRaw=" + "<?php echo e($wUrl); ?>";
                $("#jqxGrid").jqxGrid('source', dataSource);
                $(".subtitle").text("WIFI商品");
                mode = "W";
            }
        },
        {
            btnId:"btnSoftwareProd",
            btnIcon:"fa fa-folder",
            btnText:"軟體商品",
            btnFunc:function(){
                var dataSource = $("#jqxGrid").jqxGrid('source');
                dataSource._source.url = BASE_API_URL + "/admin/baseApi/getGridJson/mod_product_view?dbRaw=" + "<?php echo e($sUrl); ?>";
                $("#jqxGrid").jqxGrid('source', dataSource);
                $(".subtitle").text("軟體商品");
                mode = "S";
            }
        },
        <?php endif; ?>
        {
            btnId:"btnExportSoft",
            btnIcon:"fa fa-cloud-download",
            btnText:"匯出",
            btnFunc:function(){
                var ids = getGridData();
                switch(mode) {
                    case "N":
                        $.fileDownload(BASE_URL + "/exportNormal?ids="+ids);
                    break;
                    case "W":
                        $.fileDownload(BASE_URL + "/exportWifi?ids="+ids);
                    break;
                    case "S":
                        $.fileDownload(BASE_URL + "/exportSoftware?ids="+ids);
                    break;
                    default:
                        $.fileDownload(BASE_URL + "/exportNormal?ids="+ids);
                    break;
                }
                
            }
        },
        {
            btnId:"btnUploadExcel",
            btnIcon:"fa fa-cloud-upload",
            btnText:"匯入",
            btnFunc:function(){
                $("#excelFile").click();
            }
        },
        {
            btnId:"btnBatchAdded",
            btnIcon:"fa fa-cloud-upload",
            btnText:"<?php echo e(trans('common.added')); ?>",
            btnFunc:function(){
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var ids = new Array();

                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    if(typeof row != "undefined") {
                        ids.push(row.id);
                    }
                }
                if(ids.length == 0) {
                    swal("請至少選擇一筆資料", "", "warning");
                    return;
                }

                $.post(BASE_URL + '/product/multi/added', {'ids': ids}, function(data){
                    if(data.msg == "success") {
                        swal("操作成功", "", "success");
                        $("#jqxGrid").jqxGrid('updatebounddata');
                        $('#jqxGrid').jqxGrid('clearselection');
                    }
                    else{
                        swal("操作失敗", "", "error");
                    }
                });
            }
        },
        {
            btnId:"btnBatchRemove",
            btnIcon:"fa fa-cloud-download",
            btnText:"<?php echo e(trans('common.remove')); ?>",
            btnFunc:function(){
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var ids = new Array();

                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    if(typeof row != "undefined") {
                        ids.push(row.id);
                    }
                }
                if(ids.length == 0) {
                    swal("請至少選擇一筆資料", "", "warning");
                    return;
                }

                $.post(BASE_URL + '/product/multi/remove', {'ids': ids}, function(data){
                    if(data.msg == "success") {
                        swal("操作成功", "", "success");
                        $("#jqxGrid").jqxGrid('updatebounddata');
                        $('#jqxGrid').jqxGrid('clearselection');
                    }
                    else{
                        swal("操作失敗", "", "error");
                    }
                });
            }
        }
    ];


    function getGridData() {
        var data = $('#jqxGrid').jqxGrid('getrows');
        var str = "";
        for(var i=0; i<data.length; i++) {
            var id = data[i].id;

            str += id + ",";
        }

        return str.substring(0, str.length - 1);
    }


    $(function(){
        $("#myForm").submit(function () {
            var postData = new FormData($(this)[0]);
            var mainCol = 20;
            if(mode == "W") {
                mainCol = 15;
            }
            else if(mode == "S") {
                mainCol = 12;
            }
			$.ajax({
				url: BASE_URL + "/product/importExcel/" + mode + "/" + mainCol,
				type: 'POST',
				data: postData,
				async: false,
				beforeSend: function () {
					
				},
				error: function () {
                    swal("excel 匯入發生錯誤", "", "error");
                    $('#myForm')[0].reset();
					return false;
				},
				success: function (data) {
					if(data.msg == "error") {
                        swal("excel 匯入發生錯誤", "", "error");
                        $('#myForm')[0].reset();
                        return;
                    }
                    
                    swal("匯入成功", "", "success");
                    $('#myForm')[0].reset();
                    $("#jqxGrid").jqxGrid('updatebounddata');
                    $("#jqxGrid").jqxGrid('clearselection');
				},
				cache: false,
				contentType: false,
				processData: false
			});
			return false;
        });
        
        $("#excelFile").on("change", function(){
            $("#myForm").submit();
        });
    })

</script>
<?php $__env->stopSection(); ?> 
<?php echo $__env->make('backpack::template.search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


<?php echo $__env->make('backpack::layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>