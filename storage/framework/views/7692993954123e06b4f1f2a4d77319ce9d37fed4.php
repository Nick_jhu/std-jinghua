 
<?php $__env->startSection('header'); ?>
<section class="content-header">
	<h1>
		會員總覽
		<small></small>
	</h1>
	<ol class="breadcrumb">
		<li class="active">會員總覽</li>
	</ol>
</section>
<?php $__env->stopSection(); ?> 
<?php $__env->startSection('before_scripts'); ?>


<script>
    var gridOpt = {};
    gridOpt.pageId        = "modMember";
    gridOpt.enabledStatus = false;
    gridOpt.fieldsUrl     = "<?php echo e(url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/mod_member')); ?>";
    gridOpt.dataUrl       = "<?php echo e(url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/mod_member')); ?>";
    gridOpt.fieldsUrl     = gridOpt.fieldsUrl + "?key=" + gridOpt.dataUrl;
    gridOpt.createUrl     = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/MemberMgmt/create')); ?>";
    gridOpt.editUrl       = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/MemberMgmt')); ?>" + "/{id}/edit";
    gridOpt.height        = 800;
    gridOpt.selectionmode = "checkbox";
    gridOpt.searchOpt     = true;
    gridOpt.enablebrowserselection = true;
    gridOpt.rowdoubleclick = true;
    statusGridObj = [{"key":"SEND","val":"123"}];
    var btnGroup = [
        {
            btnId: "btnOpenGridOpt",
            btnIcon: "fa fa-table",
            btnText: "<?php echo e(trans('common.gridOption')); ?>",
            btnFunc: function () {
                $('#gridOptModal').modal('show');
            }
        },
        {
            btnId: "btnAdd",
            btnIcon: "fa fa-edit",
            btnText: "<?php echo e(trans('common.add')); ?>",
            btnFunc: function () {
            window.open(gridOpt.createUrl);
            }
        },
        {
            btnId:"btnDelete",
            btnIcon:"fa fa-trash-o",
            btnText:"<?php echo e(trans('common.delete')); ?>",
            btnFunc:function(){
                searchMultiDel("jqxGrid", BASE_URL + '/member/multi/del');
            }
        },
        {
            btnId:"btnActiveMember",
            btnIcon:"fa fa-user-plus",
            btnText:"啟用會員",
            btnFunc:function(){
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var ids = new Array();
                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    if(typeof row != "undefined") {
                        ids.push(row.id);
                    }
                }
                if(ids.length == 0) {
                    swal("請至少選擇一筆資料", "", "warning");
                    return;
                }

                $.post(BASE_URL + '/activeMember', {'ids': ids}, function(data){
                    if(data.msg == "success") {
                        swal("操作成功", "", "success");
                        $("#jqxGrid").jqxGrid('updatebounddata');
                        $('#jqxGrid').jqxGrid('clearselection');
                    }
                    else{
                        swal("操作失敗", "", "error");
                    }
                });

                
            }
        },
    ];

</script>
<?php $__env->stopSection(); ?> 
<?php echo $__env->make('backpack::template.search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('backpack::layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>