<?php $__env->startSection('after_style'); ?>
    <link rel="stylesheet" href="<?php echo e(url('assets/css/reset.css')); ?>" type="text/css" media="screen">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo e(url('assets/libs/bootstrap-3.3.7.min.css')); ?>">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?php echo e(url('assets/css/style.css')); ?>" type="text/css" media="screen">
    <link rel="stylesheet" href="<?php echo e(url('assets/css/prod.css?t=249807234')); ?>" type="text/css" media="screen">

    <script type="text/javascript" src="<?php echo e(url('assets/libs/jquery-3.3.1.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/libs/tether.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/libs/bootstrap-3.3.7.min.js')); ?>"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
	<style>
		a.disabled {
			color: #bbb !important;
			border-color: #bbb !important;
			pointer-events: none;
		}
	</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('header'); ?>
    <?php echo $__env->make('FrontEnd.layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="breadcrumbs-box">
	<div class="content-box">
		<div class="name">美Z商城</div>
		<div class="breadcrumbs">
				<a href="<?php echo e(url('/')); ?>">美z.人生</a> &gt; <a href="<?php echo e(url('product')); ?>">美Z商城</a> &gt; <?php if(isset($prodData)): ?> <?php echo e($prodData->title); ?> <?php endif; ?>
		</div>
	</div>
</div>
<div id="content-box">
	<div class="left-box">
		<div class="title"><i class="fas fa-square"></i> Categories</div>
		<ul class="categories">
            <?php $__currentLoopData = $cateData; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <a href="<?php echo e(url('product?cateId='.$row->id)); ?>">
                <li <?php if($prodData->cate_id == $row->id): ?> class="active" <?php endif; ?> ><?php echo e($row->name); ?></li>
            </a>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		</ul>
		<div class="title"><i class="fas fa-square"></i> 相關商品</div>
		<ul class="recently">
			<?php $__currentLoopData = $relateData; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<a href="<?php echo e(url('/productDetail/'.$row->id)); ?>">
				<li>
					<div class="icon-box">
						<img src="<?php echo e((isset($row->img1)?Storage::url($row->img1):url('assets/images/no_image.png'))); ?>" width="71" height="71">
					</div>
					<div class="desc-box">
						<div class="name" style="font-size: 12px;"><?php echo e($row->title); ?></div>
						<?php if($row->t_price > 0): ?>
						<div class="cost">$ <?php echo e(number_format($row->f_price)); ?> ~ $ <?php echo e(number_format($row->t_price)); ?></div>
						<?php else: ?>
						<div class="cost">$ <?php echo e(number_format($row->f_price)); ?></div>
						<?php endif; ?>
					</div>
				</li>
			</a>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		</ul>
	</div>
	<div class="right-box">
		<div class="prod-box">
			<div class="img-box">
				<img src="<?php echo e((isset($prodData->img1)?Storage::url($prodData->img1):url('assets/images/no_image.png'))); ?>">
				<div class="<?php if($prodData->sell_type == 'R'): ?> red-s <?php else: ?> origne-s <?php endif; ?> show">
					<div class="txt"><?php if($prodData->sell_type == 'R'): ?>租 <?php else: ?> 購 <?php endif; ?></div>
				</div>
			</div>
		</div>
		<div class="info-box">
			<div class="name"><?php echo e($prodData->title); ?></div>
			<div class="o-price">$ <?php echo e(number_format($prodData->t_price)); ?></div>
			<?php if($prodData->t_price > 0): ?>
			<div class="price">$ <?php echo e(number_format($prodData->f_price)); ?> ~ $ <?php echo e(number_format($prodData->t_price)); ?></div>
			<?php else: ?>
			<div class="price">$ <?php echo e(number_format($prodData->f_price)); ?></div>
			<?php endif; ?>
			<div class="desc">
				<?php echo $prodData->slogan; ?>

			</div>
			<div class="select-box">
				<table>
					<?php if($prodData->sell_type == "R"): ?>
					<tr>
						<td>旅遊期間：</td>
						<td>
							<div class="form-group col-md-6">                       
								<div id="datepicker-start" class="input-group date" data-date-format="yyyy-mm-dd">
									<input class="form-control" type="text" readonly name="startDate" value="<?php echo e($startDate); ?>" required/>
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								</div>
							</div>
							<div class="form-group col-md-6">                          
								<div id="datepicker-end" class="input-group date" data-date-format="yyyy-mm-dd">
									<input class="form-control" type="text" readonly name="endDate" value="<?php echo e($endDate); ?>" required/>
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								</div>
							</div>
						</td>
					</tr>
					<?php endif; ?>
					<tr>
						<td>
							<?php if($prodData->sell_type == "S"): ?>
							商品型號：
							<?php else: ?>
							選擇國家：
							<?php endif; ?>
						</td>
						<td>
                            <?php $__currentLoopData = $prodDetailData; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dRow): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<a href="#" style="margin:5px;" class="prodDetailLink <?php if($dRow->stock <=0): ?> disabled <?php endif; ?>" 
								o-price="<?php echo e($dRow->o_price); ?>" 
								d-price="<?php echo e($dRow->d_price); ?>" 
								prod-id="<?php echo e($dRow->prod_id); ?>"
								prod-detail-id="<?php echo e($dRow->id); ?>"
								sell-type="<?php echo e($prodData->sell_type); ?>"
								prod-nm="<?php echo e($prodData->title); ?>(<?php echo e($dRow->title); ?>)"
								><?php if($dRow->stock > 0): ?>
								<?php echo e($dRow->title); ?>

								<?php else: ?>
								<?php echo e($dRow->title); ?>(售完)
								<?php endif; ?></a>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</td>
					</tr>
				</table>
			</div>
			<div class="num-box">
				<table>
					
					<tr>
						<td>數量：</td>
						<td>
							<div id="number-picker" class="form-group">
								<div class="input-group">
									<span class="input-group-btn">
									<button type="button" class="btn btn-default cut-btn">-</button>
									</span>
									<input id="num" class="form-control" type="text" value="1" min="1" max="100" style="text-align: center;">
									<span class="input-group-btn">
									<button type="button" class="btn btn-default add-btn">+</button>
									</span>
								</div>
							</div>
						</td>
					</tr>
					
				</table>
			</div>
			<div class="addto-box">
				<button class="btn btn-block addto-btn" id="addCart">加人購物車</button>
				
			</div>
		</div>
		<div class="tabs-box">
			<ul class="nav nav-tabs">
				<li class="active"><a data-toggle="tab" href="#tab-content-1">商品詳情</a></li>
				<li><a data-toggle="tab" href="#tab-content-2">支援地區</a></li>
			</ul>
			<div class="tab-content">
				<div id="tab-content-1" class="tab-pane fade in active">
					商品詳情
                    <hr>
                    <?php echo $prodData->descp; ?>

				</div>
				<div id="tab-content-2" class="tab-pane fade">
					支援地區
					<hr>
					<?php echo $prodData->other_content; ?>

				</div>
			</div>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer'); ?>
    <?php echo $__env->make('FrontEnd.layouts.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('after_scripts'); ?>
	<script type="text/javascript" src="<?php echo e(url('assets/js/prod.js?t=8738473')); ?>"></script>
	
	<?php if(count($prodDetailData) == 1): ?> 
	<script>
		$(function(){
			$(".prodDetailLink").not(".disabled").click();
		});
	</script>
	<?php endif; ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('FrontEnd.layout',[
	"seo_title" => $prodData->title,
	"seo_desc" => preg_replace("/\n+/","",strip_tags($prodData->slogan)),
	"seo_img" => (isset($prodData->img1)?url(Storage::url($prodData->img1)):url('assets/images/no_image.png'))
], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>