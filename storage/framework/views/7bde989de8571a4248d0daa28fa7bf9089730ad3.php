<?php $__env->startSection('after_style'); ?>
<link rel="stylesheet" href="<?php echo e(url('assets/css/reset.css')); ?>" type="text/css" media="screen">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">
<link rel="stylesheet" href="<?php echo e(url('assets/libs/bootstrap-3.3.7.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(url('assets/css/style.css')); ?>" type="text/css" media="screen">
<link rel="stylesheet" href="<?php echo e(url('assets/css/sign.css')); ?>" type="text/css" media="screen">

<script type="text/javascript" src="<?php echo e(url('assets/libs/jquery-3.3.1.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(url('assets/libs/tether.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(url('assets/libs/bootstrap-3.3.7.min.js')); ?>"></script>
	<?php if($errors->has('message')): ?>
    <script>
        var errorMsg = "<?php echo e($errors->get('message')[0]); ?>";
        alert(errorMsg);
	</script>
	<?php endif; ?>

	<script type='text/javascript'>
		var captchaContainer = null;
		var loadCaptcha = function() {
			captchaContainer = grecaptcha.render('captcha_container', {
			'sitekey' : '6LfaRlIUAAAAAH06_yVHG1W2PC488GrHqoFPz7wf',
			'callback' : function(response) {
				//alert(response);
			}
			});
		};
	</script>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('header'); ?>
    <?php echo $__env->make('FrontEnd.layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="breadcrumbs-box">
	<div class="content-box">
		<div class="name">會員專區</div>
		<div class="breadcrumbs">
			<a href="<?php echo e(url('/')); ?>">美z.人生</a> &gt; 會員專區
		</div>
	</div>            
</div>
<div id="content-box">
	<div class="logo">
		<img src="<?php echo e(url('assets/images/logo-black.png')); ?>" width="200" />
	</div>
	<div class="form tabs-box box">
		<div class="btn-box">
			<ul>
				<li class="active">登入</li>
				<li class="">註冊</li>
			</ul>
		</div>
		<div class="content-box">
			<!-- 登入 -->
			<div class="signin-box box active">
				<form class="form" role="form" method="POST" action="<?php echo e(route('member.login.submit')); ?>">
					<?php echo e(csrf_field()); ?>

					<div class="input-group">
						<div class="input-group-addon"><i class="fas fa-user"></i></div>
						<input type="text" class="form-control" name="phone"  placeholder="請輸入手機號碼" required>
					</div>
					<div class="input-group">
						<div class="input-group-addon"><i class="fas fa-key"></i></div>
						<input type="password" class="form-control" name="password"  placeholder="請輸入密碼(注意大小寫)" required>
					</div>  

					<div class="verificationcode-box">
						<!-- <div class="top-box">
							<div class="verificationcode"></div>
							<div class="reloadbtn"><a href="#">更換一組驗證碼</a></div>
							</div>
							<input type="text" class="form-control"  placeholder="請輸入顯示的文字(英文可不區分大小寫)"> -->
							<!-- <div class="g-recaptcha" data-sitekey="6LfaRlIUAAAAAH06_yVHG1W2PC488GrHqoFPz7wf"></div> -->

							<div class="form-group">
								<small>你是機器人嗎?</small>
								<div id="captcha_container"></div>
							</div>
					</div>
					<div class="form-check mb-2 mr-sm-2">
						<input class="form-check-input" type="checkbox" id="inlineFormCheck" name="remember">
						<label class="form-check-label" for="inlineFormCheck">
						記住我
						</label>
					</div>
					<button type="submit" class="btn btn-primary btn-block">登入</button>
				</form>
			</div>
			<!-- 註冊 -->
			<div class="signup-box box">
				<form class="form" role="form" method="POST" action="<?php echo e(url('do/register')); ?>">
					<?php echo e(csrf_field()); ?>

					<div class="input-group">
						<div class="input-group-addon"><i class="fas fa-user"></i></div>
						<input type="text" class="form-control"  name="phone" value="<?php echo e(old('phone')); ?>" placeholder="使用手機號碼註冊" min="10" max="10" required>
						<?php if($errors->has('phone')): ?>
						<script>
							alert("<?php echo e($errors->first('phone')); ?>");
						</script>
						<?php endif; ?>
					</div>
					<p style="color:red; font-size: 12px;padding:0;margin:0">(如非台灣手機請加國碼，如香港為852，則輸入852123456789。)</p>
					<div class="input-group">
						<div class="input-group-addon"><i class="fas fa-key"></i></div>
						<input type="password" class="form-control"  name="password" placeholder="請輸入密碼(請輸入6到16位)" required>
					</div> 
					<div class="input-group">
						<div class="input-group-addon"><i class="fas fa-key"></i></div>
						<input type="password" class="form-control"  name="confirm_password" placeholder="確認密碼(請輸入6到16位)" required>
					</div> 
					<a href="<?php echo e(url('reSendVcode')); ?>">重新發送驗證碼</a>
					<button type="submit" class="btn btn-primary btn-block">註冊</button>
					<!-- <button type="button" class="btn btn-light btn-block">使用電子信箱註冊</button> -->
				</form>
			</div>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer'); ?>
    <?php echo $__env->make('FrontEnd.layouts.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('after_scripts'); ?>
	<script src="https://www.google.com/recaptcha/api.js?onload=loadCaptcha&render=explicit" async defer></script>
    <script type="text/javascript" src="<?php echo e(url('assets/js/sign.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('FrontEnd.layout',[
	"seo_title" => "會員登入/註冊",
	"seo_desc" => preg_replace("/\n+/","",strip_tags("會員登入/註冊")),
	"seo_img" => null
], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>