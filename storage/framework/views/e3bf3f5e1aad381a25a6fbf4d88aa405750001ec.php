<?php $__env->startSection('header'); ?>
<section class="content-header">
    <h1>
    <?php echo e(trans('dashboard.titleName')); ?><small></small>
    </h1>
    <ol class="breadcrumb">
        <li class="active"><?php echo e(trans('dashboard.titleName')); ?></li>
    </ol>
</section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('after_scripts'); ?>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('backpack::layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>