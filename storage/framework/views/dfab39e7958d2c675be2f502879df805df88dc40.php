<link href="<?php echo e(url('assets/css/slide-menu.css')); ?>" rel="stylesheet">
<!-- Start Navigation -->
<nav class="navbar navbar-default navbar-mobile  bootsnav">
    <div class="nav-header-fixed text-center responsive-slider-01">
        <?php $__currentLoopData = $marqueeData; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="col-xs-12"><a href="<?php echo e($row->link); ?>" style="color:#868d96"><?php echo e($row->descp); ?></a></div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
    <!-- Start Top Search -->
    <div class="top-search">
        <div class="container">
            <form action="<?php echo e(url('/product')); ?>" method="GET">
            <div class="input-group">
                
                    <input type="text" class="form-control" placeholder="輸入關鍵字" name="search">
                    <span class="input-group-addon"><i class="fa fa-search"></i></span>
                    <span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
                
            </div>
            </form>
        </div>
    </div>
    <!-- End Top Search -->

    <div class="navcontainer">            
        <!-- Start Atribute Navigation -->
        <div class="attr-nav">
            <ul>
                <li class="search"><a href="#"><i class="fa fa-search" style="font-size: 18px;" title="搜尋"></i></a></li>
                <li class="member-icon <?php if(Auth::guard('member')->check()): ?> is-active <?php endif; ?>"> <a href="<?php echo e(url('center')); ?>"><i class="fa fa-user" style="font-size: 18px;" <?php if(Auth::guard('member')->check()): ?> title="會員專區" <?php endif; ?>  <?php if(!Auth::guard('member')->check()): ?> title="登入" <?php endif; ?>"></i></a></li>

                <li>
                    <a href="<?php echo e(url('cart')); ?>">
                        <i class="fa fa-shopping-bag" style="font-size: 18px;" title="購物車"></i>
                        <span class="badge header-num-box">0</span>
                    </a>
                </li>
                <?php if(Auth::guard('member')->check()): ?>
                <li><a href="<?php echo e(url('member/logout')); ?>"><i class="fa fa-sign-out" style="font-size: 18px;" title="登出"></i></a></li>
                <?php endif; ?>
            </ul>
        </div>
        <!-- End Atribute Navigation -->

        <!-- Start Header Navigation -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle slide-menu-control" data-action="toggle" data-target="navbar-menu-mobile">
                <i class="fa fa-bars" style="font-size: 30px;"></i>
            </button>
            <a class="navbar-brand"  href="<?php echo e(url('/')); ?>"><img src="<?php echo e(url('assets/images/zizi_logo.png')); ?>" class="logo" alt=""></a>
        </div>
        <!-- End Header Navigation -->

        <nav class="slide-menu" id="navbar-menu-mobile">
            <div class="controls">
                <button type="button" class="btn slide-menu-control" data-action="close"><i class="fa fa-times"></i></button>
            </div>
            <div class="container text-center">
                <ul class="nav navbar-nav navbar-left" data-in="fadeInDown" data-out="fadeOutUp">
                    <?php $__currentLoopData = $menuData; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li class="dropdown megamenu-fw mobile-menu" cate-id="<?php echo e($row->id); ?>">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo e($row->name); ?></a>
                        <ul class="nav navbar-nav navbar-left">
                            <li class="dropdown megamenu-fw"><a href="<?php echo e(url('product')); ?>?cateId=<?php echo e($row->id); ?>">所有<?php echo e($row->name); ?></a></li>
                            <?php $__currentLoopData = $mobileMenu[$row->id]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                <li class="dropdown megamenu-fw">
                                    <a href="<?php if(empty($row1->children)): ?> <?php echo e(url('product')); ?>?cateId=<?php echo e($row1->id); ?> <?php endif; ?>"><?php echo e($row1->name); ?></a>
                                    <?php if(!empty($row1->children)): ?>
                                    <ul class="nav navbar-nav navbar-left">
                                        <li class="dropdown megamenu-fw"><a href="<?php echo e(url('product')); ?>?cateId=<?php echo e($row1->id); ?>">所有<?php echo e($row1->name); ?></a></li>
                                        <?php $__currentLoopData = $row1->children; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                        <li class="dropdown megamenu-fw">
                                            <a href="<?php if(empty($row2->children)): ?> <?php echo e(url('product')); ?>?cateId=<?php echo e($row2->id); ?> <?php endif; ?>"><?php echo e($row2->name); ?></a>
                                            <?php if(!empty($row2->children)): ?>
                                            <ul class="nav navbar-nav navbar-left">
                                                <li class="dropdown megamenu-fw"><a href="<?php echo e(url('product')); ?>?cateId=<?php echo e($row2->id); ?>">所有<?php echo e($row2->name); ?></a></li>
                                                <?php $__currentLoopData = $row2->children; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row3): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                                <li class="dropdown megamenu-fw">
                                                    <a href="<?php if(empty($row3->children)): ?> <?php echo e(url('product')); ?>?cateId=<?php echo e($row3->id); ?> <?php endif; ?>"><?php echo e($row3->name); ?></a>
                                                    <?php if(!empty($row3->children)): ?>
                                                    <ul class="nav navbar-nav navbar-left">
                                                        <li class="dropdown megamenu-fw"><a href="<?php echo e(url('product')); ?>?cateId=<?php echo e($row3->id); ?>">所有<?php echo e($row3->name); ?></a></li>
                                                        <?php $__currentLoopData = $row3->children; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row4): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                                        <li class="dropdown megamenu-fw">
                                                            <a href="<?php if(empty($row4->children)): ?> <?php echo e(url('product')); ?>?cateId=<?php echo e($row4->id); ?> <?php endif; ?>"><?php echo e($row4->name); ?></a>
                                                            <?php if(!empty($row4->children)): ?>
                                                            <ul class="nav navbar-nav navbar-left">
                                                                <li class="dropdown megamenu-fw"><a href="<?php echo e(url('product')); ?>?cateId=<?php echo e($row4->id); ?>">所有<?php echo e($row4->name); ?></a></li>
                                                                <?php $__currentLoopData = $row4->children; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row5): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                                                <li class="dropdown megamenu-fw">
                                                                    <a href="<?php if(empty($row5->children)): ?> <?php echo e(url('product')); ?>?cateId=<?php echo e($row5->id); ?> <?php endif; ?>"><?php echo e($row5->name); ?></a>
                                                                    <?php if(!empty($row5->children)): ?>
                                                                    <ul class="nav navbar-nav navbar-left">
                                                                        <li class="dropdown megamenu-fw"><a href="<?php echo e(url('product')); ?>?cateId=<?php echo e($row5->id); ?>">所有<?php echo e($row5->name); ?></a></li>
                                                                        <?php $__currentLoopData = $row5->children; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row6): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                                                        <li class="dropdown megamenu-fw">
                                                                            <a href="<?php if(empty($row6->children)): ?> <?php echo e(url('product')); ?>?cateId=<?php echo e($row6->id); ?> <?php endif; ?>"><?php echo e($row6->name); ?></a>
                                                                            <?php if(!empty($row6->children)): ?>
                                                                            <ul class="nav navbar-nav navbar-left">
                                                                                <li class="dropdown megamenu-fw"><a href="<?php echo e(url('product')); ?>?cateId=<?php echo e($row6->id); ?>">所有<?php echo e($row6->name); ?></a></li>
                                                                                <?php $__currentLoopData = $row6->children; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row7): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                                                                <li class="dropdown megamenu-fw">
                                                                                    <a href="<?php if(empty($row7->children)): ?> <?php echo e(url('product')); ?>?cateId=<?php echo e($row7->id); ?> <?php endif; ?>"><?php echo e($row7->name); ?></a>
                                                                                </li>
                                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                            </ul>
                                                                            <?php endif; ?>
                                                                        </li>
                                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                    </ul>
                                                                    <?php endif; ?>
                                                                </li>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                            </ul>
                                                            <?php endif; ?>
                                                        </li>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </ul>
                                                    <?php endif; ?>
                                                </li>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </ul>
                                            <?php endif; ?>
                                        </li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>
                                    <?php endif; ?>
                                </li>
                                
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                    </li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
            </div>
        </nav>

            <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navbar-menu">
            <div class="container text-center">
                <ul class="nav navbar-nav navbar-right" data-in="fadeInDown" data-out="fadeOutUp">
                    <?php $__currentLoopData = $menuData; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li class="dropdown megamenu-fw" cate-id="<?php echo e($row->id); ?>" level="first">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo e($row->name); ?></a>
                        <ul class="dropdown-menu megamenu-content" role="menu">
                            <li class="next-menu-<?php echo e($row->id); ?>">
                                <div class="row menubox">
                                    <div class="col-menu col-md-9 menurec">
                                        <h6 class="title">熱門推薦</h6>
                                        <div class="content">
                                            <ul class="menu-col ">
                                                <li class="nav-pro hot-prod">
                                                    
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- end col-3 -->
                                    <div class="col-menu col-md-3 show" id="menu-<?php echo e($row->id); ?>">
                                        <div class="content">
                                            <ul class="menu-col next-menu">
                                                <li class=""><a href="<?php echo e(url('product')); ?>?cateId=<?php echo e($row->id); ?>"><?php echo e($row->name); ?></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    
                                </div>
                                <!-- end row -->
                            </li>
                        </ul>
                    </li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
            </div>
        </div> <!-- /.navbar-collapse -->
    </div>   
</nav>
<!-- End Navigation -->
<div class="clearfix"></div>
<script src="<?php echo e(url('assets/js/slide-menu.min.js')); ?>"></script>
<script src="<?php echo e(url('assets/js/owl.carousel.min.js')); ?>"></script>
<script src="<?php echo e(url('assets/js/menu.js')); ?>?v=<?php echo e(Config::get('app.version')); ?>"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script>
    $(function(){
        var cartNum = $.cookie('cartNum');
        var cartId  = $.cookie('cartId');
        var isLogin = "<?php echo e(Auth::guard('member')->check()); ?>";
        if((typeof cartNum == "undefined" || cartNum == "null" || cartId == 0 || typeof cartId == "undefined") && isLogin != "1") {
            cartNum = 0;
        }

        $(".header-num-box").text(cartNum);

        $('.responsive-slider-01').slick({
            dots: false,
            infinite: true,
            speed: 600,
            autoplay: true,
            autoplaySpeed: 4000,
            slidesToShow: 1,
            slidesToScroll: 1,
        });
    });
</script>