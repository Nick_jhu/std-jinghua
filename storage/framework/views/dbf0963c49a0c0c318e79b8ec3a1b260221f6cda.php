 
<?php $__env->startSection('header'); ?>
<section class="content-header">
	<h1>
		訂單總覽
		<small></small>
	</h1>
	<ol class="breadcrumb">
		<li class="active">訂單總覽</li>
	</ol>
</section>
<?php $__env->stopSection(); ?> 
<?php $__env->startSection('before_scripts'); ?>


<script>
    var gridOpt = {};
    gridOpt.pageId        = "modOrderView";
    gridOpt.enabledStatus = true;
    gridOpt.fieldsUrl     = "<?php echo e(url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/mod_order_view')); ?>";
    gridOpt.dataUrl       = "<?php echo e(url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/mod_order_view')); ?>";
    gridOpt.fieldsUrl     = gridOpt.fieldsUrl + "?key=" + gridOpt.dataUrl;
    gridOpt.createUrl     = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/OrderMgmt/create')); ?>";
    gridOpt.editUrl       = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/OrderMgmt')); ?>" + "/{id}/edit";
    gridOpt.height        = 800;
    gridOpt.selectionmode = "checkbox";
    gridOpt.searchOpt     = true;
    gridOpt.enablebrowserselection = true;
    gridOpt.rowdoubleclick = true;
    statusGridObj = [{"key":"SEND","val":"123"}];
    var btnGroup = [
        {
            btnId: "btnOpenGridOpt",
            btnIcon: "fa fa-table",
            btnText: "<?php echo e(trans('common.gridOption')); ?>",
            btnFunc: function () {
                $('#gridOptModal').modal('show');
            }
        },

        {
            btnId:"btnDelete",
            btnIcon:"fa fa-trash-o",
            btnText:"<?php echo e(trans('common.delete')); ?>",
            btnFunc:function(){
                searchMultiDel("jqxGrid", BASE_URL + '/order/multi/del');
            }
        },

        {
            btnId:"btnPaymentConfirm",
            btnIcon:"fa fa-credit-card",
            btnText:"付款確認",
            btnFunc:function(){
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var ids = new Array();

                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    if(typeof row != "undefined") {
                        
                        if(row.status != "未付款") {
                            swal("未付款的訂單才能做付款確認", "", "warning");
                            return;
                        }
                        ids.push(row.id);
                    }
                }
                if(ids.length == 0) {
                    swal("請至少選擇一筆資料", "", "warning");
                    return;
                }

                $.post(BASE_URL + '/paymentConfirm', {'ids': ids}, function(data){
                    if(data.msg == "success") {
                        swal("操作成功", "", "success");
                        $("#jqxGrid").jqxGrid('updatebounddata');
                        $('#jqxGrid').jqxGrid('clearselection');
                    }
                    else{
                        swal("操作失敗", "", "error");
                    }
                });

                
            }
        },

        {
            btnId:"btnConfirmCancel",
            btnIcon:"fa fa-check-circle",
            btnText:"取消確認",
            btnFunc:function(){
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var ids = new Array();
                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    if(typeof row != "undefined") {
                        ids.push(row.id);
                    }
                }
                if(ids.length == 0) {
                    swal("請至少選擇一筆資料", "", "warning");
                    return;
                }

                $.post(BASE_URL + '/confirmOrderCancel', {'ids': ids}, function(data){
                    if(data.msg == "success") {
                        swal("操作成功", "", "success");
                        $("#jqxGrid").jqxGrid('updatebounddata');
                        $('#jqxGrid').jqxGrid('clearselection');
                    }
                    else{
                        swal("操作失敗", "", "error");
                    }
                });

                
            }
        },

    ];

</script>
<?php $__env->stopSection(); ?> 
<?php echo $__env->make('backpack::template.search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('backpack::layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>