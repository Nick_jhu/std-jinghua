<?php $__env->startSection('after_style'); ?>
    <link rel="stylesheet" href="<?php echo e(url('assets/css/reset.css')); ?>" type="text/css" media="screen">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo e(url('assets/libs/bootstrap-3.3.7.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(url('assets/css/style.css')); ?>" type="text/css" media="screen">
    <link rel="stylesheet" href="<?php echo e(url('assets/css/member.css')); ?>" type="text/css" media="screen">

    <script type="text/javascript" src="<?php echo e(url('assets/libs/jquery-3.3.1.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/libs/tether.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/libs/bootstrap-3.3.7.min.js')); ?>"></script>
    <style>
        input {padding: 5px !important;}
    </style>

    <?php if(Session::get('message')): ?>
    <script>
        alert("<?php echo e(Session::get('message')); ?>");
    </script>
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('header'); ?>
    <?php echo $__env->make('FrontEnd.layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="breadcrumbs-box">
    <div class="content-box">
        <div class="name">美Z商城</div>
        <div class="breadcrumbs">
            <a href="<?php echo e(url('/')); ?>">美z.人生</a>  &gt; 會員專區
        </div>
    </div>
</div>
<div id="content-box">
    <div class="tabs-box">
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#tab-content-1">訂單列表</a></li>
            <li><a data-toggle="tab" href="#tab-content-2">會員資料</a></li>
            <li><a data-toggle="tab" href="#tab-content-3">歷史訂單</a></li>
        </ul>
        <div class="tab-content">
            <!-- /// -->
            <div id="tab-content-1" class="tab-pane fade in active">
                <h3>訂單列表</h3>
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col" class="col-md-3 text-center">訂單編號</th>
                            <th scope="col" class="col-md-2 text-center">訂單狀態</th>
                            <th scope="col" class="col-md-2 text-center">訂單日期</th>
                            <th scope="col" class="col-md-2 text-center">訂單金額</th>
                            <th scope="col" class="col-md-1 text-center">付款</th>
                            <th scope="col" class="col-md-1 text-center">取消</th>
                            <th scope="col" class="col-md-2 text-center">查看明細</th>
                        </tr>
                    </thead>
                </table>
                <?php $__currentLoopData = $orderData; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="panel-group" id="accordion-<?php echo e($key); ?>">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <th scope="col" class="col-md-3 text-center"><?php echo e($row->ord_no); ?></th>
                                            <th scope="col" class="col-md-2 text-center" id="orderStatus">
                                                <?php if($row->status == 'A'): ?>
                                                    未付款
                                                <?php elseif($row->status == 'B'): ?>
                                                    已付款
                                                <?php elseif($row->status == 'C'): ?>
                                                    運送中
                                                <?php elseif($row->status == 'D'): ?>
                                                    已送達
                                                <?php elseif($row->status == 'E'): ?>
                                                    已歸還
                                                <?php elseif($row->status == 'F'): ?>
                                                    通知取消
                                                <?php elseif($row->status == 'G'): ?>
                                                    已取消
                                                <?php elseif($row->status == 'H'): ?>
                                                    已刷退
                                                <?php endif; ?>
                                            </th>
                                            <th scope="col" class="col-md-2 text-center"><?php echo e($row->created_at); ?></th>
                                            <th scope="col" class="col-md-2 text-center">$ <?php echo e(number_format($row->d_price)); ?></th>
                                            <th scope="col" class="col-md-1 text-center">
                                                <?php if($row->status == 'A'): ?>
                                                <button class="btn btn-link" name="payBtn" url="<?php echo e(url('rePay')); ?>" data-toggle="modal" data-target="#myModal" ordNo="<?php echo e($row->ord_no); ?>" ordId="<?php echo e($row->id); ?>"><i class="fas fa-credit-card"></i></button>
                                                <?php endif; ?>
                                            </th>
                                            <th scope="col" class="col-md-1 text-center">
                                                <?php if($row->status == 'A'): ?>
                                                <button class="btn btn-link" name="cancelBtn" url="<?php echo e(url('cancelOrder/'.$row->id)); ?>"><i class="fas fa-trash-alt"></i></button>
                                                <?php endif; ?>
                                            </th>
                                            <th scope="col" class="col-md-2 text-center">
                                                <a class="toggle collapsed" data-toggle="collapse" data-parent="#accordion-1" 
                                                    href="#collapse-1-<?php echo e($key); ?>">
                                                <span class="open-txt">展開</span>
                                                <span class="close-txt">收合</span>
                                                </a>
                                            </th>
                                        </tr>
                                    </tbody>
                                </table>
                            </h4>
                        </div>
                        <div id="collapse-1-<?php echo e($key); ?>" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="row bs-wizard" style="border-bottom:0;">
                                    <div class="col-xs-2 bs-wizard-step <?php if($row->status == 'A'): ?> active <?php elseif($row->status == 'F' || $row->status == 'G'): ?> disabled <?php else: ?> complete <?php endif; ?>">
                                        <div class="text-center bs-wizard-stepnum" style="font-size:12px">已下單</div>
                                        <div class="progress">
                                            <div class="progress-bar"></div>
                                        </div>
                                        <a href="#" class="bs-wizard-dot"></a>
                                        <div class="bs-wizard-info text-center"></div>
                                    </div>
                                    <div class="col-xs-2 bs-wizard-step  <?php if($row->status == 'B'): ?> active <?php elseif($row->status == 'A' || $row->status == 'F' || $row->status == 'G'): ?> disabled <?php else: ?> complete <?php endif; ?>">
                                        <!-- complete -->
                                        <div class="text-center bs-wizard-stepnum" style="font-size:12px">已付款</div>
                                        <div class="progress">
                                            <div class="progress-bar"></div>
                                        </div>
                                        <a href="#" class="bs-wizard-dot"></a>
                                        <div class="bs-wizard-info text-center"></div>
                                    </div>
                                    <div class="col-xs-3 bs-wizard-step <?php if($row->status == 'C'): ?> active <?php endif; ?> <?php if($row->status == 'A' || $row->status == 'B' || $row->status == 'F' || $row->status == 'G'): ?> disabled <?php else: ?> complete <?php endif; ?>">
                                        <!-- complete -->
                                        <div class="text-center bs-wizard-stepnum" style="font-size:12px">運送中</div>
                                        <div class="progress">
                                            <div class="progress-bar"></div>
                                        </div>
                                        <a href="#" class="bs-wizard-dot"></a>
                                        <div class="bs-wizard-info text-center"></div>
                                    </div>
                                    <div class="col-xs-2 bs-wizard-step <?php if($row->status == 'D'): ?> active <?php endif; ?> <?php if($row->status == 'A' || $row->status == 'B' || $row->status == 'C' || $row->status == 'F' || $row->status == 'G'): ?> disabled <?php else: ?> complete <?php endif; ?>">
                                        <!-- active -->
                                        <div class="text-center bs-wizard-stepnum" style="font-size:12px">已送達</div>
                                        <div class="progress">
                                            <div class="progress-bar"></div>
                                        </div>
                                        <a href="#" class="bs-wizard-dot"></a>
                                        <div class="bs-wizard-info text-center"></div>
                                    </div>
                                    <div class="col-xs-3 bs-wizard-step <?php if($row->status == 'E'): ?> active <?php endif; ?> <?php if($row->status == 'A' || $row->status == 'B' || $row->status == 'C' || $row->status == 'D' || $row->status == 'F' || $row->status == 'G'): ?> disabled <?php else: ?> complete <?php endif; ?>">
                                        <!-- active -->
                                        <div class="text-center bs-wizard-stepnum" style="font-size:12px">已歸還</div>
                                        <div class="progress">
                                            <div class="progress-bar"></div>
                                        </div>
                                        <a href="#" class="bs-wizard-dot"></a>
                                        <div class="bs-wizard-info text-center"></div>
                                    </div>
                                </div>
                                <?php if(count($orderRentData[$key]) >  0): ?>
                                <div class="title"><i class="fas fa-square red"></i> 租賃列表</div>
                                
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <!-- <th scope="col">國家</th> -->
                                            <th scope="col">商品</th>
                                            <th scope="col">日期(起)</th>
                                            <th scope="col">日期(迄)</th>
                                            <th scope="col">顯示天數</th>
                                            <!-- <th scope="col">結算天數</th> -->
                                            <th scope="col">單價</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $__currentLoopData = $orderRentData[$key]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr class="table-red">
                                            <!-- <th scope="row">日本</th> -->
                                            <td><?php echo e($val->prod_nm); ?></td>
                                            <td><?php echo e(substr($val->f_day, 0, 10)); ?></td>
                                            <td><?php echo e(substr($val->e_day, 0, 10)); ?></td>
                                            <td><?php echo e($val->use_day); ?></td>
                                            <!-- <td>100</td> -->
                                            <td><?php echo e(number_format($val->amt)); ?></td>
                                        </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>
                                <?php endif; ?>
                                <br>
                                <?php if(count($orderDetailData[$key]) > 0): ?>
                                <div class="title"><i class="fas fa-square orange"></i> 購買列表</div>
                                
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th scope="col">商品</th>
                                            <th scope="col">單價</th>
                                            <th scope="col">數量</th>
                                            <th scope="col">金額</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $__currentLoopData = $orderDetailData[$key]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr class="table-orange">
                                            <th scope="row"><?php echo e($val->prod_nm); ?></th>
                                            <td><?php echo e(number_format($val->unit_price)); ?></td>
                                            <td><?php echo e($val->num); ?></td>
                                            <td><?php echo e(number_format($val->amt)); ?></td>
                                        </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <!-- 分頁 -->
                <br>
                <!-- <nav aria-label="Page navigation example" class="text-center">
                    <ul class="pagination">
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                            <span class="sr-only">Previous</span>
                            </a>
                        </li>
                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                            <span class="sr-only">Next</span>
                            </a>
                        </li>
                    </ul>
                </nav> -->
            </div>
            <!-- /// -->
            <div id="tab-content-2" class="tab-pane fade">
                <h3>會員資料</h3>
                <form class="form" role="form" method="POST" action="<?php echo e(url('member/update')); ?>">
                    <?php echo e(csrf_field()); ?>

                    <table class="table">
                        <tr>
                            <td class="col-md-2">會員手機</td>
                            <td><?php echo e($userData->phone); ?></td>
                        </tr>
                        <tr>
                            <td class="col-md-2">會員姓名</td>
                            <td>
                                <div class="form-group col-md-5">                                        
                                    <input type="text" class="form-control" name="name" value="<?php echo e($userData->name); ?>" placeholder="姓名">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-2">會員EMAIL</td>
                            <td>
                                <div class="form-group col-md-5">                                        
                                    <input type="email" class="form-control" name="email" value="<?php echo e($userData->email); ?>" placeholder="Email">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-2">修改密碼</td>
                            <td>
                                <div class="col-md-5">                                        
                                    <input type="password" class="form-control" name="password"  placeholder="密碼">
                                    <br>
                                    <input type="password" class="form-control" name="confirm_password"  placeholder="確認密碼">
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td class="col-md-2">收貨資料</td>
                            <td>
                                <div class="row">
                                    <div class="form-group col-md-2 col-xs-2">                                        
                                            <input type="text" class="form-control" id="dlv_zip" name="dlv_zip" value="<?php echo e($userData->dlv_zip); ?>" placeholder="郵地區號" readonly>
                                    </div>
                                    <div class="form-group col-md-3 col-xs-4" style="padding-left:20px;">       
                                        <select class="form-control" name="dlv_city" id="dlv_city">
                                            <option value="" selected>請選擇縣市</option>
                                            <?php $__currentLoopData = $cityData; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($row->city_nm); ?>" <?php if($userData->dlv_city == $row->city_nm): ?> selected <?php endif; ?>><?php echo e($row->city_nm); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>                                 
                                        
                                    </div>
                                    <div class="form-group col-md-3 col-xs-4" style="padding-left:20px;"> 
                                        <select class="form-control" name="dlv_area" id="dlv_area">
                                            <option value="" selected>請選擇鄉鎮市區</option>
                                            <?php $__currentLoopData = $areaData; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option zip="<?php echo e($row->zip_f); ?>" value="<?php echo e($row->dist_nm); ?>" <?php if($userData->dlv_area == $row->dist_nm): ?> selected <?php endif; ?>><?php echo e($row->dist_nm); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>                                                 
                                        <!-- <input type="text" class="form-control" name="dlv_area" value="<?php echo e($userData->dlv_area); ?>" placeholder="區域"> -->
                                    </div>
                                </div>
                                <div class="row" style="margin-top:5px;">
                                    <div class="form-group col-md-8">                                        
                                        <input type="text" class="form-control" name="dlv_addr" value="<?php echo e($userData->dlv_addr); ?>" placeholder="地址">
                                    </div>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td class="col-md-2">取貨方式</td>
                            <td>
                                <div class="form-group col-md-5">
                                    <select class="form-control" id="pick_way" name="pick_way">
                                        <option value="" selected>請選擇取貨方式</option>
                                        <?php $__currentLoopData = $pickWay; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($row->cd); ?>" <?php if($userData->pick_way == $row->cd): ?> selected <?php endif; ?>><?php echo e($row->cd_descp); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-2">還貨方式</td>
                            <td>
                                <div class="form-group col-md-5">
                                    <select class="form-control" id="return_way" name="return_way">
                                        <option value="" selected>請選擇還貨方式</option>
                                        <?php $__currentLoopData = $returnWay; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($row->cd); ?>" <?php if($userData->return_way == $row->cd): ?> selected <?php endif; ?>><?php echo e($row->cd_descp); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <br>
                    <button class="btn btn-lg btn-primary btn-block">送出</button>
                </form>
            </div>
            <!-- /// -->
            <div id="tab-content-3" class="tab-pane fade">
                <h3>歷史訂單</h3>
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col" class="col-md-4 text-center">訂單編號</th>
                            <th scope="col" class="col-md-2 text-center">訂單狀態</th>
                            <th scope="col" class="col-md-2 text-center">訂單日期</th>
                            <th scope="col" class="col-md-2 text-center">訂單金額</th>
                            <th scope="col" class="col-md-2 text-center">查看明細</th>
                        </tr>
                    </thead>
                </table>

                <?php $__currentLoopData = $orderHistoryData; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="panel-group" id="accordion-<?php echo e($key); ?>">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <table class="table">
                                    <tbody>
                                        
                                        <tr>
                                            <th scope="col" class="col-md-4 text-center"><?php echo e($row->ord_no); ?></th>
                                            <th scope="col" class="col-md-2 text-center">
                                                    <?php if($row->status == 'A'): ?>
                                                    未付款
                                                <?php elseif($row->status == 'B'): ?>
                                                    已付款
                                                <?php elseif($row->status == 'C'): ?>
                                                    運送中
                                                <?php elseif($row->status == 'D'): ?>
                                                    已送達
                                                <?php elseif($row->status == 'E'): ?>
                                                    已歸還
                                                <?php elseif($row->status == 'F'): ?>
                                                    通知取消
                                                <?php elseif($row->status == 'G'): ?>
                                                    已取消
                                                <?php elseif($row->status == 'H'): ?>
                                                    已刷退
                                                <?php endif; ?>
                                            </th>
                                            <th scope="col" class="col-md-2 text-center"><?php echo e($row->created_at); ?></th>
                                            <th scope="col" class="col-md-2 text-center">$ <?php echo e(number_format($row->o_price)); ?></th>
                                            <th scope="col" class="col-md-2 text-center">
                                                <a class="toggle collapsed" data-toggle="collapse" data-parent="#accordion-1" 
                                                    href="#collapse-3-<?php echo e($key); ?>">
                                                <span class="open-txt">展開</span>
                                                <span class="close-txt">收合</span>
                                                </a>
                                            </th>
                                        </tr>
                                    </tbody>
                                </table>
                            </h4>
                        </div>
                        <div id="collapse-3-<?php echo e($key); ?>" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="row bs-wizard" style="border-bottom:0;">
                                    <div class="col-xs-2 bs-wizard-step <?php if($row->status == 'A'): ?> active <?php else: ?> complete <?php endif; ?>">
                                        <div class="text-center bs-wizard-stepnum" style="font-size:12px">已下單</div>
                                        <div class="progress">
                                            <div class="progress-bar"></div>
                                        </div>
                                        <a href="#" class="bs-wizard-dot"></a>
                                        <div class="bs-wizard-info text-center"></div>
                                    </div>
                                    <div class="col-xs-2 bs-wizard-step  <?php if($row->status == 'B'): ?> active <?php endif; ?> <?php if($row->status == 'A'): ?> disabled <?php else: ?> complete <?php endif; ?>">
                                        <!-- complete -->
                                        <div class="text-center bs-wizard-stepnum" style="font-size:12px">已付款</div>
                                        <div class="progress">
                                            <div class="progress-bar"></div>
                                        </div>
                                        <a href="#" class="bs-wizard-dot"></a>
                                        <div class="bs-wizard-info text-center"></div>
                                    </div>
                                    <div class="col-xs-3 bs-wizard-step <?php if($row->status == 'C'): ?> active <?php endif; ?> <?php if($row->status == 'A' || $row->status == 'B'): ?> disabled <?php else: ?> complete <?php endif; ?>">
                                        <!-- complete -->
                                        <div class="text-center bs-wizard-stepnum" style="font-size:12px">運送中</div>
                                        <div class="progress">
                                            <div class="progress-bar"></div>
                                        </div>
                                        <a href="#" class="bs-wizard-dot"></a>
                                        <div class="bs-wizard-info text-center"></div>
                                    </div>
                                    <div class="col-xs-2 bs-wizard-step <?php if($row->status == 'D'): ?> active <?php endif; ?> <?php if($row->status == 'A' || $row->status == 'B' || $row->status == 'C'): ?> disabled <?php else: ?> complete <?php endif; ?>">
                                        <!-- active -->
                                        <div class="text-center bs-wizard-stepnum" style="font-size:12px">已送達</div>
                                        <div class="progress">
                                            <div class="progress-bar"></div>
                                        </div>
                                        <a href="#" class="bs-wizard-dot"></a>
                                        <div class="bs-wizard-info text-center"></div>
                                    </div>
                                </div>
                                <?php if(count($orderHistoryRentData[$key]) > 0): ?>
                                <div class="title"><i class="fas fa-square red"></i> 租賃列表</div>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <!-- <th scope="col">國家</th> -->
                                            <th scope="col">商品</th>
                                            <th scope="col">日期(起)</th>
                                            <th scope="col">日期(迄)</th>
                                            <th scope="col">顯示天數</th>
                                            <!-- <th scope="col">結算天數</th> -->
                                            <th scope="col">單價</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $__currentLoopData = $orderHistoryRentData[$key]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr class="table-red">
                                            <!-- <th scope="row">日本</th> -->
                                            <td><?php echo e($val->prod_nm); ?></td>
                                            <td><?php echo e(substr($val->f_day, 0, 10)); ?></td>
                                            <td><?php echo e(substr($val->e_day, 0, 10)); ?></td>
                                            <td><?php echo e(((strtotime($val->e_day) - strtotime($val->f_day))/3600)/24); ?></td>
                                            <!-- <td>100</td> -->
                                            <td><?php echo e(number_format($val->amt)); ?></td>
                                        </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>
                                <?php endif; ?>
                                <br>
                                <?php if(count($orderHistoryDetailData[$key]) > 0): ?>
                                <div class="title"><i class="fas fa-square orange"></i> 購買列表</div>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th scope="col">商品</th>
                                            <th scope="col">單價</th>
                                            <th scope="col">數量</th>
                                            <th scope="col">金額</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $__currentLoopData = $orderHistoryDetailData[$key]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr class="table-orange">
                                            <th scope="row"><?php echo e($val->prod_nm); ?></th>
                                            <td><?php echo e(number_format($val->unit_price)); ?></td>
                                            <td><?php echo e($val->num); ?></td>
                                            <td><?php echo e(number_format($val->amt)); ?></td>
                                        </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <!-- 分頁 -->
                <br>
                <!-- <nav aria-label="Page navigation example" class="text-center">
                    <ul class="pagination">
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                            <span class="sr-only">Previous</span>
                            </a>
                        </li>
                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                            <span class="sr-only">Next</span>
                            </a>
                        </li>
                    </ul>
                </nav> -->
            </div>
        </div>
    </div>
</div>


<div class="modal fade" tabindex="-1" role="dialog" id="myModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">付款確認-訂單號：<span id="ordNo"></span></h4>
        </div>
        <form action="<?php echo e(url('rePay')); ?>" method="POST" id="payForm">
            <?php echo e(csrf_field()); ?>

            <div class="modal-body">
                
                <div class="form-group">
                    <label for="payWay">付款方式</label>
                    <select class="form-control" id="payWay" name="payWay">
                        <option value="Credit" selected>信用卡</option>
                        <option value="CVS">超商付款</option>
                    </select>
                    <input type="hidden" name="ordId" id="ordId">
                </div>

                <div class="form-group">
                    <label for="invoiceType">發票</label>
                    <select class="form-control" id="invoiceType" name="invoiceType">
                        <option value="a1" selected>捐贈發票</option>
                        <option value="a2">二聯電子發票</option>
                        <option value="a3">三聯電子發票</option>
                    </select>
                </div>
                <div style="display:none" id="inv">
                    <div class="form-group">
                        <label for="customerIdentifier">統一編號</label>
                        <input type="text" class="form-control" style="width: 50%;display: inline-block; margin: 0 20px;" id="customerIdentifier" placeholder="" name="customerIdentifier">
                    </div>

                    <div class="form-group">
                        <label for="customerTitle">發票抬頭</label>
                        <input type="text" class="form-control" style="width: 50%;display: inline-block; margin: 0 20px;" id="customerTitle" placeholder="" name="customerTitle">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">關閉</button>
                <button type="submit" class="btn btn-primary">送出</button>
            </div>
        </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer'); ?>
    <?php echo $__env->make('FrontEnd.layouts.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('after_scripts'); ?>
    <script type="text/javascript" src="<?php echo e(url('assets/js/member.js')); ?>?t=23423423423"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('FrontEnd.layout',[
	"seo_title" => "會員專區",
	"seo_desc" => preg_replace("/\n+/","",strip_tags("會員專區")),
	"seo_img" => null
], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>