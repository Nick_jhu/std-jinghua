<?php if(Auth::check()): ?>
    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
          <div class="pull-left image">
            <img src="https://placehold.it/160x160/00a7d0/ffffff/&text=<?php echo e(mb_substr(Auth::user()->name, 0, 1)); ?>" class="img-circle" alt="User Image">
          </div>
          <div class="pull-left info">
            <p><?php echo e(Auth::user()->name); ?></p>
            
          </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
          <li class="header"><?php echo e(trans('backpack::base.administration')); ?></li>
          <!-- ================================================ -->
          <!-- ==== Recommended place for admin menu items ==== -->
          <!-- ================================================ -->

          <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('MemberMgmt')): ?>
          <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin').'/MemberMgmt')); ?>"><i class="fa fa-user"></i> <span>會員總覽</span></a></li>
          <?php endif; ?>


          <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('OrderMgmt')): ?>
          <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/OrderMgmt')); ?>"><i class="fa fa-truck"></i> <span>訂單總覽</span></a></li>
          <?php endif; ?>
          


          <!-- Users, Roles Permissions -->
          <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('Permissions')): ?>
          <li class="treeview">
            <a href="#"><i class="fa fa-folder"></i> <span><?php echo e(trans('menu.permissions')); ?></span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/user')); ?>"><i class="fa fa-user"></i> <span><?php echo e(trans('menu.users')); ?></span></a></li>
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/role')); ?>"><i class="fa fa-group"></i> <span><?php echo e(trans('menu.roles')); ?></span></a></li>
              <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/permission')); ?>"><i class="fa fa-key"></i> <span><?php echo e(trans('menu.permission')); ?></span></a></li>              
            </ul>
          </li>
          <?php endif; ?>
          <!-- ======================================= -->
          <li class="header"><?php echo e(trans('backpack::base.user')); ?></li>
          <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin').'/logout')); ?>"><i class="fa fa-sign-out"></i> <span><?php echo e(trans('backpack::base.logout')); ?></span></a></li>
        </ul>
      </section>
      <!-- /.sidebar -->
    </aside>
<?php endif; ?>
