<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableModMember20190117 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mod_member', function (Blueprint $table) {
            $table->string('card_token')->nullable();
            $table->string('card_key')->nullable();
            $table->string('last_four')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mod_member', function (Blueprint $table) {
            $table->drop('card_token');
            $table->drop('card_key');
            $table->drop('last_four');
        });
    }
}
