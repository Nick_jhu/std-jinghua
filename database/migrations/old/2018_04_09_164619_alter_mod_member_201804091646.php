<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterModMember201804091646 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mod_member', function (Blueprint $table) {
            $table->string('validation_code', 4)->nullable();
            $table->timestamp('validation_date')->nullabel();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mod_member', function (Blueprint $table) {
            $table->drop('validation_code');
            $table->drop('validation_date');
        });
    }
}
