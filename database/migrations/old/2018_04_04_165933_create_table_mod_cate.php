<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableModCate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mod_cate', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('descp');
            $table->integer('parent_id');
            $table->string('created_by');
            $table->string('updated_by');
            $table->string('g_key', 10);
            $table->string('c_key', 10);
            $table->string('s_key', 10);
            $table->string('d_key', 10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mod_cate');
    }
}
