<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableModProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mod_product', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sell_type', 1)->default('S');
            $table->integer('cate_id')->nullable();
            $table->string('cate_name')->nullable();
            $table->string('country')->nullable();
            $table->string('title');
            $table->string('sub_title')->nullable();
            $table->decimal('f_price', 18,2)->nullable();
            $table->decimal('t_price', 18,2)->nullable();
            $table->string('pay_way')->nullable();
            $table->string('ship_way')->nullable();
            $table->text('descp')->nullable();
            $table->integer('ttl_sell_amt')->nullable();
            $table->string('is_added',1)->nullable()->default('N');
            $table->string('created_by');
            $table->string('updated_by');
            $table->timestamps();
            $table->string('g_key', 10);
            $table->string('c_key', 10);
            $table->string('s_key', 10);
            $table->string('d_key', 10);
            $table->timestamp('added_on')->nullable();
            $table->string('img1', 100)->nullable();
            $table->string('img2', 100)->nullable();
            $table->string('img3', 100)->nullable();
            $table->string('img4', 100)->nullable();
            $table->string('img5', 100)->nullable();
            $table->string('img_descp1', 255)->nullable();
            $table->string('img_descp2', 255)->nullable();
            $table->string('img_descp3', 255)->nullable();
            $table->string('img_descp4', 255)->nullable();
            $table->string('img_descp5', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mod_product');
    }
}
