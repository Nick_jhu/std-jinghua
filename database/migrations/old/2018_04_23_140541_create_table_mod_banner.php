<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableModBanner extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mod_banner', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 50)->nullabe();
            $table->text('descp')->nullabe();
            $table->integer('banner_order')->nullabe();
            $table->string('image',100)->nullable();
            $table->string('video',100)->nullable();
            $table->string('prod_id',10)->nullable();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mod_banner');
    }
}
