<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableModGift20190305 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mod_gift', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('prod_id');
            $table->string('descp',100)->nullable();
            $table->string('img1',100)->nullable();
            $table->string('img2',100)->nullable();
            $table->string('img3',100)->nullable();
            $table->string('link',300)->nullable();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mod_gift');
    }
}
