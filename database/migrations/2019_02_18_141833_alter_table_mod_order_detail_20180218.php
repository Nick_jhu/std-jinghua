<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableModOrderDetail20180218 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mod_order', function (Blueprint $table) {
            $table->string('ship_flag', 1)->default('Z');
        });

        Schema::table('mod_order_detail', function (Blueprint $table) {
            $table->string('vendor', 500);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mod_order', function (Blueprint $table) {
            $table->drop('ship_flag');
        });

        Schema::table('mod_order_detail', function (Blueprint $table) {
            $table->drop('vendor');
        });
    }
}
