<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=0; $i<5000; $i++){
            DB::table('customers')->insert([
                'cust_no' => str_random(50),
                'name' => str_random(50),
                'email' => str_random(10).'@gmail.com',
                'phone' => str_random(50),
                'address' => str_random(50),
                'cust_contact' => str_random(50),
                'gender' => str_random(50),
                'cust_date' => date("Y-m-d H:i:s"),
                'city' => str_random(50),
                'state' => str_random(50),
                'spec_service' => str_random(50),
                'cust_file' => str_random(50),
                'remark' => str_random(100),
            ]);
        }
        
    }
}
