var CART_CONTENT = null;
var googlePaySetting = {
	// Optional in sandbox, Required in production
	googleMerchantId: "1251-5071-2398",
	tappayGoogleMerchantId: "standardinfomation_CTBC",
	allowedCardAuthMethods: ["PAN_ONLY", "CRYPTOGRAM_3DS"],
	merchantName: "TapPay Test!",
	emailRequired: true, // optional
	shippingAddressRequired: true, // optional,
	billingAddressRequired: true, // optional
	billingAddressFormat: "MIN", // FULL, MIN

	allowPrepaidCards: true,
	allowedCountryCodes: ['TW'],

	phoneNumberRequired: true // optional
}

TPDirect.googlePay.setupGooglePay(googlePaySetting);  

var paymentRequest = {
	allowedNetworks: ["AMEX", "JCB", "MASTERCARD", "VISA"],
	price: "1", // optional
	currency: "TWD", // optional
}

TPDirect.googlePay.setupPaymentRequest(paymentRequest, function(err, result){
	console.log(result);
	if (result.canUseGooglePay) {
		canUseGooglePay = true
	}
});

function samsungPay(amt) {
	TPDirect.samsungPay.setup({
		country_code: 'tw'
	});

	var paymentRequest = {
		supportedNetworks: ['AMEX', 'MASTERCARD', 'VISA'],
		total: {
			label: 'TapPay',
			amount: {
				currency: 'TWD',
				value: String(amt)
			}
		}
	}
	TPDirect.samsungPay.setupPaymentRequest(paymentRequest);
}

function applePay(amt) {
	var data = {
		supportedNetworks: ['MASTERCARD', 'VISA', 'AMEX'],
		supportedMethods: ['apple_pay'],
		displayItems: [{
			label: 'iPhone8',
			amount: {
				currency: 'TWD',
				value: String(amt)
			}
		}],
		total: {
			label: '付給 TapPay',
			amount: {
				currency: 'TWD',
				value: String(amt)
			}
		}
	};

	TPDirect.paymentRequestApi.setupApplePay({
		// required, your apple merchant id
		// merchant.tech.cherri.global.test 是 DEMO 頁面專門使用的 merchant id
		// 如果要在自己頁面上使用 Apple Pay 請參考 Required 去申請 Apple Pay merchant id
		merchantIdentifier: 'merchant.com.standardinformation',
		// defaults to 'TW'
		countryCode: 'TW'
	});

	TPDirect.paymentRequestApi.setupPaymentRequest(data, function (result) {
		console.log('TPDirect.paymentRequestApi.setupPaymentRequest.result', result)
	
		// 代表瀏覽器支援 payment request api (或 apple pay)
		// 和 TPDirect.paymentRequestApi.checkAvailability() 的結果是一樣的
		// if (!result.browserSupportPaymentRequest) {
		//     return
		// }
	
		// 代表使用者是否有符合 supportedNetworks 與 supportedMethods 的卡片
		// paymentRequestApi ---> canMakePaymentWithActiveCard is result of canMakePayment
		// apple pay         ---> canMakePaymentWithActiveCard is result of canMakePaymentsWithActiveCard

		if(result.browserSupportPaymentRequest == false) {
			alert("您的系統不支援ApplePay");
			return;
		}
		
		// NOTE: apple pay 只會檢查使用者是否有在 apple pay 裡面綁卡片
		if (result.canMakePaymentWithActiveCard) {

		}
		else {
			// 如果有支援 basic-card 方式，仍然可以開啟 payment request sheet
			// 如果是 apple pay，會引導使用者去 apple pay 綁卡片
			alert("沒有可以支付的卡片");
			return;
		}
	
		if (window.ApplePaySession) {
			
		}
		
	});
}

$(function () {

	var cartStr = $("#cartStr").val();
	if (cartStr != "") {
		CART_CONTENT = JSON.parse(cartStr);
	}

	$("td .c1").click(function () {
		$("tr.a1").addClass("green-box");
		$("tr.a2").removeClass("green-box");
		$("tr.a3").removeClass("green-box");
	});

	$("td .c2").click(function () {
		$("tr.a1").removeClass("green-box");
		$("tr.a2").addClass("green-box");
		$("tr.a3").removeClass("green-box");
	});

	$("td .c3").click(function () {
		$("tr.a1").removeClass("green-box");
		$("tr.a2").removeClass("green-box");
		$("tr.a3").addClass("green-box");
	});

	//刪除購物車
	$(".cartDel").on("click", function () {
		var $thisTable = $(this);
		/*
		var cartContent = $.cookie('cartContent');
		if(typeof cartContent == "undefined" || cartContent == "null") {
		    cartContent = [];
		}

		cartContent = JSON.parse(cartContent);
		*/
		var index = $(this).attr("item-index");
		var uuid = $(this).attr("uuid");
		var sellType = $(this).attr("sell-type");

		$.post(BASE_URL + '/delCart', {
			'uuid': uuid,
			'sellType': sellType
		}, function (data) {
			if (data.status == "success") {
				$(".header-num-box").text(data.cartNum);

				$thisTable.parents("tr").remove();
				CART_CONTENT = data.cartContent;

				var num = data.cartContent['rData'].length + data.cartContent['sData'].length;

				var prodType = [];
				prodType['S'] = 0;
				prodType['N'] = 0;
				prodType['W'] = 0;

				for (var i = 0; i < data.cartContent['sData'].length; i++) {
					if (data.cartContent['sData'][i]['prodType'] == 'S') {
						prodType['S']++;
					}

					if (data.cartContent['sData'][i]['prodType'] == 'N') {
						prodType['N']++;
					}
				}

				for (var i = 0; i < data.cartContent['rData'].length; i++) {
					prodType['W']++;
				}

				if (data.cartContent['rData'].length == 0) {
					$("#rentArea").hide();
					$("tr[name='returnTr']").hide();

					if (prodType['N'] > 0) {
						//$("input[name='pick_way']").val('B');
						//$("input[name='pick_way']").trigger("change");
						//$("input[name='return_way']").val('B');
					} else {
						$("input[name='pick_way']").prop('checked', false);
					}


					$("#rentDiscountCode").val("");
					$("#rentDiscountCode").trigger("change");

					$("input[name='return_way']").prop("checked", false);
				} else {
					$("tr[name='returnTr']").show();
					$("#rentDiscountCode").trigger("change");
				}

				if (data.cartContent['sData'].length == 0) {
					$("#sellArea").hide();
					$("#pickWayForAirport").show();

					$("#sellDiscountCode").val("");
					$("#sellDiscountCode").trigger("change");
				} else {
					$("#pickWayForAirport").hide();
					$("#sellDiscountCode").trigger("change");
				}

				if (data.cartContent['sData'].length == 0 && data.cartContent['rData'] == 0) {
					$("#totalBox").hide();
					$("#showDescp").show();
					$(".invoice-box").hide();
					$(".policy").hide();
					$("#quickPayment").hide();
					$("#btnPay").hide();
				}

				if (num == 0) {
					$("#payForm").hide();
				}

				if (prodType['N'] == 0 && prodType['W'] > 0) {
					$("#pickWayForAirport").show();
				} else if (prodType['S'] > 0 && prodType['N'] == 0 && prodType['W'] == 0) {
					$("#pickWayTr").hide();
				} else if (prodType['N'] > 0 || prodType['W'] > 0) {
					$("#pickWayTr").show();
				} else {
					$("#pickWayForAirport").hide();
				}

				if(prodType['N'] == 0 && prodType['W'] == 0 && prodType['S'] > 0) {
					$(".trAddr").hide();

				}

				updateCart();
			}
		});

		/*
		if(sellType == "R") {
		  for(i in cartContent["rData"]) {
		    var id = cartContent["rData"][i].uuid;

		    if(id == uuid) {
		      cartContent["rData"].splice(i, 1);
		    }
		  }
		  
		}

		if(sellType == "S") {
		  for(i in cartContent["sData"]) {
		    var id = cartContent["sData"][i].uuid;

		    if(id == uuid) {
		      cartContent["sData"].splice(i, 1);
		    }
		  }
		}

		var cookieStr = JSON.stringify(cartContent);

		$.cookie('cartContent', cookieStr, { path:'/', expires: 3 }); 
    
		var cartNum = cartContent["rData"].length + cartContent["sData"].length;

		$("#cart-num").text(cartNum);

		$(this).parents("tr").remove();

		updateCart();
		*/
	});

	//同步會員資料
	// $("#syncUserData").on("change", function () {
	// 	var checked = $(this).prop("checked");

	// 	if (checked === true) {
	// 		$.get(BASE_URL + '/getUserData', {}, function (data) {
	// 			$("#userName").val(data.name);
	// 			$("#userPhone").val(data.phone);
	// 			$("#dlvZip").val(data.dlvZip);
	// 			$("#dlvCity").val(data.dlvCity);
	// 			$("#dlvArea").val(data.dlvArea);
	// 			$("#dlvAddr").val(data.dlvAddr);
	// 			$("#email").val(data.email);

	// 			var areaData = data.areaData;
	// 			var str = '<option value="" selected>請選擇鄉鎮市區</option>';
	// 			var sel = "";
	// 			for (i in areaData) {
	// 				if (areaData[i]["dist_nm"] == data.dlvArea) {
	// 					sel = "selected";
	// 				} else {
	// 					sel = ""
	// 				}
	// 				str += '<option zip="' + areaData[i]["zip_f"] + '" value="' + areaData[i]["dist_nm"] + '" ' + sel + '>' + areaData[i]["dist_nm"] + '</option>';
	// 			}

	// 			$("#dlvArea").html(str);

	// 		}, "JSON");
	// 	}
	// });
	$.get(BASE_URL + '/getUserData', {}, function (data) {
		$("#userName").val(data.name);
		$("#userPhone").val(data.cellphone);
		$("#dlvZip").val(data.dlvZip);
		$("#dlvCity").val(data.dlvCity);
		$("#dlvArea").val(data.dlvArea);
		$("#dlvAddr").val(data.dlvAddr);
		$("#email").val(data.email);

		var areaData = data.areaData;
		var str = '<option value="" selected>請選擇鄉鎮市區</option>';
		var sel = "";
		for (i in areaData) {
			if (areaData[i]["dist_nm"] == data.dlvArea) {
				sel = "selected";
			} else {
				sel = ""
			}
			str += '<option zip="' + areaData[i]["zip_f"] + '" value="' + areaData[i]["dist_nm"] + '" ' + sel + '>' + areaData[i]["dist_nm"] + '</option>';
		}

		$("#dlvArea").html(str);

	}, "JSON");

	$("#payWay").on("change", function(){
		var val = $(this).val();

		if(val == "Credit") {
			$("#creditCardArea").show();
		}
		else {
			$("#creditCardArea").hide();
			$("#quickPayment").hide();
			$("input[name='useQuickPay']").val("NO");
			$("#btnPay").prop("disabled", false);
		}
	});

	//勾選或取消安心險
	$("input[name='insuranceCheckbox']").on("change", function () {
		var prodDetailId = $(this).attr('prodDetailId');
		var uuid = $(this).attr('uuid');
		var num = parseInt($(this).attr("num"));
		var day = parseInt($(this).attr("day"));
		var price = parseInt($(this).attr("price"));
		var insuranceFee = 50 * day * num;

		var ttlAmt = parseInt($("input[name='ttlAmt']").val());
		var rSumAmt = parseInt($("input[name='rSumAmt']").val());

		if ($(this).prop('checked')) {
			rSumAmt = rSumAmt + insuranceFee;
			ttlAmt = ttlAmt + insuranceFee;
			price = price + insuranceFee;

			chkInsuranceForCookie(uuid, "Y");
		} else {
			rSumAmt = rSumAmt - insuranceFee;
			ttlAmt = ttlAmt - insuranceFee;
			price = price - insuranceFee;

			chkInsuranceForCookie(uuid, "N");
		}

		// $("td[uuid='"+uuid+"']").text(numberWithCommas(price));
		// $(this).attr("price", price);
		// $("input[name='ttlAmt']").val(ttlAmt);
		// $("input[name='rSumAmt']").val(rSumAmt);
		// $("#rSumAmt").text(numberWithCommas(rSumAmt));
		// $("#ttlAmt").text(numberWithCommas(ttlAmt));
	});

	$("#payForm").on("submit", function () {
		var returnWay = $("input[name='return_way']:checked").val();
		var pickWay = $("input[name='pick_way']:checked").val();
		var userName = $("#userName").val();
		var userPhone = $("#userPhone").val();
		var email = $("#email").val();
		var dlvAddr = $("#dlvAddr").val();
		var invoiceType = $("input[name='invoiceType']:checked").val();
		var ttlAmt = parseInt($("input[name='ttlAmt']").val());
		var payWay = $("#payWay").val();

		var phoneLen = chkPhone(pickWay, userPhone);
		if(phoneLen != "") {
			alert(phoneLen);
			return false;
		}

		if (!validateEmail(email)) {
			alert('email格式不正確，請檢查！！！');
			return false;
		}

		if($(".trAddr").css("display") == "none") {
			if (userName == "" || userPhone == "" || dlvAddr == "" || email == "") {
				alert("請檢查姓名、手機、住址、email是否有正確填寫");
				return false;
			}
		}
		else {
			if (userName == "" || userPhone == "" || email == "") {
				alert("請檢查姓名、手機、email是否有正確填寫");
				return false;
			}
		}

		

		if (pickWay == "A") {
			var departureTerminal = $("select[name='departureTerminal']").val();
			var departureHour = $("select[name='departureHour']").val();
			var departureMin = $("select[name='departureMin']").val();

			if (departureHour == "" || departureTerminal == "" || departureMin == "") {
				alert("請填寫取機航廈與航班大約時間");
				return false;
			}

			if (returnWay != "A") {
				alert("請選取機場還貨及填寫航廈與航班大約時間");
				return false;
			}
		}

		if (pickWay == "UNIMARTC2C" || pickWay == "FAMIC2C" || pickWay == "HILIFEC2C") {
			if (userName.length > 10) {
				alert("超商取貨名字不可大於十個字");
				return false;
			}

			if (ttlAmt >= 20000) {
				alert("超商取貨金額不能大於兩萬元");
				return false;
			}
		}

		if (payWay == "CVS") {
			if (ttlAmt >= 20000) {
				alert("超商付款金額不能大於兩萬元");
				return false;
			}
		}


		if (returnWay == "A") {
			var returnTerminal = $("select[name='returnTerminal']").val();
			var returnHour = $("select[name='returnHour']").val();
			var returnMin = $("select[name='returnMin']").val();

			if (returnHour == "" || returnTerminal == "" || returnMin == "") {
				alert("請填寫航廈與航班大約時間");
				return false;
			}
		}

		if (invoiceType == "a3") {
			var customerIdentifier = $("#customerIdentifier").val();
			var customerTitle = $("#customerTitle").val();

			if (customerIdentifier == "" || customerTitle == "") {
				alert("請填寫統一編號與發票抬頭");
				return false;
			}
		}


		if ($("#policy").prop("checked") == false) {
			alert("請勾選同意隱私權政策");
			return false;
		}

		if ($("#agreeReturn").prop("checked") == false) {
			alert("請勾選同意退貨條款");
			return false;
		}

		$("#refreshed").val("yes");

		$this = this;
		$("#btnPay").prop("disabled", true);
		payProcess();
		if(payWay == "Credit") {
			
			$("#btnPay").text("與信用卡中心連結中...請勿重新整理");
			if($("input[name='useQuickPay']").val() == "YES") {
				this.submit();	
			}
			else {
				TPDirect.card.getPrime(function(result) {
					if(result.status == 0) {
						$("input[name='prime']").val(result.card.prime);
						$("input[name='agreePayRemeber']").val($("input[name='creditRemember']:checked").val());
						$this.submit();
					}
					else {
						alert(result.msg);
					}
					
				});
			}
			
		}
		else if(payWay == "GooglePay") {
			//googlePay();
			
            TPDirect.googlePay.getPrime(function(err, prime){
				if(prime != null) {
					$("input[name='prime']").val(prime);
					$this.submit();
				}
				else {
					alert("無法使用Google Pay");
					return false;
				}
            });
		}
		else if(payWay == "LinePay") {
			TPDirect.linePay.getPrime(function(result) {
				if(result.prime != null) {
					$("input[name='prime']").val(result.prime);
					$this.submit();
				}
				else {
					alert("無法使用Line Pay");
					return false;
				}
			})
		}
		else if(payWay == "SamsungPay") {
			samsungPay(ttlAmt);
			TPDirect.samsungPay.getPrime(function (result) {
				if (result.status !== 0) {
					return console.log('getPrime failed: ' + result.msg)
				}
			
				// 把 prime 傳到您的 server，並使用 Pay by Prime API 付款
				var prime = result.prime;
				$("input[name='prime']").val(result.prime);
				$this.submit();
			})
		}
		else if(payWay == "ApplePay") {
			if(TPDirect.paymentRequestApi.checkAvailability()) {
				applePay(ttlAmt);

				TPDirect.paymentRequestApi.getPrime(function(result) {
					console.log('paymentRequestApi.getPrime result', result);
					var prime = result.prime;

					$("input[name='prime']").val(result.prime);
					$this.submit();
				});

				return false;
			}
			else {
				alert("您的瀏覽器不支援Apple Pay");
				return;
			}
		}
		else {
			this.submit();
		}
		

		return false;

	});

	var numberWithCommas = function (x) {
		return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}

	$("input[name='invoiceType']").on("change", function () {
		if ($(this).val() == "a3") {
			$("#customerIdentifier").prop("disabled", false);
			$("#customerTitle").prop("disabled", false);
		} else {
			$("#customerIdentifier").prop("disabled", true);
			$("#customerTitle").prop("disabled", true);
		}

	});

	function chkInsuranceForCookie(uuid, insurance) {
		$.post(BASE_URL + '/updateInsuranceToCart', {
			'uuid': uuid,
			'insurance': insurance
		}, function (data) {
			CART_CONTENT = data.cartContent;
			updateCart();
			if (data.status != "success") {

				alert(data.message);
			}
		});
		/*
		var cartContent = $.cookie("cartContent");
		var cartObj = JSON.parse(cartContent);

		for(i in cartObj["rData"]) {
		  var id = cartObj["rData"][i].prodDetailId;

		  if(id == prodDetailId) {
		    cartObj["rData"][i].insurance = insurance;
		  }
		}

		cartContent = JSON.stringify(cartObj);

		$.cookie("cartContent", cartContent, { path:'/', expires: 3 });
		*/
	}

	function updateCart() {
		//var cartContent = $.cookie("cartContent");
		//var cartObj = JSON.parse(cartContent);
		var cartObj = CART_CONTENT;
		var rAmt = 0;
		var sAmt = 0;
		var ttlAmt = 0;
		var isDiscount = false;
		var discountNum = 0;
		var discountPrice = 0;

		var rentDnt = parseFloat($("input[name='rentDntNum']").val()) || 0;
		var sellDnt = parseFloat($("input[name='sellDntNum']").val()) || 0;

		for (i in cartObj["rData"]) {
			var num = parseInt(cartObj["rData"][i].num);
			var startDate = cartObj["rData"][i].startDate;
			var endDate = cartObj["rData"][i].endDate;
			var dPrice = parseInt(cartObj["rData"][i].dPrice);
			var sDT = new Date(startDate);
			var eDT = new Date(endDate);
			var day = sDT.dateDiff("d", eDT) + 1;
			var insurance = cartObj["rData"][i].insurance;
			var uuid = cartObj["rData"][i].uuid;
			var thisAmt = 0;
			if (day >= 5) {
				isDiscount = true;
				for (var j = 0; j < num; j++) {
					discountNum++;
				}
			}

			rAmt = rAmt + (num * day * dPrice);
			thisAmt = num * day * dPrice;
			if (insurance == "Y") {
				rAmt = rAmt + (50 * day * num);
				thisAmt = thisAmt + (50 * day * num);
			}
			$("td[uuid='" + uuid + "']").text(numberWithCommas(thisAmt));
		}

		var rentDiscount = 0;

		if (rentDnt > 0) {
			//rentDiscount = rAmt - Math.round(rAmt * rentDnt);
			rentDiscount = rentDnt;
			$("#rentDiscount").text(rentDiscount);
		} else {
			$("#rentDiscount").text(0);
		}

		rAmt = rAmt - rentDiscount; //租用使用優惠碼

		if (cartObj["rData"].length == 0) {
			$("input[name='return_way']").prop("checked", false);
		}



		var discountObj = {
			'11': 0,
			'10': 0,
		};
		for (i in cartObj["sData"]) {
			var prodId = cartObj["sData"][i].prodId;
			var num = parseInt(cartObj["sData"][i].num);
			if (prodId == 11) {
				discountObj['11'] = discountObj['11'] + num;
			}

			if (prodId == 10) {
				discountObj['10'] = discountObj['10'] + num;
			}
		}

		for (i in cartObj["sData"]) {
			var num = parseInt(cartObj["sData"][i].num);
			var dPrice = parseInt(cartObj["sData"][i].dPrice);

			sAmt = sAmt + (num * dPrice);
		}

		var disSum = discountObj['11'] + discountObj['10'];
		for (var j = 0; j < discountObj['11']; j++) {
			if (isDiscount === true) {
				if (discountNum > 0 && disSum > 0) {
					discountPrice = discountPrice + 980;
					discountNum--;
					disSum--;
				}
			}
		}

		for (var j = 0; j < discountObj['10']; j++) {
			if (isDiscount === true) {
				if (discountNum > 0 && disSum > 0) {
					discountPrice = discountPrice + 1380;
					discountNum--;
					disSum--;
				}
			}
		}

		var sellDiscount = 0;
		if (sellDnt > 0) {
			//sellDiscount = sAmt - Math.round(sAmt * sellDnt);
			sellDiscount = sellDnt;
			$("#sellDiscount").text(sellDiscount);
		} else {
			$("#sellDiscount").text(0);
		}

		sAmt = sAmt - sellDiscount;

		ttlAmt = rAmt + sAmt;

		ttlAmt = ttlAmt - discountPrice;

		var cartObj = CART_CONTENT;
		var rDatalist = 0;
		var sDatalist = 0;
		// for(i in cartObj["rData"]) {
		//   rDatalist       += parseInt(cartObj["rData"][i].num);
		// }
		// for(i in cartObj["sData"]) {
		//   sDatalist       += parseInt(cartObj["sData"][i].num);
		// }

		var pick_way = $("input[name='pick_way']:checked").val();
		var return_way = $("input[name='return_way']:checked").val();
		var shipFee = 0;
		if (pick_way == "B") {
			shipFee += 80;
		} else if ((pick_way == "UNIMARTC2C" || pick_way == "FAMIC2C" || pick_way == "HILIFEC2C") || pick_way == "CVSCOM") {
			shipFee += 65;
		}

		if (return_way == "B") {
			shipFee += 80;
		}

		if ((cartObj["rData"].length + cartObj["sData"].length) == 0) {
			shipFee = 0;
		}
		ttlAmt += shipFee;

		if (isDiscount == true && discountPrice > 0) {
			$("#sellDiscountInput").hide();
		} else {
			$("#sellDiscountInput").show();
		}

		$("input[name='shipFee']").val(shipFee);
		$("#shipFee").text(numberWithCommas(shipFee));

		$("input[name='rSumAmt']").val(rAmt);
		$("#rSumAmt").text(numberWithCommas(rAmt));

		$("input[name='sSumAmt']").val(sAmt);
		$("#sSumAmt").text(numberWithCommas(sAmt));

		$("input[name='ttlAmt']").val(ttlAmt);
		$("#ttlAmt").text(numberWithCommas(ttlAmt));
		$("#discountPrice").text(numberWithCommas(discountPrice))
	}

	Date.prototype.dateDiff = function (interval, objDate) {
		var dtEnd = new Date(objDate);
		if (isNaN(dtEnd)) return undefined;
		switch (interval) {
			case "s":
				return parseInt((dtEnd - this) / 1000);
			case "n":
				return parseInt((dtEnd - this) / 60000);
			case "h":
				return parseInt((dtEnd - this) / 3600000);
			case "d":
				return parseInt((dtEnd - this) / 86400000);
			case "w":
				return parseInt((dtEnd - this) / (86400000 * 7));
			case "m":
				return (dtEnd.getMonth() + 1) + ((dtEnd.getFullYear() - this.getFullYear()) * 12) - (this.getMonth() + 1);
			case "y":
				return dtEnd.getFullYear() - this.getFullYear();
		}
	}

	$("input[name='pick_way']").on("change", function () {
		var val = $(this).val();
		if (val == "A") {
			$("#ShipReturnWay").hide();
		} else {
			$("#ShipReturnWay").show();
		}

		$.get(BASE_URL + '/chkship/' + val, {}, function (data) {
			if (data.onlyHomeDlv == true) {
				alert('您所購買的商品只能選擇宅配！！！');
				$('#get-wifi-2').prop('checked', true);
				$(this).prop('checked', false);
				hideMap();
			}

			updateCart();
			return;
		});


		if(val == "UNIMARTC2C" || val == "FAMIC2C" || val == "HILIFEC2C") {
			showMap();
		}
		else {
			hideMap();
		}


	});

	$("input[name='return_way']").on("change", function () {
		updateCart();
	});
	updateCart();


	$("#dlvCity").on("change", function () {
		var val = $(this).val();

		$.get(BASE_URL + '/getAreaByCity', {
			'dlv_city': val
		}, function (data) {
			if (data.status == "success") {
				var areaData = data.areaData;
				var str = '<option value="" selected>請選擇鄉鎮市區</option>';
				for (i in areaData) {
					if (dlvArea == areaData[i]["dist_nm"]) {
						str += '<option zip="' + areaData[i]["zip_f"] + '" value="' + areaData[i]["dist_nm"] + '" selected>' + areaData[i]["dist_nm"] + '</option>';
					} else {
						str += '<option zip="' + areaData[i]["zip_f"] + '" value="' + areaData[i]["dist_nm"] + '">' + areaData[i]["dist_nm"] + '</option>';
					}

				}

				$("#dlvArea").html(str);
			}
		});
	});


	$("#dlvArea").on("change", function () {
		var zip = $("#dlvArea option:selected").attr("zip");
		$("#dlvZip").val(zip);
	});



	$("#useRentDiscount").on("click", function () {
		ajaxDiscount($("#rentDiscountCode").val(), "R");
	});

	$("#useSellDiscount").on("click", function () {
		ajaxDiscount($("#sellDiscountCode").val(), "S");
	});

	$("#rentDiscountCode").on("change", function () {
		var val = $(this).val();

		if (val != "") {
			ajaxDiscount(val, "R");
		}


		if (val == "") {
			$("input[name='rentDiscountCode']").val("");
			$("input[name='rentDntNum']").val("");

			updateCart();
		}
	});

	$("#sellDiscountCode").on("change", function () {
		var val = $(this).val();

		if (val != "") {
			ajaxDiscount(val, "S");
		}


		if (val == "") {
			$("input[name='sellDiscountCode']").val("");
			$("input[name='sellDntNum']").val("");

			updateCart();
		}
	});

	function ajaxDiscount(code, sellType) {
		$.ajax({
			type: 'POST',
			url: BASE_URL + '/getDiscountCode',
			data: {
				"code": code,
				"sellType": sellType
			},
			success: function (data) {
				if (data.num == 0) {
					alert("您的優惠碼無效或已過期或商品金額未達折扣標準");
					if (sellType == "R") {
						$("input[name='rentDiscountCode']").val("");
						$("input[name='rentDntNum']").val("");
						$("#rentDiscountCode").val("");
					} else {
						$("input[name='sellDiscountCode']").val("");
						$("input[name='sellDntNum']").val("");
						$("#sellDiscountCode").val("");
					}
				} else {
					if (sellType == "R") {
						$("input[name='rentDiscountCode']").val(code);
						$("input[name='rentDntNum']").val(data.num);
					} else {
						$("input[name='sellDiscountCode']").val(code);
						$("input[name='sellDntNum']").val(data.num);
					}
				}

				updateCart();
			},
			dataType: 'JSON',
			async: false
		});

		return;
	}

	$("#dlvCity").trigger("change");


	function validateEmail(email) {
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email);
	}

	function chkPhone(pickWay, phone) {
		var msg = "";
		if(pickWay == "B") {
			if(phone.length > 20) {
				msg = "選擇宅配電話不可超過20碼";
			}
		}
		else {
			if(phone.length > 10) {
				msg = "選擇超商取貨電話只能輸09開頭的手機號碼，共10碼";
			}
		}

		return msg;
	}

	function showMap() {
		$(".trCvsMap").show();
	}

	function hideMap() {
		$(".trCvsMap").hide();
		$("#CVSStoreID").val("");
		$("#CVSStoreName").val("");
		$("#CVSAddress").val("");
	}

	var statusTable = {
		'0': '欄位已填好，並且沒有問題',
		'1': '欄位還沒有填寫',
		'2': '欄位有錯誤，此時在 CardView 裡面會用顯示 errorColor',
		'3': '使用者正在輸入中',
	}

	var defaultCardViewStyle = {
		color: 'rgb(0,0,0)',
		fontSize: '15px',
		lineHeight: '24px',
		fontWeight: '300',
		errorColor: 'red',
		placeholderColor: ''
	}

	TPDirect.card.setup('#tappay-iframe', defaultCardViewStyle)
	TPDirect.card.onUpdate(function (update) {
		var submitButton = document.querySelector('#btnPay')
		var cardViewContainer = document.querySelector('#tappay-iframe')
		if (update.canGetPrime) {
			$("#btnPay").prop("disabled", false);
		} else {
			$("#btnPay").prop("disabled", true);
		}
	});

	$("#btnPay").on("click", function (){
		$("#payForm").submit();
	});

	$("#btnChangeCard").on("click", function(){
		$("#creditCardArea").show();
		$("#quickPayment").hide();
		$("#btnPay").prop("disabled", true);
		$("input[name='useQuickPay']").val("NO");
	});

	function payProcess() {
		Swal.fire({
			type: 'info',
			title: '付款連線中...請勿關掉或重新整理',
			showConfirmButton: false
		});
	}
});