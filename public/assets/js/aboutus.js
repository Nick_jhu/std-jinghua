$(function () {

  //
  function setVideoSize() {
    console.log(window.innerWidth);
    console.log(window.innerHeight);
    if (window.innerWidth > window.innerHeight) {    
      $('.video-box video').addClass('wb');
    } else {
      $('.video-box video').addClass('hb');
    }
  }
  
  $( window ).resize(function() {    
    $('.video-box video').removeClass();
    setVideoSize();
  });

  setVideoSize();
  
});