$(function () {
	// web nav
	$('.nav-box ul li.dropdown').hover(function() {
		$(this).find('.dropdown-menu').stop(true, true).delay(100).fadeIn(300);
	}, function() {
		$(this).find('.dropdown-menu').stop(true, true).delay(100).fadeOut(300);
	});

	// mobile nav
	$('.mobile.header .menu-box').on('click', function(e){
		$('.mobile.header .bottom-box').slideToggle();
	})
	$('.mobile.header .bottom-box .mbtn.dropdown').on('click', function(e){
		var tagert = e.currentTarget;
		$(tagert).children('ul.subbtnlist').slideToggle();
	})
  
  	$('.subbtn').click(function(event) {
		event.stopPropagation();
		// Do something
	});

});