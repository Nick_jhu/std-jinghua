$(function () {
      // $("#datepicker-start").datepicker({ 
      //       autoclose: true, 
      //       todayHighlight: false,
      //       startDate: '+2d',
      //       endDate: '+90d'
      // });

      // $("#datepicker-end").datepicker({ 
      //       autoclose: true, 
      //       todayHighlight: false,
      //       startDate: '+3d',
      //       endDate: '+90d'
      // });


      $("#brand").on("change", function(){
            const val = $(this).val();
            const sorting = $("#sort-by").val();

            if(val == 'all') {
                  location.href = BASE_URL + '/product?sorting=' + sorting;
            }
            else {
                  location.href = BASE_URL + '/product?brand=' + val + '&sorting=' + sorting;
            }
      });


      $("#sort-by").on("change", function(){
            const val = $(this).val();
            const brand = $("#brand").val();
            var url = new URL(document.URL);
            var search = url.searchParams.get("search");
            const cate = url.searchParams.get("cateId");
            var searchTxt = '';
            if(search != null) {
                  searchTxt = '&search=' + search;
            }

            if(cate != null) {
                  searchTxt += '&cateId=' + cate;
            }

            if(brand == 'all') {
                  location.href = BASE_URL + '/product?sorting=' + val + searchTxt;
            }
            else {
                  location.href = BASE_URL + '/product?sorting=' + val + '&brand=' + brand + searchTxt;
            }
      });


});

function parse_query_string(query) {
      var vars = query.split("&");
      var query_string = {};
      for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split("=");
            var key = decodeURIComponent(pair[0]);
            var value = decodeURIComponent(pair[1]);
            // If first entry with this name
            if (typeof query_string[key] === "undefined") {
            query_string[key] = decodeURIComponent(value);
            // If second entry with this name
            } else if (typeof query_string[key] === "string") {
            var arr = [query_string[key], decodeURIComponent(value)];
            query_string[key] = arr;
            // If third or later entry with this name
            } else {
            query_string[key].push(decodeURIComponent(value));
            }
      }
      return query_string;
}