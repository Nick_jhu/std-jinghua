$(function () {

  // banner
  $('.carousel').carousel({
    interval: 10000
  });

  // 國家 web
  $('#country-slide-box-web .photos-box .photo-box:not(:eq(0))').hide();
  $('#country-slide-box-web .btn-box .btnitem').on('click', function(e){
    var ta = e.currentTarget;
    $('#country-slide-box-web .btn-box .btnitem').removeClass('active');
    $(ta).addClass('active');
    var idx = $(ta).index();
    if ($('#country-slide-box-web .photos-box .photo-box').eq(idx).is(':visible')) {

    } else {
      $('#country-slide-box-web .photos-box .photo-box').fadeOut();
      $('#country-slide-box-web .photos-box .photo-box').eq(idx).fadeIn();
    }
    
  });

  // 國家 mobile
  $('#country-slide-box-mobile .photos-box .photo-box:not(:eq(0))').hide();
  $('.selectCountry').on('change', function(e) {
    var idx = $('.selectCountry').val();
    $('#country-slide-box-mobile .photos-box .photo-box').fadeOut();
    $('#country-slide-box-mobile .photos-box .photo-box').eq(idx).fadeIn();
  })

  // search 
  $("#datepicker-start").datepicker({ 
        autoclose: true, 
        todayHighlight: true
  }).datepicker('update', new Date());
  
  $("#datepicker-end").datepicker({ 
        autoclose: true, 
        todayHighlight: true
  }).datepicker('update', new Date());

  // services
  $(".service-box").on("click", function(e){
    var ta = e.currentTarget;
    $(".service-box").removeClass('open');
    $(ta).addClass('open');
  })

	
  // fans
  var fansBgId = 0;
  $('#main-container #content-box-6 .images-box .img').eq(fansBgId).fadeIn("slow");
  var fansId = setInterval(()=>{
    $('#main-container #content-box-6 .images-box .img').fadeOut("fast");
    if (fansBgId<9) {
      fansBgId ++;
    } else {
      fansBgId = 0
    }
    $('#main-container #content-box-6 .images-box .img').eq(fansBgId).fadeIn("slow");
  }, 5000);

  //
  function setVideoSize() {
    //427*240
    // if (window.innerWidth > window.innerHeight - 90) {    
    //   $('.video-box video').addClass('wb');
    // } else {
    //   $('.video-box video').addClass('hb');
    // }

    console.log($('.video-box:eq(0)').width());
    console.log($('.video-box:eq(0)').height());
    var _w = $('.video-box:eq(0)').width();
    var _h = $('.video-box:eq(0)').height();
    if (427/240 > _w/_h) {
      $('.video-box video').addClass('hb');
    } else {
      $('.video-box video').addClass('wb');
    }
  }
  
  $( window ).resize(function() {    
    $('.video-box video').removeClass();
    setVideoSize();
  });

  setVideoSize();
  
});