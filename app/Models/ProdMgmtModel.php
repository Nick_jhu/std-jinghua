<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use DB;
use Carbon\Carbon;

class ProdMgmtModel extends Model
{
    use CrudTrait;

    /*
	|--------------------------------------------------------------------------
	| GLOBAL VARIABLES
	|--------------------------------------------------------------------------
	*/

	protected $table = 'mod_product';
	// protected $primaryKey = 'cust_no';
	// protected $guarded = [];
	// protected $hidden = ['id'];
	protected $guarded = array('id');
	public $timestamps = true;



	public function product()
    {
        return $this->hasMany('App\Models\Banner', 'prod_id');
	}
	
	public function processExcelData($dataFirst, $mode, $mainCol, $user) {
		$mainViewModel = [
            '商品編號' => 'prod_no',
            '銷售方式' => 'sell_type',
			'商品類別' => 'cate_id',
			'適用地區' => 'country',
            '商品名稱' => 'title',
            '副標題' => 'sub_title',
            '價格區間起' => 'f_price',
            '價格區間迄' => 't_price',
            '付款方式'  => 'pay_way',
            '送貨方式'  => 'ship_way',
            '上架時間'  => 'added_on',
            '商品簡述'  => 'slogan',
            '商品描述'  => 'descp',
            '支援地區'  => 'other_content',
			'刪除主檔'  => 'del',
			'折扣'	   => 'd_percent',
			'活動日期'	=> 'action_date',
			'熱門商品'	=> 'is_hot',
			'主檔順序'	=> 'sort',
			'品牌'     => 'brand',
			'商品圖片'     => 'img1'
        ];

        $detailViewModel = [
			'型號編號' => 'id',
			'序號' => 'sn',
			'已使用' => 'is_sell',
            '名稱' => 'title',
            '庫存' => 'stock',
            '順序' => 'seq',
            '原價' => 'o_price',
            '特價' => 'd_price',
			'刪除明細' => 'del',
			'明細圖片'     => 'img1'
        ];
		$prodData = [];

		foreach ($dataFirst as $key => $value) { 
			$seq = 0;
			$main = "";
			$detailArray = array();
			foreach($value as $idx => $val) {                   
				if($seq == 0) {
					$main = $val;
					if(!isset($prodData[$main])) {
						$prodData[$main] = array();
					}
					
					if(isset($mainViewModel[$idx])) {
						$prodData[$main][$mainViewModel[$idx]] = $val;
					}
						
				}
				else {
					if($seq < $mainCol) {
						if(isset($mainViewModel[$idx])) {
							$prodData[$main][$mainViewModel[$idx]] = $val;
						}
						
					}
					else {
						if(isset($detailViewModel[$idx])) {
							$detailArray[$detailViewModel[$idx]] = $val;
						}
					}
				}
				
				$seq++;
			}

		
			if(isset($prodData[$main]['detailItems'])) {
				array_push($prodData[$main]['detailItems'], $detailArray);
			}
			else {
				$prodData[$main]['detailItems'] = array();
				array_push($prodData[$main]['detailItems'], $detailArray);
			}
			
		}

		return $this->excelDataToDb($prodData, $mode, $user);

	}

	public function excelDataToDb($prodData, $mode, $user) {
		$mInsertArray = [];
		$mUpdateArray = [];
		$dInsertArray = [];
		$dUpdateArray = [];
		$softwareProdDetail = [];
		foreach($prodData as $k => $mainRow) {
			$mainIsExist = $this::where('prod_no', $mainRow['prod_no'])->count();
			
			if(!isset($mainRow['prod_no'])) {
				//return ["msg"=>"fail", 'log' => '請輸入商品編號'];
				continue;
			}

			if($mainRow['del'] == 'Y') {
				$this::where('prod_no', $mainRow['prod_no'])->delete();
				DB::table('mod_product_detail')->where('prod_no', $mainRow['prod_no'])->delete();
			}
			else {
				$country = null;

				if(isset($mainRow['country'])) {
					$country = DB::table('mod_cate')->where('name', $mainRow['country'])->where('parent_id', 6)->pluck('id')[0];
				}

				$main = [
					'prod_no'       => (isset($mainRow['prod_no'])) ? $mainRow['prod_no'] : null,
					'sell_type'     => (isset($mainRow['sell_type'])) ? $mainRow['sell_type'] : null,
					'cate_id'       => (isset($mainRow['cate_id'])) ? $mainRow['cate_id'] : null,
					'country'       => $country,
					'title'         => (isset($mainRow['title'])) ? $mainRow['title'] : null,
					'sub_title'     => (isset($mainRow['sub_title'])) ? $mainRow['sub_title'] : null,
					'f_price'       => (isset($mainRow['f_price'])) ? $mainRow['f_price'] : null,
					't_price'       => (isset($mainRow['t_price'])) ? $mainRow['t_price'] : null,
					'pay_way'       => (isset($mainRow['pay_way'])) ? $mainRow['pay_way'] : null,
					'ship_way'      => (isset($mainRow['ship_way'])) ? $mainRow['ship_way'] : null,
					'added_on'      => (isset($mainRow['added_on'])) ? $mainRow['added_on'] : null,
					'slogan'        => (isset($mainRow['slogan'])) ? $mainRow['slogan'] : null,
					'descp'         => (isset($mainRow['descp'])) ? $mainRow['descp'] : null,
					'other_content' => (isset($mainRow['other_content'])) ? $mainRow['other_content'] : null,
					'd_percent'     => (isset($mainRow['d_percent'])) ? $mainRow['d_percent'] : null,
					'action_date'   => (isset($mainRow['action_date'])) ? $mainRow['action_date'] : null,
					'is_hot'      	=> (isset($mainRow['is_hot'])) ? $mainRow['is_hot'] : null,
					'sort'   		=> (isset($mainRow['sort'])) ? $mainRow['sort'] : null,
					'brand'   		=> (isset($mainRow['brand'])) ? $mainRow['brand'] : null,
					'img1'   		=> (isset($mainRow['img1'])) ? $mainRow['img1'] : null,
					'brand_nm'   	=> (isset($mainRow['brand'])) ? DB::table('bscode')->where('cd_type', 'BRAND')->where('cd', $mainRow['brand'])->value('cd_descp') : null,
					'prod_type'     => $mode,
					'created_by'    => $user->email,
					'updated_by'    => $user->email,
					'created_at'    => Carbon::now()->toDateTimeString(),
					'updated_at'	=> Carbon::now()->toDateTimeString(),
					'g_key' 		=> $user->g_key,
					'c_key' 		=> $user->c_key,
					's_key' 		=> $user->s_key,
					'd_key' 		=> $user->d_key
				];


				if($mainIsExist > 0) {
					$this::where('prod_no', $mainRow['prod_no'])->update($main);
				}
				else {
					array_push($mInsertArray, $main);
					if($mode == 'S') {
						array_push($softwareProdDetail, [
							'prod_no' => (isset($mainRow['prod_no'])) ? $mainRow['prod_no']: null,
							'title'   => (isset($mainRow['title'])) ? $mainRow['title']    : null,
							'seq'     => 999,
							'o_price' => (isset($mainRow['f_price'])) ? $mainRow['f_price']: null,
							'd_price' => (isset($mainRow['f_price'])) ? $mainRow['f_price']: null
						]);
					}
					
				}

				foreach($mainRow['detailItems'] as $v => $detailRow) {
					if($mode == 'S') {
						$detailIsExist = DB::table('mod_sn')->where('id', $detailRow['id'])->count();
					}
					else {
						$detailIsExist = DB::table('mod_product_detail')->where('id', $detailRow['id'])->count();
					}
					

					if($detailRow['del'] == 'Y') {
						if($mode == 'S') {
							DB::table('mod_sn')->where('id', $detailRow['id'])->delete();
						}
						else {
							DB::table('mod_product_detail')->where('id', $detailRow['id'])->delete();
						}
						
					}
					else {

						$detail = [];

						if($mode == 'S') {
							$detail = [
								'prod_no'    => $mainRow['prod_no'],
								'sn'         => (isset($detailRow['sn'])) ? $detailRow['sn']: null,
								'is_sell'    => (isset($detailRow['is_sell'])) ? $detailRow['is_sell']: null,
								'created_at' => Carbon:: now()->toDateTimeString(),
								'updated_at' => Carbon:: now()->toDateTimeString()
							];
						}
						else {
							$detail = [
								'prod_no' => $mainRow['prod_no'],
								'title'   => (isset($detailRow['title'])) ? $detailRow['title']    : null,
								'stock'   => (isset($detailRow['stock'])) ? $detailRow['stock']    : null,
								'img1'   => (isset($detailRow['img1'])) ? $detailRow['img1']    : null,
								'seq'     => (isset($detailRow['seq'])) ? $detailRow['seq']        : 999,
								'o_price' => (isset($detailRow['o_price'])) ? $detailRow['o_price']: null,
								'd_price' => (isset($detailRow['d_price'])) ? $detailRow['d_price'] : null,
								'created_by'    => $user->email,
								'updated_by'    => $user->email,
								'created_at'    => Carbon::now()->toDateTimeString(),
								'updated_at'	=> Carbon::now()->toDateTimeString()
							];
						}

						if($detailIsExist > 0) {
							if($mode == 'S') {
								DB::table('mod_sn')->where('id', $detailRow['id'])->update($detail);
							}
							else {
								DB::table('mod_product_detail')->where('id', $detailRow['id'])->update($detail);
							}
							
						}
						else {

							if($mode == 'S' && isset($detail['sn'])) {
								array_push($dInsertArray, $detail);
							}
							else if(isset($detail['title'])) {
								array_push($dInsertArray, $detail);
							}
							
						}
					}
				}
			}	
		}

		if(count($mInsertArray) > 0) {
			$this::insert($mInsertArray);

			if(count($softwareProdDetail) > 0) {
				DB::table('mod_product_detail')->insert($softwareProdDetail);
			}
		}

		if(count($dInsertArray) > 0) {
			if($mode == 'S') {
				DB::table('mod_sn')->insert($dInsertArray);
			}
			else {
				DB::table('mod_product_detail')->insert($dInsertArray);
			}
			
		}

		return ["msg"=>"success"];

	}

	public function getShipWayByProdNoToArray($prodNo) {
		$data = array();
		$shipWay = $this::where('prod_no', $prodNo)->value('ship_way');
		if(isset($shipWay))
			$data = explode(',', $shipWay);

		return $data;
	}

	public function getPeriodsToArray($prodNo) {
		$data = array();
		$periods = $this::where('prod_no', $prodNo)->value('number_of_periods');

		if(isset($periods)) {
			$periods = explode(',', $periods);

			$bscode = DB::table('bscode')->where('cd_type', 'PERIODS')->whereIn('cd', $periods)->get();
		
			foreach($bscode as $row) {
				$data[(string)$row->cd] = array(
						'cd' => $row->cd,
						'cd_descp' => $row->cd_descp,
						'showTxt' => $row->cd_descp,
						'periods' => $row->value1,
						'value' => $row->value2,
						'value3' => $row->value3,
						'amt' => 0
					);
			}
		}

		return $data;
	}

	public function updatePeriodsArrayLabel($periodsArray, $ttlAmt) {
		foreach($periodsArray as $key=>$row) {
			$val = (double)($row['value'] / 100);
			
			$pAmt = round(($ttlAmt + ($ttlAmt * $val)) / $row['periods'], 0);
			
			$periodsArray[$key]['cd_descp'] = "{$row['periods']}期 x {$pAmt}元({$row['value']}%)";

			$periodsArray[$key]['amt'] = $pAmt;
		}
		
		return $periodsArray;
	}

	public function getInstalmentRate($cd) {
		$rate = DB::table('bscode')->where('cd_type', 'PERIODS')->where('cd', $cd)->value('value2');

		if(isset($rate)) {
			return $rate / 100;
		}

		return 0;
	}

	public function processPeriodsArray($periods) {
		$bsdata = DB::table('bscode')->where('cd_type', 'PERIODS')->get();
		foreach($bsdata as $key=>$row) {
			if(isset($periods[$row->value1.'a']) && isset($periods[$row->value1.'_'.'0'])) {
				unset($periods[$row->value1.'_'.'0']);
			}
		}

		usort($periods, function($a, $b)
		{
			if($a['value'] > $b['value'])
				return 1;
			else 
				return -1;
		});

		$data = array();

		foreach($periods as $row) {
			$data[$row['cd']] = $row;
		}

		return $data;
	}
	public function updateactionprice() {
		$now = date('Y-m-d');
		$f_price = 0;
		$t_price = 0;
		$prodMgmtModel = DB::table('mod_product')->where('action_date' ,'<', $now)->get();
        if(isset($prodMgmtModel)) {
            foreach ($prodMgmtModel as $row) {
                $prodDetail = DB::table('mod_product_detail')
                            ->select(
								'mod_product_detail.id',
                                DB::raw('(select max(d.end_price) from mod_product_detail as d where d.prod_no=mod_product_detail.prod_no) as t_price'),
								DB::raw('(select min(d.end_price) from mod_product_detail as d where d.prod_no=mod_product_detail.prod_no) as f_price'),
								'mod_product_detail.end_price'
                            )
                            ->where('prod_no', $row->prod_no)
                            ->where('end_price' ,'>', 0)
							->get();
                if (isset($prodDetail)) {
                    foreach ($prodDetail as $rowdetail) {
						$f_price=$rowdetail->f_price;
						$t_price=$rowdetail->t_price;
						DB::table('mod_product_detail')
						->where('id', $rowdetail->id)
						->update(['d_price' => $rowdetail->end_price,
								'end_price' => null,
								'updated_at'=> Carbon::now()->toDateTimeString()]);
					}
					if ($f_price == $t_price) {
						DB::table('mod_product')
						->where('id', $row->id)
						->update(['f_price' => $f_price,
								't_price' =>null,
								'updated_at'=> Carbon::now()->toDateTimeString()
								]);
					} else {
						DB::table('mod_product')
						->where('id', $row->id)
						->update(['f_price' => $f_price,
								't_price' => $t_price,
								'updated_at'=> Carbon::now()->toDateTimeString()
						]);
					}
                }
            }
        }
    }
}
