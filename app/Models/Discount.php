<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use App\Models\ModCart;
use DB;

class Discount extends Model {

	use CrudTrait;

    /*
	|--------------------------------------------------------------------------
	| GLOBAL VARIABLES
	|--------------------------------------------------------------------------
	*/

	protected $table = 'mod_discount';
	// protected $primaryKey = 'id';
	//protected $guarded = ['id'];
	// protected $hidden = ['id'];
	protected $fillable = ['title', 'descp','discount','f_date','t_date','created_by','updated_by','discount_type','sell_type','cd','amt', 'limit_amt', 'prod_id', 'discount_mode', 'discount_way'];
	public $timestamps = true;
	protected $casts = [
        'f_date' => 'datetime',
		't_date' => 'datetime',
		'prod_id' => 'array'
    ];

	/*
	|--------------------------------------------------------------------------
	| FUNCTIONS
	|--------------------------------------------------------------------------
    */
    
    public function sendUserDiscountCode($crud = false)
    {
		$id            = $this->attributes['id'];
        $discount_type = $this->attributes['discount_type'];

        if($discount_type == 'B') {
            return false;
		}

		return '<a onclick="return confirm(\'確定要發送嗎?\');" class="btn btn-xs btn-default" href="'.url(config('backpack.base.route_prefix', 'admin')).'/sendUserDiscountCode/'.$id.'"><i class="fa fa-envelope"></i>產生優惠碼</a>';
	}

	public function olduseDiscount($code, $sellType, $userId) {
		$cart = DB::table('mod_cart')->where('user_id', $userId)->first();
		$discount = DB::table('mod_discount_member')->where('cd', $code)->first();
		
		$num = 0;
		$today = new \DateTime();

		$today = strtotime($today->format('Y-m-d H:i:s'));
		$canUseDiscount = false;
		$prodId = 0;
		$cartAmt = 0;
		if(isset($discount) && isset($cart)) {
			$cartContent = $cart->content;
			$cartData = json_decode($cartContent);
			
			if($discount->discount_mode == "B") {
				$prodArray = json_decode($discount->prod_id);

				$cartProds = $this->getCartIds($cartData, $sellType);

				for($i=0; $i<count($cartProds); $i++) {
					for($j=0; $j<count($prodArray); $j++) {
						if($cartProds[$i] == $prodArray[$j]) {
							$prodId = $prodArray[$j];
							$canUseDiscount = true;
							break;
						}
					}
				}
			}
			else {

				if($sellType == "R") {
					$cartAmt = $this->getRentAmt($cartData);
				}
				else {
					$cartAmt = $this->getSellAmt($cartData);
				}

				if($discount->limit_amt <= $cartAmt) {
					$canUseDiscount = true;
				}

				
			}

			
			if($sellType  == $discount->sell_type && $canUseDiscount == true) {
				if($discount->discount_type == "B") {
					
					if($today >= strtotime($discount->f_date) && $today <= strtotime($discount->t_date)) {
						//$num = $discount->discount / 100;
						$num = $discount->discount;
					}
					else {
						return $num;
					}
				}
				else {
					if($today >= strtotime($discount->f_date) && $today <= strtotime($discount->t_date) && $discount->status == 'N') {
						//$num = $discount->discount / 100;
						$num = $discount->discount;
					}
					else {
						return $num;
					}
				}
			}
			else {
				return $num;
			}
		}

		return $num;
	}

	public function useDiscount($code, $sellType, $userId, $uuid, $prodDetailId) {
		$cart = DB::table('mod_cart')->where('user_id', $userId)->first();
		$discount = DB::table('mod_discount_member')->where('cd', $code)->first();

		$num = 0;
		$today = new \DateTime();

		$today = strtotime($today->format('Y-m-d H:i:s'));
		$canUseDiscount = false;
		$prodId = 0;
		$cartAmt = 0;
		if(isset($discount) && isset($cart)) {
			$cartId = $cart->id;
			$cartContent = $cart->content;
			$cartData = json_decode($cartContent);
			
			if($discount->discount_mode == "B") {
				$prodArray = json_decode($discount->prod_id);

				//$cartProds = $this->getCartIds($cartData, $sellType);

				for($j=0; $j<count($prodArray); $j++) {
					if($prodDetailId == $prodArray[$j]) {
						$prodId = $prodArray[$j];
						$canUseDiscount = true;
						break;
					}
				}

				if($sellType == "R") {
					$cartAmt = $this->getRentAmt($cartData, $uuid);
				}
				else {
					$cartAmt = $this->getSellAmt($cartData, $uuid);
				}
			}
			else {

				if($sellType == "R") {
					$cartAmt = $this->getRentAmt($cartData, $uuid);
				}
				else {
					$cartAmt = $this->getSellAmt($cartData, $uuid);
				}

				if($discount->limit_amt <= $cartAmt) {
					$canUseDiscount = true;
				}

				
			}
			
			$modCart = new ModCart();
			if($sellType  == $discount->sell_type && $canUseDiscount == true) {
				if($discount->discount_type == "B") {
					
					if($today >= strtotime($discount->f_date) && $today <= strtotime($discount->t_date)) {
						//$num = $discount->discount / 100;
						$num = $discount->discount;
						$modCart->updateCartConent($cartData, array(
							'discountCode' => $code,
							'discountAmt'  => $this->getDiscountAmt($discount->discount_way, $cartAmt, $num)
						), $uuid, $cartId);
					}
					else {
						$modCart->updateCartConent($cartData, array(
							'discountCode' => null,
							'discountAmt'  => 0
						), $uuid, $cartId);
						
						return ['num' => $num, 'discountWay' => ''];
					}
				}
				else {
					$canUseDiscount = $this->checkCartSameCode($cartData, $code, $uuid);
					
					if($canUseDiscount == true) {
						if($today >= strtotime($discount->f_date) && $today <= strtotime($discount->t_date) && $discount->status == 'N') {
							//$num = $discount->discount / 100;
							$num = $discount->discount;
							$modCart->updateCartConent($cartData, array(
								'discountCode' => $code,
								'discountAmt'  => $this->getDiscountAmt($discount->discount_way, $cartAmt, $num)
							), $uuid, $cartId);
						}
						else {
							$modCart->updateCartConent($cartData, array(
								'discountCode' => null,
								'discountAmt'  => 0
							), $uuid, $cartId);
	
							return ['num' => $num, 'discountWay' => ''];
						}
					}
					else {
						$modCart->updateCartConent($cartData, array(
							'discountCode' => null,
							'discountAmt'  => 0
						), $uuid, $cartId);

						return ['num' => $num, 'discountWay' => ''];
					}
				}
			}
			else {
				$modCart->updateCartConent($cartData, array(
					'discountCode' => null,
					'discountAmt'  => 0
				), $uuid, $cartId);

				return ['num' => $num, 'discountWay' => ''];
			}
		}
		else {
			$cartId = $cart->id;
			$cartContent = $cart->content;
			$cartData = json_decode($cartContent);
			$modCart = new ModCart();
			$modCart->updateCartConent($cartData, array(
				'discountCode' => null,
				'discountAmt'  => 0
			), $uuid, $cartId);
		}

		return ['num' => $num, 'discountWay' => (isset($discount->discount_way))?$discount->discount_way:null];
	}

	public function getDiscountAmt($discountWay, $oldAmt, $num) {
		$amt = 0;
		if($discountWay == 'P') {
			$amt = $oldAmt - (round($oldAmt * ($num/100)));
		}
		else {
			$amt = $num;
		}

		return $amt;
	}

	public function useDiscountForRePay($code) {
		$discount = DB::table('mod_discount_member')->where('cd', $code)->first();
		
		$num = 0;
		$discountWay = "";
		if(isset($discount)) {	
			$num = $discount->discount;
			$discountWay = $discount->discount_way;
		}

		return ['num' => $num, 'discountWay' => $discountWay];
	}

	public function voidDiscount($code) {

		DB::table('mod_discount_member')->where('cd', $code)->update(['status' => 'Y']);

		return;
	}

	public function getRentAmt($cartData, $uuid) {
		$rAmt = 0;
		foreach($cartData->rData as $key=>$row) {
			if($uuid == $row->uuid) {
				$day = (((strtotime($row->endDate) - strtotime($row->startDate))/3600)/24) + 1;
				$itemPrice = (int)($row->dPrice*$day*$row->num);
				$prodName = $row->prodNm."(".$day."天)";


				$rAmt = $rAmt + $itemPrice;
			}
			
		}

		return (int)$rAmt;
	}

	public function getSellAmt($cartData, $uuid) {
		$sAmt = 0;
		foreach($cartData->sData as $key=>$row) {
			if($uuid == $row->uuid)
				$sAmt = $sAmt + ($row->dPrice * $row->num);
		}
		
		return (int)$sAmt;
	}

	public function getCartIds($cartData, $sellType) {
		$prodIds = "";

		if($sellType == "R") {
			foreach($cartData->rData as $key=>$row) {
				$prodIds .= (string)$row->prodDetailId.",";
			}
		}
		else {
			foreach($cartData->sData as $key=>$row) {
				$prodIds .= (string)$row->prodDetailId.",";
			}
		}

		$cartProds = explode(",", substr($prodIds, 0, -1));

		if(!is_array($cartProds)) {
			$a = array();
			array_push($a, $cartProds);
			$cartProds = $a;
		}
		
		return $cartProds;
	}

	public function checkCartSameCode($cartData, $code, $uuid) {

		foreach($cartData->rData as $key=>$row) {
			if($row->uuid != $uuid) {
				if($row->discountCode == $code) {
					return false;
				}
			}
		}

		foreach($cartData->sData as $key=>$row) {
			if($row->uuid != $uuid) {
				if($row->discountCode == $code) {
					return false;
				}
			}
		}

		return true;
	}


	/*
	|--------------------------------------------------------------------------
	| RELATIONS
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| SCOPES
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| ACCESORS
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| MUTATORS
	|--------------------------------------------------------------------------
	*/
}