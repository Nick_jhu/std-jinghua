<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use DB;

class ModCart extends Model {

	use CrudTrait;

    /*
	|--------------------------------------------------------------------------
	| GLOBAL VARIABLES
	|--------------------------------------------------------------------------
	*/

	protected $table = 'mod_cart';
	// protected $primaryKey = 'id';
	protected $guarded = ['id'];
	// protected $hidden = ['id'];
	//protected $fillable = ['title', 'descp','discount','f_date','t_date','created_by','updated_by','discount_type','sell_type','cd','amt', 'limit_amt', 'prod_id', 'discount_mode'];
	public $timestamps = true;

	/*
	|--------------------------------------------------------------------------
	| FUNCTIONS
	|--------------------------------------------------------------------------
    */

    function updateCart($cartId, $content) {
        $cart = $this::find($cartId);

        $cart->content = json_encode($content);

        $cart->save();

        return;
    }

    function updateCartConent($cartContent, $data, $uuid, $cartId) {
        foreach($cartContent->rData as $key=>$row) {
			if($uuid == $row->uuid) {
                if(isset($data['discountCode'])) {
					$row->discountCode = $data['discountCode'];
				}
				else {
					$row->discountCode = null;
				}
                    
                
                if(isset($data['discountAmt']))
                    $row->discountAmt = $data['discountAmt'];
                
                if(isset($data['num']))
                    $row->num = $data['num'];
			}
        }
        
        foreach($cartContent->sData as $key=>$row) {
			if($uuid == $row->uuid) {
                if(isset($data['num'])) {
                    $row->num = $data['num'];
                }
                else {
                    if(isset($data['discountCode'])) {
						$row->discountCode = $data['discountCode'];
					}
					else {
						$row->discountCode = null;
					}
                    $row->discountAmt = $data['discountAmt'];
                }
			}
        }

        $this->updateCart($cartId, $cartContent);

        return;
    }


	/*
	|--------------------------------------------------------------------------
	| RELATIONS
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| SCOPES
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| ACCESORS
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| MUTATORS
	|--------------------------------------------------------------------------
	*/
}