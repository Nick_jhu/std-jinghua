<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Redirect;
use Input;

use App\Models\MemberMgmtModel;
use App\Models\ProdMgmtModel;


class ProductController extends Controller
{
    protected $subArray;

    public function index(Request $request) {

        $sellType  = $request->sellType;
        $country   = $request->country;
        $startDate = $request->startDate;
        $endDate   = $request->endDate;
        $num       = $request->num;
        $cateId    = $request->cateId;
        $sorting = $request->sorting;
        $brand = $request->brand;
        $search = $request->search;

        $thisQuery = DB::table('mod_product');

        switch($sorting) {
            case 'price-ascending':
                $thisQuery->orderBy('f_price', 'asc');
                break;
            case 'price-descending':
                $thisQuery->orderBy('f_price', 'desc');
                break;
            case 'created-ascending':
                $thisQuery->orderBy('created_at', 'asc');
                break;
            case 'created-descending':
                $thisQuery->orderBy('created_at', 'desc');
                break;
            case 'best-selling':
                $thisQuery->where('is_hot', 'Y');
                break;
            default:
                $thisQuery->orderBy('created_at', 'desc');
                break;
        }

        if(isset($brand)) {
            $thisQuery->where('brand', $brand);
        }

        if(isset($search)) {
            $thisQuery->where('title', 'like', '%'.$search.'%');
        }

        $cateData = array();
        if(isset($cateId)) {
            
            $stop = 0;
            do {

                $c = DB::table('mod_cate')->where('id', $cateId)->first();
                
                if(isset($c)) {
                    $cateId = $c->parent_id;

                    if($c->parent_id == -1) {
                        $stop = 9999;
                    }
                    
                    if($c->parent_id != -1) {
                        array_push($cateData, $c);
                    }
                }

                $stop++;

            } while ($stop < 10);

            $cateIdArray = DB::table('mod_cate')->where('id', $cateId)->pluck('id')->toArray();

            $subCateData = $this->categoryTree($request->cateId);
            array_push($subCateData, $request->cateId);

            $thisQuery->whereIn('cate_id', $subCateData);
        }

        $cateData = array_reverse($cateData);

        


        $today      = new \DateTime();
        $str_date    = $today->format('Y-m-d H:i:s');
        $thisQuery->where('is_added', 'Y');
        $thisQuery->whereNotIn('prod_type', ['W', 'S']);
        $thisQuery->where('added_on', '<=', $str_date);
        // $thisQuery->where(function($query) use ($str_date){
        //     $query->whereRaw("action_date is null or action_date >= "."'".$str_date."'");
        // });


        $prodData = $thisQuery->paginate(20);
        //$cateData = DB::table('mod_cate')->select('id', 'name')->where('parent_id', 1)->orderBy('id', 'asc')->get();
        //$areaData = DB::table('mod_cate')->select('id', 'name')->where('parent_id', 6)->orderBy('id', 'asc')->get();

        $cateId = (isset($request->cateId))?$request->cateId:"";

        $brandData = DB::table('bscode')
                        ->where('cd_type', 'BRAND')
                        ->get();

        $viewData = array(
            'viewName'  => 'product',
            'prodData'  => $prodData->appends(Input::except('page')),
            'cateData'  => $cateData,
            //'areaData'  => $areaData,
            'cateId'    => $cateId,
            'sellType'  => $request->sellType,
            'country'   => $request->country,
            'startDate' => $request->startDate,
            'endDate'   => $request->endDate,
            'num'       => $request->num,
            'brandData' => $brandData,
            'brand'     => $brand,
            'sorting'   => $sorting
        );

        return view('FrontEnd.newProduct')->with($viewData);
    }


    public function productDetail($id) {
        $today      = new \DateTime();
        $str_date    = $today->format('Y-m-d H:i:s');
        $startDate = request('startDate');
        $endDate   = request('endDate');

        $prodData = DB::table('mod_product')
                    ->where('id', $id)
                    ->where('is_added', 'Y')
                    // ->where(function($query) use ($str_date){
                    //     $query->whereRaw("action_date is null or action_date >= "."'".$str_date."'");
                    // })
                    ->first();
        $cateData = DB::table('mod_cate')->select('id', 'name')->where('parent_id', 1)->orderBy('id', 'asc')->get();

        if(!isset($prodData)) {
            return redirect('product')->withErrors(['message' => '查無此商品']);
        }

        $prodDetailData = DB::table('mod_product_detail')->where('prod_no', $prodData->prod_no)->orderBy('seq', 'asc')->get();

        if(isset($startDate) && isset($endDate)) {

            foreach($prodDetailData as $row) {
                $id = $row->id;
    
                $stockData = DB::table('mod_stock')
                            ->selectRaw('count(id) as sum')
                            ->whereRaw('(start_date between ? and ?  or end_date between ? and ?) and prod_detail_id=?', [$startDate, $endDate, $startDate, $endDate, $id])
                            ->first();

                if(isset($stockData)) {
                    $row->stock = $row->stock - $stockData->sum;
                }
            }
                        
        }

        if($prodData->prod_type == 'S') {
            foreach($prodDetailData as $row) {
                $snSum = DB::table('mod_sn')->where('prod_no', $prodData->prod_no)->where('is_sell', 'N')->count();
                $row->stock = $snSum;
            }
        }


        $today = date('Y-m-d');
        $day = (((strtotime($today) - strtotime($startDate))/3600)/24) + 1;
        if($day <= 2) {
            $startDate = "";
        }

        $day = (((strtotime($startDate) - strtotime($endDate))/3600)/24) + 1;
        if($day <= 4) {
            $endDate = "";
        }

        $relateData = DB::table('mod_product')->where('cate_id', $prodData->cate_id)->where('id', '<>', $prodData->id)->where('is_added', 'Y')->limit(5)->get();

        $ProdMgmtModel = new ProdMgmtModel();
        $periodsArray = $ProdMgmtModel->getPeriodsToArray($prodData->prod_no);
        $periodsArray = $ProdMgmtModel->processPeriodsArray($periodsArray);
        $instalment = $ProdMgmtModel->updatePeriodsArrayLabel($periodsArray, $prodData->f_price);

        $instalmentLastKey = '';
        foreach($instalment as $key=>$row) {
            $instalmentLastKey = $key;
        }

        $giftData = DB::table('mod_gift')->where('prod_id', $prodData->id)->get();

        $viewData = array(
            'viewName'       => 'product',
            'prodData'       => $prodData,
            'cateData'       => $cateData,
            'prodDetailData' => $prodDetailData,
            'relateData'     => $relateData,
            'startDate'      => $startDate,
            'endDate'        => $endDate,
            'instalment'     => $instalment,
            'instalmentLastKey' => $instalmentLastKey,
            'giftData'       => $giftData
        );

        

        return view('FrontEnd.newProductDetail')->with($viewData);
    }

    public function getHotProductForMenu($cateId) {

        $menuId = array();
        // $menuData = Db::table('mod_cate')->where('parent_id', $cateId)->pluck('id');

        // do {
        //     if(empty($menuId)) {
        //         foreach($menuData as $row) {
        //             array_push($menuId, $row);
        //         }
        //     }

        //     $menuData = Db::table('mod_cate')->whereIn('parent_id', $menuData)->pluck('id');

        //     if(!empty($menuData)) {
        //         foreach($menuData as $row) {
        //             array_push($menuId, $row);
        //         }
        //     }
            

        // } while ( empty($menuData) );

        //array_push($menuId, $cateId);

        $menuId = $this->categoryTree($cateId);
        array_push($menuId, (int)$cateId);

        $hotData = DB::table('mod_product')
                        ->where(function($query) use ($menuId)
                        {
                            $query->where('is_hot', 'Y')
                            ->where('is_added', 'Y')
                            ->whereIn('cate_id', $menuId);

                        })->get();
        
        $nextMenu = Db::table('mod_cate')->where('parent_id', $cateId)->orderBy('sort', 'asc')->get();

        foreach($nextMenu as $row) {
            $cnt = Db::table('mod_cate')->where('parent_id', $row->id)->count();

            if($cnt > 0) {
                $row->has_next = 'Y';
            }
            else {
                $row->has_next = 'N';
            }
        }

        return response()->json(['data' => $hotData, 'nextMenu' => $nextMenu]);
    }

    public function getNextMenu($cateId) {
        $nextMenu = Db::table('mod_cate')->where('parent_id', $cateId)->get();

        foreach($nextMenu as $row) {
            $cnt = Db::table('mod_cate')->where('parent_id', $row->id)->count();

            if($cnt > 0) {
                $row->has_next = 'Y';
            }
            else {
                $row->has_next = 'N';
            }
        }

        return response()->json(['data' => $nextMenu]);
    }

    function categoryTree($parent_id = 0){

        $query = DB::table('mod_cate')->where('parent_id', $parent_id)->get()->toArray();

        if(empty($this->subArray)) {
            $this->subArray = array();
        }

        if(count($query) > 0){
            foreach($query as $row){
                array_push($this->subArray, $row->id);
                $this->categoryTree($row->id);
            }
        }

        return $this->subArray;
    }
}
