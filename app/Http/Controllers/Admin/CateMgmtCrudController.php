<?php namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\CateMgmtCrudRequest as StoreRequest;
use App\Http\Requests\CateMgmtCrudRequest as UpdateRequest;

use Illuminate\Support\Facades\Auth;

class CateMgmtCrudController extends CrudController {
	public function setup() {
        $user = Auth::user();
        $this->crud->setModel('App\Models\CateMgmtModel');
        $this->crud->setRoute(config('backpack.base.route_prefix')  . '/CateMgmt');
        $this->crud->setEntityNameStrings('類別', '類別管理');

        $this->crud->setColumns([
            [
                'name' => 'id',
                'label' => 'ID'
            ],
            [
                'name' => 'name',
                'label' => '類別名稱'
            ],
            [
                'name' => 'descp',
                'label' => '描述'
            ],
            [
                'name' => 'srot',
                'label' => '順序'
            ]
        ]);
        //$this->crud->removeButton('update');
        $this->crud->addButtonFromModelFunction('line', 'go_next', 'goNext', 'beginning'); // add a button whose HTML is returned by a method in the CRUD model

        $both_array = array(
            [
                'name' => 'name',
                'label' => '類別名稱',
                'type' => 'text'
            ],
            [
                'name' => 'descp',
                'label' => '類別描述',
                'type' => 'textarea'
            ],
            [
                'name' => 'sort',
                'label' => '順序',
                'type' => 'text'
            ]
        );

        $create_array = array(
            [
                'name' => 'created_by',
                'type' => 'hidden',
                'value' => $user->email
            ],
            [
                'name' => 'updated_by',
                'type' => 'hidden',
                'value' => $user->email
            ],
            [
                'name' => 'g_key',
                'type' => 'hidden',
                'value' => $user->g_key
            ],
            [
                'name' => 'c_key',
                'type' => 'hidden',
                'value' => $user->c_key
            ],
            [
                'name' => 's_key',
                'type' => 'hidden',
                'value' => $user->s_key
            ],
            [
                'name' => 'd_key',
                'type' => 'hidden',
                'value' => $user->d_key
            ],
            [
                'name' => 'parent_id',
                'type' => 'hidden',
                'value' => (request('id') != null)?request('id'):-1
            ]
        );

        $update_array = array(
            [
                'name' => 'updated_by',
                'type' => 'hidden',
                'value' => $user->email
            ]
        );

        $this->crud->addFields($both_array, 'both');
        $this->crud->addFields($create_array, 'create');
        $this->crud->addFields($update_array, 'update');

        if(request('id')) {
            $this->crud->removeButton('create');
            
            $this->crud->addButtonFromModelFunction('top', 'create', 'createButton', 'beginning'); // add a button whose HTML is returned by a method in the CRUD model
            $this->crud->addButtonFromModelFunction('top', 'goBack', 'backButton', 'beginning'); // add a button whose HTML is returned by a method in the CRUD model
            
        }

    }

    public function index()
    {
        $parent_id = request('id');

        $this->crud->hasAccessOrFail('list');

        $this->data['crud'] = $this->crud;
        $this->data['title'] = $this->crud->entity_name_plural;

        if($parent_id > 0) {
            $this->crud->addClause('where', 'parent_id', $parent_id); 
        }
        else {
            $this->crud->addClause('where', 'parent_id', '-1'); 
        }

        // get all entries if AJAX is not enabled
        if (! $this->data['crud']->ajaxTable()) {
            $this->data['entries'] = $this->data['crud']->getEntries();
        }

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getListView(), $this->data);
    }

	public function store(StoreRequest $request)
	{
        $url = url(config('backpack.base.route_prefix', 'admin')).'/CateMgmt';
        $saveAction = \Request::input('save_action', config('backpack.crud.default_save_action', 'save_and_back'));
        $result = parent::storeCrud();

        $parentId = $this->crud->entry['attributes']['parent_id'];

        if($parentId != -1) {
            $redirectUrl = '';
            switch ($saveAction) {
                case 'save_and_new':
                    $redirectUrl = $url.'/create';
                    break;
                case 'save_and_edit':
                    $redirectUrl = $url.'/'.$this->crud->entry->getKey().'/edit';
                    if (\Request::has('locale')) {
                        $redirectUrl .= '?locale='.\Request::input('locale');
                    }
                    break;
                default:
                    $redirectUrl = $url;
                    break;
            }

            return \Redirect::to($redirectUrl.'?id='.$parentId); 
        }

        return $result;
	}

	public function update(UpdateRequest $request)
	{
        $url = url(config('backpack.base.route_prefix', 'admin')).'/CateMgmt';
        $saveAction = \Request::input('save_action', config('backpack.crud.default_save_action', 'save_and_back'));
        $result = parent::updateCrud();

        $parentId = $this->crud->entry['attributes']['parent_id'];

        if($parentId != -1) {
            $redirectUrl = '';
            switch ($saveAction) {
                case 'save_and_new':
                    $redirectUrl = $url.'/create';
                    break;
                case 'save_and_edit':
                    $redirectUrl = $url.'/'.$this->crud->entry->getKey().'/edit';
                    // if (\Request::has('locale')) {
                    //     $redirectUrl .= '?locale='.\Request::input('locale');
                    // }
                    break;
                default:
                    $redirectUrl = $url;
                    break;
            }

            return \Redirect::to($redirectUrl.'?id='.$parentId); 
        }

        return $result;
	}
}