<?php namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\DiscountCrudRequest as StoreRequest;
use App\Http\Requests\DiscountCrudRequest as UpdateRequest;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use DB;
use App\Models\DiscountMember;
use App\Models\Discount;

class DiscountCrudController extends CrudController {

	public function setup() {
        $this->crud->setModel('App\Models\Discount');
        $this->crud->setRoute(config('backpack.base.route_prefix')  . '/modDiscount');
        $this->crud->setEntityNameStrings('折扣設定', '折扣設定');

        $this->crud->setColumns([
            [
                'name' => 'title',
                'label' => "標題",
                'type'  => 'text'
            ],
            [
                'name' => 'cd',
                'label' => "優惠碼",
                'type'  => 'text'
            ],
            [
                'name' => 'discount_way',
                'label' => '折扣方法',
                'type'  => 'select_from_array',
                'options' => ['C' => '扣', 'P' => '折扣(%)']
            ],
            [
                'name' => 'discount',
                'label' => "折扣",
                'type'  => 'text'
            ],
            [
                'name' => 'discount_type',
                'label' => '使用類型',
                'type'  => 'radio',
                'options' => ['A' => '一次', 'B' => '無限']
            ],
            [
                'name' => 'sell_type',
                'label' => '購物類型',
                'type'  => 'radio',
                'options' => ['S' => '購', 'R' => '租']
            ],
            [
                'name' => 'f_date',
                'label' => "開始日期",
                'type'  => 'text'
            ],
            [
                'name' => 't_date',
                'label' => "結束日期",
                'type'  => 'text'
            ],
            [
                'name' => 'is_send',
                'label' => "發送",
                'type'  => 'radio',
                'options' => ['Y' => '已發送', 'N' => '未發送']
            ],
            [
                'name' => 'created_by',
                'label' => "建立者",
                'type'  => 'text'
            ],
            [
                'name' => 'created_at',
                'label' => "時間",
                'type'  => 'text'
            ]
        ]);

        $this->crud->addButtonFromModelFunction('line', 'sned_user_discount_code', 'sendUserDiscountCode', 'beginning'); // add a button whose HTML is returned by a method in the CRUD model

        $this->crud->addField([
            'name' => 'title',
            'label' => '標題',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-3',
            ],
        ]);

        $this->crud->addField([       // Select2Multiple = n-n relationship (with pivot table)
            'label'   => "折扣類型",
            'name'    => 'discount_mode',
            'type'    => 'select_from_array',
            'default' => 'A',
            'options' => ['A' => '滿額扣', 'B' => '綁商品'],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-3',
            ],
        ]);

        $this->crud->addField([       // Select2Multiple = n-n relationship (with pivot table)
            'label'   => "折扣方式",
            'name'    => 'discount_way',
            'type'    => 'select_from_array',
            'default' => 'C',
            'options' => ['C' => '扣', 'P' => '折扣'],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-3',
            ],
        ]);

        $this->crud->addField([
            'name' => 'discount',
            'label' => '折扣數',
            'type' => 'number',
            'attributes' => ["step" => "any"], // allow decimals
            'suffix' => "元",
            'wrapperAttributes' => [
                'class' => 'form-group col-md-3',
            ],
        ]);

        $this->crud->addField([       // Select2Multiple = n-n relationship (with pivot table)
            'label'   => "購物類型",
            'name'    => 'sell_type',
            'type'    => 'select_from_array',
            'default' => 'R',
            'options' => ['R' => '租', 'S' => '購'],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-3',
            ],
        ]);

        $this->crud->addField([       // Select2Multiple = n-n relationship (with pivot table)
            'label'   => "使用類型",
            'name'    => 'discount_type',
            'type'    => 'select_from_array',
            'default' => 'B',
            'options' => ['B' => '無限次', 'A' => '一次'],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-3',
            ],
        ], 'create');

        $this->crud->addField([
            'name' => 'limit_amt',
            'label' => '最低限制',
            'type' => 'number',
            'attributes' => ["step" => "any"], // allow decimals
            'suffix' => "元",
            'wrapperAttributes' => [
                'class' => 'form-group col-md-3',
            ],
        ]);

        $this->crud->addField([
            'name' => 'amt',
            'label' => '組數',
            'type' => 'number',
            'attributes' => ["step" => "any"], // allow decimals
            'wrapperAttributes' => [
                'class' => 'form-group col-md-3',
            ],
        ]);

        $this->crud->addField([       // Select2Multiple = n-n relationship (with pivot table)
            'label'     => "綁定商品",
            'type'      => 'select2_from_array',
            'name'      => 'prod_id', // the method that defines the relationship in your Model
            'options'     => DB::table('mod_product_detail')->join('mod_product', 'mod_product.prod_no', '=', 'mod_product_detail.prod_no')->where('mod_product.is_added', 'Y')->pluck(DB::raw('CONCAT(mod_product.title,"-",mod_product_detail.title) as title'), 'mod_product_detail.id'), // foreign key model
            'allows_multiple' => true,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-12',
            ],
            // 'select_all' => true, // show Select All and Clear buttons?
        ]);

        $today    = new \DateTime();
        $tomorrow = new \DateTime('+2 days');
        $this->crud->addField([
            'name' => 'event_date_range', // a unique name for this field
            'start_name' => 'f_date', // the db column that holds the start_date
            'end_name' => 't_date', // the db column that holds the end_date
            'label' => '使用期限',
            'type' => 'date_range',
            // OPTIONALS
            'start_default' => $today->format('Y-m-d H:i:s'), // default value for start_date
            'end_default' => $tomorrow->format('Y-m-d H:i:s'), // default value for end_date
            'date_range_options' => [ // options sent to daterangepicker.js
                'timePicker' => true,
                'locale' => ['format' => 'YYYY-MM-DD HH:mm:ss']
            ]
        ]);

        $this->crud->addField([
            'name' => 'descp',
            'label' => '折扣內容',
            'type' => 'ckeditor',
            // optional:
            'extra_plugins' => ['oembed', 'widget','justify']
        ]);

        $this->crud->addField([
            'name' => 'discountScript',
            'type' => 'discount',
        ]);

        $this->crud->addField([   // DateTime
            'name' => 'start',
            'label' => 'Event start',
            'type' => 'datetime_picker',
            // optional:
            'datetime_picker_options' => [
                'format' => 'YYYY-MM-DD HH:mm',
                'language' => 'en'
            ],
            'allows_null' => true,
        ]);

        /*
        $this->crud->addField([       // Select2Multiple = n-n relationship (with pivot table)
            'label'   => "是否全選會員",
            'name'    => 'sel_type',
            'type'    => 'select_from_array',
            'default' => 'N',
            'options' => ['N' => '否', 'Y' => '是'],
        ]);

        
        $this->crud->addField([       // Select2Multiple = n-n relationship (with pivot table)
            'label'      => "會員",
            //'type'       => 'select2_multiple',
            'name'       => 'member_id',
            'type' => 'select2_from_array',
            'options' => DB::table('mod_member')->where('is_actived', 'Y')->orderBy('phone','asc')->pluck('phone', 'id'),
            'allows_multiple' => true
            // 'entity'     => 'members', // the method that defines the relationship in your Model
            // 'attribute'  => 'phone', // foreign key attribute that is shown to user
            // 'model'      => "App\Models\MemberMgmtModel", // foreign key model
            // 'pivot'      => true, // on create&update, do you need to add/delete pivot table entries?
            // 'select_all' => false, // show Select All and Clear buttons?
        ]);
        */

        $this->crud->addField([   // Hidden
            'name' => 'created_by',
            'type' => 'hidden',
            'value' => Auth::user()->name
        ]);

        $this->crud->addField([   // Hidden
            'name' => 'updated_by',
            'type' => 'hidden',
            'value' => Auth::user()->name
        ]);

        
    }

	public function store(StoreRequest $request)
	{
        $response = parent::storeCrud($request);
        $code = null;

        if(isset($request->f_date)) {
            $request->merge(['f_date' => $request->f_date.':00']);
        }

        if(isset($request->t_date)) {
            $request->merge(['t_date' => $request->t_date.':00']);
        }

        if($request->discount_type == 'B') {
            $dnt = DB::table('mod_discount')->where('id', $this->data['entry']->getKey())->first();
            $code = $this->genCode();
            $a = array(
                'discount_id'   => $dnt->id,
                'cd'            => $code,
                'title'         => $dnt->title,
                'descp'         => $dnt->descp,
                'discount'      => $dnt->discount,
                'f_date'        => $dnt->f_date,
                't_date'        => $dnt->t_date,
                'created_by'    => $dnt->created_by,
                'updated_by'    => $dnt->updated_by,
                'updated_at'    => \Carbon::now()->toDateTimeString(),
                'created_at'    => \Carbon::now()->toDateTimeString(),
                'discount_type' => $dnt->discount_type,
                'sell_type'     => $dnt->sell_type,
                'limit_amt'     => $dnt->limit_amt,
                'discount_mode' => $dnt->discount_mode,
                'prod_id'       => $dnt->prod_id,
                'discount_way'  => $dnt->discount_way
            );

            DB::table('mod_discount_member')->insert($a);

            $discount = Discount::find($this->data['entry']->getKey());
            $discount->cd = $code;
            $discount->save();
        }
        
		return $response;
	}

	public function update(UpdateRequest $request)
	{
        $user = Auth::user();
        $id = $request->id;

        $response = parent::updateCrud();

        
        $discount = DB::table('mod_discount_member')->where('discount_id', $id)->get();
        $updateArray = array();

        $dnt = DB::table('mod_discount')->where('id', $id)->first();

        if(count($discount) > 0) {
            $a = array(
                'title'         => $request->title,
                'descp'         => $request->descp,
                'discount'      => $request->discount,
                'f_date'        => $request->f_date,
                't_date'        => $request->t_date,
                'created_by'    => $request->created_by,
                'updated_by'    => $request->updated_by,
                'updated_at'    => \Carbon::now()->toDateTimeString(),
                'sell_type'     => $request->sell_type,
                'limit_amt'     => $dnt->limit_amt,
                'discount_mode' => $dnt->discount_mode,
                'prod_id'       => $dnt->prod_id,
                'discount_way'  => $dnt->discount_way
            );
            
            DB::table('mod_discount_member')->where('discount_id', $id)->update($a);
        }
        

        return $response;
    }

    public function destroy($id)
    {
        $this->crud->hasAccessOrFail('delete');

        $discount = DiscountMember::where('discount_id', $id);
        $discount->delete();

        return $this->crud->delete($id);
    }

    public function sendUserDiscountCode($id) {

        $discount = Discount::find($id);
        $insertArray = array();

        if(isset($discount)) {
            if($discount->amt > 0) {
                for($i=0; $i<$discount->amt; $i++) {
                    $a = array(
                        'discount_id'   => $discount->id,
                        'cd'            => $this->genCode(),
                        'title'         => $discount->title,
                        'descp'         => $discount->descp,
                        'discount'      => $discount->discount,
                        'f_date'        => $discount->f_date,
                        't_date'        => $discount->t_date,
                        'created_by'    => $discount->created_by,
                        'updated_by'    => $discount->updated_by,
                        'updated_at'    => \Carbon::now()->toDateTimeString(),
                        'created_at'    => \Carbon::now()->toDateTimeString(),
                        'discount_type' => $discount->discount_type,
                        'sell_type'     => $discount->sell_type,
                        'g_key'         => 'STD',
                        'c_key'         => 'STD',
                        's_key'         => 'STD',
                        'd_key'         => 'STD',
                        'limit_amt'     => $discount->limit_amt,
                        'discount_mode' => $discount->discount_mode,
                        'prod_id'       => json_encode($discount->prod_id),
                        'discount_way'  => $discount->discount_way
                    );

                    array_push($insertArray, $a);
                }
            }
            else {
                /*
                $members = DB::table('mod_member')->where('is_actived', 'Y')->get();

                foreach($members as $row) {
                    $a = array(
                        'discount_id'   => $discount->id,
                        'cd'            => $this->genCode(),
                        'member_id'     => $row->id,
                        'phone'         => $row->phone,
                        'title'         => $discount->title,
                        'descp'         => $discount->descp,
                        'discount'      => $discount->discount,
                        'f_date'        => $discount->f_date,
                        't_date'        => $discount->t_date,
                        'created_by'    => $discount->created_by,
                        'updated_by'    => $discount->updated_by,
                        'updated_at'    => \Carbon::now()->toDateTimeString(),
                        'created_at'    => \Carbon::now()->toDateTimeString(),
                        'discount_type' => $discount->discount_type,
                        'sell_type'     => $discount->sell_type,
                        'g_key'         => 'STD',
                        'c_key'         => 'STD',
                        's_key'         => 'STD',
                        'd_key'         => 'STD',
                        'limit_amt'     => $discount->limit_amt,
                        'discount_mode' => $discount->discount_mode,
                        'prod_id'       => $discount->prod_id
                    );

                    array_push($insertArray, $a);
                }
                */
            }
        }
        

        if(count($insertArray) > 0) {
            DB::table('mod_discount_member')->insert($insertArray);
        }

        $discount->is_send = 'Y';
        $discount->save();

        \Alert::success('發送成功')->flash();


        return redirect()->back()->send();
    }

    private function genCode($length=8) {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function discountView() {
        $data = array(
            'crud' => (object)array(
                'colModel' => array()
            )
        );
        return view('DiscountMgmt.discount', $data);
    }

    public function multiDel() {
        $ids = request('ids');

        if(count($ids) > 0) {
            for($i=0; $i<count($ids); $i++) {
                $DiscountMember = DiscountMember::find($ids[$i]);
                $DiscountMember->delete();
            }
        }

        return response()->json(array('msg' => 'success'));
    }

}