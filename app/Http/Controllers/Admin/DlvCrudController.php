<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Requests\CrudRequest;
use Illuminate\Support\Facades\DB;
use Validator;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

use App\Models\DlvModel;
use App\Models\OrderMgmtModel;
use App\Models\DlvPlanModel;
use App\Models\CarLoadModel;
use App\Models\OrderPackModel;
use App\Models\BaseModel;
use App\Models\CommonModel;
use App\Models\TransRecordModel;
use App\Models\TrackingModel;
use App\Models\CalculateModel;
use App\Models\FcmModel;
use App\Models\SysNoticeModel;

use App\Http\Requests\DlvCrudRequest as StoreRequest;
use App\Http\Requests\DlvCrudRequest as UpdateRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Session\Store as Session;

class DlvCrudController extends CrudController
{
    
    public function setup() {
        $user = Auth::user();
        $this->crud->setModel("App\Models\DlvModel");
        $this->crud->setEntityNameStrings(trans('modDlv.titleName'), trans('modDlv.titleName'));
        $this->crud->setRoute(config('backpack.base.route_prefix').'/TranPlanMgmt/SendCar');
    
        $this->crud->setColumns(['name']);
        
        $this->crud->setCreateView('tranPlan.edit');
        $this->crud->setEditView('tranPlan.edit');
        $this->crud->setListView('tranPlan.index');
        $this->crud->enableAjaxTable();

        $this->crud->addField([
            'name' => 'cust_no',
            'type' => 'lookup',
            'title' => '卡車公司查詢',
            'info1' => Crypt::encrypt('sys_customers'), //table
            'info2' => Crypt::encrypt("cust_no+cname,cust_no,cname"), //column
            'info3' => Crypt::encrypt("status='B'"), //condition
            'info4' => "cust_no=cust_no" //field mapping
        ]);

        $this->crud->addField([
            'name' => 'dlv_no',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'status',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'load_rate',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'load_tweight',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'load_tcbm',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'car_no',
            'type' => 'lookup',
            'title' => '車輛查詢',
            'info1' => Crypt::encrypt('mod_car'), //table
            'info2' => Crypt::encrypt("car_no,car_no,car_type,car_type_nm,load_rate,driver_nm"), //column
            'info3' => Crypt::encrypt(""), //condition
            'info4' => "car_no=car_no;car_type=car_type;load_rate=load_rate;driver_nm=driver_nm" //field mapping
        ]);

        $this->crud->addField([
            'name' => 'car_type',
            'type' => 'select',
            'options' => DB::table('bscode')
                            ->select('cd as code', 'cd_descp as descp')
                            ->where('cd_type', 'CARTYPE')
                            ->where('g_key', $user->g_key)
                            ->where('c_key', $user->c_key)
                            ->get()
        ]);

        $this->crud->addField([
            'name' => 'driver_nm',
            'type' => 'text',
            // 'title' => '司機查詢',
            // 'info1' => Crypt::encrypt('users'), //table
            // 'info2' => Crypt::encrypt("name,name,phone"), //column
            // 'info3' => Crypt::encrypt(""), //condition
            // 'info4' => "name=driver_nm;phone=driver_phone" //field mapping
        ]);
        
        $this->crud->addField([
            'name' => 'driver_phone',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'dlv_date',
            'type' => 'date_picker'
        ]);

    }

    public function get($cust_no=null) 
    {
        $sample_detail = [];
        //dd($cust_no);
        if($cust_no != null) {
            //dd($this->crud);
            //$sample_detail = CustomerModel::find($cust_no)->sampleDetail;//DB::table('sample_detail')->get()->where('cust_no', $cust_no);
            $this_query = DB::table('sample_detail');
            $this_query->where('cust_no', $cust_no);
            $sample_detail = $this_query->get();
            
        }
        
        $data[] = array(
            'Rows' => $sample_detail,
        );

        return response()->json($data);
    }

    public function store(StoreRequest $request)
	{
        $user = Auth::user();
        $commonFunc = new CommonModel;
        $request = $commonFunc->processData($request, $this->crud->create_fields);
        $today      = new \DateTime();
        $str_date    = $today->format('Ym');
        $params = array(
            "c_key"   => $user->c_key,
            "date_ym" => substr($str_date, 2, strlen($str_date))
        );
        $BaseModel = new BaseModel();
        $dlvNo = $BaseModel->getAutoNumber("dlv",$params);
        $request->merge(array('dlv_no' => $dlvNo));

        try {
            $response = parent::storeCrud($request);
        }
        catch (\Exception $e) {
            return ["msg"=>"error", "errorLog"=>$e];
        }
        
        $request->session()->forget('alert_messages');
        return ["msg"=>"success", "response"=>$response, "lastId"=>$this->data['entry']->getKey()];
	}

	public function update(UpdateRequest $request)
	{
        
        $commonFunc = new CommonModel;
        $request = $commonFunc->processData($request, $this->crud->create_fields);

        try {
            $driverNm = $request->driver_nm;
            $carNo    = $request->car_no;
            $carType  = $request->car_type;
            $custNo   = $request->cust_no;

            $response = parent::updateCrud($request);

            $dlvData = DlvModel::find($request->id);
            if(isset($driverNm) || isset($carNo) || isset($carType) || isset($custNo)) {
                $updateData = [
                    'driver'       => $dlvData->driver_nm, 
                    'car_no'       => $dlvData->car_no, 
                    'car_type'     => $dlvData->car_type,
                    'truck_cmp_no' => $dlvData->cust_no
                ];
                if(isset($custNo)) {
                    $custData = DB::table('sys_customers')->where('cust_no', $custNo)->first();
                    if(isset($custData)) {
                        $truck_cmp_nm = $custData->cname;
                        $updateData['truck_cmp_nm'] = $truck_cmp_nm;
                    }
                }
                $ordsData = OrderMgmtModel::where('dlv_no', $dlvData->dlv_no)->update($updateData);                
            }
        }
        catch (\Exception $e) {
            return ["msg"=>"error", "errorLog"=>$e->getMessage(), "id" => $dlvData->dlv_no];
        }

        $request->session()->forget('alert_messages');
        return ["msg"=>"success", "response"=>$response];
    }

    public function getPack() {
        $ordArray = request('ordArray');

        $data[] = array(
            'Rows' => [],
        );

        if(count($ordArray) > 0) {
            
            $ordPack = DB::table('mod_order_pack')
                        ->where('load_comp', 'N')
                        ->whereIn('ord_no', $ordArray)
                        ->orderBy('ord_no', 'asc')
                        ->orderBy('pack_no', 'asc')
                        ->get();
            $data['Rows'] = $ordPack;
        }

        return response()->json($data);
    }

    public function getCarLoad($dlv_no=null) {
        $carLoadData = [];
        if($dlv_no != null) {
            $this_query = DB::table('mod_car_load');
            $this_query->where('dlv_no', $dlv_no);
            $carLoadData = $this_query->get();
            $carLoadData = $this_query->get();
            
        }
        
        $data[] = array(
            'Rows' => $carLoadData,
        );

        return response()->json($data);
    }

    public function handPickup() {
        $data = request('data');

        
        try {
            $carLoadData = json_decode($data);

            
            foreach($carLoadData as $key=>$val) {
                $carLoad = new CarLoadModel;
                $carLoad->dlv_no = $val->dlv_no;
                $carLoad->ord_no = $val->ord_no;
                $carLoad->pack_no = $val->pack_no;
                $carLoad->save();

                OrderPackModel::where('ord_no', $val->ord_no)
                ->where('pack_no', $val->pack_no)
                ->update(['load_comp' => 'Y']);
            }
            
        }
        catch (\Exception $e) {
            return ["msg"=>"error", "errorLog"=>$e];
        }

        return ["msg"=>"success"];
    }


    public function autoPickup() {
        $data = request('data');

        
        try {
            $carLoadData = json_decode($data);

            $dlv_no = request('dlv_no');

            $mod_dlv = DB::table('mod_dlv')->select('load_rate', 'load_tcbm')->where('dlv_no', $dlv_no)->first();

            $truck_cbm  = $mod_dlv->load_rate * $mod_dlv->load_tcbm;
            $ttl_cbm    = 0;
            $old_ord_no = "";
            $ord_cbm    = [];
            foreach($carLoadData as $key=>$val) {
                $ord_no = $val->ord_no;
                $cbm = $val->cbm;

                if($key == 0) {
                    $old_ord_no = $ord_no;
                    $ord_cbm[$ord_no] = 0;
                }

                if($old_ord_no == $ord_no) {
                    $ord_cbm[$ord_no] += $cbm;
                }
                else {
                    $ord_cbm[$ord_no] = $cbm;
                }

                $old_ord_no = $ord_no;
            }
            
            $i = 0;
            $old_key = "";
            $put_key = [];
            $ord_no = "";
            foreach($ord_cbm as $key=>$val) {
                $ttl_cbm += $val;

                if($i == 0) {
                    $old_key = $key;
                }
                
                if($ttl_cbm > $truck_cbm) {
                    /*
                    foreach($carLoadData as $key=>$val) {
                        if(in_array($val->ord_no, $put_key)) {
                            $carLoad          = new CarLoadModel;
                            $carLoad->dlv_no  = $dlv_no;
                            $carLoad->ord_no  = $val->ord_no;
                            $carLoad->pack_no = $val->pack_no;
                            $carLoad->save();
        
                            OrderPackModel::where('ord_no', $val->ord_no)
                            ->where('pack_no', $val->pack_no)
                            ->update(['load_comp' => 'Y']);

                            $ord_no = $val->ord_no;
                        }
                    }
                    $ttl_cbm = 0;
                    $temp_cbm = 0;

                    foreach($ord_cbm as $v) {
                        if(!in_array($ord_no, $put_key)) {
                            $temp_cbm += $v;
                        }
                    }

                    $modDlv    = DlvModel::where('dlv_no', $dlv_no)->first();
                    $newModDlv = $modDlv->replicate();
                    $cust_no   = $newModDlv->cust_no;
                    $car_type  = $newModDlv->car;
                    $modCar    = DB::table('mod_car')->where('cust_no', $cust_no)->get();
                    $near_cbm  = [];
                    $car_no    = "";
                    $b         = 0;
                    foreach($modCar as $k=>$carVal) {
                        $c = ($carVal->cbm * $carVal->load_rate) - $temp_cbm;
                        if($k == 0) {
                            $b = $c;
                        }

                        if($c > 0 && $c < $b) {
                            $car_no = $carVal->car_no;
                        }
                        else {
                            $b = $c;
                            $car_no = $carVal->car_no;
                        }
                    }

                    if($car_no != "") {
                        $modCar = DB::table('mod_car')->select('car_no','driver_nm','car_type')->where('cust_no', $cust_no)->where('car_no', $car_no)->first();

                        $newModDlv->car_no    = $modCar->car_no;
                        $newModDlv->driver_nm = $modCar->driver_nm;
                        $newModDlv->car_type  = $modCar->car_type;
                        $newModDlv->dlv_no    = 'DLV00002';
                        $newModDlv->save();

                        
                    }

                    */
                    break;
                }

                $i++;
                $old_key = $key;
                array_push($put_key, $key);
            }

            $all_ord = [];
            foreach($carLoadData as $key=>$val) {
                if(in_array($val->ord_no, $put_key)) {
                    $carLoad          = new CarLoadModel;
                    $carLoad->dlv_no  = $dlv_no;
                    $carLoad->ord_no  = $val->ord_no;
                    $carLoad->pack_no = $val->pack_no;
                    $carLoad->save();

                    OrderPackModel::where('ord_no', $val->ord_no)
                    ->where('pack_no', $val->pack_no)
                    ->update(['load_comp' => 'Y']);

                    $ord_no = $val->ord_no;
                }
            }
            array_push($all_ord, $val->ord_no);
            
        }
        catch (\Exception $e) {
            return ["msg"=>"error", "errorLog"=>$e];
        }

        $ordPack = DB::table('mod_order_pack')
                        ->where('load_comp', 'N')
                        ->whereIn('ord_no', $all_ord)
                        ->orderBy('ord_no', 'asc')
                        ->orderBy('pack_no', 'asc')
                        ->get();
        $packData['Rows'] = $ordPack;

        return ["msg"=>"success", "data" =>$packData];
    }

    public function getDlvData($dlv_no=null) {
        header('Access-Control-Allow-Origin: *');
        header('Content-Security-Policy: upgrade-insecure-requests');
        $returnData = array();
        $returnData["dlvDatas"] = DB::table('mod_dlv')->get();
        $returnData["msg"] = "success";
        
        return response()->json($returnData);
    }

    public function DlvCar() {
        $title = trans('dlvCar.title');
        echo "<title>$title</title>";
        $user = Auth::user();
        $carData = DB::table('sys_customers')->where('type', 'SELF')->where('cust_no', $user->c_key)->where('identity', 'c')->first();
        return view('tranPlan.dlvCar')->with('carData', $carData);
    }

    public function sendDlvCar(Request $request) {
        $user         = Auth::user();
        $car_no       = $request->car_no;
        $driver_nm    = $request->driver_nm;
        $driver_phone = $request->driver_phone;
        $ord_ids      = $request->ord_ids;
        $dlv_date     = $request->dlv_date;
        $carType      = $request->car_type;

        $dlv = DB::table('mod_dlv')->orderBy("id", "desc")->first();
        $dlv_id = "";
        if(isset($dlv)) {
            $id = $dlv->id + 1;
            $dlv_id = str_pad($id,3,'0',STR_PAD_LEFT);
        }
        else {
            $dlv_id = str_pad(1,3,'0',STR_PAD_LEFT);
        }
        

        $ords = DB::table('mod_order')
                    ->whereIn('id', $ord_ids)
                    ->get();

        $car = DB::table('mod_car')->where('car_no', $car_no)->first();
        
        $ttl_cbm     = 0;
        $ttl_pkg_num = 0;
        $dlv_detail  = [];
        $today       = new \DateTime();
        $str_date    = $today->format('Ym');
        $params = array(
            "c_key"   => $user->c_key,
            "date_ym" => substr($str_date, 2, strlen($str_date))
        );
        $BaseModel = new BaseModel();
        $dlv_no = $BaseModel->getAutoNumber("dlv",$params);
        if(count($ords) > 0) {
            foreach($ords as $key=>$ord) {
                $ttl_cbm     += $ord->total_cbm;
                $ttl_pkg_num += $ord->pkg_num;

                $ordCnt = DB::table('mod_dlv_plan')
                            ->where('status', 'FINISHED')
                            ->where('dlv_type', 'P')
                            ->where('sys_ord_no', $ord->sys_ord_no)
                            ->count();
                //$ord->dlv_type != 'P' 代表有提貨在其它配送單裡
                if(!empty($ord->pick_addr) && $ordCnt == 0 && $ord->dlv_type != 'P') {
                    $detail = array(
                        "ord_id"      => $ord->id,
                        "cust_nm"     => $ord->pick_cust_nm,
                        "dlv_type"    => 'P',
                        "ord_no"      => $ord->ord_no,
                        "sys_ord_no"  => $ord->sys_ord_no,
                        "status"      => "UNTREATED",
                        "dlv_no"      => $dlv_no,
                        "addr"        => $ord->pick_city_nm.$ord->pick_area_nm.$ord->pick_addr,
                        "etd"         => $ord->etd,
                        "sort"        => $key,
                        "remark"      => htmlspecialchars($ord->pick_remark),
                        "updated_at"  => $today->format('Y-m-d H:i:s'),
                        "updated_by"  => $user->email,
                        "created_at"  => $today->format('Y-m-d H:i:s'),
                        "created_by"  => $user->email,
                        "g_key"       => $user->g_key,
                        "c_key"       => $user->c_key,
                        "s_key"       => $user->s_key,
                        "d_key"       => $user->d_key
                    );
                    array_push($dlv_detail, $detail);
                }

                if(!empty($ord->dlv_addr)) {
                    $detail = array(
                        "ord_id"     => $ord->id,
                        "cust_nm"    => $ord->dlv_cust_nm,
                        "dlv_type"   => 'D',
                        "ord_no"     => $ord->ord_no,
                        "sys_ord_no" => $ord->sys_ord_no,
                        "status"     => "UNTREATED",
                        "dlv_no"     => $dlv_no,
                        "addr"       => $ord->dlv_city_nm.$ord->dlv_area_nm.$ord->dlv_addr,
                        "etd"        => $ord->etd,
                        "sort"       => $key,
                        "remark"     => htmlspecialchars($ord->dlv_remark),
                        "updated_at" => $today->format('Y-m-d H:i:s'),
                        "updated_by" => $user->email,
                        "created_at" => $today->format('Y-m-d H:i:s'),
                        "created_by" => $user->email,
                        "g_key"      => $user->g_key,
                        "c_key"      => $user->c_key,
                        "s_key"      => $user->s_key,
                        "d_key"      => $user->d_key
                    );
    
                    array_push($dlv_detail, $detail);
                }
                
            }

            $modDlv               = new DlvModel;
            $modDlv->dlv_no       = $dlv_no;
            $modDlv->dlv_date     = $dlv_date;
            $modDlv->status       = "UNTREATED";
            $modDlv->load_rate    = $car->load_rate;
            $modDlv->load_tweight = 0;//$car->load_weight;
            $modDlv->load_tcbm    = $ttl_cbm;
            $modDlv->dlv_num      = $ttl_pkg_num;
            $modDlv->cust_no      = $car->cust_no;
            $modDlv->car_no       = $car->car_no;
            $modDlv->car_type     = $carType;
            $modDlv->car_type_nm  = $car->car_type_nm;
            $modDlv->driver_nm    = $driver_nm;
            $modDlv->driver_phone = $driver_phone;
            $modDlv->updated_by   = $user->email;
            $modDlv->created_by   = $user->email;
            $modDlv->g_key        = $user->g_key;
            $modDlv->c_key        = $user->c_key;
            $modDlv->s_key        = $user->s_key;
            $modDlv->d_key        = $user->d_key;
            $modDlv->save();

            DB::table('mod_dlv_plan')->insert($dlv_detail);

            DB::table('mod_order')
            ->whereIn('id', $ord_ids)
            ->update([
                "status"   => "SEND", 
                "car_no"   => $car->car_no,
                "truck_no" => $car->car_no,
                "dlv_no"   => $dlv_no,
                "car_type" => $carType,
                "dlv_type" => $car->dlv_type,
                "driver"   => $driver_nm
            ]);

            foreach($ords as $key=>$ord) {
                $tsData = DB::table("mod_trans_status")->where("ts_type", "A")->where('order', 3)->where('g_key', $user->g_key)->where('c_key', $user->c_key)->orderBy("order", "desc")->first();
                $data = [
                    'ts_no'      => $tsData->id,
                    'ts_type'    => $tsData->ts_type,
                    'sort'       => $tsData->order,
                    'ts_name'    => $tsData->ts_name,
                    'ts_desc'    => $tsData->ts_desc,
                    'ref_no1'    => $dlv_no,
                    'ref_no2'    => $ord->ord_no,
                    'ref_no3'    => $car_no,
                    'ref_no4'    => $ord->sys_ord_no,
                    'g_key'      => $ord->g_key,
                    'c_key'      => $ord->c_key,
                    's_key'      => $ord->s_key,
                    'd_key'      => $ord->d_key,
                    'created_by' => $user->name,
                    'updated_by' => $user->name,
                ];
        
                $TransRecordModel = new TransRecordModel();
                $TransRecordModel->createRecord($data);

                if(isset($ord->dlv_email)) {
                    $TrackingModel = new TrackingModel();
                    $TrackingModel->sendTrackingMail($ord->ord_no, $ord->dlv_email, '貨況通知-訂單號：'.$ord->ord_no);
                }

                $c = new CalculateModel();
                $amt = $c->calculateAmt($ord->id);
                DB::table('mod_order')
                ->where('id', $ord->id)
                ->update([
                    'amt' => $amt
                ]);
            }
        }

        return response()->json(["msg" => "success"]);
    }

    public function getDlvDataByCarNo($car_no=null) {
        header('Access-Control-Allow-Origin: *');
        header('Content-Security-Policy: upgrade-insecure-requests');
        $returnData = array();
        $returnData["dlvDatas"] = DB::table('mod_dlv')->where('car_no', $car_no)->get();
        $returnData["msg"] = "success";
        
        return response()->json($returnData);
    }

    public function getDlvPlan($dlvNo=null) {
        $user = Auth::user();
        $dlvPlanData = DB::table('mod_dlv_plan')
                            ->select('mod_order.dlv_cust_nm', 'mod_order.pick_cust_nm', 'mod_dlv_plan.*')
                            ->join('mod_order', 'mod_order.sys_ord_no', '=', 'mod_dlv_plan.sys_ord_no')
                            ->where('mod_dlv_plan.dlv_no', $dlvNo)
                            ->where('mod_order.c_key', $user->c_key)->get();

        return response()->json($dlvPlanData);
    }

    public function rmDlvPlan() {
        $user     = Auth::user();
        $ids      = request('ids');
        $dlvNo    = request('dlvNo');

        if(count($ids) > 0) {
            for($i=0; $i<count($ids); $i++) {
                //$ordNo = $ids[$i];
                $dlvPlan = DlvPlanModel::find($ids[$i]);
                if(isset($dlvPlan)) {
                    if($dlvPlan->dlv_type == 'P') {
                        $delDlvPlan = DlvPlanModel::where('ord_id', $dlvPlan->ord_id)
                                                    ->where('dlv_no', $dlvPlan->dlv_no);
                        
                        if(isset($delDlvPlan)) {
                            $order = OrderMgmtModel::find($dlvPlan->ord_id);
                            $order->status      = 'UNTREATED';
                            $order->dlv_no      = null;
                            $order->car_no      = null;
                            $order->truck_no    = null;
                            $order->driver      = null;
                            $order->amt         = null;
                            $order->car_type    = null;
                            $order->dlv_type    = null;
                            $order->ord_type_nm = null;
                            $order->save();
                        
                            $delDlvPlan->delete();
                        }
                    }
                    else {
                        $pickCnt = DlvPlanModel::where('ord_id', $dlvPlan->ord_id)
                                            ->where('dlv_type', 'P')
                                            ->count();

                        
                        if($pickCnt > 0) {
                            $order = OrderMgmtModel::find($dlvPlan->ord_id);
                            $order->dlv_type = 'P';
                            $order->save();
                        }
                        else {
                            $order = OrderMgmtModel::find($dlvPlan->ord_id);
                            $order->status      = 'UNTREATED';
                            $order->dlv_no      = null;
                            $order->car_no      = null;
                            $order->truck_no    = null;
                            $order->driver      = null;
                            $order->amt         = null;
                            $order->car_type    = null;
                            $order->dlv_type    = null;
                            $order->ord_type_nm = null;
                            $order->save();
                        }
    
                        $dlvPlan = DlvPlanModel::where('ord_id', $dlvPlan->ord_id)->where('dlv_no', $dlvPlan->dlv_no)->where('dlv_type', 'D');
                        $dlvPlan->delete();
                    }
                }
                
                
            }

            $cnt = DB::table('mod_dlv_plan')->where('dlv_no', $dlvNo)->count();

            if($cnt == 0) {
                $dlv = DlvModel::where('dlv_no', $dlvNo)->where('c_key', $user->c_key);
                $dlv->delete();
            }
            
            $fCnt = DB::table('mod_dlv_plan')->where('dlv_no', $dlvNo)->whereRaw("(status = 'FINISHED' or status = 'ERROR')")->count();

            if($cnt == $fCnt) {
                DB::table('mod_dlv')
                    ->where('dlv_no', $dlvNo)
                    ->update(['status' => 'FINISHED']);
            }
        }

        return response()->json(['msg' => 'success']);
    }

    public function insertToDlv(Request $request) {
        $user    = Auth::user();
        $dlv_no  = $request->dlv_no;
        $ord_ids = $request->ord_ids;
        $carType = $request->car_type;

        $dlvData = DB::table('mod_dlv')
                        ->where('dlv_no', $dlv_no)
                        ->where('g_key', $user->g_key)
                        ->where('c_key', $user->c_key)
                        ->first();
        
        if(isset($dlvData)) {
            $ords = DB::table('mod_order')
                    ->whereIn('id', $ord_ids)
                    ->get();

            $car = DB::table('mod_car')->where('car_no', $dlvData->car_no)->first();
            $today       = new \DateTime();
            $dlv_detail = array();
            $dlvStatus = "UNTREATED";
            if($dlvData->status == "DLV" || $dlvData->status == "SEND") {
                $dlvStatus = "DLV";
            }
            
            if(count($ords) > 0) {
                foreach($ords as $key=>$ord) { 
                    $ordCnt = DB::table('mod_dlv_plan')
                            ->where('status', 'FINISHED')
                            ->where('dlv_type', 'P')
                            ->where('sys_ord_no', $ord->sys_ord_no)
                            ->count();   
                    //$ord->dlv_type != 'P' 代表已有提貨再其它配送單裡
                    if(!empty($ord->pick_addr) && $ordCnt == 0 && $ord->dlv_type != 'P') {
                        $detail = array(
                            "ord_id"      => $ord->id,
                            "cust_nm"     => $ord->pick_cust_nm,
                            "dlv_type"    => 'P',
                            "ord_no"      => $ord->ord_no,
                            "sys_ord_no"  => $ord->sys_ord_no,
                            "status"      => $dlvStatus,
                            "dlv_no"      => $dlv_no,
                            "addr"        => $ord->pick_city_nm.$ord->pick_area_nm.$ord->pick_addr,
                            "etd"         => $ord->etd,
                            "sort"        => $key,
                            "remark"      => htmlspecialchars($ord->pick_remark),
                            "updated_at" => $today->format('Y-m-d H:i:s'),
                            "updated_by" => $user->email,
                            "created_at" => $today->format('Y-m-d H:i:s'),
                            "created_by" => $user->email,
                            "g_key"      => $user->g_key,
                            "c_key"      => $user->c_key,
                            "s_key"      => $user->s_key,
                            "d_key"      => $user->d_key
                        );
                        array_push($dlv_detail, $detail);
                    }
    
                    if(!empty($ord->dlv_addr)) {
                        $detail = array(
                            "ord_id"     => $ord->id,
                            "cust_nm"    => $ord->dlv_cust_nm,
                            "dlv_type"   => 'D',
                            "ord_no"     => $ord->ord_no,
                            "sys_ord_no" => $ord->sys_ord_no,
                            "status"     => $dlvStatus,
                            "dlv_no"     => $dlv_no,
                            "addr"       => $ord->dlv_city_nm.$ord->dlv_area_nm.$ord->dlv_addr,
                            "etd"        => $ord->etd,
                            "sort"       => $key,
                            "remark"     => htmlspecialchars($ord->dlv_remark),
                            "updated_at" => $today->format('Y-m-d H:i:s'),
                            "updated_by" => $user->email,
                            "created_at" => $today->format('Y-m-d H:i:s'),
                            "created_by" => $user->email,
                            "g_key"      => $user->g_key,
                            "c_key"      => $user->c_key,
                            "s_key"      => $user->s_key,
                            "d_key"      => $user->d_key
                        );
        
                        array_push($dlv_detail, $detail);
                    }

                    
                    
                }

                DB::table('mod_dlv_plan')->insert($dlv_detail);
                
                $ordStatus = "SEND";
                if($dlvData->status == "DLV") {
                    $ordStatus = "SETOFF";
                }

                if($dlvData->status == "SEND") {
                    $ordStatus = "LOADING";
                }
                DB::table('mod_order')
                ->whereIn('id', $ord_ids)
                ->update([
                    "status"   => $ordStatus, 
                    "car_no"   => $car->car_no,
                    "truck_no" => $car->car_no,
                    "dlv_no"   => $dlv_no,
                    "car_type" => $dlvData->car_type,
                    "dlv_type" => $car->dlv_type,
                    "driver"   => $dlvData->driver_nm,
                    "ord_type_nm" => "插單"
                ]);
                
    
                foreach($ords as $key=>$ord) {
                    $tm = new TrackingModel();
                    $tm->insertTracking('', $ord->ord_no, '', $ord->sys_ord_no, '', 'A', 3, $user->g_key, $user->c_key, $user->s_key, $user->d_key);

                    if($dlvData->status == "DLV") {
                        $tm = new TrackingModel();
                        $tm->insertTracking('', $ord->ord_no, '', $ord->sys_ord_no, '', 'B', 4, $user->g_key, $user->c_key, $user->s_key, $user->d_key);
                    }
                    // $tsData = DB::table("mod_trans_status")->where("ts_type", "A")->where('order', 3)->where('g_key', $user->g_key)->where('c_key', $user->c_key)->orderBy("order", "desc")->first();
                    // $data = [
                    //     'ts_no'      => $tsData->id,
                    //     'ts_type'    => $tsData->ts_type,
                    //     'sort'       => $tsData->order,
                    //     'ts_name'    => $tsData->ts_name,
                    //     'ts_desc'    => $tsData->ts_desc,
                    //     'ref_no1'    => $dlv_no,
                    //     'ref_no2'    => $ord->ord_no,
                    //     'ref_no3'    => $car->car_no,
                    //     'ref_no4'    => null,
                    //     'g_key'      => $ord->g_key,
                    //     'c_key'      => $ord->c_key,
                    //     's_key'      => $ord->s_key,
                    //     'd_key'      => $ord->d_key,
                    //     'created_by' => $user->name,
                    //     'updated_by' => $user->name,
                    // ];
            
                    // $TransRecordModel = new TransRecordModel();
                    // $TransRecordModel->createRecord($data);
    
                    // if(isset($ord->dlv_email)) {
                    //     $TrackingModel = new TrackingModel();
                    //     //$TrackingModel->sendTrackingMail($ord->ord_no, $ord->dlv_email, '貨況通知-訂單號：'.$ord->ord_no);
                    // }
    
                    $c = new CalculateModel();
                    $amt = $c->calculateAmt($ord->id);
                    DB::table('mod_order')
                    ->where('id', $ord->id)
                    ->update([
                        'amt' => $amt
                    ]);
                }
            }
        }

        return response()->json(["msg" => "success"]);
    }

    public function sendToApp() {
        $ids = request('ids');
        $user = Auth::user();
        try {
            if(count($ids) > 0) {
                for($i=0; $i<count($ids); $i++) {
                    //$dlvPlanData = DB::table('mod_dlv_plan')->select('ord_id');
                    $dlv = DlvModel::find($ids[$i]);
                    if($dlv->status != "UNTREATED") {
                        return response()->json(array('msg' => 'error', 'error_log' => '只有「尚未安排」的配送單，才能發送app'));
                    }
                    $dlv->status = 'SEND';
                    $dlv->save();

                    DB::table('mod_dlv_plan')
                        ->where('dlv_no', $dlv->dlv_no)
                        ->where('c_key', $user->c_key)
                        ->update(['status' => 'DLV']);
    
                    $dlvPlanData = DB::table('mod_dlv_plan')
                                    ->select('ord_id')
                                    ->where('dlv_no', $dlv->dlv_no)
                                    ->where('g_key', $dlv->g_key)
                                    ->where('c_key', $dlv->c_key)
                                    ->get();
                    $carData = DB::table('users')->select('d_token')->where('email', $dlv->car_no)->first();
                    if(isset($carData)) {
                        $f = new FcmModel;
                        $f->sendToFcm('系統通知', '您有新配送單：'.$dlv->dlv_no, $carData->d_token);

                        $s = new SysNoticeModel;
                        $s->insertNotice('系統通知', '您有新配送單：'.$dlv->dlv_no, $dlv->car_no, $user);
                    }
                    
                    
                    if(count($dlvPlanData)) {
                        foreach($dlvPlanData as $row) {
                            $order = OrderMgmtModel::find($row->ord_id);
                            $order->status = 'LOADING';
                            $order->save();
                        }
                    }
                }
            }
        }
        catch(\Exception $e) {
            return response()->json(array('msg' => 'error', 'error_log' => $e->getMessage()));
        }
        

        return response()->json(array('msg' => 'success'));
    }


    public function exportDetail() {
        $dlvNo = request('dlvNo');
        
        $now = date('Ymd');
        Excel::create('配送單-'.$now, function($excel) use($dlvNo) {
            $dlvNoArray = explode(';', $dlvNo);
            for($i=0; $i<count($dlvNoArray); $i++) {
                $dlvNo = $dlvNoArray[$i];
                $excel->sheet($dlvNoArray[$i], function($sheet) use($dlvNo) {
                    $user = Auth::user();
                    $dlvPlanData = DB::table('mod_dlv_plan')
                                        ->select('mod_order.dlv_cust_nm', 'mod_order.pick_cust_nm', 'mod_order.pkg_num', 'mod_dlv_plan.*')
                                        ->join('mod_order', 'mod_order.ord_no', '=', 'mod_dlv_plan.ord_no')
                                        ->where('mod_dlv_plan.dlv_no', $dlvNo)
                                        ->where('mod_order.c_key', $user->c_key)
                                        ->get();
            
                    $sheet->row(1, array('配送單：'.$dlvNo));
                    $sheet->row(2, array(
                        '派車別', '預計送達時間', '系統訂單號', '訂單號', '件數', '客戶名稱', '地址' 
                    ));
    
                    foreach($dlvPlanData as $key=>$row) {
                        $dlvType = '提貨';
                        $custNm  = $row->pick_cust_nm;
                        if($row->dlv_type == 'D') {
                            $dlvType = '配送';
                            $custNm = $row->dlv_cust_nm;
                        }
    
                        $sheet->row($key+3, array(
                            $dlvType, $row->etd, $row->sys_ord_no, $row->ord_no, $row->pkg_num, $custNm, $row->addr 
                        ));
                    }
            
                });
            }
            
        })->export('xls');   
        
    }

    public function DlvPlanSearch() {
        $data = array(
            'crud' => (object)array(
                'colModel' => array()
            )
        );
        return view('tranPlan.dlvPlan', $data);
    }
}
