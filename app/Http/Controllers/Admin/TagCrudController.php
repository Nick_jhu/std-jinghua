<?php namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\TagCrudRequest as StoreRequest;
use App\Http\Requests\TagCrudRequest as UpdateRequest;
use Illuminate\Support\Facades\DB;
class TagCrudController extends CrudController {

	public function setup() {
        $this->crud->setModel("App\Models\Tag");
        $this->crud->setRoute("admin/tag");
        $this->crud->setEntityNameStrings('tag', 'tags');
        $customers = DB::table('customers')->pluck('name', 'id')->where('id','>=',0);
        //dd($customers);
        $this->crud->setColumns([
            [
                'name' => 'cust_no',
                'label' => "cust_no",
                'type'  => 'text'
            ],
            [
                'name' => 'name',
                'label' => "Tag name",
                'type'  => 'text'
            ],
            [
                'name'  => 'address', // DB column name (will also be the name of the input)
                'label' => 'Street address', // the human-readable label for the input
                'type'  => 'text' // the field type (text, number, select, checkbox, etc)
            ],
            [   // Number
                'name' => 'phone',
                'label' => 'Number',
                'type' => 'number',
                // optionals
                // 'attributes' => ["step" => "any"], // allow decimals
                // 'prefix' => "$",
                // 'suffix' => ".00",
            ],
            [
                'name' => 'cust_contact',
                'label' => "cust_contact",
                'type'  => 'text'
            ],
            [
                'name'        => 'gender', // the name of the db column
                'label'       => 'gender', // the input label
                'type'        => 'radio',
                'options'     => [ // the key will be stored in the db, the value will be shown as label; 
                                    'm' => "male",
                                    'f' => "female"
                                ],
                // optional
                //'inline'      => false, // show the radios all on the same line?
            ],
            [   // Email
                'name' => 'email',
                'label' => 'Email Address',
                'type' => 'email'
            ],         
            [   // date_picker
                'name' => 'cust_date',
                'type' => 'datetime_picker',
                'label' => 'Date',
                 // optional:
                'datetime_picker_options' => [
                'format' => 'DD/MM/YYYY HH:mm',
                'language' => 'en'
                ]            
             ],
             [   // DateTime
                'name' => 'cust_time',
                'label' => 'Event start',
                'type' => 'datetime_picker',
                // optional:
                'datetime_picker_options' => [
                    'format' => 'LT'                  
                ]
             ],
             [  // Select
                'label' => "city",
                'type' => 'select2_from_array',
                'name' => 'city', 
                'options' => $customers,
                'allows_null' => false                
             ],
             [   // Textarea
                'name' => 'remark',
                'label' => 'Remark',
                'type' => 'textarea'
             ]
            ]);
        //$this->crud->addColumns(['address']);
        $this->crud->addFields([
            [
                'name' => 'cust_no',
                'label' => "cust_no",
                'type'  => 'text'
            ],
            [
            'name' => 'name',
            'label' => "Tag name",
            'type'  => 'text'
            ],
            [
                // MANDATORY
            'name'  => 'address', // DB column name (will also be the name of the input)
            'label' => 'Street address', // the human-readable label for the input
            'type'  => 'text', // the field type (text, number, select, checkbox, etc)
            
                // OPTIONAL + example values
            'default'    => 'some value', // default value
            'hint'       => 'Some hint text', // helpful text, show up after input
            'attributes' => [
                'placeholder' => 'Some text when empty',
                'class' => 'form-control'
                ], // extra HTML attributes and values your input might need
                'wrapperAttributes' => [
                'class' => 'form-group col-md-12'
                ] // extra HTML attributes for the field wrapper - mostly for resizing fields using the bootstrap column classes
                ],
                [   // Number
                    'name' => 'phone',
                    'label' => 'Number',
                    'type' => 'number',
                    // optionals
                    // 'attributes' => ["step" => "any"], // allow decimals
                    // 'prefix' => "$",
                    // 'suffix' => ".00",
                ],
                [
                    'name' => 'cust_contact',
                    'label' => "cust_contact",
                    'type'  => 'text'
                ],
                [
                    'name'        => 'gender', // the name of the db column
                    'label'       => 'gender', // the input label
                    'type'        => 'radio',
                    'options'     => [ // the key will be stored in the db, the value will be shown as label; 
                                        'm' => "male",
                                        'f' => "female"
                                    ],
                    // optional
                    //'inline'      => false, // show the radios all on the same line?
                ],
                [   // Email
                    'name' => 'email',
                    'label' => 'Email Address',
                    'type' => 'email'
                ],
                [   // date_picker
                    'name' => 'cust_date',
                    'type' => 'datetime_picker',
                    'label' => 'Date',
                     // optional:
                    'datetime_picker_options' => [
                    'format' => 'DD/MM/YYYY HH:mm',
                    'language' => 'en'
                    ]            
                 ],
                //  [   // DateTime
                //     'name' => 'cust_time',
                //     'label' => 'Event start',
                //     'type' => 'datetime_picker',
                //     // optional:
                //     'datetime_picker_options' => [
                //         'format' => 'LT'
                //     ]
                // ],
                // [  // Select
                //     'label' => "city",
                //     'type' => 'select',
                //     'name' => 'city', // the db column for the foreign key
                //     'entity' => 'parent', // the method that defines the relationship in your Model
                //     'attribute' => 'name', // foreign key attribute that is shown to user
                //     'model' => "\Backpack\MenuCRUD\app\Models\MenuItem" // foreign key model
                // ],
                [  // Select
                    'label' => "city",
                    'type' => 'select2_from_array',
                    'name' => 'city', 
                    'options' => $customers,
                    'allows_null' => false,
                    'allows_multiple' => true               
                 ],         
                [   // CustomHTML
                    'name' => 'spec_service',
                    'type' => 'custom_html',
                    'value' => '<div class="col-md-4"><div class="form-group"><label for="spec_service">Checkbox</label>
                        <div class="checkbox">
                            <label class="checkbox-inline">
                                <input type="checkbox" name="spec_service[]" value="1">checkbox1
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" name="spec_service[]" value="2">checkbox2
                            </label>
                        </div>
                    </div>
                </div>'
                ],
                [   // Upload
                    'name' => 'cust_file',
                    'label' => 'File',
                    'type' => 'upload_multiple',
                    'upload' => true        
                ],
                [   // Textarea
                    'name' => 'remark',
                    'label' => 'Remark',
                    'type' => 'textarea'
                ]
       ]);
    }

	public function store(StoreRequest $request)
	{
        
        $this->processData($request);

		return parent::storeCrud($request);
	}

	public function update(UpdateRequest $request)
	{
		return parent::updateCrud();
    }
    
    public function processData($request) {
        
        foreach($this->crud->create_fields as $key=>$value) {
            if($value['type']  == 'checkbox' || $value['type'] == 'custom_html' || $value['type'] == 'checklist') {

                $postVal = $request[$key];
                if(count($request[$key]) > 0) {
                    $request[$key] = join(',', $postVal);
                }
                else {
                    $request[$key] = '';
                }

            }
            else if($value['type'] == 'select2_multiple' || $value['type'] == 'select_multiple' || $value['type'] == 'select_from_array' || $value['type'] == 'select2_from_array' || $value['type'] == 'select2_from_ajax') {
                $postVal = $request[$key];
                if(count($request[$key]) > 0) {
                    $request[$key] = join(',', $postVal);
                }
                else {
                    $request[$key] = '';
                }
            }
        }
    }
}