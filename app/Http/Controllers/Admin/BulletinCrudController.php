<?php namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\BulletinCrudRequest as StoreRequest;
use App\Http\Requests\BulletinCrudRequest as UpdateRequest;
use Illuminate\Support\Facades\DB;
use App\Models\CommonModel;
class BulletinCrudController extends CrudController {

	public function setup() {
        $this->crud->setModel("App\Models\BulletinModel");        
        $this->crud->setRoute(config('backpack.base.route_prefix').'/bulletin');
        $this->crud->setEntityNameStrings(trans('bulletin.titleName'), trans('bulletin.titleName'));
        $customers = DB::table('customers')->pluck('name', 'id')->where('id','>=',0);
        //dd($customers);
        $this->crud->setColumns([
            [
                'name' => 'title',
                'label' => trans('bulletin.title'),
                'type'  => 'text'
            ],                
            [   // date_picker
                'name' => 'public_date',
                'type' => 'datetime_picker',
                'label' => trans('bulletin.date'),
                 // optional:
                'datetime_picker_options' => [
                'format' => 'DD/MM/YYYY HH:mm',
                //'language' => 'en'
                ]            
             ]
            ]);
        //$this->crud->addColumns(['address']);
        $this->crud->addFields([
                [
                    'name' => 'title',
                    'label' => trans('bulletin.title'),
                    'type'  => 'text'
                ],            
                [   // date_picker
                    'name' => 'public_date',
                    'type' => 'datetime_picker',
                    'label' => trans('bulletin.date'),
                     // optional:
                    'datetime_picker_options' => [
                    'format' => 'DD/MM/YYYY HH:mm',
                    //'language' => 'en'
                    ]            
                ],               
                [ // Table
                    'name' => 'description',
                    'label' => trans('bulletin.description'),
                    'type' => 'summernote'
                ]
       ]);
    }

	public function store(StoreRequest $request)
	{
        //dd($request->all());
        $commonFunc = new CommonModel;
        $request = $commonFunc->processData($request, $this->crud->create_fields);

		return parent::storeCrud($request);
	}

	public function update(UpdateRequest $request)
	{
        //dd($request->all());
        $commonFunc = new CommonModel;
        $request = $commonFunc->processData($request, $this->crud->create_fields);
		return parent::updateCrud($request);
    }
}