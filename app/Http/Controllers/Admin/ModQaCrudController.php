<?php namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ModQaCrudRequest as StoreRequest;
use App\Http\Requests\ModQaCrudRequest as UpdateRequest;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use DB;

class ModQaCrudController extends CrudController {

	public function setup() {
        $this->crud->setModel('App\Models\ModQaModel');
        $this->crud->setRoute(config('backpack.base.route_prefix')  . '/modQa');
        $this->crud->setEntityNameStrings('Q&A', 'Q&A');

        $this->crud->setColumns([
            [
                'name' => 'q_title',
                'label' => "問題",
                'type'  => 'text'
            ],
            [ // n-n relationship (with pivot table)
                'label'     => '類別', // Table column heading
                'type' => 'select_from_array',
                'name' => 'cate_id', // the db column for the foreign key
                'attribute' => 'name', // foreign key attribute that is shown to user
                'options' => DB::table('mod_cate')->where('parent_id', 2)->pluck('descp', 'id'),
            ],
            [
                'name' => 'created_by',
                'label' => "建立者",
                'type'  => 'text'
            ],
            [
                'name' => 'created_at',
                'label' => "時間",
                'type'  => 'text'
            ]
        ]);

        $this->crud->addField([
            'name' => 'q_title',
            'label' => '問題',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6',
            ],
        ]);

        $this->crud->addField([   // color_picker
            'label' => '標題顏色',
            'name' => 'title_color',
            'type' => 'color_picker',
            'color_picker_options' => ['customClass' => 'custom-class'],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6',
            ],
        ]);

        $this->crud->addField([  // Select
            'label' => "類別",
            'type' => 'select_from_array',
            'name' => 'cate_id', // the db column for the foreign key
            'attribute' => 'name', // foreign key attribute that is shown to user
            'options' => DB::table('mod_cate')->where('parent_id', 2)->pluck('descp', 'id'),
        ]);


        $this->crud->addField([   // CKEditor
            'name' => 'answer',
            'label' => '解答',
            'type' => 'ckeditor',
            // optional:
            'extra_plugins' => ['oembed', 'widget','justify']
        ]);

        $this->crud->addField([   // Hidden
            'name' => 'created_by',
            'type' => 'hidden',
            'value' => Auth::user()->name
        ]);

        $this->crud->addField([   // Hidden
            'name' => 'updated_by',
            'type' => 'hidden',
            'value' => Auth::user()->name
        ]);

        
    }

	public function store(StoreRequest $request)
	{
		return parent::storeCrud();
	}

	public function update(UpdateRequest $request)
	{
		return parent::updateCrud();
    }

}