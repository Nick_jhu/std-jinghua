<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Requests\CrudRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;

use App\Http\Requests\CustomerProfileCrudRequest as StoreRequest;
use App\Http\Requests\CustomerProfileCrudRequest as UpdateRequest;
use App\Models\CommonModel;
use App\Models\CustomerItemModel;
use App\Models\CustomerProfileModel;
use Illuminate\Support\Facades\Auth;

use HTML;

class CustomerProfileCrudController extends CrudController
{
    
    public function setup() {
        $user = Auth::user();
        $this->crud->setModel("App\Models\CustomerProfileModel");
        $this->crud->setEntityNameStrings(trans('sysCustomers.titleName'), trans('sysCustomers.titleName'));
        $this->crud->setRoute(config('backpack.base.route_prefix').'/customerProfile');
    
        $this->crud->setColumns(['name']);

        
        $this->crud->setCreateView('customerProfile.edit');
        $this->crud->setEditView('customerProfile.edit');
        $this->crud->setListView('customerProfile.index');
        $this->crud->enableAjaxTable();

        $this->crud->addField([
            'name' => 'cust_no',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'cname',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'ename',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'phone',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'fax',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'address',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'contact',
            'type' => 'text'
        ]);       

        $this->crud->addField([
            'name' => 'cmp_abbr',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'email',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'remark',
            'type' => 'textarea'
        ]);

        $this->crud->addField([
            'name' => 'created_at',
            'type' => 'date_picker',
            // optional:
            'date_picker_options' => [
                'format' => 'YYYY-MM-DD',
                //'language' => 'en'
            ]
        ]);

        $this->crud->addField([
            'name' => 'updated_at',
            'type' => 'date_picker',
            // optional:
            'date_picker_options' => [
                'format' => 'YYYY-MM-DD',
                //'language' => 'en'
            ]
        ]);                     

        $this->crud->addField([
            'name' => 'status',
            'type' => 'select'
        ]);

        $this->crud->addField([
            'name' => 'created_by',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'updated_by',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'cust_type',
            'type' => 'select2_multiple',
            'options' => DB::table('bscode')->select('cd as code', 'cd_descp as descp')->where('cd_type', 'CT')->where('c_key', $user->c_key)->get()
        ]);

        $this->crud->addField([
            'name' => 'g_key',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'c_key',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 's_key',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'd_key',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'type',
            'type' => 'hidden'
        ]);

        $this->crud->addField([
            'name' => 'city_no',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'city_nm',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'zip',
            'type' => 'lookup',
            'title' => '郵地區號查詢',
            'info1' => Crypt::encrypt('sys_area'), //table
            'info2' => Crypt::encrypt("dist_cd+city_nm+dist_nm,city_nm,dist_cd,dist_nm"), //column
            'info3' => Crypt::encrypt(""), //condition
            'info4' => "dist_cd=zip;city_nm=city_nm;dist_nm=area_nm;" //field mapping
        ]);

        $this->crud->addField([
            'name' => 'area_id',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'area_nm',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'def',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'tax_id',
            'type' => 'text'
        ]);
    }

    public function getData($table=null,$id) {
        $data = DB::table('sys_customers')->where('id', $id)->first();

        return response()->json($data);
    }

    public function edit($id)
    {
        $user = Auth::user();
        $this->crud->hasAccessOrFail('update');

        // get the info for that entry
        $this->data['entry'] = $this->crud->getEntry($id);
        $this->data['entry'] = str_replace("'", "",json_encode($this->data['entry']));

        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getUpdateFields($id);
        $this->data['title'] = trans('backpack::crud.edit').' '.$this->crud->entity_name;

        $this->data['id'] = $id;


        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getEditView(), $this->data);
    }


    public function store(StoreRequest $request)
	{
        $user = Auth::user();
        $type = $request->cust_type;    
        $request->merge(['type' => "OTHER"]);
        $commonFunc = new CommonModel;
        $request = $commonFunc->processData($request, $this->crud->create_fields);
        

        try {
            if(isset($type) && count($type) > 0) {
                if(in_array('CT003', $type) && $request->def == 1) {
                    DB::table('sys_customers')
                        ->where('cust_type', $value)
                        ->where('g_key', $user->g_key)
                        ->where('c_key', $user->c_key)
                        ->update(['def' => 0]);
                }
            }
            $response = parent::storeCrud($request);
            if(isset($type) && count($type) > 0) {
                if(in_array('CT003', $type) && $request->def == 1) {
                    DB::table('sys_customers_item')
                        ->where('cust_type', $value)
                        ->where('g_key', $user->g_key)
                        ->where('c_key', $user->c_key)
                        ->update(['def' => 0]);
                }
                foreach($type as $value) {
                    $customerItem             = new CustomerItemModel;
                    $customerItem->cust_type  = $value;
                    $customerItem->cust_no    = $request->cust_no;
                    $customerItem->cname      = $request->cname;
                    $customerItem->ename      = $request->ename;
                    $customerItem->phone      = $request->phone;
                    $customerItem->address    = $request->address;
                    $customerItem->contact    = $request->contact;
                    $customerItem->cmp_abbr   = $request->cmp_abbr;
                    $customerItem->email      = $request->email;
                    $customerItem->remark     = $request->remark;
                    $customerItem->g_key      = $user->g_key;
                    $customerItem->c_key      = $user->c_key;
                    $customerItem->s_key      = $user->s_key;
                    $customerItem->d_key      = $user->d_key;
                    $customerItem->created_by = $user->email;
                    $customerItem->def        = $request->def;
                    $customerItem->save();
                }
            }
            
        }
        catch (\Exception $e) {
            
            \Log::error($e);
            
            return ["msg"=>"error", "errorLog"=>$e];
        }
        
        return ["msg"=>"success", "response"=>$response, "lastId"=>$this->data['entry']->getKey()];
	}

	public function update(UpdateRequest $request)
	{
        $user = Auth::user();
        $commonFunc = new CommonModel;
        $request = $commonFunc->processData($request, $this->crud->create_fields);

        try {
            if($request->def == 1) {
                DB::table('sys_customers')
                    ->where('cust_type', 'CT003')
                    ->where('g_key', $user->g_key)
                    ->where('c_key', $user->c_key)
                    ->update(['def' => 0]);
            }
            $response = parent::updateCrud($request);
            $customerProfile = CustomerProfileModel::find($request->id)->first();
            //$customerProfile = DB::table('sys_customers')->select('*')->where('cust_no', $customer->cust_no)->where('g_key', $user->g_key)->where('c_key', $user->c_key)->where('type', '=', 'OTHER')->first();           
            $customerItem = CustomerItemModel::where("cust_no",$customerProfile->cust_no)->where('g_key', $user->g_key)->where('c_key', $user->c_key);                
            $customerItem->delete();
            $type = $customerProfile->cust_type;
            if($type!=""){               
                $types = explode(",", $type); 
                if($request->def == 1) {
                    DB::table('sys_customers_item')
                        ->where('cust_type', 'CT003')
                        ->where('g_key', $user->g_key)
                        ->where('c_key', $user->c_key)
                        ->update(['def' => 0]);
                }              
                foreach($types as $value) {
                    $customerItem             = new CustomerItemModel;
                    $customerItem->cust_type  = $value;
                    $customerItem->cust_no    = $customerProfile->cust_no;
                    $customerItem->cname      = $customerProfile->cname;
                    $customerItem->ename      = $customerProfile->ename;
                    $customerItem->phone      = $customerProfile->phone;
                    $customerItem->address    = $customerProfile->address;
                    $customerItem->contact    = $customerProfile->contact;
                    $customerItem->cmp_abbr   = $customerProfile->cmp_abbr;
                    $customerItem->email      = $customerProfile->email;
                    $customerItem->remark     = $customerProfile->remark;
                    $customerItem->g_key      = $user->g_key;
                    $customerItem->c_key      = $user->c_key;
                    $customerItem->s_key      = $user->s_key;
                    $customerItem->d_key      = $user->d_key;
                    $customerItem->created_by = $user->email;
                    $customerItem->updated_by = $user->email;
                    $customerItem->def        = $customerProfile->def;
                    $customerItem->save();
                }  
            }
            
        }
        catch (\Exception $e) {            
            return ["msg"=>"error", "errorLog"=>$e->getMessage()];
        }
        
        
        return ["msg"=>"success", "response"=>$response];
    }
    
    public function destroy($id)
    {
        $this->crud->hasAccessOrFail('delete');

        $cust_no = DB::table('sys_customers')->where('id', $id)->pluck('cust_no');

        if(isset($cust_no[0])) {
            $customerItem = CustomerItemModel::where('cust_no', $cust_no[0]);
            
            $customerItem->delete();
            
        }

        return $this->crud->delete($id);
    }
    /*
    public function index()
    {
        $this->data['title'] = trans('backpack::logmanager.log_manager');

        return view('customer.index', $this->data);
    }

    public function edit()
    {
        $this->data['title'] = trans('backpack::logmanager.log_manager');

        return view('customer.edit', $this->data);
    }
    */

    public function multiDel() {
        $ids = request('ids');

        if(count($ids) > 0) {
            for($i=0; $i<count($ids); $i++) {
                $customerProfile = CustomerProfileModel::find($ids[$i]);
                $customerProfile->delete();
            }
        }

        return response()->json(array('msg' => 'success'));
    }
}
