<?php 

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Allpay;
use Recca0120\Mitake\Client;
use Illuminate\Support\Facades\Session;

class AllPayController extends Controller 
{	

	//-------------------------------------------------------------------------

    private function GetPaymentWay($p)
    {
        $val = "";

        switch ($p) {
            case 'ALL':
                $val = \ECPay_PaymentMethod::ALL;
                break;
            case 'Credit':
                $val = \ECPay_PaymentMethod::Credit;
                break;
            case 'CVS':
                $val = \ECPay_PaymentMethod::CVS;
                break;
            default:
                $val = \ECPay_PaymentMethod::ALL;
                break;
        }

        return $val;
    }

	//-------------------------------------------------------------------------

    public function index() 
    {
		Session::put("www","aaa");
    	return view('ecpay::demo');
		//return redirect('allpay/payinmart')->with('ddd', 'Login Failed');
	}

    public function checkout(Request $request) 
    {       
		
        //基本參數(請依系統規劃自行調整)
        Allpay::ecp()->Send['ReturnURL']         = "http://www.allpay.com.tw/receive.php" ;
        Allpay::ecp()->Send['MerchantTradeNo']   = "Test".time() ;           //訂單編號
        Allpay::ecp()->Send['MerchantTradeDate'] = date('Y/m/d H:i:s');      //交易時間
        Allpay::ecp()->Send['TotalAmount']       = 2000;                     //交易金額
        Allpay::ecp()->Send['TradeDesc']         = "good to drink" ;         //交易描述
        Allpay::ecp()->Send['ChoosePayment']     = $this->GetPaymentWay($request->payway);     //付款方式

        //訂單的商品資料
        array_push(Allpay::ecp()->Send['Items'], array('Name' => "歐付寶黑芝麻豆漿123121", 'Price' => (int)"2000",
                   'Currency' => "元", 'Quantity' => (int) "1", 'URL' => "dedwed"));

		//Go to AllPay
		

        echo "線上刷卡頁面導向中...";
		# 電子發票參數
        
        Allpay::ecp()->Send['InvoiceMark'] = "Y";//是否開立發票
        Allpay::ecp()->SendExtend['RelateNumber'] = "Test".time();//串連訂單號碼
        Allpay::ecp()->SendExtend['CustomerEmail'] = 'kingwang317@gmail.com';
        Allpay::ecp()->SendExtend['CustomerPhone'] = '0911222333';
		Allpay::ecp()->SendExtend['TaxType'] = '1';//應稅 1,零稅率 2,免稅 3
        Allpay::ecp()->SendExtend['CustomerAddr'] = '台北市南港區三重路19-2號5樓D棟';
        Allpay::ecp()->SendExtend['InvoiceItems'] = array();
        // 將商品加入電子發票商品列表陣列
        foreach (Allpay::ecp()->Send['Items'] as $info)
        {
            array_push(Allpay::ecp()->SendExtend['InvoiceItems'],array('Name' => $info['Name'],'Count' =>
                $info['Quantity'],'Word' => '個','Price' => $info['Price'],'TaxType' => '1'));
        }
        Allpay::ecp()->SendExtend['InvoiceRemark'] = '測試發票備註';
        Allpay::ecp()->SendExtend['DelayDay'] = '0';//延遲N日開啟
		Allpay::ecp()->SendExtend['InvType'] = '07';// 一般稅額 07 特種稅額 08
		

		//捐款
		Allpay::ecp()->SendExtend['Donation'] = '1';
		Allpay::ecp()->SendExtend['LoveCode'] = '168001';
		
        
        //產生訂單(auto submit至ECPay)
		
		echo Allpay::ecp()->CheckOut();
	}
    public function checkout_reply(Request $request) 
    {     
		try {
			// 收到綠界科技的付款結果訊息，並判斷檢查碼是否相符
			$AL = new ECPay_AllInOne();
			$AL->MerchantID = '2000132';
			$AL->HashKey = '5294y06JbISpM5x9';
			$AL->HashIV = 'v77hoKGq4kWxNNIS';
			// $AL->encryptType = ECPay_EncryptType::ENC_MD5; // MD5
			Allpay::i()->EncryptType = 1; // SHA256
			$data = Allpay::i()->CheckOutFeedback();
			// 以付款結果訊息進行相對應的處理
			/*
			回傳的綠界科技的付款結果訊息如下:
			Array
			(
				[MerchantID] =>
				[MerchantTradeNo] =>
				[StoreID] =>
				[RtnCode] =>
				[RtnMsg] =>
				[TradeNo] =>
				[TradeAmt] =>
				[PaymentDate] =>
				[PaymentType] =>
				[PaymentTypeChargeFee] =>
				[TradeDate] =>
				[SimulatePaid] =>
				[CustomField1] =>
				[CustomField2] =>
				[CustomField3] =>
				[CustomField4] =>
				[CheckMacValue] =>
			)
			*/
			// 在網頁端回應 1|OK
			echo '1|OK';
		} catch(Exception $e) {
			echo '0|' . $e->getMessage();
		}
	}

	public function payinmart(Request $request) 
    {   
	
		dd(Session::All());

        Allpay::l()->HashKey = '5294y06JbISpM5x9';
        Allpay::l()->HashIV = 'v77hoKGq4kWxNNIS';
        Allpay::l()->Send = array(
            'MerchantID' => '2000132',
            'MerchantTradeNo' => 'no' . date('YmdHis'),
            'MerchantTradeDate' => date('Y/m/d H:i:s'),
            'LogisticsType' => 'CVS',
            'LogisticsSubType' => 'UNIMART',
            'GoodsAmount' => 1500,
            'CollectionAmount' => 10,
            'IsCollection' => 'Y',
            'GoodsName' => '測試商品',
            'SenderName' => '測試寄件者',
            'SenderPhone' => '0226550115',
            'SenderCellPhone' => '0911222333',
            'ReceiverName' => '測試收件者',
            'ReceiverPhone' => '0226550115',
            'ReceiverCellPhone' => '0933222111',
            'ReceiverEmail' => 'test_emjhdAJr@test.com.tw',
            'TradeDesc' => '測試交易敘述',
            'ServerReplyURL' => 'http://www.yourwebsites.com.tw/ReturnURL',
            'LogisticsC2CReplyURL' => 'http://www.yourwebsites.com.tw/ReturnURL',
            'Remark' => '測試備註',
            'PlatformID' => '',
        );
        Allpay::l()->SendExtend = array(
            'ReceiverStoreID' => '991182',
            'ReturnStoreID' => '991182'
        );
        // BGCreateShippingOrder()
        $this_Result =Allpay::l()->BGCreateShippingOrder();
        echo '<pre>' . print_r($this_Result, true) . '</pre>';
	}
	public function payinmart_reply(Request $request) 
    {   
		$data = array();		
		$data['merchant_trade_no'] = $request->input('MerchantTradeNo'); //訂單編號
		$data['LogisticsSubType'] = $request->input('LogisticsSubType'); //物流通路代碼,如統一:UNIMART
		$data['CVSStoreID'] = $request->input('CVSStoreID');//商店代碼
		$data['CVSStoreName'] = $request->input('CVSStoreName');
		$data['CVSAddress'] = $request->input('CVSAddress');//User 所選之超商店舖地址
		$data['CVSTelephone'] = $request->input('CVSTelephone');//User 所選之超商店舖電話
		$data['ExtraData'] = $request->input('ExtraData');//額外資訊,原資料回傳
	}


	public function send_msg(Request $request) 
    {   
		//require __DIR__.'/vendor/autoload.php';
	
		$mitake = app(\Mitake\Client::class);

		$message = (new \Mitake\Message\Message())
			->setDstaddr('0953652406')
			->setSmbody('美麗點人生網路購物會員註冊驗證碼：8793，驗證碼將於十分鐘後失效');
		$result = $mitake->send($message);

		dd($result);
	}
}
