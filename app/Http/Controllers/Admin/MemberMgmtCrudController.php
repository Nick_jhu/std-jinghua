<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Requests\CrudRequest;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\UploadedFile;
use Storage;
use File;
use Response;

use App\Http\Requests\MemberMgmtCrudRequest as StoreRequest;
use App\Http\Requests\MemberMgmtCrudRequest as UpdateRequest;
use App\Models\CommonModel;
use App\Models\BaseModel;
use App\Models\MemberMgmtModel;
use Illuminate\Support\Facades\Auth;

use HTML;

class MemberMgmtCrudController extends CrudController
{
    public function setup() {
        $user = Auth::user();
        $this->crud->setModel("App\Models\MemberMgmtModel");
        $this->crud->setEntityNameStrings('會員總覽', '會員總覽');
        $this->crud->setRoute(config('backpack.base.route_prefix').'/MemberMgmt');
    
        $this->crud->setColumns(['name']);
        
        $this->crud->setCreateView('MemberMgmt.edit');
        $this->crud->setEditView('MemberMgmt.edit');
        $this->crud->setListView('MemberMgmt.index');
        $this->crud->enableAjaxTable();

        $array_of_fields_definition_arrays = array(
            [
                'name' => 'name',
                'type' => 'text'
            ],
            [
                'name' => 'phone',
                'type' => 'text'
            ],
            [
                'name' => 'email',
                'type' => 'text'
            ],
            [
                'name' => 'password',
                'type' => 'text'
            ],
            [
                'name' => 'addr',
                'type' => 'text'
            ],
            [
                'name' => 'pick_way',
                'type' => 'text',
                'options' => DB::table('bscode')
                            ->select('cd as code', 'cd_descp as descp', 'value2 as def')
                            ->where('cd_type', 'PICKWAY')
                            ->orderBy('value1', 'asc')
                            ->where('g_key', Auth::user()->g_key)
                            ->where('c_key', Auth::user()->c_key)
                            ->get()
            ],
            [
                'name' => 'return_way',
                'type' => 'text',
                'options' => DB::table('bscode')
                            ->select('cd as code', 'cd_descp as descp', 'value2 as def')
                            ->where('cd_type', 'PICKWAY')
                            ->orderBy('value1', 'asc')
                            ->where('g_key', Auth::user()->g_key)
                            ->where('c_key', Auth::user()->c_key)
                            ->get()
            ],
            [
                'name' => 'is_actived',
                'type' => 'select'
            ],
            [
                'name' => 'dlv_city',
                'type' => 'text'
            ],
            [
                'name' => 'dlv_area',
                'type' => 'text'
            ],
            [
                'name' => 'dlv_addr',
                'type' => 'text'
            ],
            [
                'name' => 'g_key',
                'type' => 'text'
            ],
            [
                'name' => 'c_key',
                'type' => 'text'
            ],
            [
                'name' => 's_key',
                'type' => 'text'
            ],
            [
                'name' => 'd_key',
                'type' => 'text'
            ],
            [
                'name' => 'created_by',
                'type' => 'text'
            ],
            [
                'name' => 'updated_by',
                'type' => 'text'
            ],
            [
                'name' => 'dlv_zip',
                'type' => 'lookup',
                'title' => '郵地區號查詢',
                'info1' => Crypt::encrypt('sys_area'), //table
                'info2' => Crypt::encrypt("dist_cd+city_nm+dist_nm,city_nm,dist_cd,dist_nm"), //column
                'info3' => Crypt::encrypt(""), //condition
                'info4' => "dist_cd=dlv_zip;city_nm=dlv_city;dist_nm=dlv_area;" //field mapping
            ],
        );


        $this->crud->addFields($array_of_fields_definition_arrays);
    }

    public function index() {
        $user = Auth::user();

        $this->crud->hasAccessOrFail('list');
        
        $this->data['crud'] = $this->crud;
        $this->data['title'] = $this->crud->entity_name_plural;

        // get all entries if AJAX is not enabled
        if (! $this->data['crud']->ajaxTable()) {
            $this->data['entries'] = $this->data['crud']->getEntries();
        }
        
        $BaseModel = new BaseModel();
        $obj = null;

        $layout = $BaseModel->getLayout(url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/mod_member'));
        $obj = json_decode($layout);

        $fieldData = $BaseModel->getColumnInfo("mod_member");
        if($obj != null) {
            array_push($fieldData, $obj);
        }

        $colModel = array('mod_member' => $fieldData);

        $this->crud->colModel = $fieldData;

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getListView(), $this->data);
    }

    public function getData($table=null,$id) {
        $data = DB::table('mod_member')->where('id', $id)->first();

        return response()->json($data);
    }

    public function edit($id)
    {
        $user = Auth::user();
        $this->crud->hasAccessOrFail('update');

        // get the info for that entry
        $this->data['entry'] = $this->crud->getEntry($id);
        $this->data['entry'] = str_replace("'", "",json_encode($this->data['entry']));

        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getUpdateFields($id);
        $this->data['title'] = trans('backpack::crud.edit').' '.$this->crud->entity_name;

        $this->data['id'] = $id;


        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getEditView(), $this->data);
    }


    public function store(StoreRequest $request)
	{
        $user = Auth::user();
        $type = $request->cust_type;    
        $commonFunc = new CommonModel;
        $request = $commonFunc->processData($request, $this->crud->create_fields);
        

        try {
            $response = parent::storeCrud($request);
        }
        catch (\Exception $e) {
            
            \Log::error($e);
            
            return ["msg"=>"error", "errorLog"=>$e];
        }
        
        return ["msg"=>"success", "response"=>$response, "lastId"=>$this->data['entry']->getKey()];
	}

	public function update(UpdateRequest $request)
	{
        $user = Auth::user();
        $commonFunc = new CommonModel;
        $request = $commonFunc->processData($request, $this->crud->create_fields);

        if(isset($request->password)) {
            $request->merge(array('password' => \Hash::make($request->password)));
        }

        try {
            $response = parent::updateCrud($request);
        }
        catch (\Exception $e) {            
            return ["msg"=>"error", "errorLog"=>$e->getMessage()];
        }
        
        
        return ["msg"=>"success", "response"=>$response];
    }
    
    public function destroy($id)
    {
        $this->crud->hasAccessOrFail('delete');

        return $this->crud->delete($id);
    }

    public function multiDel() {
        $ids = request('ids');

        if(count($ids) > 0) {
            for($i=0; $i<count($ids); $i++) {
                $customerProfile = MemberMgmtModel::find($ids[$i]);
                $customerProfile->delete();
            }
        }

        return response()->json(array('msg' => 'success'));
    }

    public function activeMember() {
        $ids = request('ids');
        if(count($ids) > 0) {
            DB::table('mod_member')->whereIn('id', $ids)->update([
                'is_actived' => 'Y',
                'updated_at' => \Carbon::now(),
            ]);
        }

        return response()->json(array('msg' => 'success'));
    }
}
