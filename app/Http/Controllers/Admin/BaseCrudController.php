<?php namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\BaseCrudRequest as StoreRequest;
use App\Http\Requests\BaseCrudRequest as UpdateRequest;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use DB;
use Redirect;

class BaseCrudController extends CrudController {

	public function setup() {
        $this->crud->setModel('App\Models\Base');
        $this->crud->setRoute(config('backpack.base.route_prefix')  . '/modBase');
        $this->crud->setEntityNameStrings('基本設定', '基本設定');


        $this->crud->addField([
            'name' => 'seo_title',
            'label' => 'SEO 標題',
            'tab' => '基本資訊'
        ]);

        $this->crud->addField([
            'name' => 'seo_desc',
            'label' => 'SEO 描述',
            'type' => 'textarea',
            'tab' => '基本資訊'
        ]);

        $this->crud->addField([
            'name' => 'contract_us',
            'label' => '聯絡我們',
            'type' => 'textarea',
            'tab' => '基本資訊',
            'attributes' => [
                'rows' => '10'
            ],
        ]);

        $this->crud->addField([
            'name' => 'title1',
            'label' => '第1格標題',
            'tab' => '首頁四格'
        ]);

        $this->crud->addField([   // CKEditor
            'name' => 'descp1',
            'label' => '第一格內容',
            'type' => 'ckeditor',
            'extra_plugins' => ['oembed', 'widget','justify'],
            'tab' => '首頁四格'
        ]);


        $this->crud->addField([
            'name' => 'title2',
            'label' => '第2格標題',
            'tab' => '首頁四格'
        ]);

        $this->crud->addField([   // CKEditor
            'name' => 'descp2',
            'label' => '第2格內容',
            'type' => 'ckeditor',
            'extra_plugins' => ['oembed', 'widget','justify'],
            'tab' => '首頁四格'
        ]);


        $this->crud->addField([
            'name' => 'title3',
            'label' => '第3格標題',
            'tab' => '首頁四格'
        ]);

        $this->crud->addField([   // CKEditor
            'name' => 'descp3',
            'label' => '第3格內容',
            'type' => 'ckeditor',
            'extra_plugins' => ['oembed', 'widget','justify'],
            'tab' => '首頁四格'
        ]);


        $this->crud->addField([
            'name' => 'title4',
            'label' => '第4格標題',
            'tab' => '首頁四格'
        ]);

        $this->crud->addField([   // CKEditor
            'name' => 'descp4',
            'label' => '第4格內容',
            'type' => 'ckeditor',
            'extra_plugins' => ['oembed', 'widget','justify'],
            'tab' => '首頁四格'
        ]);

        $this->crud->addField([
            'name' => 'about_us',
            'label' => '內容',
            'type' => 'textarea',
            'tab' => '關於我們',
            'attributes' => [
                'rows' => '10'
            ],
        ]);

        $this->crud->addField([   // Upload
            'name' => 'about_img',
            'label' => '關於我們圖片',
            'type' => 'upload',
            'upload' => true,
            'disk' => 'public' // if you store files in the /public folder, please ommit this; if you store them in /storage or S3, please specify it;
        ]);



            /*

        $this->crud->addField([   // CKEditor
            'name' => 'content',
            'label' => '內容',
            'type' => 'ckeditor',
            // optional:
            'extra_plugins' => ['oembed', 'widget','justify']
        ]);

        $this->crud->addField([   // Hidden
            'name' => 'created_by',
            'type' => 'hidden',
            'value' => Auth::user()->name
        ]);

        $this->crud->addField([   // Hidden
            'name' => 'updated_by',
            'type' => 'hidden',
            'value' => Auth::user()->name
        ]);

        */
    }

	public function store(StoreRequest $request)
	{
        $r = parent::storeCrud();
        
        return Redirect::to('admin/modBase/1/edit');
	}

	public function update(UpdateRequest $request)
	{
        $r = parent::updateCrud();
        
        return Redirect::to('admin/modBase/1/edit');
    }

}