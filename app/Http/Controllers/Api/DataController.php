<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Carbon\Carbon;

use App\User;         
use App\Models\OrderMgmtModel;
use App\Models\SysNoticeModel;    
use App\Models\Map\Shape;
use App\Models\Map\ShapePoint;
use Validator;


use App\Models\TrackingModel;
class DataController extends Controller
{    

    public function getUserData(Request $request)
    {
        $type = $request->cust_type;     
        $sample_detail = [];

        $user = Auth::user();

        $this_query = DB::table('users')->where('id', $user->id);            
        $sample_detail = $this_query->get();                    
        $data[] = array(
            'Rows' => $sample_detail,
        );

        return response()->json($data);
    }

    public function getDlvData(Request $request)
    {   
        $user = Auth::user();     
        //$carNo = $request->car_no;
        $status = $request->status;     
        $modDlv = [];  

        $this_query = DB::table('mod_dlv');
        $this_query->where('car_no', $user->email);
        if($status == 'FINISHED') {
            $this_query->whereRaw("(status = ? or status = 'ERROR')", [$status]);
            $this_query->orderBy('created_at', 'desc');
        }
        else {
            $this_query->whereRaw("(status = ? or status = 'DLV' or status = 'ERROR')", [$status]);
        }
        
        $this_query->where('g_key', $user->g_key);
        $this_query->where('c_key', $user->c_key);
        $this_query->where('s_key', $user->s_key);
        $this_query->where('d_key', $user->d_key);
            
        $modDlv = $this_query->get();                           

        return response()->json(['msg' => 'success', 'data' => $modDlv]);
    }

    public function getDlvDetailData(Request $request)
    {
        $user       = Auth::user();        
        $dlvNo      = $request->dlv_no;
        $status     = $request->status;   
        $filed      = $request->filed;
        $orderType  = $request->orderType;
        $modDlvPlan = [];    

        $this_query = DB::table('mod_dlv_plan');
        $this_query->join('mod_order', 'mod_order.sys_ord_no', '=', 'mod_dlv_plan.sys_ord_no');
        $this_query->select('mod_order.pick_attn', 'mod_order.pick_tel', 'mod_order.pick_cust_nm', 'mod_order.dlv_attn', 'mod_order.dlv_tel', 'mod_order.dlv_cust_nm', 'mod_dlv_plan.*');
        $this_query->where('mod_dlv_plan.dlv_no', $dlvNo);
        //$this_query->where('mod_dlv_plan.status', $status);
        if($status == 'FINISHED') {
            $this_query->whereRaw("(mod_dlv_plan.status = ? or mod_dlv_plan.status = 'ERROR')", [$status]);
        }
        else {
            $this_query->whereRaw("(mod_dlv_plan.status = ? or mod_dlv_plan.status = 'DLV')", [$status]);
        }
        $this_query->where('mod_dlv_plan.g_key', $user->g_key);
        $this_query->where('mod_dlv_plan.c_key', $user->c_key);
        $this_query->where('mod_dlv_plan.s_key', $user->s_key);
        $this_query->where('mod_dlv_plan.d_key', $user->d_key);
        
        if(isset($filed) && isset($orderType)) {
            $this_query->orderBy('mod_dlv_plan.'.$filed, $orderType);
        }
        else {
            $this_query->orderBy('mod_dlv_plan.sys_ord_no', 'asc');
        }
        $modDlvPlan = $this_query->get();

        return response()->json(['msg' => 'success', 'data' => $modDlvPlan]);
    }

    public function confirmOrderData(Request $request)
    {  
        try
        { 
            $user = Auth::user(); 
            if(empty($request->ord_no))
            {
                return ["msg"=>"error","data"=>"訂單號為空值，請確認"];  
            }
            else
            {     
                if($request->hasFile('file'))
                {
                    $image = $request->file('file');
                    //Storage::disk('local')->put($request->ord_no.'.jpg', base64_decode($imgData1));
                    $name = $request->ord_no.'.'.$image->getClientOriginalExtension();
                    $destinationPath = storage_path()."/image";;
                    $image->move($destinationPath, $name);
                    //$this->save();
                }
                    OrderMgmtModel::where('ord_no', $request->ord_no)
                    ->where('g_key',$user->g_key)
                    ->where('c_key',$user->c_key)
                    ->where('s_key',$user->s_key)
                    ->where('d_key',$user->d_key)
                    ->update(['status' => $request->status]);
                if(!empty($request->exp_type) && !empty($request->exp_reason))
                {
                    OrderMgmtModel::where('ord_no', $request->ord_no)
                    ->where('g_key',$user->g_key)
                    ->where('c_key',$user->c_key)
                    ->where('s_key',$user->s_key)
                    ->where('d_key',$user->d_key)
                    ->update(['exp_type' => $request->exp_type,'exp_reason'=>$request->exp_reason]);
                }
            }                     
        }
        catch(\Exception $e)
        {
            return ["msg"=>"error","data"=>$e];
        }
        return ["msg"=>"success"];
    }

    public function getErrorData(Request $request)
    {        
        $user = Auth::user();    
        $errorType = array(
            "data" => null
        );       

        $this_query = DB::table('bscode');
        $this_query->where('cd_type', 'ERRORTYPE');
        $this_query->where('g_key', $user->g_key);
        $this_query->where('c_key', $user->c_key);                              
        $errorType["data"] = $this_query->get();                           

        return response()->json($errorType);
    }

    public function getNoticeData(Request $request)
    {   
        $user = Auth::user();     
        $sysNotice = [];     

        $this_query = DB::table('sys_notice');
        $this_query->where('car_no', $user->email);
        $this_query->where('g_key', $user->g_key);
        $this_query->where('c_key', $user->c_key);
        $this_query->where('s_key', $user->s_key);
        $this_query->where('d_key', $user->d_key);       
        $this_query->orderBy('notice_time', 'desc');                         
        $sysNotice = $this_query->get();                           

        return response()->json(['msg' => 'success', 'data' => $sysNotice]);
    }

    public function updateNoticeData(Request $request)
    {  
        try
        { 
            //$user = Auth::user();
            $id = $request->id;
            if(empty($id))
            {
                return ["msg"=>"error","data"=>"id為空值，請確認"];  
            }
            else
            {                     
                SysNoticeModel::where('id', $request->id)                
                ->update(['is_read' => "Y"]);                
            }                                  
        }
        catch(\Exception $e)
        {
            return ["msg"=>"error","data"=>$e];
        }
        return ["msg"=>"success"];
    }

    public function sendGpsData(Request $request=null) {
        $shape = new Shape();
        $user = Auth::user();

        $gps = array();
        if(isset($request->gps)) {
            $gps = $request->gps;
        }

        $lng = 0;
        $lat = 0;
        $speed = 0;
        $insertData = array();
        $now = date("Y-m-d H:i:s");

        try {
            $cnt = count($gps);
            for($i=0; $i<$cnt; $i++) {
                $lng       = $gps[$i]['lng'];
                $lat       = $gps[$i]['lat'];
                $speed     = $gps[$i]['speed'];
                $timestamp = $gps[$i]['timestamp'];

                // $shape->addPoint(
                //     new ShapePoint($lat, $lng, $i, $speed, $timestamp)
                // );
                
                $data = [
                    'car_no'        => $user->email,
                    'lat'           => $lat,
                    'lng'           => $lng,
                    'speed'         => round($speed),
                    'location_time' => $timestamp,
                    'driver_no'     => null,
                    'driver_nm'     => $user->name,
                    'person_no'     => null,
                    'person_nm'     => null,
                    'dlv_no'        => null,
                    'g_key'         => $user->g_key,
                    'c_key'         => $user->c_key,
                    's_key'         => $user->s_key,
                    'd_key'         => $user->d_key,
                    'created_by'    => $user->email,
                    'updated_by'    => $user->email,
                    'created_at'    => $now,
			        'updated_at'    => $now,
                ];

                array_push($insertData, $data);
            }

            $TrackingModel = new TrackingModel();
            $TrackingModel->insertGps($insertData);

            $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/TMSApp-1bf8dcff7c4b.json');

            $firebase = (new Factory)
                ->withServiceAccount($serviceAccount)
                ->withDatabaseUri('https://tmsapp-22e76.firebaseio.com/')
                ->create();
            $database = $firebase->getDatabase();
            $reference = $database->getReference('/raw-locations//'.$user->c_key.'/'.$user->email);

            $value = $reference->getValue();
            if($value == null) {
                $database->getReference('/raw-locations//'.$user->c_key) // this is the root reference
                ->update([
                    $user->email => [
                        0 => [
                            'lat'   => $lat,
                            'lng'   => $lng,
                            'power' => $request->power,
                            'speed' => $speed,
                            'time'  => Carbon::now()->toDateTimeString()
                        ],
                        1=> [
                            'lat'   => $lat,
                            'lng'   => $lng,
                            'power' => $request->power,
                            'speed' => $request->speed,
                            'time'  => Carbon::now()->toDateTimeString()
                        ]
                    ]
                ]);
            }
            else {
                $lastInfo = $value[1];
            
                $newData = [
                    'lat'   => $lat,
                    'lng'   => $lng,
                    'power' => $request->power,
                    'speed' => $speed,
                    'time'  => Carbon::now()->toDateTimeString()
                ];


                $updates = [
                    '/raw-locations//'.$user->c_key.'//'.$user->email.'/0' => $lastInfo,
                    '/raw-locations//'.$user->c_key.'//'.$user->email.'/1' => $newData,
                ];

                $database->getReference() // this is the root reference
                ->update($updates);
            }            
        }
        catch(\Exception $e) {
            return response()->json(["msg"=>"error","data"=>$e->getMessage()]);
        }

        return response()->json(["msg"=>"success"]);
    }

    /**
     * 退出登录
     */
    public function logout()
    {
        if (\Auth::guard('api')->check()) {
            \Auth::guard('api')->user()->token()->delete();
        }

        return ['msg' => 'success',  'data' => '登出成功'];
    }

    public function modifyUser() {
        try {
            $user = Auth::user();
            $update = array();

            if(request('password') != null) {
                $update = array(
                    'name'     => request('name'),
                    'password' => Hash::make(request('password')),
                    'phone'    => request('phone')
                );
            }
            else {
                $update = array(
                    'name'     => request('name'),
                    'phone'    => request('phone')
                );
            }
            
            
            $userData = DB::table('users')
                            ->where('id', $user->id)
                            ->update($update);;
            
        }
        catch(\Exception $e) {
            return response()->json(['msg' => 'error', 'error_log' => $e]);
        }

        return response()->json(['msg' => 'success']);
    }

    public function getChatroomList() {
        $user = Auth::user();
        $chatroomList = array();
        try {
            $chatroomList = DB::table('users')
                                ->where('g_key', $user->g_key)
                                ->where('c_key', $user->c_key)
                                ->where('identity', 'C')
                                ->select('id', 'name','email', 'phone', 'online')
                                ->get();
            if(count($chatroomList) > 0) {
                foreach($chatroomList as $key=>$row) {
                    $row->img = 'https://placehold.it/160x160/7986CB/ffffff/&text='.mb_substr($row->name, 0, 1);
                }
            }
        }
        catch(\Exception $e) {
            return response()->json(['msg' => 'error', 'error_log' => $e]);
        }

        return response()->json(['msg' => 'success', 'data' => $chatroomList]);
    }

    public function trackingSwitch($switch=null) {
        $user = Auth::user();

        try {
            if($switch != null) {
                DB::table('users')
                ->where('email', $user->email)
                ->update(['online' => $switch]);
            }
            else {
                return response()->json(['msg' => 'error', 'error_log' => '請說明您要開還是關']);
            }
        }
        catch(\Exception $e) {
            return response()->json(['msg' => 'error', 'error_log' => $e]);
        }

        return response()->json(['msg' => 'success']);
    }

    public function getOrdDetail($sysOrdNo=null,$type=null) {
        $user   = Auth::user();
        $main   = null;
        $detail = null;
        try {
            $thisQuery = DB::table('mod_order')
                            ->where('sys_ord_no', $sysOrdNo);
            if($type == 'D') {
                $thisQuery->select(DB::raw('dlv_attn as attn'), DB::raw('dlv_tel as tel'), DB::raw("CONCAT(dlv_city_nm,dlv_area_nm,dlv_addr) as address"), 'pkg_num', 'total_gw', 'cbm', 'total_cbm', 'id', 'sys_ord_no', DB::raw('dlv_remark as remark'), 'dlv_no');
            }
            else {
                $thisQuery->select(DB::raw('pick_attn as attn'), DB::raw('pick_tel as tel'), DB::raw("CONCAT(pick_city_nm,pick_area_nm,pick_addr) as address"), 'pkg_num', 'total_gw', 'cbm', 'total_cbm', 'id', 'sys_ord_no', DB::raw('pick_remark as remark'), 'dlv_no');
            }

            $main = $thisQuery->first();

            if(isset($main)) {
                $detail = DB::table('mod_order_detail')
                            ->where('ord_id', $main->id)
                            ->get();
            }
        }
        catch(\Exception $e) {
            return response()->json(['msg' => 'error', 'error_log' => $e->getMessage()]);
        }

        $result = array(
            'msg'    => 'success',
            'main'   => $main,
            'detail' => $detail
        );

        return response()->json($result);
    }

    public function ordConfirm() {
        $user   = Auth::user();
        $sysOrdNo = request('sysOrdNo');
        $dlvNo    = request('dlvNo');
        $img      = request('img');
        $signImg  = request('signImg');
        $lng      = request('lng');
        $lat      = request('lat');
        $dlvType  = request('dlvType');
        $insertData = array();

        $dlvData = DB::table('mod_dlv')
                            ->select('status')
                            ->where('dlv_no', $dlvNo)
                            ->first();
        
        if(isset($dlvData) && $dlvData->status != "DLV") {
            return response()->json(['msg' => 'error', 'msgLog' => '還沒出發不能完成，請先按下出發']);
        }

        $ordStatus = 'FINISHED';

        if($dlvType == 'D') {

            $dlvStatus = DB::table('mod_dlv_plan')
                            ->select('status')
                            ->where('sys_ord_no', $sysOrdNo)
                            ->where('dlv_type', 'P')
                            ->where('dlv_no', $dlvNo)
                            ->first();
            if(isset($dlvStatus)) {
                if($dlvStatus->status != 'FINISHED') {
                    return response()->json(['msg' => 'error', 'msgLog' => '尚有提貨未完成，請先完成提貨']);
                }
            }
        }
        else {
            $ordStatus = 'PICKED';
        }
        
        try {
            if(is_array($img)) {
            
                for($i=0; $i<count($img); $i++) {
                    $guid = $this->guid();
                    Storage::disk('local')->put($guid.'.jpg', base64_decode($img[$i]));
                    
                    array_push($insertData, [
                        'guid'        => $guid.'.jpg',
                        'ref_no'      => $sysOrdNo,
                        'type_no'     => 'FINISH',
                        'file_format' => 'jpg',
                        'g_key'       => $user->g_key,
                        'c_key'       => $user->c_key,
                        's_key'       => $user->s_key,
                        'd_key'       => $user->d_key,
                        'created_by'  => $user->email,
                        'updated_by'  => $user->email,
                        'created_at'  => Carbon::now()->toDateTimeString(),
                        'updated_at'  => Carbon::now()->toDateTimeString()
                    ]);
                }
            }
    
            if(isset($signImg)) {
                $guid = $this->guid();
                Storage::disk('local')->put($guid.'.jpg', base64_decode($signImg));
                array_push($insertData, [
                    'guid'        => $guid.'.jpg',
                    'ref_no'      => $sysOrdNo,
                    'type_no'     => 'SIGNIN',
                    'file_format' => 'jpg',
                    'g_key'       => $user->g_key,
                    'c_key'       => $user->c_key,
                    's_key'       => $user->s_key,
                    'd_key'       => $user->d_key,
                    'created_by'  => $user->email,
                    'updated_by'  => $user->email,
                    'created_at'  => Carbon::now()->toDateTimeString(),
                    'updated_at'  => Carbon::now()->toDateTimeString()
                ]);
            }
    
            if(count($insertData) > 0) {
                DB::table('mod_file')->insert($insertData);
            }
            
            $ordModel = new OrderMgmtModel;
            $ordModel->updateOrdStatusAndFinishDate($sysOrdNo, $ordStatus);
            // DB::table('mod_order')
            //     ->where('sys_ord_no', $sysOrdNo)
            //     ->update(['status' => $ordStatus]);

            DB::table('mod_dlv_plan')
                ->where('sys_ord_no', $sysOrdNo)
                ->where('dlv_type', $dlvType)
                ->where('dlv_no', $dlvNo)
                ->update(['status' => 'FINISHED', 'updated_at' => Carbon::now()->toDateTimeString()]);

            $cnt  = DB::table('mod_dlv_plan')->where('dlv_no', $dlvNo)->count();
            $fCnt = DB::table('mod_dlv_plan')->where('dlv_no', $dlvNo)->whereRaw("(status = 'FINISHED' or status = 'ERROR')")->count();

            if($cnt == $fCnt) {
                DB::table('mod_dlv')
                    ->where('dlv_no', $dlvNo)
                    ->update(['status' => 'FINISHED']);
            }

            $dlvPlanData = DB::table('mod_dlv_plan')
                            ->where('sys_ord_no', $sysOrdNo)
                            ->where('dlv_type', $dlvType)
                            ->where('dlv_no', $dlvNo)
                            ->first();

            $dlvData = DB::table('mod_dlv')
                            ->where('dlv_no', $dlvNo)
                            ->where('g_key', $user->g_key)
                            ->where('c_key', $user->c_key)
                            ->first();

            $addr = null;
            $custNm = null;
            $driverNm = $user->email;

            if(isset($dlvData)) {
                $addr = $dlvPlanData->addr;
                $custNm = $dlvPlanData->cust_nm;
            }

            if(isset($dlvData)) {
                $driverNm = $dlvData->driver_nm;
            }  

            if($dlvType == 'D') {
                $tm = new TrackingModel();
                $tm->insertTracking('', '', '', $sysOrdNo, '', 'E', 5, $user->g_key, $user->c_key, $user->s_key, $user->d_key);

                $gpsData = [
                    'car_no'     => $user->email,
                    'lat'        => $lat,
                    'lng'        => $lng,
                    'driver_no'  => null,
                    'driver_nm'  => $driverNm,
                    'person_no'  => null,
                    'person_nm'  => null,
                    'dlv_no'     => null,
                    'g_key'      => $user->g_key,
                    'c_key'      => $user->c_key,
                    's_key'      => $user->s_key,
                    'd_key'      => $user->d_key,
                    'created_by' => $user->email,
                    'updated_by' => $user->email,
                    'created_at' => Carbon::now()->toDateTimeString(),
                    'updated_at' => Carbon::now()->toDateTimeString(),
                    'status'     => 'FINISHED',
                    'cust_nm'    => $custNm,
                    'addr'       => $addr,
                ];
    
                $TrackingModel = new TrackingModel();
                $TrackingModel->insertGps($gpsData);
            }

            
            

            
        }
        catch(\Exception $e) {
            \Log::info($e->getMessage());
            return response()->json(['msg' => 'error', 'error_log' => $e->getMessage()]);
        }

        return response()->json(['msg' => 'success']);
    }

    public function errorReport() {
        $user       = Auth::user();
        $sysOrdNo   = request('sysOrdNo');
        $dlvNo      = request('dlvNo');
        $img        = request('img');
        $errorType  = request('errorType');
        $dlvType    = request('dlvType');
        $insertData = array();

        $dlvData = DB::table('mod_dlv')
                            ->select('status')
                            ->where('dlv_no', $dlvNo)
                            ->first();
        
        if(isset($dlvData) && $dlvData->status != "DLV") {
            return response()->json(['msg' => 'error', 'msgLog' => '還沒出發不能回報異常，請先按下出發']);
        }

        if($dlvType == 'D') {

            $dlvStatus = DB::table('mod_dlv_plan')
                            ->select('status')
                            ->where('sys_ord_no', $sysOrdNo)
                            ->where('dlv_type', 'P')
                            ->where('dlv_no', $dlvNo)
                            ->first();
            if(isset($dlvStatus)) {
                if($dlvStatus->status != 'FINISHED' && $dlvStatus->status != 'ERROR') {
                    return response()->json(['msg' => 'error', 'msgLog' => '尚有提貨未完成，請先完成提貨']);
                }
            }
        }
        
        try {
            if(is_array($img)) {
            
                for($i=0; $i<count($img); $i++) {
                    $guid = $this->guid();
                    Storage::disk('local')->put($guid.'.jpg', base64_decode($img[$i]));
                    
                    array_push($insertData, [
                        'guid'        => $guid.'.jpg',
                        'ref_no'      => $sysOrdNo,
                        'type_no'     => 'ERROR',
                        'file_format' => 'jpg',
                        'g_key'       => $user->g_key,
                        'c_key'       => $user->c_key,
                        's_key'       => $user->s_key,
                        'd_key'       => $user->d_key,
                        'created_by'  => $user->email,
                        'updated_by'  => $user->email,
                        'created_at'  => Carbon::now()->toDateTimeString(),
                        'updated_at'  => Carbon::now()->toDateTimeString()
                    ]);
                }
            }
    
            if(count($insertData) > 0) {
                DB::table('mod_file')->insert($insertData);
            }

            $thisQuery = DB::table('bscode');
            $thisQuery->where('cd_type', "ERRORTYPE");
            $thisQuery->where('cd', $errorType);
            $thisQuery->where('g_key', $user->g_key);
            $thisQuery->where('c_key', $user->c_key);
            $thisQuery->where('s_key', $user->s_key);
            $thisQuery->where('d_key', $user->d_key);
            $bscode = $thisQuery->first();

            $cdDescp = "";

            if(isset($bscode)) {
                $cdDescp = $bscode->cd_descp;
            }
    
            DB::table('mod_order')
                ->where('sys_ord_no', $sysOrdNo)
                ->update(['status' => 'ERROR', 'error_remark' => $cdDescp, 'exp_reason' => $cdDescp, 'dlv_type' => null]);
            
            if($dlvType == 'P') {
                DB::table('mod_dlv_plan')
                    ->where('sys_ord_no', $sysOrdNo)
                    ->where('dlv_no', $dlvNo)
                    ->update(['status' => 'ERROR']);
            }
            else {
                DB::table('mod_dlv_plan')
                    ->where('sys_ord_no', $sysOrdNo)
                    ->where('dlv_no', $dlvNo)
                    ->where('dlv_type', $dlvType)
                    ->update(['status' => 'ERROR']);
            }
            

            $cnt  = DB::table('mod_dlv_plan')->where('dlv_no', $dlvNo)->count();
            $fCnt = DB::table('mod_dlv_plan')->where('dlv_no', $dlvNo)->whereRaw("(status = 'FINISHED' or status = 'ERROR')")->count();

            if($cnt == $fCnt) {
                DB::table('mod_dlv')
                    ->where('dlv_no', $dlvNo)
                    ->update(['status' => 'FINISHED']);
            }
        }
        catch(\Exception $e) {
            \Log::info($e->getMessage());
            return response()->json(['msg' => 'error', 'error_log' => $e->getMessage()]);
        }

        return response()->json(['msg' => 'success']);
    }

    public function departure($dlvNo=null) {
        try {
            $user = Auth::user();
            $lng = request('lng');
            $lat = request('lat');

            DB::table('mod_dlv')
                ->where('dlv_no', $dlvNo)
                ->update(['status' => 'DLV', 'updated_at' => Carbon::now()->toDateTimeString()]);

            $ordData = DB::table('mod_dlv_plan')->select('sys_ord_no')->where('dlv_no', $dlvNo)->get();

            if(count($ordData) > 0) {
                foreach($ordData as $row) {
                    $sysOrdNo = $row->sys_ord_no;

                    DB::table('mod_order')
                        ->where('sys_ord_no', $sysOrdNo)
                        ->update(['status' => 'SETOFF', 'updated_at' => Carbon::now()->toDateTimeString()]);

                    $ordNo = DB::table('mod_order')->select('ord_no')->where('sys_ord_no', $sysOrdNo)->first();
                    
                    $tm = new TrackingModel();
                    $tm->insertTracking('', $ordNo->ord_no, '', $sysOrdNo, '', 'B', 4, $user->g_key, $user->c_key, $user->s_key, $user->d_key);
                }
            }

            DB::table('mod_dlv_plan')
                ->where('dlv_no', $dlvNo)
                ->where('c_key', $user->c_key)
                ->update(['status' => 'DLV', 'updated_at' => Carbon::now()->toDateTimeString()]);

            $insertData = [
                'car_no'        => $user->email,
                'lat'           => $lat,
                'lng'           => $lng,
                'driver_no'     => null,
                'driver_nm'     => $user->name,
                'person_no'     => null,
                'person_nm'     => null,
                'dlv_no'        => null,
                'g_key'         => $user->g_key,
                'c_key'         => $user->c_key,
                's_key'         => $user->s_key,
                'd_key'         => $user->d_key,
                'created_by'    => $user->email,
                'updated_by'    => $user->email,
                'created_at'    => Carbon::now()->toDateTimeString(),
                'updated_at'    => Carbon::now()->toDateTimeString(),
                'status'        => 'SETOFF'
            ];

            $TrackingModel = new TrackingModel();
            $TrackingModel->insertGps($insertData);
        }
        catch(\Exception $e) {
            return response()->json(['msg' => 'error', 'error_log' => $e->getMessage()]);
        }
        
        return response()->json(['msg' => 'success']);
    }

    public function getTrackingStatus() {
        $user = Auth::user();
        $userData = DB::table('users')->select('online')->where('email', $user->email)->first();
        
        return response()->json(['msg' => 'success', 'status' => $userData->online]);
    }

    public function guid(){
        mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
        $charid = strtoupper(md5(uniqid(rand(), true)));
        $uuid = substr($charid, 0, 8)
            .substr($charid, 8, 4)
            .substr($charid,12, 4)
            .substr($charid,16, 4)
            .substr($charid,20,12);
        return $uuid;
    }

    public function updateDtoken() {
        $user = Auth::user();
        $dToken = request('dToken');
        try {
            DB::table('users')->where('email', $user->email)->update(['d_token' => $dToken]);
        }
        catch(\Exception $e) {
            return response()->json(['msg' => 'error', 'error_log' => $e->getMessage()]);
        }
        

        return response()->json(['msg' => 'success']);
    }

    public function getNoticeCnt() {
        $user = Auth::user();
        $noticeCnt = 0;
        try {
            $noticeCnt = DB::table('sys_notice')
                            ->where('car_no', $user->email)
                            ->where('is_read', 0)
                            ->count();
        }
        catch(\Exception $e) {
            return response()->json(['msg' => 'error', 'error_log' => $e->getMessage()]);
        }
        

        return response()->json(['msg' => 'success', 'cnt' => $noticeCnt]);
    }

    public function setNoticeRead() {
        $user = Auth::user();
        try {
            $noticeCnt = DB::table('sys_notice')
                            ->where('car_no', $user->email)
                            ->update(['is_read' => 1]);
        }
        catch(\Exception $e) {
            return response()->json(['msg' => 'error', 'error_log' => $e->getMessage()]);
        }
        

        return response()->json(['msg' => 'success']);
    }
}