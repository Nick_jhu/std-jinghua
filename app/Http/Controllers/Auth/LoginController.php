<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\MemberMgmtModel;
use App\Models\OrderMgmtModel;
use Socialite;
use Redirect;
use DB;

class LoginController extends Controller
{
    /**
     * Redirect the user to the facebook authentication page.
     *
     * @return Response
     */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from google.
     *
     * @return Response
     */
    public function handleProviderCallback($provider, $goTo=null)
    {
        
        try {
            $user = Socialite::driver($provider)->user();

        } catch (\Exception $e) {

            return redirect()->route('FrontEnd.index');
        }

        // check if they're an existing user
        $existingUser = MemberMgmtModel::where('email', $user->email)->first();
        $urlPrevious = url()->previous();

        if($existingUser){
            // log them in
            Auth::guard('member')->loginUsingId($existingUser->id, true);
            $cartId = isset($_COOKIE['cartId'])?$_COOKIE['cartId']:null;

            $o = new OrderMgmtModel();
            $o->mappingCart($cartId, $existingUser->id);
            $o->processCart($existingUser);

            if($provider == 'facebook') {
                $fbid = DB::table('mod_member')->where('id', $existingUser->id)->value('fb_id');
                if(!isset($fbid)) {
                    DB::table('mod_member')->where('id', $existingUser->id)->update([
                        'provider' => 'facebook',
                        'fb_id'    => $user->id
                    ]);
                }
            }
            else if($provider == 'google') {
                $gid = DB::table('mod_member')->where('id', $existingUser->id)->value('g_id');
                if(!isset($fbid)) {
                    DB::table('mod_member')->where('id', $existingUser->id)->update([
                        'provider' => 'google',
                        'g_id'     => $user->id
                    ]);
                }
            }
            
        } else {
            // create a new user
            $newUser                  = new MemberMgmtModel;
            $newUser->name            = $user->name;
            $newUser->email           = $user->email;
            if($provider == 'facebook') {
                $newUser->fb_id     = $user->id;
            }
            else if($provider == 'google') {
                $newUser->g_id     = $user->id;
            }
            $newUser->provider        = $provider;
            $newUser->is_actived      = 'Y';
            $newUser->save();
            Auth::guard('member')->loginUsingId($newUser->id, true);

            $cartId = isset($_COOKIE['cartId'])?$_COOKIE['cartId']:null;

            $o = new OrderMgmtModel();
            $o->mappingCart($cartId, $newUser->id);
            $o->processCart($newUser);
        }

        if($urlPrevious == url('cart')) {
            return Redirect::to($urlPrevious);
        }

        return redirect()->route('FrontEnd.index');        
    }
}