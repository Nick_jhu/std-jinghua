<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;

class BscodeKindCrudRequest extends \Backpack\CRUD\app\Http\Requests\CrudRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'cd_type' => 'required|unique:bscode_kind,cd_type,NULL,id,c_key,'.Auth::user()->c_key.'|min:1|max:20',
                    'cd_descp' => 'nullable|min:1|max:300'
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [
                   
                ];
            }
            default:break;
        }
    }

}