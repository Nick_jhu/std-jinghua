<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        '/member/logout',
        'allPayReply',
        'spgCallBack',
        'spgCallBackSchedule',
        'spgCustomerCallBack',
        'mapCallBack',
        'allPayReplySchedule',
        'trackingReply',
        'testMapCallBack',
        'thirdPartyPayCallBack',
        'thirdPartyPayCallBack1'
    ];
}
